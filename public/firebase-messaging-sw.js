importScripts('https://www.gstatic.com/firebasejs/7.2.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.2.1/firebase-messaging.js');

firebase.initializeApp({
  'messagingSenderId': '498429201060'
});

const messaging = firebase.messaging();

// self.addEventListener('push', function (event) {
//   const data = event.data.json();
//   let body = data.notification.body;
//   body = body.replace(/(<\/?(?:img)[^>]*>)|<[^>]+>/ig, '$1');
//   const icon = data.notification.icon || '';
//   const title = data.notification.title || '';

//   event.waitUntil(
//     self.registration.showNotification(title, {
//       body: body,
//       icon: icon
//     })
//   );
// })