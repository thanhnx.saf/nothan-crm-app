import helpers from "@/utils/utils";
import draggable from 'vuedraggable';

export default {
  components: {
    draggable
  },
  props: {
    all_attribute: {
      type: Array,
      default: []
    },
    fix_position: {
      type: Array,
      default: []
    },
    custom_column: {
      type: Object,
      default: {}
    }

  },
  
  data() {
    return {
      drag: false,
      mutableList: []
    }
  },

  computed: {
    helpers() {
      return helpers
    },
    attributesPositionFixed() {
      return helpers.getAttributeCodesByAllAttributes(this.all_attribute, this.fix_position, true);
    }
  },

  mounted() {
    this.setAttributeDefault(this.all_attribute)
  },

  watch: {
    all_attribute: {
      handler(after) {
        if (after) {
          this.setAttributeDefault(after)
        }
      },
      deep: true
    }
  },

  methods: {
    setAttributeDefault(data) {
      data.forEach(attribute => {
        if (this.fix_position.indexOf(attribute.attribute_code) !== -1) {
          attribute.default = 1
        }
      })

      this.mutableList = data
    },


    onGroupsChange() {
      let vm = this;
      vm.mutableList.forEach(function (item, key) {
        item.sort_order = key
      });
      vm.$emit('change_table_attribute', vm.mutableList);
    },

    changeStatusDisplayAttribute($event, attribute) {
      let vm = this;
      let statusInput = $event.target.checked;
      if (!statusInput) {
        vm.mutableList.find(item => item.id === attribute.id).is_show = 0;
      } else {
        vm.mutableList.find(item => item.id === attribute.id).is_show = 1;
      }
      vm.$emit('change_table_attribute', vm.mutableList);
    },
  }
}