import { mapGetters } from 'vuex'

import { CUSTOMER_GET_CONTACT } from '@/types/actions.type'

export default {
  name: 'ListContact',
  props: {
    customer_id: {
      type: Number,
      required: true,
    },
    is_dropdown: {
      type: Boolean,
      default: false,
    },
    contact_value: {
      type: Number,
      required: false,
    },
  },
  computed: {
    ...mapGetters(['currentTeam']),
  },
  watch: {
    customer_id: {
      handler() {
        const vm = this
        vm.getListContact()
      },
      deep: true,
    },
  },
  created() {
    this.getListContact()
  },
  data() {
    return {
      contacts: [],
      isLoader: false,
    }
  },
  methods: {
    getListContact() {
      const vm = this
      vm.isLoader = true
      const params = {
        customer_id: vm.customer_id,
        team_id: vm.currentTeamId,
      }
      vm.$store.dispatch(CUSTOMER_GET_CONTACT, params).then((response) => {
        if (vm.is_dropdown) {
          const contacts = response.data.data
          contacts.forEach(function (contact) {
            if (contact.id) {
              vm.contacts[contact.id] = contact.full_name
            }
          })
        } else {
          vm.contacts = response.data.data
        }
        vm.isLoader = false
      })
    },
  },
}
