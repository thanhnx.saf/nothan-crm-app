/* eslint-disable no-undef */
import VueTheMask from "vue-the-mask";
import Vue from "vue";
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import {
  CUSTOMER_FILTER_CREATE,
  CUSTOMER_FILTER_UPDATE,
  CUSTOMER_FILTER_DELETE,
  DEAL_FILTER_CREATE,
  DEAL_FILTER_UPDATE,
  DEAL_FILTER_DELETE,
  STUDENT_FILTER_CREATE,
  STUDENT_FILTER_UPDATE,
  STUDENT_FILTER_DELETE
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";

Vue.use(VueTheMask);

const messageFilter = {
  custom: {
    name: {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    }
  }
};

export default {
  props: {
    filter_data: {
      default: null,
      type: Array
    },
    full_attributes: {
      default: null,
      type: Array
    },
    total_items: {
      default: 0,
      type: Number
    },
    filter_attributes: {
      type: Array
    },
    cache_attributes_filter: {
      type: String
    },
    filter_type: {
      type: [String, Number]
    },
    filter_active: {
      default: true,
      type: Boolean
    },
    filter_is_show_action: {
      default: true,
      type: Boolean
    },
    filter_share_is_show: {
      default: true,
      type: Boolean
    }
  },

  components: {},

  data() {
    return {
      isMinimizeFilter: false,
      fullAttributes: [],
      filterIsActive: false,
      filterData: [],
      showFilter: false,
      filterActive: null,
      addNewFilter: {
        name: "",
        status: 1,
        unique: false
      },
      attributeActive: null,
      isAllData: true,
      isAddNew: false,
      isPlusFilter: false,
      attributeFilters: [],
      selectedDelete: null,
      filterAttributes: [],
      typingTimer: null,
      doneTypingInterval: 500,
      isChangeOldFilter: false,
      _beforeEditFilterOfMe: null,
      _beforeSaveFilter: null,
      clickLoader: false,
      textInput: [{
          id: 1,
          condition: "LIKE",
          name: "Chứa ký tự"
        },
        {
          id: 2,
          condition: "NOT_LIKE",
          name: "Không chứa ký tự"
        }
      ],
      dateTimeInput: [{
          id: 2,
          name: "Hôm qua",
          condition: "=",
          value: "yesterday"
        },
        {
          id: 3,
          name: "Hôm nay",
          condition: "=",
          value: "today"
        },
        {
          id: 4,
          name: "Tuần này",
          condition: "<>",
          value: "week"
        },
        {
          id: 5,
          name: "Tháng này",
          condition: "<>",
          value: "month"
        },
        {
          id: 6,
          name: "Ngày chính xác",
          condition: "="
        },
        {
          id: 7,
          name: "Trong khoảng",
          condition: "<>"
        }
      ],
      numberInput: [{
          id: 1,
          name: "Bằng",
          condition: "="
        },
        {
          id: 2,
          name: "Khác",
          condition: "!="
        },
        {
          id: 3,
          name: "Lớn hơn",
          condition: ">"
        },
        {
          id: 4,
          name: "Nhỏ hơn",
          condition: "<"
        },
        {
          id: 5,
          name: "Trong khoảng",
          condition: "<>"
        }
      ],
      lang: {
        days: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        months: [
          "Tháng 1",
          "Tháng 2",
          "Tháng 3",
          "Tháng 4",
          "Tháng 5",
          "Tháng 6",
          "Tháng 7",
          "Tháng 8",
          "Tháng 9",
          "Tháng 10",
          "Tháng 11",
          "Tháng 12"
        ],
        pickers: [
          "7 ngày tiếp theo",
          "30 ngày tiếp theo ",
          "7 ngày trước",
          "30 ngày trước"
        ],
        placeholder: {
          date: "Chọn ngày",
          dateRange: "Chọn khoảng thời gian"
        },
      },
      customer_types: [],
      isSelectedFullAttribute: false,
      oldParams: {
        filter: ""
      },
      firstParamsWithSelect: null,
      isErrorValidate: false,
      shippingStatus: [{
          id: 1,
          name: "Chưa giao hàng"
        },
        {
          id: 2,
          name: "Đã giao hàng"
        }
      ],
      cacheFilterAttributesBeforeReload: ""
    }
  },

  created() {
    let vm = this;
    vm.$bus.$on('resetFilterByComponentParent', ($event) => {
      if ($event) {
        let filterData = vm.filter_data;
        if (filterData && filterData.length > 0) {
          let filterActive = '';
          filterData.forEach((filter_data) => {
            if (filter_data.id === $event) {
              filterActive = filter_data;
            }
          });
          vm.filterActive = filterActive;
        } else {
          vm.resetFilterCriteria();
        }
      }
    })
  },

  watch: {
    filter_data: {
      handler: function () {
        let vm = this;
        vm.filterData = vm.filter_data;
      },
      deep: true
    },
    // filterData: {
    //   handler: function () {
    //     let vm = this;
    //     if (vm.filterData.length > 0) {
    //       vm.filterIsActive = true;
    //       vm.filterActive = vm.filterData[0];
    //     }
    //   },
    //   deep: true
    // },
    showFilter: {
      handler: function () {
        let vm = this;
        vm.$emit('show_filter', vm.showFilter);
      },
      deep: true
    },
    isAllData: {
      handler: function () {
        let vm = this;
        vm.$emit('show_is_all_data', vm.isAllData);
      },
      deep: true
    },
    filter_attributes: {
      handler: function () {
        let vm = this;
        vm.filterAttributes = vm.filter_attributes;
      },
      deep: true
    },
    filterAttributes: {
      handler: function (after) {
        let vm = this;
        let listIds = [];

        vm.filterAttributes.forEach(function (selected) {
          if (selected.is_disabled) {
            listIds.push(selected.attribute_id);
          }
        });
        // vm.filterAttributes.forEach(function (item) {
        //   if (item.data_type == 8 || item.data_type == 9 || item.data_type == 10 || item.data_type == 11 || item.data_type == 13 || item.data_type == 14) {
        //     item.attribute_values.attribute_value = item.attribute_values.attribute_value;
        //   }
        // });
        vm.fullAttributes.forEach(function (item) {
          if ($.inArray(item.id, listIds) !== -1) {
            item.is_disable = true;
          } else {
            item.is_disable = false;
          }


          if (vm.filter_type != 3) {
            if (item.is_hidden === 1) {
              item.is_disable = true;
            }
          }

          if (after && after.length > 0) {
            after.forEach(function (attribute_filter) {
              if (item.attribute_code == attribute_filter.attribute_code || item.attribute_code == attribute_filter.code_external_attribute) {
                item.is_disable = true;
                attribute_filter.is_hidden = item.is_hidden;
              }
            });
          }
        })
        if (vm.filterAttributes && vm.filterAttributes.length > 0 && vm.fullAttributes && vm.fullAttributes.length > 0) {
          if (vm.filterAttributes.length == vm.fullAttributes.length) {
            vm.isSelectedFullAttribute = true;
          } else {
            vm.isSelectedFullAttribute = false;
          }
        }
      },
      deep: true
    }
  },

  computed: {
    helpers() {
      return helpers;
    },
    isChangeBase() {
      let vm = this;
      if (vm.cache_attributes_filter !== "" && vm.cache_attributes_filter !== null) {
        let baseAttributes = JSON.parse(vm.cache_attributes_filter);
        if (vm.compareArray(baseAttributes.data, vm.filterAttributes) === false) {
          return true;
        } else {
          return false;
        }
      } else if (vm.cacheFilterAttributesBeforeReload !== "" && vm.cacheFilterAttributesBeforeReload !== null) {
        let baseAttributes = JSON.parse(vm.cacheFilterAttributesBeforeReload);
        if (vm.compareArray(baseAttributes.data, vm.filterAttributes) === false) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },
    filterCompleted() {
      let vm = this;
      if (vm.filterAttributes.length > 0) {
        let error = 0;
        vm.filterAttributes.forEach(function (item) {
          if (item.data_type == 1 || item.data_type == 4 || item.data_type == 5 || item.data_type == 12 || item.data_type == 9 && item.attribute_code == 'contact_user' || item.external_attribute == 1 && (item.code_external_attribute == 'product_code' || item.code_external_attribute == 'product_name' || item.code_external_attribute == 'customer_address' || item.code_external_attribute == 'last_user_used')) {
            error = vm.validateFilterText(item, error);
          } else if (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) {
            error = vm.validateFilterDateTime(item, error);
          } else if (item.data_type == 6 || item.data_type == 7 || item.external_attribute == 1 && (item.code_external_attribute == 'product_quantity' || item.code_external_attribute == 'product_discount' || item.code_external_attribute == 'order_discount' || item.code_external_attribute == 'amount_must_be_paid' || item.code_external_attribute == 'single_sale_price' || item.code_external_attribute == 'price_after' || item.code_external_attribute == 'product_price')) {
            error = vm.validateFilterNumber(item, error);
          } else if (item.data_type == 8 || item.data_type == 9 && item.attribute_code !== 'contact_user' || item.data_type == 10 || item.data_type == 11 || item.data_type == 13 || item.data_type == 14 || item.data_type == 15 || item.data_type == 16 || item.external_attribute == 1 && (item.code_external_attribute == 'charge_person' || item.code_external_attribute == 'user_id' || item.code_external_attribute == 'customer_address')) {
            error = vm.validateFilterSelect(item, error);
          } else if (item.external_attribute == 1 && item.code_external_attribute == 'type_customer') {
            error = vm.validateFilterSelect(item, error);
          } else if (item.data_type == "" || item.data_type == null) {
            error++;
          }
        })

        if (error > 0) {
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    },
    ...mapGetters(["currentUser", "currentTeam", "listUsers"])
  },

  mounted() {
    let vm = this;
    vm.filterData = vm.filter_data;
    if (vm.filter_active && vm.filterData.length > 0) {
      vm.filterActive = vm.filterData[0];
    }
    if (vm.full_attributes.length > 0) {
      vm.customer_types = vm.full_attributes[0].customer_type;
      vm.full_attributes.forEach(function (item) {
        // && item.is_hidden === 0
        if ((vm.filter_type === 1 || vm.filter_type === 4) && item.attribute_code !== "avatar" || vm.filter_type === 3) {
          let tempAttribute = {
            attribute_code: item.attribute_code,
            attribute_name: item.attribute_name,
            attribute_options: item.attribute_options,
            code_external_attribute: item.code_external_attribute,
            customer_type: item.customer_type,
            data_type: item.data_type,
            data_type_id: item.data_type_id,
            default_value: item.default_value,
            external_attribute: item.external_attribute,
            hint: item.hint,
            id: item.id,
            is_default_attribute: item.is_default_attribute,
            is_auto_gen: item.is_auto_gen,
            is_hidden: item.is_hidden,
            is_multiple_value: item.is_multiple_value,
            is_required: item.is_required,
            is_show: item.is_show,
            is_unique: item.is_unique,
            note: item.note,
            sort_order: item.sort_order,
            team_id: item.team_id,
            user_id: item.user_id
          };
          vm.fullAttributes.push(tempAttribute);
        }
      })
    }

    $("#filterButtonToggle").click(function () {
      if ($(this).hasClass("m-dropdown--open")) {
        $(this).removeClass("m-dropdown--open");
      } else {
        $(this).addClass("m-dropdown--open");
      }
    })
    $(document).click(function (event) {
      if (!$(event.target).closest("#filterButtonToggle").length) {
        if ($("#filterButtonToggle").is(":visible")) {
          if ($("#filterButtonToggle").hasClass("m-dropdown--open")) {
            $("#filterButtonToggle").removeClass("m-dropdown--open");
          }
        }
      }
    });
    vm.$validator.localize("en", messageFilter);
  },

  methods: {
    setActiveFilter(filter) {
      let vm = this;
      vm.filterActive = filter;
      if (filter.status === 0 && filter.name === "Tất cả khách hàng" || filter.status === 0 && filter.name === "Tất cả giao dịch" || filter.status === 0 && filter.name === "Tất cả học viên") {
        vm.showFilter = false;
        vm.isAllData = true;
        vm.resetFilterCriteria();
        vm.$emit("show_filter_id", vm.filterActive);
      } else {
        vm.showFilter = true;
        vm.isAllData = false;
        vm.$emit("show_filter_id", vm.filterActive);
      }
      vm.isPlusFilter = false;
      vm.isAddNew = false;
    },

    toggleShowFilter() {
      if (this.showFilter) {
        this.showFilter = false
      } else {
        this.showFilter = true
      }
      this.isMinimizeFilter = !this.isMinimizeFilter
      this.$emit('minimizeFilter', this.isMinimizeFilter)
    },

    createFilter() {
      let vm = this;
      vm.filterAttributes = [];
      let createData = {
        attribute_name: '',
        attribute_code: '',
        data_type: '',
        is_required: '',
        is_default_attribute: '',
        default_value: '',
        is_unique: '',
        is_multiple_value: '',
        attribute_option: '',
        attribute_values: '',
        cache_attribute_values: '',
        sort_order: ''
      };
      let createFilter = {
        id: "",
        name: "",
        status: 1,
        team_id: vm.currentTeamId,
        user_id: vm.currentUser.id
      };
      vm.filterActive = createFilter;
      vm.filterAttributes.push(createData);
      vm.filterIsActive = true;
      vm.attributeActive = createData;
      vm.isAddNew = true;
      vm.showFilter = true;
      // vm.doneTyping();
      vm.$validator.reset();
    },

    addNewCriteria() {
      let vm = this;
      let createData = {
        attribute_name: '',
        attribute_code: '',
        data_type: '',
        is_required: '',
        is_default_attribute: '',
        default_value: '',
        is_unique: '',
        is_multiple_value: '',
        attribute_option: '',
        attribute_values: '',
        cache_attribute_values: '',
        sort_order: ''
      };
      vm.filterAttributes.push(createData);
      vm.isPlusFilter = true;
    },

    selectedFilterAttribute(event, index) {
      let vm = this;
      let thisElement = event.currentTarget;
      let attribute_id = $(thisElement).val();
      let attribute = vm.fullAttributes.find(function (item) {
        return item.id == attribute_id;
      })
      if (attribute == null || typeof attribute == "undefined") {
        if (vm.filterAttributes[index].attribute_values) {
          vm.filterAttributes[index].attribute_values = {};
          vm.filterAttributes[index].is_disabled = false;
          let indexParams = -1;
          if (vm.oldParams && vm.oldParams.filter) {
            vm.oldParams.filter.forEach(function (params, index) {
              if (vm.filterAttributes[index].attribute_id == params.attribute_id) {
                indexParams = index;
              }
            })
          }
          if (indexParams > -1) {
            vm.oldParams.filter.splice(indexParams, 1);
          }
        }
      } else {
        vm.filterAttributes[index].attribute_id = attribute.id;
        vm.filterAttributes[index].data_type = attribute.data_type.id;
        vm.filterAttributes[index].attribute_name = attribute.attribute_name;
        vm.filterAttributes[index].attribute_code = attribute.attribute_code;
        vm.filterAttributes[index].external_attribute = attribute.external_attribute && typeof attribute.external_attribute !== "undefined" ? attribute.external_attribute : 0;
        vm.filterAttributes[index].code_external_attribute = attribute.code_external_attribute && typeof attribute.code_external_attribute !== "undefined" ? attribute.code_external_attribute : "";
        vm.filterAttributes[index].attribute_options = attribute.attribute_options;
        vm.filterAttributes[index].show_position = attribute.show_position;
        vm.filterAttributes[index].is_auto_gen = attribute.is_auto_gen;
        vm.filterAttributes[index].is_disabled = true;
        let attribute_values = {
          condition: null,
          attribute_value: "",
          attribute_id: attribute.id
        };
        if (attribute.data_type && attribute.data_type.id && (attribute.data_type.id == 1 || attribute.data_type.id == 12)) {
          attribute_values.type = "string";
        } else if (attribute.data_type && attribute.data_type.id && (attribute.data_type.id == 2 || attribute.data_type.id == 3) || attribute.external_attribute == 1 && (attribute.code_external_attribute == 'created_at' || attribute.code_external_attribute == 'updated_at')) {
          attribute_values.id = null;
          attribute_values.type = "date";
          attribute_values.rangeDate = [];
          attribute_values.dateRange = {
            day: "",
            month: "",
            year: ""
          };
          attribute_values.errors = "";
        } else if (attribute.data_type && attribute.data_type.id && (attribute.data_type.id == 6 || attribute.data_type.id == 7) || attribute.external_attribute == 1 && (attribute.code_external_attribute == 'product_quantity' || attribute.code_external_attribute == 'product_discount' || attribute.code_external_attribute == 'order_discount' || attribute.code_external_attribute == 'amount_must_be_paid' || attribute.code_external_attribute == 'single_sale_price' || attribute.code_external_attribute == 'price_after' || attribute.code_external_attribute == 'product_price')) {
          attribute_values.error = false;
          attribute_values.numberRange = {
            min: "",
            max: ""
          };
        } else if (attribute.data_type && attribute.data_type.id && (attribute.data_type.id == 8 || attribute.data_type.id == 9 && attribute.attribute_code !== 'contact_user' || attribute.data_type.id == 10 || attribute.data_type.id == 11 || attribute.data_type.id == 13 || attribute.data_type.id == 14 || attribute.data_type.id == 15 || attribute.data_type.id == 16) || attribute.external_attribute == 1 && (attribute.code_external_attribute == 'charge_person' || attribute.code_external_attribute == 'user_id' || attribute.code_external_attribute == 'shipping_status')) {
          attribute_values.attribute_value = [];
          attribute_values.condition = "IN";
        } else if (attribute.data_type && attribute.data_type.id && attribute.data_type.id == 'type_customer') {
          attribute_values.attribute_value = [];
          attribute_values.condition = "IN";
        }
        vm.filterAttributes[index].attribute_values = attribute_values;
        // vm.doneTyping();
      }
    },

    removeFilterCriteria(index) {
      let vm = this;
      vm.filterAttributes.splice(index, 1);
      vm.keyUpFilterChange();
    },

    keyUpFilterChange() {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(vm.doneTyping, vm.doneTypingInterval);
    },

    keyDownFilterChange() {
      let vm = this;
      clearTimeout(vm.typingTimer);
    },

    doneTyping() {
      let vm = this;
      let attributes = [];
      let result = [];
      vm.isErrorValidate = false;
      vm.filterAttributes.forEach(function (item) {
        let value = item.attribute_values.attribute_value;
        let dataType = vm.getDataType(item);

        if (item.attribute_values && item.attribute_values.id && item.attribute_values.id != 6 && item.attribute_values.id != 7 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at'))) {
          item.attribute_values.dateRange = {};
          item.attribute_values.rangeDate = [];
          value = vm.convertDateTime(item);
          //filter_date_type 1 : Trong khoảng, Hôm nay, Hôm qua, Tuần này, Tháng này

          item.attribute_values.filter_date_type = 1;
          item.attribute_values.condition = "=";
          if (value == "empty_value") {
            value = "";
            item.attribute_values.condition = "empty_value";
          }
          item.attribute_values.attribute_value = value;
          if (Array.isArray(value)) {
            result.push(item);
            return;
          }
        } else if (item.attribute_values && item.attribute_values.id && item.attribute_values.id == 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at'))) {
          item.attribute_values.attribute_value = "";
          item.attribute_values.rangeDate = [];
          let dateValue = item.attribute_values.dateRange;
          if (dateValue.day > 31) {
            item.attribute_values.errors = "Ngày không thể lớn hơn 31";
            vm.isErrorValidate = true;
            return true;
          } else if (dateValue.day && dateValue.day <= 0) {
            item.attribute_values.errors = "Ngày không thể nhỏ hơn 0";
            vm.isErrorValidate = true;
            return true;
          } else {
            item.attribute_values.errors = "";
            vm.isErrorValidate = false;
          }

          if (dateValue.month > 12) {
            item.attribute_values.errors = "Tháng không thể lớn hơn 12";
            vm.isErrorValidate = true;
            return true;
          } else if (dateValue.month && dateValue.month <= 0) {
            item.attribute_values.errors = "Tháng không thể nhỏ hơn 0";
            vm.isErrorValidate = true;
            return true;
          } else {
            item.attribute_values.errors = "";
            vm.isErrorValidate = false;
          }

          let currentYear = new Date().getFullYear();
          if (dateValue.year > currentYear) {
            item.attribute_values.errors = "Năm không thể lớn hơn năm hiện tại";
            vm.isErrorValidate = true;
            return true;
          } else if (dateValue.year && dateValue.year <= 0) {
            item.attribute_values.errors = "Năm không thể nhỏ hơn 0";
            vm.isErrorValidate = true;
            return true;
          } else {
            item.attribute_values.errors = "";
            vm.isErrorValidate = false;
          }

          let dateValueResult = {
            day: "",
            month: "",
            year: ""
          };
          if (dateValue.day) {
            dateValueResult.day = dateValue.day;
          }
          if (dateValue.month) {
            dateValueResult.month = dateValue.month;
          }
          if (dateValue.year) {
            dateValueResult.year = dateValue.year;
          }

          value = {};

          if (dateValueResult.day || dateValueResult.month || dateValueResult.year) {
            value.day = dateValueResult.day;
            value.month = dateValueResult.month;
            value.year = dateValueResult.year;
          }
          item.attribute_values.condition = "=";
          //filter_date_type 2 : Ngày chính xác
          item.attribute_values.filter_date_type = 2;
        } else if (item.attribute_values && item.attribute_values.id && item.attribute_values.id == 7 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at'))) {
          item.attribute_values.attribute_value = "";
          item.attribute_values.dateRange = {};
          item.attribute_values.filter_date_type = 1;
          result.push(item);
          return;
        } else if ((item.data_type == 6 || item.data_type == 7 || item.external_attribute == 1 && (item.code_external_attribute == 'product_quantity' || item.code_external_attribute == 'product_discount' || item.code_external_attribute == 'order_discount' || item.code_external_attribute == 'amount_must_be_paid' || item.code_external_attribute == 'single_sale_price' || item.code_external_attribute == 'price_after' || item.code_external_attribute == 'product_price')) && item.attribute_values.condition != '<>') {
          value = item.attribute_values.attribute_value.replace(",", ".");
        } else if ((item.data_type == 6 || item.data_type == 7 || item.external_attribute == 1 && (item.code_external_attribute == 'product_quantity' || item.code_external_attribute == 'product_discount' || item.code_external_attribute == 'order_discount' || item.code_external_attribute == 'amount_must_be_paid' || item.code_external_attribute == 'single_sale_price' || item.code_external_attribute == 'price_after' || item.code_external_attribute == 'product_price')) && item.attribute_values.condition == '<>') {
          let numberValue = item.attribute_values.numberRange;
          if (numberValue.max != '' && numberValue.min != '') {
            if (parseFloat(numberValue.max) < parseFloat(numberValue.min)) {
              item.attribute_values.error = true;
              vm.isErrorValidate = true;
            } else {
              item.attribute_values.error = false;
              vm.isErrorValidate = false;
            }
          }
          result.push(item);
          return;
        } else if (item.data_type == 8 || item.data_type == 9 && item.attribute_code !== 'contact_user' || item.data_type == 10 || item.data_type == 11 || item.data_type == 13 || item.data_type == 14 || item.data_type == 15 || item.data_type == 16 || item.external_attribute == 1 && (item.code_external_attribute == 'charge_person' || item.code_external_attribute == 'user_id' || item.code_external_attribute == 'shipping_status')) {
          let listOptions = item.attribute_values.attribute_value;
          value = listOptions;
        } else if (item.external_attribute == 1 && item.code_external_attribute == 'type_customer') {
          let listOptions = item.attribute_values.attribute_value;
          value = listOptions;
        }
        let args = {
          attribute_id: item.attribute_values.attribute_id,
          data_type: dataType,
          attribute_code: item.attribute_code,
          attribute_value: value,
          condition: item.attribute_values.condition,
          filter_date_type: item.attribute_values.filter_date_type,
        }
        attributes.push(args);
      })
      if (result.length > 0) {
        let multiArgs = [];
        result.forEach(function (item) {
          if (Array.isArray(item.attribute_values.attribute_value)) {
            let temp_data = item.attribute_values.attribute_value;
            if (item.attribute_values && item.attribute_values.id && item.attribute_values.id != 7 && item.attribute_values.id != 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_data[0] != 'undefined' && typeof temp_data[1] != 'undefined') {
              if (temp_data[0]) {
                let temp_args_min = {
                  attribute_id: item.attribute_id,
                  data_type: "2",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data[0],
                  condition: ">=",
                  filter_date_type: 1
                };
                multiArgs.push(temp_args_min);
              }
              if (temp_data[1]) {
                let temp_args_max = {
                  attribute_id: item.attribute_id,
                  data_type: "2",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data[1],
                  condition: "<=",
                  filter_date_type: 1
                }
                multiArgs.push(temp_args_max);
              }
            }
          } else if (item.attribute_values.rangeDate && Array.isArray(item.attribute_values.rangeDate) && item.attribute_values.rangeDate.length > 0) {
            let temp_range_date = item.attribute_values.rangeDate;
            if (item.attribute_values && item.attribute_values.id && item.attribute_values.id == 7 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_range_date[0] != 'undefined' && typeof temp_range_date[1] != 'undefined') {
              let dateMin = new Date(temp_range_date[0]);
              let min = dateMin.getDate() + '/' + (dateMin.getMonth() + 1) + '/' + dateMin.getFullYear();
              if (min != '1/1/1970') {
                let temp_args_min = {
                  attribute_id: item.attribute_id,
                  data_type: "2",
                  attribute_code: item.attribute_code,
                  attribute_value: min,
                  condition: ">=",
                  filter_date_type: 1
                };
                multiArgs.push(temp_args_min);
              }
              let dateMax = new Date(temp_range_date[1]);
              let max = dateMax.getDate() + '/' + (dateMax.getMonth() + 1) + '/' + dateMax.getFullYear();
              if (max != '1/1/1970') {
                let temp_args_max = {
                  attribute_id: item.attribute_id,
                  data_type: "2",
                  attribute_code: item.attribute_code,
                  attribute_value: max,
                  condition: "<=",
                  filter_date_type: 1
                }
                multiArgs.push(temp_args_max);
              }
            }
          } else if (typeof item.attribute_values.attribute_value == 'string') {
            if (item.data_type == 6 || item.data_type == 7 || item.external_attribute == 1 && (item.code_external_attribute == 'product_quantity' || item.code_external_attribute == 'product_discount' || item.code_external_attribute == 'order_discount' || item.code_external_attribute == 'amount_must_be_paid' || item.code_external_attribute == 'single_sale_price' || item.code_external_attribute == 'price_after' || item.code_external_attribute == 'product_price')) {
              let temp_data = item.attribute_values.numberRange;
              if (typeof temp_data.min != 'undefined' && typeof temp_data.max != 'undefined') {
                if (temp_data.min) {
                  let temp_args_min = {
                    attribute_id: item.attribute_id,
                    data_type: "1",
                    attribute_code: item.attribute_code,
                    attribute_value: temp_data.min,
                    condition: ">=",
                    filter_date_type: 1
                  };
                  multiArgs.push(temp_args_min);
                }
                if (temp_data.max) {
                  let temp_args_max = {
                    attribute_id: item.attribute_id,
                    data_type: "1",
                    attribute_code: item.attribute_code,
                    attribute_value: temp_data.max,
                    condition: "<=",
                    filter_date_type: 1
                  }
                  multiArgs.push(temp_args_max);
                }
              }
            }
          } else {
            return;
          }
        })
        $.merge(attributes, multiArgs);
      }
      if (!vm.isErrorValidate) {
        if (vm.checkArrayNotEmptyValue(attributes)) {
          let params = {
            filter: attributes
          }
          vm.oldParams = params;
          vm.$emit("start_filter", params);
          vm.isChangeOldFilter = true;
        } else if (attributes && attributes.length > 1 && !vm.checkArrayNotEmptyValue(attributes)) {
          let newParams = [];
          attributes.forEach(function (item) {
            if (item.condition == "empty_value" || typeof item.attribute_value == "object" && Object.keys(item.attribute_value).length > 0 || typeof item.attribute_value !== "object" && item.attribute_value !== "" && typeof item.attribute_value !== 'undefined') {
              newParams.push(item);
            }
          })
          if (newParams && newParams.length > 0) {
            vm.oldParams.filter = newParams;
          }
          vm.isChangeOldFilter = true;
          vm.$emit("start_filter", vm.oldParams);
        } else if (attributes && attributes.length == 1 && !vm.checkArrayNotEmptyValue(attributes)) {
          let newParams = [];
          attributes.forEach(function (item) {
            if (item.condition == "empty_value" && typeof item.attribute_value !== 'undefined') {
              newParams.push(item);
            }
          })
          if (newParams && newParams.length > 0) {
            vm.oldParams.filter = newParams;
          } else {
            vm.oldParams = {};
          }
          vm.isChangeOldFilter = true;
          vm.$emit("start_filter", vm.oldParams);
        } else {
          vm.oldParams = {};
          vm.isChangeOldFilter = true;
          vm.$emit("start_filter", vm.oldParams);
        }
      }
    },

    getDataType(item) {
      if (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) {
        return "2";
      } else if (item.attribute_code == "total_paid_amount") {
        return "3";
      } else {
        return "1";
      }
    },

    convertDateTime(attribute) {
      let date_id = attribute.attribute_values && attribute.attribute_values.id ? attribute.attribute_values.id : '';
      let today = new Date();
      let yesterday = new Date(today);
      yesterday.setDate(today.getDate() - 1);
      let yesterdayValue = yesterday.getDate() + '/' + (yesterday.getMonth() + 1) + '/' + yesterday.getFullYear();
      let todayValue = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
      if (attribute.data_type == 2 || attribute.data_type == 3 || attribute.external_attribute == 1 && (attribute.code_external_attribute == 'created_at' || attribute.code_external_attribute == 'updated_at')) {
        if (date_id == "empty_value") {
          return "empty_value";
        } else if (date_id == 2) {
          return yesterdayValue;
        } else if (date_id == 3) {
          return todayValue;
        } else if (date_id == 4) {
          let firstWeek = today.getDate() - today.getDay();
          // if (firstWeek < 0) {
          // }
          let lastWeek = firstWeek + 6;
          let newFirstWeek = new Date();
          let firstWeekDate = new Date(newFirstWeek.setDate(firstWeek));
          let newLastWeek = new Date();
          let lastWeekDate = new Date(newLastWeek.setDate(lastWeek));
          let firstDayOfWeek = firstWeekDate.getDate() + '/' + (firstWeekDate.getMonth() + 1) + '/' + firstWeekDate.getFullYear();
          let lastDayOfWeek = lastWeekDate.getDate() + '/' + (lastWeekDate.getMonth() + 1) + '/' + lastWeekDate.getFullYear();
          let returnValue = [
            firstDayOfWeek,
            lastDayOfWeek
          ];
          return returnValue;
        } else if (date_id == 5) {
          let firstMonthDate = new Date(today.getFullYear(), today.getMonth(), 1);
          let lastMonthDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          let firstDayOfMonth = firstMonthDate.getDate() + '/' + (firstMonthDate.getMonth() + 1) + '/' + firstMonthDate.getFullYear();
          let lastDayOfMonth = lastMonthDate.getDate() + '/' + (lastMonthDate.getMonth() + 1) + '/' + lastMonthDate.getFullYear();
          let returnValue = [
            firstDayOfMonth,
            lastDayOfMonth
          ];
          return returnValue;
        }
      }
    },

    selecteDeleted(filter) {
      let vm = this;
      vm.selectedDelete = filter;
    },

    deleteFilterOfMe() {
      let vm = this;
      let filterId = vm.selectedDelete.id;
      let params = {
        filter_id: filterId,
        user_id: vm.currentUser.id,
        team_id: vm.currentTeamId
      };
      let store = "";
      if (vm.filter_type == 1) {
        //Customer
        store = CUSTOMER_FILTER_DELETE;
      } else if (vm.filter_type == 2) {
        //Product
      } else if (vm.filter_type == 3) {
        //Deal
        store = DEAL_FILTER_DELETE;
      } else if (vm.filter_type == 4) {
        //Student
        store = STUDENT_FILTER_DELETE;
      }

      vm.$store.dispatch(store, params).then(response => {
        if (response.data.status_code == 422 && response.data.errors.not_permission_update_deal) {
          vm.$snotify.error('Xoá bộ lọc thất bại.', TOAST_ERROR);
        } else if (response && response.data.data.success) {
          vm.resetFilterCriteria();
          vm.$emit("is_response_success", true);
          vm.$snotify.success('Xoá bộ lọc thành công.', TOAST_SUCCESS);
        } else {
          vm.$snotify.error('Xoá bộ lọc thất bại.', TOAST_ERROR);
        }
      }).catch(() => {
        vm.$snotify.error('Xoá bộ lọc thất bại.', TOAST_ERROR);
      });
    },

    resetFilterCriteria() {
      let vm = this;
      vm.filterAttributes = [];
      vm.filterActive = null;
      if (vm.filter_active) {
        vm.filterActive = vm.filterData[0];
      }
      vm.showFilter = false;
      vm.isAddNew = false;
      vm.isAllData = true;
      vm.isPlusFilter = false;
      vm.$emit('cancel_filter', true);
    },

    setBeforeSaveCache(filter) {
      let vm = this;
      vm._beforeSaveFilter = Object.assign({}, filter);
    },

    resetDataCreateFilter() {
      let vm = this;
      vm.addNewFilter = {
        name: "",
        status: 1,
        unique: false
      };
      vm.$validator.reset();
    },

    saveCreateFilter(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.has(scope + '.name')) {
          let attributes = [];
          vm.filterAttributes.forEach(function (item) {
            let temp_data = {
              attribute_id: item.attribute_id,
              attribute_values: item.attribute_values,
              external_attribute: item.external_attribute,
              code_external_attribute: item.code_external_attribute
            };
            attributes.push(temp_data);
          })
          attributes = vm.convertAttributesPushToServer(attributes);
          let params = {
            name: vm.addNewFilter.name,
            user_id: vm.currentUser.id,
            team_id: vm.currentTeamId,
            status: vm.addNewFilter.status === 0 ? 1 : vm.addNewFilter.status,
            attributes: attributes
          };

          vm.clickLoader = true;
          let store = "";
          if (vm.filter_type == 1) {
            //Customer
            store = CUSTOMER_FILTER_CREATE;
          } else if (vm.filter_type == 2) {
            //Product
          } else if (vm.filter_type == 3) {
            //Deal
            store = DEAL_FILTER_CREATE;
          } else if (vm.filter_type == 4) {
            //Student
            store = STUDENT_FILTER_CREATE;
          }

          vm.$store.dispatch(store, params).then(response => {
            if (response.data.status_code == 422 && response.data.errors.not_permission_update_deal) {
              vm.$snotify.error('Thêm mới bộ lọc thất bại.', TOAST_ERROR);
            } else if (response.data.status_code == 422 && response.data.errors.name) {
              vm.errors.add({
                field: "name",
                msg: "Tên bộ lọc bị trùng",
                scope: scope
              });
            } else if (response.data.data && response.data.data.status) {
              vm.isAddNew = false;
              vm.isChangeOldFilter = false;
              vm.addNewFilter = {
                name: "",
                status: 1
              };
              vm.filterActive = response.data.data;
              if (vm.cache_attributes_filter === "" || vm.cache_attributes_filter === null) {
                vm.cacheFilterAttributesBeforeReload = JSON.stringify(attributes);
              }
              vm.$validator.reset();
              $(".s--modal-create-filter-save").modal("hide");
              vm.$emit("is_response_success", true);
              vm.$snotify.success('Thêm mới bộ lọc thành công.', TOAST_SUCCESS);
            } else {
              vm.$snotify.error('Thêm mới bộ lọc thất bại.', TOAST_ERROR);
            }
            vm.clickLoader = false;
          }).catch(() => {
            vm.$snotify.error('Thêm mới bộ lọc thất bại.', TOAST_ERROR);
          });

        }
      });
    },

    viewEditFilterOfMe() {
      let vm = this;
      vm._beforeEditFilterOfMe = Object.assign({}, vm.filterActive);
    },

    cancelEditFilterOfMe() {
      let vm = this;
      vm.filterActive = vm._beforeEditFilterOfMe;
    },

    updateFilterOfMe(scope) {
      let vm = this;
      let attributes = [];
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.has(scope + '.name')) {
          vm.filterAttributes.forEach(function (item) {
            let temp_data = {
              attribute_id: item.attribute_id,
              attribute_values: item.attribute_values,
              external_attribute: item.external_attribute,
              code_external_attribute: item.code_external_attribute
            };
            attributes.push(temp_data);
          })
          attributes = vm.convertAttributesPushToServer(attributes);
          let params = {
            filter_id: vm.filterActive.id,
            name: vm.filterActive.name,
            user_id: vm.currentUser.id,
            team_id: vm.currentTeamId,
            status: vm.filterActive.status === 0 ? 1 : vm.filterActive.status,
            attributes: attributes
          };
          vm.clickLoader = true;

          let store = "";
          if (vm.filter_type == 1) {
            //Customer
            store = CUSTOMER_FILTER_UPDATE;
          } else if (vm.filter_type == 2) {
            //Product
          } else if (vm.filter_type == 3) {
            //Deal
            store = DEAL_FILTER_UPDATE;
          } else if (vm.filter_type == 4) {
            //Student
            store = STUDENT_FILTER_UPDATE;
          }

          vm.$store.dispatch(store, params).then(response => {
            if (response.data.status_code == 422 && response.data.errors.not_permission_update_deal) {
              vm.$snotify.success('Cập nhật bộ lọc thất bại.', TOAST_ERROR);
            } else if (response.data.status_code == 422 && response.data.errors.name) {
              vm.errors.add({
                field: "name",
                msg: "Tên bộ lọc bị trùng",
                scope: scope
              });
            } else if (response.data.data && response.data.data.status) {
              vm.isChangeOldFilter = false;
              vm.filterActive = response.data.data;
              if (vm.cache_attributes_filter === "" || vm.cache_attributes_filter === null) {
                vm.cacheFilterAttributesBeforeReload = JSON.stringify(attributes);
              }
              $(".s--modal-update-filter-save").modal("hide");
              vm.$emit("is_response_success", true);
              vm.$validator.reset();
              vm.$snotify.success('Cập nhật bộ lọc thành công.', TOAST_SUCCESS);
            } else {
              vm.$snotify.success('Cập nhật bộ lọc thất bại.', TOAST_ERROR);
            }
            vm.clickLoader = false;
          }).catch(() => {
            vm.$snotify.success('Cập nhật bộ lọc thất bại.', TOAST_ERROR);
          });
        }
      });
    },

    convertAttributesPushToServer(attributes) {
      let vm = this;
      let newAttributes = [];

      if (attributes && attributes.length) {
        attributes.forEach(attribute => {
          if (attribute && attribute.attribute_values && attribute.attribute_values.rangeDate && attribute.attribute_values.rangeDate.length) {
            // tranform rangeDate (Trong khoảng)
            let newRangeDate = [];
            attribute.attribute_values.rangeDate.forEach(date => {
              newRangeDate.push(vm.helpers.convertTimetoYMD(date));
            })

            attribute.attribute_values.rangeDate = newRangeDate;
          }
          newAttributes.push(attribute);
        })
      }

      return newAttributes;
    },

    checkArrayNotEmptyValue(items) {
      let count = 0;
      if (items && items.length > 0) {
        items.forEach(function (item) {
          if ((typeof item.attribute_value == "object" && item.attribute_value && Object.keys(item.attribute_value).length > 0) || (typeof item.attribute_value !== "object" && item.attribute_value !== "" && typeof item.attribute_value !== "undefined")) {
            count++;
          }
        })
        if (count == items.length) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },

    generateOptions(listOptions, stop, label) {
      let vm = this;
      let result = [];
      if (listOptions && listOptions.length > 0 || listOptions && Object.keys(listOptions).length > 0) {
        listOptions.forEach(function (item) {
          if (item[label]) {
            item[label] = vm.helpers.textTruncate(item[label], stop, "...");
          }
          result.push(item);
        })
      }
      return result;
    },

    generateOptionsExistEmpty(listOptions, stop, label) {
      let vm = this;
      let temp = {};
      temp["id"] = "empty_value";
      temp[label] = "Bỏ trống";
      let result = [
        temp
      ];
      if (listOptions && listOptions.length > 0 || listOptions && Object.keys(listOptions).length > 0) {
        listOptions.forEach(function (item) {
          if (item[label]) {
            item[label] = vm.helpers.textTruncate(item[label], stop, "...");
          }
          result.push(item);
        })
      }
      return result;
    },

    validateFilterText(item, error) {
      if (item.attribute_values.condition !== 'empty_value' && (!item.attribute_values || (item.attribute_values.attribute_value == "" || item.attribute_values.attribute_value == null))) {
        error++;
      }
      if (typeof item.attribute_values == "object" && Object.keys(item.attribute_values).length == 0) {
        error++;
      }
      return error;
    },

    validateFilterNumber(item, error) {
      if (!item.attribute_values) {
        error++;
      }
      if (typeof item.attribute_values == "object" && Object.keys(item.attribute_values).length == 0) {
        error++;
      }
      if (item.attribute_values.condition == "<>") {
        if (!item.attribute_values.numberRange) {
          error++;
        }
        if (item.attribute_values.numberRange && (item.attribute_values.numberRange.min == "" || item.attribute_values.numberRange.max == "")) {
          error++;
        }
        if (item.attribute_values.numberRange && parseFloat(item.attribute_values.numberRange.max) < parseFloat(item.attribute_values.numberRange.min)) {
          error++;
        }
      }
      if (item.attribute_values.condition != "<>" && item.attribute_values.condition !== 'empty_value' && (item.attribute_values.attribute_value == "" || item.attribute_values.attribute_value == null)) {
        error++;
      }
      return error;
    },

    validateFilterDateTime(item, error) {
      if (!item.attribute_values || !item.attribute_values.id) {
        error++;
      }
      if (typeof item.attribute_values == "object" && Object.keys(item.attribute_values).length == 0) {
        error++;
      }
      if (item.attribute_values.id !== 'empty_value' && item.attribute_values.id != 6 && item.attribute_values.id != 7 && item.attribute_values.id != 1 && item.attribute_values.attribute_value == "") {
        error++;
      }
      if (item.attribute_values.id == 6) {
        if (!item.attribute_values.dateRange.day && !item.attribute_values.dateRange.month && !item.attribute_values.dateRange.year || item.attribute_values.dateRange.day == "" &&
          item.attribute_values.dateRange.month == "" &&
          item.attribute_values.dateRange.year == "") {
          error++;
        }
      }
      if (item.attribute_values.id == 7) {
        if (item.attribute_values.rangeDate == "" || item.attribute_values.rangeDate == null) {
          error++;
        }
      }
      return error;
    },

    validateFilterSelect(item, error) {
      if (!item.attribute_values) {
        error++;
      }
      if (typeof item.attribute_values == "object" && Object.keys(item.attribute_values).length == 0) {
        error++;
      }
      if (item.attribute_values.attribute_value == "") {
        error++;
      }
      if (item.attribute_values.attribute_value && item.attribute_values.attribute_value.length <= 0) {
        error++;
      }
      return error;
    },

    keydownCheckNumber(event, key = "call", data_type = 0) {
      let keyCode = event.keyCode;
      if (!((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 106) || keyCode == 8 || (data_type == 6 || data_type == 'product_quantity') && keyCode == 188 || keyCode == 37 || keyCode == 39)) {
        event.preventDefault();
      } else {
        if (key == "phone") {
          let value = event.target.value;
          if (value && value.length > 12) {
            event.preventDefault();
          }
        } else if (key == "call") {
          let vm = this;
          clearTimeout(vm.typingTimer);
        }
      }
    },

    compareArray(arr1, arr2) {
      if (!arr1 || !arr2) return false;
      let count = 0;
      arr1.forEach(function (e1, id1) {
        arr2.forEach(function (e2, id2) {
          if (typeof arr1 == 'object' && typeof arr2 == 'object') {
            if (id1 === id2 && JSON.stringify(e1) === JSON.stringify(e2)) {
              count++;
            } else {
              return;
            }
          } else {
            return false;
          }
        })
      })
      if (arr1.length > arr2.length) {
        if (arr1.length === count) {
          return true;
        } else {
          return false;
        }
      } else {
        if (arr2.length === count) {
          return true;
        } else {
          return false;
        }
      }
    },

    checkUniqueFilterName() {
      let vm = this;
      let count = 0;
      if (vm.addNewFilter.name && vm.addNewFilter.name.length > 0) {
        vm.filterData.forEach(function (item) {
          if (item.name.toLowerCase() === vm.addNewFilter.name.toLowerCase()) {
            count++;
          }
        })
      }
      if (count > 0) {
        vm.addNewFilter.unique = true;
      } else {
        vm.addNewFilter.unique = false;
      }
    }
  }
}