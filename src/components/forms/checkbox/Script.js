export default {
  props: {
    data: {
      type: Array,
      default: []
    },
    label: {
      type: String,
      default: "name"
    },
    value: {
      type: [String, Number, Array, Boolean],
      default: ''
    }
  },
  watch: {
    value: {
      handler(after) {
        if (Array.isArray(after)) {
          this.new_value = after;
        } else {
          this.new_value = [];
        }
      },
      deep: true
    },
    new_value: {
      handler(after) {
        if (Array.isArray(after)) {
          this.new_value = after;
        } else {
          this.new_value = [];
        }
        this.$emit('input', after)
        this.$emit('keyup', after)
      },
      deep: true
    },
  },
  data() {
    return {
      new_value: this.value
    };
  },
  methods: {}
};