export default {
  props: {
    value: {
      type: [String, Date],
      default: ""
    },
    not_before: {
      type: [String, Date],
      default: ""
    },
    not_after: {
      type: [String, Date],
      default: ""
    },
  },
  watch: {
    value: {
      handler: function () {
        this.newValue = this.value;
      }
    }
  },
  data() {
    return {
      lang: {
        days: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        months: [
          "Tháng 1",
          "Tháng 2",
          "Tháng 3",
          "Tháng 4",
          "Tháng 5",
          "Tháng 6",
          "Tháng 7",
          "Tháng 8",
          "Tháng 9",
          "Tháng 10",
          "Tháng 11",
          "Tháng 12"
        ],
        pickers: [
          "7 ngày tiếp theo",
          "30 ngày tiếp theo ",
          "7 ngày trước",
          "30 ngày trước"
        ],
        placeholder: {
          date: "Chọn ngày",
          dateRange: "Chọn khoảng thời gian"
        }
      },
      newValue: this.value
    };
  },
};