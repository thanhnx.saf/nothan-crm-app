/* eslint-disable no-undef */

export default {
  props: {
    value: {
      type: [String, Number],
      default: () => {
        return ''
      }
    },
    el_id: {
      type: String,
      default: "el_id"
    },
    is_edit: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    value: {
      handler() {
        this.newValue = this.value;
      }
    },
    is_edit: {
      handler() {
        this.isEdit = this.is_edit;
      }
    }
  },
  data() {
    return {
      newValue: this.value,
      isEdit: this.is_edit
    };
  },
  methods: {

    showUpdate() {
      this.isEdit = true
      setTimeout(() => {
        $('#' + this.el_id + ' .nt-input-update').focus()
      }, 100)
    },

    hideUpdate() {
      this.isEdit = false
    },

    update() {
      this.isEdit = false
    }
  }
};