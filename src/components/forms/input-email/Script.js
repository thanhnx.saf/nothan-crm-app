export default {
  props: {
    value: {
      type: String,
      default: ""
    }
  },
  watch: {
    value: {
      handler: function () {
        this.new_value = this.value;
      },
      deep: true
    }
  },
  data() {
    return {
      new_value: this.value
    };
  },
};
