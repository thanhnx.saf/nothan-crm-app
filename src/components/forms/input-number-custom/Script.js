/* eslint-disable no-undef */
export default {
  props: {
    value: {
      type: [String, Number],
      default: ""
    },
    is_decimal: {
      type: Boolean,
      default: true
    }
  },
  methods: {
    updateValue: function (inputValue) {
      let clearVal = this.clearFormatNumber(inputValue);
      let formattedVal = "";
      if (String(clearVal).indexOf(",") > -1) {
        let decimalValue = String(clearVal).substring(
          String(clearVal).indexOf(",") + 1,
          String(clearVal).length
        );
        if (decimalValue.length > 0) {
          formattedVal = this.formatNumber(clearVal);
        } else {
          formattedVal = clearVal;
        }
      } else {
        formattedVal = this.formatNumber(clearVal);
      }
      this.$refs.input.value = formattedVal;
      this.$emit("input", clearVal);
    },

    displayValue: function (inputValue) {
      return this.formatNumber(inputValue);
    },

    selectAll: function (event) {
      setTimeout(function () {
        event.target.select();
      }, 0);
    },

    formatNumber(valueInput) {
      if (valueInput !== "") {
        if (String(valueInput).indexOf(",") > -1) {
          let originalValue = String(valueInput).substring(
            0,
            String(valueInput).indexOf(",")
          );
          originalValue = String(parseFloat(originalValue));
          let decimalValue = String(valueInput).substring(
            String(valueInput).indexOf(",") + 1,
            String(valueInput).length
          );
          if (decimalValue.length <= 0 && originalValue.length <= 0) {
            return "";
          } else if (originalValue.length > 0 && decimalValue.length <= 0) {
            return valueInput;
          } else {
            let originValueConvert = originalValue;
            let decimalValueConvert = "";
            if (parseInt(decimalValue.charAt(2)) >= 5) {
              if (
                parseInt(decimalValue.charAt(1)) >= 9 &&
                parseInt(decimalValue.charAt(0)) >= 9
              ) {
                originValueConvert = parseFloat(originValueConvert) + 1;
                decimalValueConvert = 0;
              } else {
                decimalValueConvert =
                  parseInt(decimalValue.substring(0, 2)) + 1;
              }
            } else {
              decimalValueConvert = decimalValue.substring(0, 2);
            }
            let newValue = "";
            if (isNaN(originValueConvert)) {
              originValueConvert = 0;
            }
            originValueConvert = originValueConvert.replace(
              /(\d)(?=(\d{3})+(?:\.\d+)?$)/g,
              "$1."
            );
            if ($.isNumeric(decimalValueConvert)) {
              newValue = originValueConvert + "," + decimalValueConvert;
            } else {
              newValue = originValueConvert;
            }
            return newValue;
          }
        } else {
          let newValue = String(parseFloat(valueInput)).replace(
            /(\d)(?=(\d{3})+(?:\.\d+)?$)/g,
            "$1."
          );
          return newValue;
        }
      }
      return "";
    },

    clearFormatNumber(valueInput) {
      if (valueInput !== "") {
        if (String(valueInput).indexOf(",") > -1) {
          let originalValue = String(valueInput).substring(
            0,
            String(valueInput).indexOf(",")
          );
          let decimalValue = String(valueInput).substring(
            String(valueInput).indexOf(",") + 1,
            String(valueInput).length
          );
          if (decimalValue.length <= 0 && originalValue.length <= 0) {
            return "";
          } else if (originalValue.length > 0 && decimalValue.length <= 0) {
            return valueInput;
          } else {
            let originValueConvert = originalValue.replace(/[^\d]/g, "");
            let decimalValueConvert = decimalValue.replace(/[^\d]/g, "");
            let newValue = originValueConvert + "," + decimalValueConvert;
            return newValue;
          }
        } else {
          let newValue = String(valueInput).replace(/[^\d]/g, "");
          if (newValue !== "") {
            return newValue;
          } else {
            return "";
          }
        }
      }
      return "";
    },

    setOnlyNumberInInput(event) {
      let keyCode = event.keyCode;
      if (this.is_decimal) {
        if (
          !(
            (keyCode > 47 && keyCode < 58) ||
            (keyCode > 95 && keyCode < 106) ||
            keyCode === 8 ||
            keyCode === 9 ||
            keyCode === 188 ||
            keyCode === 37 ||
            keyCode === 39
          )
        ) {
          event.preventDefault();
        }
      } else {
        if (
          !(
            (keyCode > 47 && keyCode < 58) ||
            (keyCode > 95 && keyCode < 106) ||
            keyCode === 8 ||
            keyCode === 9 ||
            keyCode === 37 ||
            keyCode === 39
          )
        ) {
          event.preventDefault();
        }
      }
    }
  }
};