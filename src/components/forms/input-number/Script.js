/* eslint-disable no-undef */
export default {
  props: {
    value: {
      type: [String, Number],
      default: ""
    }
  },
  methods: {
    updateValue: function (inputValue) {
      var valToSend = inputValue;

      var numVal = numeral(valToSend).value();

      if (isNaN(numVal)) {
        valToSend = this.value.toString();
        numVal = numeral(valToSend).value();
      }

      var formattedVal = numeral(numVal).format("0,0.[000]");

      this.$refs.input.value =
        formattedVal + (valToSend.endsWith(",") ? "," : "");

      this.$emit("input", numeral(formattedVal).value());
    },
    displayValue: function (inputValue) {
      return numeral(inputValue).format("0,0.[000]");
    },
    selectAll: function (event) {
      setTimeout(function () {
        event.target.select();
      }, 0);
    }
  }
};