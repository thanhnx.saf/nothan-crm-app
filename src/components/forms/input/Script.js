export default {
  props: {
    value: {
      type: [String, Number],
      default: () => { return '' }
    }
  },
  watch: {
    value: {
      handler: function () {
        this.newValue = this.value;
      }
    }
  },
  data() {
    return {
      newValue: this.value
    };
  },
};
