/* eslint-disable no-undef */
export default {
  props: {
    data: {
      type: Array,
      required: true
    },
    label: {
      type: String,
      required: true
    },
    index: {
      type: String,
      required: true
    },
    is_search: {
      type: Boolean,
      default: false
    },
    position_select: {
      type: [String, Number],
      required: true
    },
    placeholder: {
      type: String
    },
    width: {
      type: String,
      default: "100%"
    },
    text: {
      type: String,
      default: "thuộc tính"
    },
    save_change: {
      type: Boolean,
      default: true
    },
    value: {},
    is_add_new: {
      type: Boolean,
      default: false
    },
    label_text: {
      type: String,
      default: "Thêm thuộc tính"
    },
    model_addnew: {
      type: String
    },
    return_object: {
      type: Boolean,
      default: false
    },
    is_deal_nsd: {
      type: Boolean,
      default: false
    },
    buyer_deal_id: {
      type: [String, Number],
      default: ""
    },
    is_load_more_data: {
      type: Boolean,
      default: false
    },
    current_object: {
      type: [Object, Array]
    },
    is_edit: {
      type: Boolean,
      default: false
    },
    is_loading: {
      type: Boolean,
      default: false
    }
  },
  mounted() {
    let vm = this;
    vm.loadDataSelect();
  },
  data() {
    return {
      noItemSearch: false,
      selectedItemOutput: null,
      loadingCustomer: false
    };
  },

  watch: {
    selectedItemOutput: {
      handler(after) {
        let vm = this;
        vm.$emit("change", after);
      },
      deep: true
    },
    is_loading: {
      handler() {
        let vm = this;
        vm.loadDataSelect();
        vm.$emit("loading_success", true);
      },
      deep: true
    }
  },

  methods: {
    getValueWhenLoad() {
      let vm = this;
      if (vm.return_object && vm.value && vm.value[vm.index]) {
        if (vm.is_deal_nsd) {
          return vm.value[vm.label];
        } else {
          let data = vm.data.find(function (item) {
            return item.id === vm.value[vm.index];
          });
          if (data) {
            return data[vm.label];
          } else {
            return "";
          }
        }
      } else if (
        (vm.is_edit &&
          vm.value &&
          vm.value[vm.index] &&
          vm.value[vm.index] === vm.buyer_deal_id &&
          vm.current_object) ||
        (!vm.is_edit && vm.current_object)
      ) {
        return vm.current_object[vm.label];
      } else if (
        vm.is_edit &&
        Object.keys(vm.value) <= 0 &&
        vm.current_object
      ) {
        return vm.current_object[vm.label];
      } else {
        let data = vm.data.find(function (item) {
          return item.id === vm.value;
        });
        if (data) {
          return data[vm.label];
        } else {
          return "";
        }
      }
    },

    searchValueInOptions(event) {
      let vm = this;
      let dropDown = $("#nt-customer-select-" + vm.position_select).children(
        ".nt-customer-select-dropdown"
      );
      if (!dropDown.hasClass("nt-show-dropdown")) {
        $("#nt-customer-select-" + vm.position_select)
          .children(".nt-customer-select-main")
          .addClass("show-dropdown");
        dropDown.addClass("nt-show-dropdown");
        dropDown.slideDown(100);
        vm.toggleArrow();
      }
      let value = event.target.value;
      if (!vm.is_deal_nsd) {
        let currentValue = value.toUpperCase();
        let li, a, i, txtValue;
        li = $(
          "#nt-customer-select-" +
          vm.position_select +
          " .nt-customer-select-dropdown .nt-customer-select-dropdown-item"
        );
        let quantity = 0;
        for (i = 0; i < li.length; i++) {
          a = li[i];
          txtValue = a.innerText;
          if (txtValue.toUpperCase().indexOf(currentValue) > -1) {
            li[i].style.display = "";
          } else {
            quantity++;
            li[i].style.display = "none";
          }
        }
        if (quantity === li.length) {
          vm.noItemSearch = true;
        } else {
          vm.noItemSearch = false;
        }
      } else {
        vm.$emit("input", value);
      }
    },

    toggleDropdown(event) {
      let vm = this;
      let currentTarget = event.currentTarget;
      let dropDown = $(currentTarget)
        .parent(".nt-customer-select")
        .children(".nt-customer-select-dropdown");
      if (dropDown.hasClass("nt-show-dropdown")) {
        dropDown.removeClass("nt-show-dropdown");
        $("#nt-customer-select-" + vm.position_select)
          .children(".nt-customer-select-main")
          .removeClass("show-dropdown");
        dropDown.slideUp(50);
        if (vm.is_search) {
          $(
            "#nt-customer-select-" +
            vm.position_select +
            " .nt-customer-select-textsearch"
          ).blur();
        }
        vm.toggleArrow();
      } else {
        $("#nt-customer-select-" + vm.position_select)
          .children(".nt-customer-select-main")
          .addClass("show-dropdown");
        dropDown.addClass("nt-show-dropdown");
        dropDown.slideDown(100);
        vm.toggleArrow();
      }
    },

    toggleArrow() {
      let vm = this;
      let iconArrow = $("#nt-customer-select-" + vm.position_select)
        .children(".nt-customer-select-main")
        .children(".nt-customer-select-main-icon");
      if (iconArrow.hasClass("icon-down-arrow-1")) {
        iconArrow.removeClass("icon-down-arrow-1");
        iconArrow.addClass("icon-up-arrow-1");
      } else {
        iconArrow.removeClass("icon-up-arrow-1");
        iconArrow.addClass("icon-down-arrow-1");
      }
    },

    selectedItem(item) {
      let vm = this;
      let dropDown = $("#nt-customer-select-" + vm.position_select).children(
        ".nt-customer-select-dropdown"
      );
      if (vm.return_object) {
        vm.selectedItemOutput = item
        vm.$emit("update:value", item);
        vm.$emit("update_value", {
          index: vm.position_select,
          data: vm.selectedItemOutput
        });
      } else {
        vm.selectedItemOutput = item[vm.index];
        vm.$emit("update:value", item[vm.index]);
      }
      if (vm.is_search) {
        if (vm.save_change) {
          let currentValue = item[vm.label];
          if (vm.is_deal_nsd) {
            let status = "";
            if (item && item[vm.index] && item[vm.index] === vm.buyer_deal_id) {
              status = " (Người mua)";
            } else {
              status = "";
            }
            currentValue = currentValue + status;
          }
          $(
            "#nt-customer-select-" +
            vm.position_select +
            " .nt-customer-select-textsearch"
          ).val(currentValue);

        } else {
          $(
            "#nt-customer-select-" +
            vm.position_select +
            " .nt-customer-select-textsearch"
          ).val("");
          vm.selectedItemOutput = null;
        }
      } else {
        $(
          "#nt-customer-select-" +
          vm.position_select +
          " .nt-customer-select-textshow"
        ).text(item[vm.label]);
      }
      $("#nt-customer-select-" + vm.position_select)
        .children(".nt-customer-select-main")
        .removeClass("show-dropdown");
      dropDown.removeClass("nt-show-dropdown");
      dropDown.slideUp(50);
      vm.toggleArrow();
    },

    focusToInputSearch(event) {
      let vm = this;

      if (vm.is_deal_nsd) {
        let value = event.target.value;
        if (value.indexOf("(Người mua)") > -1) {
          let newValue = value.replace(" (Người mua)", "");
          $(
            "#nt-customer-select-" +
            vm.position_select +
            " .nt-customer-select-textsearch"
          ).val(newValue);
        }
      }
    },

    blurToInputSearch(event) {
      let vm = this;
      if (vm.is_deal_nsd) {
        if (vm.return_object) {
          vm.$emit("update:value", vm.selectedItemOutput);
          vm.$emit("update_value", {
            index: vm.position_select,
            data: vm.selectedItemOutput
          });
        } else {
          vm.$emit("update:value", 0);
        }

        let currentValueInInput = event.target.value;
        let currentValue = "";
        if (currentValueInInput.length > 0) {
          let status = "";
          // ||
          // (vm.selectedItemOutput &&
          //   vm.selectedItemOutput[vm.index] &&
          //   vm.selectedItemOutput[vm.index] === vm.buyer_deal_id)
          if (
            (vm.value &&
              vm.value[vm.index] &&
              vm.value[vm.index] === vm.buyer_deal_id)
          ) {
            status = " (Người mua)";
          } else {
            status = "";
          }
          currentValue = vm.value[vm.label] && vm.value[vm.label] ? vm.value[vm.label] + status : vm.selectedItemOutput[vm.label] + status;
          // currentValue =
          //   vm.selectedItemOutput && vm.selectedItemOutput[vm.label] ?
          //   vm.selectedItemOutput[vm.label] + status :
          //   vm.value[vm.label] + status;
          $(
            "#nt-customer-select-" +
            vm.position_select +
            " .nt-customer-select-textsearch"
          ).val(currentValue);
        } else {
          $(
            "#nt-customer-select-" +
            vm.position_select +
            " .nt-customer-select-textsearch"
          ).val("");
        }
      }
    },

    hideDropdown() {
      let vm = this;
      let dropDown = $("#nt-customer-select-" + vm.position_select).children(
        ".nt-customer-select-dropdown"
      );
      if (dropDown.hasClass("nt-show-dropdown")) {
        dropDown.removeClass("nt-show-dropdown");
        dropDown.slideUp(50);
        if (vm.is_search) {
          $(
            "#nt-customer-select-" +
            vm.position_select +
            " .nt-customer-select-textsearch"
          ).blur();
        }
        vm.toggleArrow();
      }
    },

    loadDataSelect() {
      let vm = this;
      if (vm.current_object) {
        vm.selectedItemOutput = vm.value ? vm.value : vm.current_object;
      }
      if (vm.is_search) {
        let currentValue = vm.getValueWhenLoad();
        if (vm.is_deal_nsd) {
          let status = "";
          if (
            (vm.value &&
              vm.value[vm.index] &&
              vm.value[vm.index] === vm.buyer_deal_id) ||
            (vm.is_edit &&
              Object.keys(vm.value) <= 0 &&
              vm.current_object &&
              vm.current_object.customer_id) ||
            (!vm.is_edit &&
              vm.selectedItemOutput &&
              vm.selectedItemOutput[vm.index] &&
              vm.selectedItemOutput[vm.index] === vm.buyer_deal_id)
          ) {
            status = " (Người mua)";
          } else {
            status = "";
          }
          currentValue = currentValue + status;
        }
        $(
          "#nt-customer-select-" +
          vm.position_select +
          " .nt-customer-select-textsearch"
        ).val(currentValue);
      } else {
        $(
          "#nt-customer-select-" +
          vm.position_select +
          " .nt-customer-select-textshow"
        ).text(vm.getValueWhenLoad());
      }
      $(document).ready(function () {
        $("#nt-customer-select-" + vm.position_select).css("width", vm.width);
        $(
          "#nt-customer-select-" +
          vm.position_select +
          " .nt-customer-select-dropdown"
        ).css("width", vm.width);
        $("#nt-customer-select-dropdown-main" + vm.position_select).scroll(
          function () {
            let scrollHeight = $(this)[0].scrollHeight;
            let scrollTop = $(this).scrollTop();
            let height = $(this).height();
            if (height + scrollTop === scrollHeight - 20) {
              vm.$emit("load_more_data", true);
            }
          }
        );
      });
      $(document).click(function (event) {
        if (
          !$(event.target).closest("#nt-customer-select-" + vm.position_select)
          .length
        ) {
          if (
            $("#nt-customer-select-" + vm.position_select).is(":visible") &&
            $(
              "#nt-customer-select-" +
              vm.position_select +
              " .nt-customer-select-dropdown"
            ).is(":visible")
          ) {
            if (
              $(
                "#nt-customer-select-" +
                vm.position_select +
                " .nt-customer-select-dropdown"
              ).hasClass("nt-show-dropdown")
            ) {
              $(
                "#nt-customer-select-" +
                vm.position_select +
                " .nt-customer-select-dropdown"
              ).removeClass("nt-show-dropdown");
              $("#nt-customer-select-" + vm.position_select)
                .children(".nt-customer-select-main")
                .removeClass("show-dropdown");
              $(
                "#nt-customer-select-" +
                vm.position_select +
                " .nt-customer-select-dropdown"
              ).slideUp(50);
              $(
                "#nt-customer-select-" +
                vm.position_select +
                " .nt-customer-select-dropdown .nt-customer-select-dropdown-item"
              ).css("display", "block");
              if (vm.is_search) {
                let currentValue = "";
                if (vm.is_deal_nsd) {
                  let status = "";
                  if (
                    (vm.value &&
                      vm.value[vm.index] &&
                      vm.value[vm.index] === vm.buyer_deal_id) ||
                    (vm.selectedItemOutput &&
                      vm.selectedItemOutput[vm.index] &&
                      vm.selectedItemOutput[vm.index] === vm.buyer_deal_id)
                  ) {
                    status = " (Người mua)";
                  } else {
                    status = "";
                  }
                  let currentValueInInput = $(
                    "#nt-customer-select-" +
                    vm.position_select +
                    " .nt-customer-select-textsearch"
                  ).val();
                  if (currentValueInInput.length > 0) {
                    currentValue = vm.value[vm.label] && vm.value[vm.label] ? vm.value[vm.label] + status : vm.selectedItemOutput[vm.label] + status;
                    // currentValue =
                    //   vm.selectedItemOutput && vm.selectedItemOutput[vm.label] ?
                    //   vm.selectedItemOutput[vm.label] + status :
                    //   vm.value[vm.label] + status;
                  } else {
                    currentValue = "";
                  }
                }
                if (vm.save_change) {
                  if (vm.return_object) {
                    vm.$emit("update:value", vm.selectedItemOutput);
                    vm.$emit("update_value", {
                      index: vm.position_select,
                      data: vm.selectedItemOutput
                    });
                  } else {
                    vm.$emit("update:value", 0);
                  }
                  $(
                    "#nt-customer-select-" +
                    vm.position_select +
                    " .nt-customer-select-textsearch"
                  ).val(currentValue);
                } else {
                  $(
                    "#nt-customer-select-" +
                    vm.position_select +
                    " .nt-customer-select-textsearch"
                  ).val("");
                }
                vm.noItemSearch = false;
                $(
                  "#nt-customer-select-" +
                  vm.position_select +
                  " .nt-customer-select-textsearch"
                ).blur();
              } else {
                $(
                  "#nt-customer-select-" +
                  vm.position_select +
                  " .nt-customer-select-textshow"
                ).text(vm.getValueWhenLoad());
              }
              vm.toggleArrow();
            }
          }
        }
      });
    }
  }
};