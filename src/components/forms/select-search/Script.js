export default {
  props: {
    data: {
      type: Array,
      default: []
    },
    label: {
      type: String,
      default: "name"
    },
    value: {
      type: [String, Number, Array],
      default: ""
    }
  },
  watch: {
    value: {
      handler: function () {
        this.new_value = this.value;
      },
      deep: true
    }
  },
  data() {
    return {
      new_value: this.value
    };
  },
};