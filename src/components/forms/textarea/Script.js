export default {
  props: {
    value: {
      type: [String, Array, Number],
      default: ""
    }
  },
  watch: {
    value: {
      handler: function () {
        this.newValue = this.value;
      }
    }
  },
  data() {
    return {
      newValue: this.value
    };
  },
};
