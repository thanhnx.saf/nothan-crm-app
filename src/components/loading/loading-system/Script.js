export default {
  mounted() {
    this.onInit()
  },
  destroyed() {
    this.onDestroy()
  },
  data() {
    return {
      replaceColor: [],
      bgColors: ['#EA4C66', '#FFDC00', '#69BE31'],
      interval: -1
    }
  },
  methods: {
    onInit() {
      const body = document.getElementsByTagName('body')[0]
      body.classList.add('s-body-fix')
      this.interval = setInterval(() => {
        this.bgColors.forEach((color, index) => {
          setTimeout(() => {
            this.replaceColor[index] = this.bgColors[0]
            if (index >= 1) {
              this.replaceColor[index - 1] = '#fff'
            } else {
              this.replaceColor[this.bgColors.length - 1] = '#fff'
            }
            if (index === this.bgColors.length - 1) {
              this.bgColors.unshift(this.bgColors.pop())
            }
            this.$forceUpdate()
          }, (index + 1) * 300)
        })
      }, 900)
    },

    onDestroy() {
      const body = document.getElementsByTagName('body')[0]
      body.classList.remove('s-body-fix')
      clearInterval(this.interval)
    },
  },
}