/* eslint-disable no-undef */
import {
  mapState,
  mapGetters
} from "vuex";

import {
  FILE_UPLOAD_NOTE,
  NOTE_CREATE,
  NOTE_GET_ALL_BY_OBJECT_ID
} from "@/types/actions.type";
import utils from "@/utils/utils";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";
const customMessagessNote = {
  custom: {
    content: {
      required: "Không được để trống trường này.",
      max: "Giới hạn 1025 ký tự."
    }
  }
};
import {
  PERMISSION_ADD_NOTE_CUSTOMER,
  PERMISSION_ADD_NOTE_DEAL,
  IS_PRIVATE_FILE
} from "@/types/const";

export default {
  props: {
    type: {
      type: Number,
      required: true
    },
    object_id: {
      type: Number,
      required: true
    },
    permission_service: {
      type: String,
      default: "customer"
    },
    permission_action: {
      type: String,
      default: ""
    },
    deleted_at: {
      type: String,
      default: null
    },
    modal_trash_id: {
      type: String,
      default: ""
    }
  },
  computed: {
    ...mapState({
      currentTeam: state => state.organization.currentTeam,
      notes: state => state.note.listByObjectId
    }),
    utils() {
      return utils;
    },
    ...mapGetters(["userCheckAddOn", "userCurrentPermissions"])
  },
  created() {},
  mounted() {
    let vm = this;
    vm.setNote();
    vm.$validator.localize("en", customMessagessNote);
    vm.getAllNoteByOjectId({
      object_id: vm.object_id,
      type: vm.type
    });
  },
  data() {
    return {
      PERMISSION_ADD_NOTE_CUSTOMER: PERMISSION_ADD_NOTE_CUSTOMER,
      PERMISSION_ADD_NOTE_DEAL: PERMISSION_ADD_NOTE_DEAL,

      note: {},
      editorInfoToolbar: [
        ["bold", "italic", "underline"]
      ],
      editorOptions: {
        formats: ["bold", "italic", "underline"]
      },
      noteFiles: [],
      files: [],
      filesPreview: [],
      isLoader: false,
      isDisabled: false,
      filesUploadMax: 5,
      filesUploadMaxSize: 10 * 1024 * 1024, // 10Mb 10 * 1024 * 1024
      filesUploadMaxSizeError: false,
      filesUploadAcceptError: false,
      server_errors: {},
      noteIndex: null
    };
  },
  methods: {
    changeFiles() {
      let vm = this;
      let files = vm.$refs.uploaFilesNote.files;
      if (files && files.length > 0) {
        vm.files = files;
      }
      vm.validateFiles();
      vm.getFilesPreview();
    },

    async uploadFiles() {
      let vm = this;
      let filesPromises = Object.keys(vm.files).map(async function (index) {
        let formData = new FormData();
        formData.append("files", vm.files[index]);
        formData.append("customer_id", vm.object_id);
        formData.append("is_publish", IS_PRIVATE_FILE);

        let fileData = await vm.$store.dispatch(FILE_UPLOAD_NOTE, formData);

        let data = fileData.data.data;

        if (data) {
          let parseData = {};
          parseData.id = data.id;
          parseData.path_url = data.path_on_server;
          parseData.name = data.name ? data.name : data.title;
          parseData.original_name = data.original_name;
          parseData.is_publish = data.is_publish ? data.is_publish : IS_PRIVATE_FILE;
          parseData.size = utils.formatSizeUnits(data.size);
          parseData.type = data.name.replace(/^.*\./, "");
          return parseData;
        }
      });

      let fileIds = await Promise.all(filesPromises);
      vm.note.files = fileIds;
    },

    getFilesPreview() {
      let vm = this;
      vm.filesPreview = [];

      if (
        !vm.filesUploadMaxSizeError &&
        !vm.filesUploadAcceptError &&
        vm.files &&
        vm.files.length <= vm.filesUploadMax
      ) {
        Object.keys(vm.files).forEach(function (index) {
          // Check accept type
          let fileType = vm.files[index].name.replace(/^.*\./, "");
          let url = "";
          if (
            fileType === "jpg" ||
            fileType === "jpeg" ||
            fileType === "png" ||
            fileType === "gif"
          ) {
            url = "picture.png";
          } else if (fileType === "doc" || fileType === "docx") {
            url = "word.png";
          } else if (fileType === "xls" || fileType === "xlsx") {
            url = "excel.png";
          } else if (fileType === "txt") {
            url = "txt.png";
          } else if (fileType === "pdf") {
            url = "pdf.png";
          }

          let data = {
            name: vm.files[index].name,
            url: url
          };
          vm.filesPreview.push(data);
        });
      }
    },

    validateFiles() {
      let vm = this;
      vm.isDisabled = false;
      vm.filesUploadMaxSizeError = false;
      vm.filesUploadAcceptError = false;

      if (vm.files && vm.files.length > vm.filesUploadMax) {
        vm.isDisabled = true;
        $("#note_upload_files_warning").modal("show");
        return;
      }

      if (vm.files && vm.files.length > 0) {
        Object.keys(vm.files).forEach(function (index) {
          // Check max size
          if (vm.files[index].size > vm.filesUploadMaxSize) {
            vm.filesUploadMaxSizeError = true;
            vm.isDisabled = true;
            $("#note_upload_files_warning").modal("show");
            return;
          }

          // Check accept type
          let fileType = vm.files[index].name.replace(/^.*\./, "");
          let acceptType = [
            "jpg",
            "jpeg",
            "png",
            "gif",
            "doc",
            "docx",
            "xls",
            "xlsx",
            "txt",
            "pdf"
          ];

          if ($.inArray(fileType, acceptType) === -1) {
            vm.filesUploadAcceptError = true;
            vm.isDisabled = true;
            $("#note_upload_files_warning").modal("show");
            return;
          }
        });
      }
    },

    setNoteIndex(index) {
      let vm = this;
      vm.noteIndex = index;

      $("#note_detail").modal("show");
    },

    getAllNoteByOjectId(params) {
      let vm = this;
      vm.$store.dispatch(NOTE_GET_ALL_BY_OBJECT_ID, params);
    },

    createNote(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          vm.uploadFiles().then(() => {
            vm.note.team_id = vm.currentTeamId;
            vm.$store
              .dispatch(NOTE_CREATE, vm.note)
              .then(response => {
                if (response.status === 422) {
                  vm.server_errors = response.data.errors;
                  if (vm.server_errors.sort_delete) {
                    $("#" + vm.modal_trash_id).modal("show");
                  }
                  vm.$snotify.error("Thêm mới ghi chú thất bại.", TOAST_ERROR);
                } else {
                  if (response.data.data && response.data.data.success) {
                    vm.$snotify.success(
                      "Thêm mới ghi chú thành công.",
                      TOAST_SUCCESS
                    );
                    vm.cancelCreateNote();
                    vm.getAllNoteByOjectId({
                      object_id: vm.object_id,
                      type: vm.type
                    });
                  }
                }
                vm.isLoader = false;
              })
              .catch(error => {
                vm.$snotify.error("Thêm mới ghi chú thất bại.", TOAST_ERROR);
                vm.isLoader = false;
                console.log(error);
              });
          });
        }
      });
    },

    setNote() {
      let vm = this;
      vm.note = {
        content: "",
        files: [],
        type: vm.type,
        object_id: vm.object_id,
        team_id: vm.currentTeamId
      };
    },

    cancelCreateNote() {
      let vm = this;
      vm.setNote();
      vm.isLoader = false;
      vm.isDisabled = false;
      vm.filesUploadMaxSizeError = false;
      vm.filesUploadAcceptError = false;
      vm.$refs.uploaFilesNote.value = "";
      vm.files = [];
      vm.$validator.reset();
      vm.filesPreview = [];
    },

    showPopupUpload() {
      let vm = this;
      $("#uploadImagesButton" + vm.type).click();
    }
  }
};