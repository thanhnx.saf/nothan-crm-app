import InfiniteLoading from 'vue-infinite-loading'
import firebase from 'firebase/app'
import 'firebase/messaging'
import {
  mapGetters
} from "vuex"
// import {
//   TOAST_SUCCESS
// } from "@/types/config"
import {
  FIREBASE_CONFIG
} from '@/types/config'
import NoResults from '../no-results/NoResults.vue'
import NotiService from '@/services/noti'

import {
  SAVE_DEVICE_TOKEN_NOTI
} from "@/types/actions.type"

const tabs = {
  org: 'organization',
  system: 'system',
}

export default {
  components: {
    InfiniteLoading,
    NoResults
  },
  data() {
    return {
      tabs: tabs,
      activeTab: tabs.org,
      numberOfOrgNotis: 0,
      numberOfSystemNotis: 0,
      isActive: false,
      announcement: [],
      notis: [],
      page: 0
    }
  },
  computed: {
    ...mapGetters(['currentUser']),
    numberOfNotis() {
      const count = this.numberOfOrgNotis + this.numberOfSystemNotis
      if (count > 99) {
        return '99+'
      }
      return count
    },
  },
  watch: {
    currentUser: {
      handler(after) {
        if (after.id) {
          this.fetch()
        }
      }
    }
  },
  mounted() {
    if (this.currentUser && this.currentUser.id) {
      this.fetch()
    }
  },
  created() {
    this.setup()
  },
  methods: {
    fetch($state) {
      this.page++
      const user_id = this.currentUser.id
      NotiService.getList(user_id, this.page).then(res => {
        if (res.data) {
          this.numberOfOrgNotis = res.data.total_un_seen
          const data = res.data.data
          if ($state && !data.length) {
            $state.complete()
            return
          }
          this.notis = [...this.notis, ...data]
          this.tranformNotis()
          if ($state) {
            $state.loaded()
          }
        }
      })
    },

    tranformNotis() {
      this.notis.forEach(noti => {
        const link = noti.click_action
        noti.click_action = link ? link.slice(link.indexOf('/team')) : ''
        noti.unRead = noti.status !== 3
      })
      this.$forceUpdate()
    },

    setup() {
      if (firebase.apps.length) {
        return
      }
      firebase.initializeApp(FIREBASE_CONFIG)

      const messaging = firebase.messaging()

      messaging.requestPermission().then(() => {
        return messaging.getToken()
      }).then(token => {
        if (token) {
          let params = {
            type_token: 'web',
            device_token: token
          }
          this.saveDeviceTokenNoti(params)
        }
      }).catch(error => console.log(error))

      messaging.onMessage(payload => {
        const noti = payload.notification
        const notiId = payload.data.notification_id
        let body = payload.data.message_body ? payload.data.message_body : payload.notification.body
        noti.body = body

        // let toastMessage = body ? body : ''
        // toastMessage = toastMessage.replace(/(<\/?(?:img)[^>]*>)|<[^>]+>/ig, '$1')

        // this.$snotify.success(
        //   toastMessage,
        //   TOAST_SUCCESS
        // )

        if (noti.click_action) {
          const currentTeam = noti.click_action.split('/').filter(Number)[0]
          if (currentTeam === this.$route.params.team_id) {
            this.numberOfOrgNotis += 1
            this.notis.unshift({
              ...noti,
              time: new Date(),
              unRead: true,
              id: parseInt(notiId),
              status: 1
            })
            this.tranformNotis()
          }
        }
      })
    },

    openNoti() {
      this.isActive = true

      let notiDropdown = document.getElementById('noti-dropdown')
      notiDropdown.classList.toggle('m-dropdown--open')

      let notiScrollContent = document.getElementById('nt_noti_scroll_content')
      notiScrollContent.scrollTop = 0

      setTimeout(() => {
        document.body.addEventListener('click', () => {
          this.removeCount()
          this.isActive = false
        }, {
          once: true
        })
      }, 300)
    },

    removeCount() {
      switch (this.activeTab) {
        case tabs.org:
          this.numberOfOrgNotis = 0
          NotiService.markAsSeen()
          break

        case tabs.system:
          this.numberOfSystemNotis = 0
          break
      }
    },

    markAsRead(tab, index, notification) {
      let notiDropdown = document.getElementById('noti-dropdown')
      notiDropdown.classList.toggle('m-dropdown--open')
      switch (tab) {
        case tabs.system:
          this.announcement[index].unRead = false
          break

        default:
          this.notis[index].unRead = false
          break
      }
      if (notification.status !== 3) {
        NotiService.markAsRead(notification.id).then((res) => {
          if (res.data.data) {
            return window.location.href = notification.click_action
          }
        })
      } else {
        return window.location.href = notification.click_action
      }
    },

    saveDeviceTokenNoti(params) {
      this.$store.dispatch(SAVE_DEVICE_TOKEN_NOTI, params)
    }
  }
}