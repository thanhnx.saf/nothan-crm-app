export default {
  props: {
    pagination: {
      type: Object,
      default: () => { }
    },
    loader: {
      type: Boolean,
      default: false
    }
  },
  methods: {
    isCurrentPage(page) {
      let vm = this;
      return vm.pagination.current_page === page;
    },
    changePage(page) {
      let vm = this;
      if (page > vm.pagination.last_page) {
        page = vm.pagination.last_page;
      }
      vm.pagination.current_page = page;
      vm.$emit("paginate", page);
    },
    checkPage(current, total) {
      return current < total
    }
  },
  computed: {
    pages() {
      let vm = this;
      const range = [];

      for (let i = vm.minPage; i < vm.maxPage; i++) {
        range.push(i);
      }

      return range;
    },
    minPage() {
      let minPage = this.pagination.current_page - 4;

      if (
        (minPage <= 0 && this.pagination.total_pages <= 5) ||
        (this.pagination.total_pages > 5 && this.pagination.current_page === 1)
      ) {
        minPage = 1;
      } else if (
        this.pagination.current_page === 2 &&
        this.pagination.total_pages > 5
      ) {
        minPage = this.pagination.current_page;
      } else if (this.pagination.total_pages > 5 && minPage <= 1) {
        minPage = 2;
      } else {
        minPage = this.pagination.current_page - 4;
      }
      return minPage;
    },
    maxPage() {
      let maxPage = this.minPage + 5;
      if (maxPage > this.pagination.total_pages) {
        maxPage = this.pagination.total_pages + 1;
      }
      return maxPage;
    }
  }
};
