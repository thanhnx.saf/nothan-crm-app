/* eslint-disable no-undef */
import {
    mapGetters
} from "vuex"

import {
    ID_BASE_URL
} from "@/types/config"

import qs from 'qs'

export default {
    props: {
        className: {
            type: String,
            default: 'ml-auto'
        },
        icon: {
            type: String,
            default: 'icon icon-external-link-01'
        },
        query: {
            type: Object,
            default: () => {}
        }
    },

    watch: {
        query: {
            handler(after) {
                this.setIdpAddonsUrl(after)
            },
            deep: true
        },

    },

    mounted() {
        this.setIdpAddonsUrl(this.query)
    },

    computed: {
        ...mapGetters(['currentTeam']),
        canAccess() {
            if (!this.currentTeam) {
                return
            }
            $('[data-toggle="m-tooltip"]').tooltip('hide')
            const permissionIds = [1, 2]
            const index = permissionIds.indexOf(parseInt(this.currentTeam.role_id))
            return index !== -1
        }
    },

    data() {
        return {
            idpAddonsUrl: '',
        }
    },

    methods: {
        setIdpAddonsUrl(query) {
            this.idpAddonsUrl = ID_BASE_URL + '/team/' + this.$route.params.team_id + '/addons?' + qs.stringify(query)
        }
    }
}