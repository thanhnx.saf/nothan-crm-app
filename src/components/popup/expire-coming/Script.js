import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import { ID_BASE_URL } from "@/types/config";

export default {
  props: ["addOnExpireAfter15Day"],
  computed: {
    ...mapGetters(["appInfoObj", "currentTeam"]),
    helpers() {
      return helpers;
    },
    linkListAddonUsing() {
      let teamId = this.$route.params.team_id ? this.$route.params.team_id : this.currentTeam.team_id;
      return ID_BASE_URL + '/team/' + teamId + '/addons';
    }
  },
  data() {
    return {
    }
  }
}
