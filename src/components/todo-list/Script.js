/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex"

import utils from '@/utils/utils'
import TaskService from '@/services/task'
import Loading from '@/components/loading/LoadingTable.vue'
import NoResult from '@/components/no-results/NoResults.vue'

const filterOptions = {
  today: 'Hôm nay',
  tomorrow: 'Ngày mai',
  week: 'Tuần này',
  all: 'Tất cả'
}

const categoriesNames = {
  todo: 'Chưa thực hiện',
  doing: 'Đang thực hiện',
  done: 'Đã hoàn thành'
}

export default {
  components: {
    Loading,
    NoResult
  },
  data() {
    return {
      categories: [],
      categoriesNames: categoriesNames,
      error: '',
      filterOptions: filterOptions,
      freezeData: [],
      isLoading: false,
      isLoadingAdd: false,
      isShowAdd: false,
      orders: [],
      selectedDate: filterOptions.today,
      selectedRange: {},
      settings: {},
      showDone: true,
      todoInput: '',
      todoList: [],
      utils: utils,
    }
  },
  watch: {
    todoInput: {
      handler(after) {
        this.error = after.length >= 250 ? 'Giới hạn 250 ký tự.' : ''
      }
    }
  },
  computed: {
    ...mapGetters(['currentUser']),
    todoCategory() {
      if (!this.categories.length) {
        return
      }
      return this.categories.filter(t => t.name === categoriesNames.todo)[0]
    },
    doneCategory() {
      if (!this.categories.length) {
        return
      }
      return this.categories.filter(t => t.name === categoriesNames.done)[0]
    },
    numberOfDoneTask() {
      if (!this.categories.length || !this.todoList.length) {
        return 0
      }
      return this.todoList.filter(t => t.category_id === this.doneCategory.id).length
    }
  },
  methods: {
    getCategories() {
      const order = [categoriesNames.doing, categoriesNames.todo, categoriesNames.done]
      TaskService.getCategories().then(res => {
        res.data.forEach((category) => {
          const index = order.indexOf(category.name)
          category.order = index
          return category
        })
        this.categories = res.data.sort((a, b) => (a.order > b.order) ? 1 : (a.order < b.order) ? -1 : 0)
      })
    },

    getUserConfig() {
      this.isLoading = true
      this.orders = []
      TaskService.getUserConfig().then(res => {
        this.settings = res.data
        const configs = res.data && res.data.configs
        if (configs) {
          const ids = configs.map(t => t.id)
          this.categories.forEach(t => {
            const index = ids.indexOf(t.id)
            this.orders = [...this.orders, ...configs[index].orders]
          })
        }
        this.queryAllTasks()
      })
    },

    updateUserConfig() {
      const configs = []
      const categoryIds = this.categories.map(t => t.id)
      categoryIds.forEach(id => {
        configs.push({
          id,
          orders: []
        })
      })
      this.todoList.forEach(t => {
        const index = categoryIds.indexOf(t.category_id)
        configs[index].orders.push(t.id)
      })
      if (this.settings.id) {
        TaskService.updateUserConfig({
          configs
        }, this.settings.id)
      } else {
        TaskService.saveUserConfig({
          configs
        }).then(res => {
          this.settings = res.data
        })
      }
    },

    queryAllTasks() {
      const params = {
        ...this.selectedRange
      }
      TaskService.queryAllTasks(params).then(res => {
        this.isLoading = false
        if (this.orders.length) {
          res.data.map(t => {
            const index = this.orders.indexOf(t.id)
            t.order = index
            return t
          })
          this.todoList = res.data.sort((a, b) => (a.order > b.order) ? 1 : (a.order < b.order) ? -1 : 0)
        } else {
          this.todoList = res.data.map(t => {
            return t
          })
        }
        this.todoList.forEach(t => this.freezeData.push(Object.assign({}, t)))
        this.toggleDoneTask(this.showDone)
      })
    },

    onClose() {
      $('.nt-dropdown-backdrop-todo').removeClass('show')
      const app = document.getElementById('app')
      app.click()
    },

    onAddTask() {
      this.isShowAdd = true
      $('[data-toggle="m-tooltip"]').tooltip('hide')
      setTimeout(() => {
        const input = document.getElementById('todoInput')
        input.focus()
      }, 100);
    },

    addTask(event) {
      if (this.isLoadingAdd) {
        return
      }
      if (event.key === 'Enter') {
        if (!this.todoInput.trim()) {
          return
        }
        this.isLoadingAdd = true
        const body = {
          title: this.todoInput,
          assigner_id: this.currentUser.id,
          category_id: this.todoCategory.id
        }
        TaskService.createTask(body).then(res => {
          this.todoInput = ''
          this.isLoadingAdd = false
          if (this.selectedDate === filterOptions.all) {
            const ids = this.todoList.map(t => t.category_id)
            const index = ids.indexOf(this.todoCategory.id)
            this.todoList.splice(index, 0, res.data)
          }
          this.updateUserConfig()
          this.$bus.$emit('onAddTask', res.data)
          this.onAddTask()
        })
      }
      if (event.key === 'Escape' && !input.value.trim()) {
        this.isShowAdd = false
      }
    },

    onChangeStatus(checked, task) {
      task.isLoading = true
      this.$forceUpdate()
      if (checked) {
        TaskService.updateCategoryId(task.id, this.doneCategory.id).then(() => {
          this.$bus.$emit('onUpdateTaskCategoryId', {
            categoryId: this.doneCategory.id,
            task
          })
          task.isLoading = false
          task.category_id = this.doneCategory.id
          this.onUpdatePosition(task)
        })
      } else {
        const ids = this.freezeData.map(t => t.id)
        const index = ids.indexOf(task.id)
        const freezeTask = Object.assign({}, this.freezeData[index])
        const id = freezeTask.category_id === this.doneCategory.id ? this.todoCategory.id : freezeTask.category_id
        TaskService.updateCategoryId(task.id, id).then(() => {
          this.$bus.$emit('onUpdateTaskCategoryId', {
            categoryId: id,
            task
          })
          task.isLoading = false
          task.category_id = id
          this.onUpdatePosition(task)
        })
      }
    },

    onUpdatePosition(task) {
      setTimeout(() => {
        const taskIds = this.todoList.map(t => t.id)
        const taskIndex = taskIds.indexOf(task.id)
        this.todoList.splice(taskIndex, 1)
        const categoryIds = this.todoList.map(t => t.category_id)
        let categoryIndex = categoryIds.indexOf(task.category_id)
        if (categoryIndex !== -1) {
          this.todoList.splice(categoryIndex, 0, task)
        } else {
          switch (task.category_id) {
            case this.todoCategory.id:
              categoryIndex = categoryIds.indexOf(this.doneCategory.id)
              this.todoList.splice(categoryIndex, 0, task)
              break

            case this.doneCategory.id:
              this.todoList.push(task)
              break

            default:
              this.todoList.unshift(task)
              break
          }
        }
        this.toggleDoneTask(this.showDone)
      }, 300)
    },

    onFocusout() {
      const input = document.getElementById('todoInput')
      if (!input.value.trim()) {
        this.isShowAdd = false
      }
    },

    async fetch() {
      await this.getCategories()
      await this.onFilter()
      setTimeout(() => {
        $('.m-dropdown__dropoff').remove()
        $('.nt-dropdown-backdrop-todo').addClass('show')
      }, 100)
    },

    getDate(date) {
      return moment(date).format('YYYY-MM-DD')
    },

    getTime(time) {
      return moment(time).format('HH:mm')
    },

    toggleDoneTask(showDone) {
      this.showDone = showDone
      this.todoList.filter(task => task.category_id === this.doneCategory.id).map(task => {
        task.class = this.showDone ? '' : 'd-none'
        return task
      })
    },

    onFilter() {
      this.todoList = []
      const today = moment().format('YYYY-MM-DD')
      const tomorrow = moment().add(1, 'd').format('YYYY-MM-DD')
      const monday = moment().startOf('w').add(1, 'd').format('YYYY-MM-DD')
      const sunday = moment().endOf('w').add(1, 'd').format('YYYY-MM-DD')

      switch (this.selectedDate) {
        case filterOptions.today:
          this.selectedRange = {
            from: today,
            to: today
          }
          break

        case filterOptions.tomorrow:
          this.selectedRange = {
            from: tomorrow,
            to: tomorrow
          }
          break

        case filterOptions.week:
          this.selectedRange = {
            from: monday,
            to: sunday
          }
          break

        default:
          this.selectedRange = null
          break
      }
      this.getUserConfig()
    },

    isShowExpire(deadline) {
      if (!deadline) {
        return true
      }
      const current = moment().format('YYYY-MM-DD H:mm:s')
      return moment(deadline, 'YYYY-MM-DD H:mm:s').isAfter(current)
    },
  },
}