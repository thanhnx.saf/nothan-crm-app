import {
    mapGetters
} from "vuex"
import moment from 'moment'

import DashboardApi from '@/services/dashboard'
import Loading from '@/components/loading/LoadingTable.vue'
import NoResults from '@/components/no-results/NoResults.vue'
import PermissionCheck from '@/components/permission-check/PermissionCheck.vue'

const packageNames = {
    feature: 'ADDONS',
    upgrade: 'UPGRADE'
}

const categories = {
    customers: 1,
    users: 2,
    emails: 5,
    smss: 6
}

export default {
    components: {
        Loading,
        NoResults,
        PermissionCheck,
    },
    data() {
        return {
            addonPackages: [],
            isLoading: false,
            packageNames: packageNames,
            selectedPackage: packageNames.feature,
            teamAddons: {},
            upgradePackages: [],
            categoryCheckExpireAddons: [categories.emails, categories.smss]
        }
    },
    computed: {
        ...mapGetters(['currentTeam']),
        canAccess() {
            if (!this.currentTeam) {
                return
            }
            const permissionIds = [1, 2]
            const index = permissionIds.indexOf(parseInt(this.currentTeam.role_id))
            return index !== -1
        }
    },

    created() {
        // this.getActiveTeamAddons()
    },

    methods: {
        checkExpire(endDate) {
            let isExpire = false
            if (moment().valueOf() > moment(endDate).valueOf()) {
                isExpire = true
            }
            return isExpire
        },


        getActiveTeamAddons() {
            this.isLoading = true
            DashboardApi.getActiveTeamAddons().then(res => {
                this.isLoading = false
                if (!res.data) {
                    return
                }

                let teamAddons = res.data

                let convertObjectCategoryCheckExpireAddons = {}
                Object.keys(teamAddons).forEach((addonCategoryId) => {
                    addonCategoryId = parseInt(addonCategoryId)
                    if (this.categoryCheckExpireAddons.indexOf(addonCategoryId) !== -1) {
                        let listAddons = []
                        if (teamAddons[addonCategoryId].length) {
                            teamAddons[addonCategoryId].forEach(addon => {
                                if (!this.checkExpire(addon.end_date)) {
                                    listAddons.push(addon)
                                }
                            })
                        }

                        convertObjectCategoryCheckExpireAddons[addonCategoryId] = listAddons
                    }
                })

                teamAddons = {
                    ...teamAddons,
                    ...convertObjectCategoryCheckExpireAddons
                }

                this.teamAddons = teamAddons

                this.getAddons()
            })
        },

        getAddons() {
            const keys = Object.keys(this.teamAddons)
            const packages = []
            keys.forEach(key => {
                const addons = this.teamAddons[key]
                if (addons.length > 0) {
                    if (addons.length === 1) {
                        const addon = addons[0]
                        addon.isLoading = true
                        this.getStatisticData(addon)
                        packages.push(addon)
                    } else {
                        if (!addons[0].value) {
                            const addon = addons[0]
                            addon.isLoading = true
                            this.getStatisticData(addon)
                            packages.push(addon)
                        } else {
                            const addon = addons[addons.length - 1]
                            addon.isLoading = true
                            this.getStatisticData(addon)
                            packages.push(addon)
                        }
                    }
                }
            })
            this.addonPackages = packages.filter(t => t.code.indexOf(packageNames.feature) !== -1)
            this.upgradePackages = packages.filter(t => t.code.indexOf(packageNames.upgrade) !== -1)
        },

        getStatisticData(addon) {
            addon.isLoading = true
            addon.numberOfUsed = 0
            const {
                addon_id
            } = addon
            switch (addon.category_id) {
                case categories.customers:
                    DashboardApi.getNumberOfCustomers().then(res => this.getNumberOfUsed(addon, res.data.data))
                    break
                case categories.users:
                    DashboardApi.getNumberOfUsers().then(res => this.getNumberOfUsed(addon, res.data.data))
                    break
                case categories.emails:
                    DashboardApi.getNumberOfEmails({
                        addon_id
                    }).then(res => this.getNumberOfUsed(addon, res.data.data))
                    break
                case categories.smss:
                    DashboardApi.getNumberOfSmss({
                        addon_id
                    }).then(res => this.getNumberOfUsed(addon, res.data.data))
                    break
                default:
                    addon.isLoading = false
                    break
            }
        },

        getNumberOfUsed(addon, number) {
            addon.isLoading = false
            addon.numberOfUsed = number
            this.$forceUpdate()
        },

        getDuration(startDate, endDate) {
            const startMonth = moment(startDate, 'YYYY-MM-DD HH:mm:ss').month()
            const endMonth = moment(endDate, 'YYYY-MM-DD HH:mm:ss').month()
            const duration = endMonth - startMonth
            const months = duration <= 0 ? duration + 12 : duration
            return `${months} tháng`
        },

        getDurationLeft(endDate) {
            const current = moment()
            endDate = moment(endDate, 'YYYY-MM-DD HH:mm:ss')
            const duration = moment.duration(endDate.diff(current))
            if (duration <= 0) {
                return '<span style="color: #BB1130">Đã hết hạn</span>'
            }
            const days = Math.trunc(duration.as('d'))
            const hours = duration.get('h')
            return `${days} ngày ${hours} giờ`
        },

        onClose() {
            const dropOffs = document.getElementsByClassName('m-dropdown__dropoff')
            for (let index = 0; index < dropOffs.length; index++) {
                dropOffs[index].click()
            }
            const app = document.getElementById('app')
            app.click()
        }
    },
}
