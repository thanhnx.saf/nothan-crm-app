export const TOAST_SUCCESS = 'Thành công'
export const TOAST_ERROR = 'Lỗi'
export const TOAST_WARNING = 'Cảnh báo'
export const TOAST_INFO = 'Thông tin'

export const IS_DATE =  2
export const IS_DATE_TIME = 3
export const IS_TEXT = 1
export const IS_TEXT_AREA = 12
export const IS_EMAIL = 5
export const IS_PHONE = 4
export const IS_NUMBER = 6
export const IS_PRICE = 7
export const IS_SELECT = 8
export const IS_SELECT_FIND = 9
export const IS_RADIO = 14
export const IS_MULTIPLE_SELECT = 10
export const IS_CHECKBOX_MULTIPLE = 13
export const IS_TAG = 11
export const IS_USER = 15
export const IS_USER_MULTIPLE = 16
export const IS_STATUS_BAR = 17


export const MAX_INPUT = 250
export const MAX_AREA  = 1025
export const MIN_PHONE = 10
export const MAX_PHONE = 12
export const MAX_EMAIL = 75

// CUSTOMER
export const CUSTOMER_DETAIL_BOX_GENERAL = 1
export const CUSTOMER_DETAIL_BOX_BUY = 2
export const CUSTOMER_DETAIL_BOX_USE = 3
