import Vue from 'vue'
import moment from 'moment'


Vue.filter('formatDateTime', (time, dateTime = true, minutes = false) => {
  const dateTimeParse = moment(time, 'YYYY-MM-DD hh:mm:ss').valueOf()

  if (Number.isNaN(dateTimeParse)) {
    return
  }

  if (dateTimeParse) {
    const newDateTime = new Date(dateTimeParse)
    const getDate = ('0' + newDateTime.getDate()).slice(-2)
    const getMonth = ('0' + (newDateTime.getMonth() + 1)).slice(-2)
    const getFullYear = newDateTime.getFullYear()
    const getHours = ('0' + newDateTime.getHours()).slice(-2)
    const getMinutes = ('0' + newDateTime.getMinutes()).slice(-2)
    const getSeconds = ('0' + newDateTime.getSeconds()).slice(-2)

    if (dateTime) {
      return getDate + '/' + getMonth + '/' + getFullYear
    }
    if (minutes) {
      return getDate + '/' + getMonth + '/' + getFullYear + ' ' + getHours + ':' + getMinutes
    }
    return getDate + '/' + getMonth + '/' + getFullYear + ' ' + getHours + ':' + getMinutes + ':' + getSeconds
  }
  return
})

Vue.filter('formatDateTimeNoti', (date) => {
  return moment(date).locale('vi').fromNow()
})

Vue.filter('formatDateTimeRecently', (dateTime, getTime = true, recently = false) => {
  const dateTimeParse = moment(dateTime, 'YYYY-MM-DD hh:mm:ss').valueOf()
  const currentDateTime = new Date()

  if (Number.isNaN(dateTimeParse)) {
    return
  }

  if (dateTimeParse) {
    const newDateTime = new Date(dateTimeParse)
    const getDate = ('0' + newDateTime.getDate()).slice(-2)
    const getMonth = ('0' + (newDateTime.getMonth() + 1)).slice(-2)
    const getFullYear = newDateTime.getFullYear()
    const getHours = ('0' + newDateTime.getHours()).slice(-2)
    const getMinutes = ('0' + newDateTime.getMinutes()).slice(-2)
    const getSeconds = ('0' + newDateTime.getSeconds()).slice(-2)

    if (getTime) {
      if (recently) {
        if (currentDateTime.getDate() === newDateTime.getDate()) {
          return 'Hôm nay ' + getHours + ':' + getMinutes
        } else if (currentDateTime.getDate() - 1 === newDateTime.getDate()) {
          return 'Hôm qua ' + getHours + ':' + getMinutes
        } else {
          return getDate + '/' + getMonth + '/' + getFullYear + ' ' + getHours + ':' + getMinutes
        }
      } else {
        return getDate + '/' + getMonth + '/' + getFullYear + ' ' + getHours + ':' + getMinutes + ':' + getSeconds
      }
    } else {
      return getDate + '/' + getMonth + '/' + getFullYear
    }
  }
  return
})

Vue.filter('formatTime', (time) => {
  if (time && typeof time !== 'undefined') {
    const newValue = time.substring(0, time.lastIndexOf(':'))
    return newValue
  }
})

Vue.filter('formatPhone', (phone) => {
  if (!phone) {
    return
  }
  return phone.replace(/(\d{4})(\d{3})(\d{3,5})/, '$1 $2 $3')
})

Vue.filter("percentage", function (value, decimals) {
  if (!value) value = 0;
  if (!decimals) decimals = 0;

  value = value * 100;
  return Math.round(value * Math.pow(10, decimals)) / Math.pow(10, decimals) + "%";
})

Vue.filter('formatNumberPrice', (price) => {
  if (price > 0 && price < 1000) {
    return price
  } else if (price >= 1000) {
    return String(price).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }
  return '0'
})

Vue.filter('formatNumber', (input) => {
  input = input ? String(input).replace('.', ',') : ''
  // console.log(input, 'input')
  if (parseInt(input, undefined) > 999) {
    return input ? input.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') : ''
  }
  return input
})

Vue.filter('formatFileSize', (size) => {
  let sizeFormat = ''

  if (size <= 1024) {
    sizeFormat = size + ' KB'
  } else {
    sizeFormat = (size / 1024).toFixed(1) + ' MB'
  }
  return sizeFormat
})

Vue.filter('truncateText', (text, stop = 50, clamp = '...') => {
  if (typeof (text) !== 'string') {
    return
  }
  return text.slice(0, stop) + (stop < text.length ? clamp : '')
})

Vue.filter('truncateWords', (text, stop = 20, clamp = '...') => {
  if (typeof (text) !== 'string') {
    return
  }
  var index = text.indexOf(' ', stop);
  if (index == -1) return text;
  let newText = text ? text.substring(0, index) : '';
  return newText + ' ' + clamp;
})