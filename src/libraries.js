import Vue from 'vue'
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

import VeeValidate from 'vee-validate'
Vue.use(VeeValidate, {
  statusEvents: {
    pristine: 'blur',
    invalid: 'change',
    valid: 'blur',
  },
  delay: 800,
})

import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)

import DatePicker from 'vue2-datepicker'
Vue.use(DatePicker)

import {
  VueEditor
} from 'vue2-editor'
Vue.component('vue-editor', VueEditor)

import VueNumeric from 'vue-numeric'
Vue.use(VueNumeric)

import Snotify, {
  SnotifyPosition
} from "vue-snotify";
import 'vue-snotify/styles/material.css';
Vue.use(Snotify, {
  toast: {
    style: "material",
    position: SnotifyPosition.leftBottom,
    timeout: 5000,
    progressBar: false,
    closeClick: true,
    newTop: true,
    backdrop: -1,
    dockMax: 8,
    blockMax: 6,
    pauseHover: false,
    titleMaxLength: 30,
    bodyMaxLength: 120,
    oneAtTime: false,
    preventDuplicates: false
  }
});