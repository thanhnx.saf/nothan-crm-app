import Vue from "vue";
import {
  mapGetters
} from "vuex";

import {
  EM_BASE_URL
} from "@/types/config";

import {
  FILE_DOWNLOAD
} from "@/types/actions.type";

Vue.mixin({
  computed: {
    currentTeamId() {
      if (this.$route.params.team_id) {
        return this.$route.params.team_id
      }
      return ''
    }
  },
  data() {
    return {
      titleNotPermisson: "Bạn không có quyền thực hiện thao tác này"
    }
  },
  ...mapGetters(["userCheckAddOn", "userCurrentPermissions", "appInfo"]),
  methods: {
    accessible(userAddons, addon, action, userCurrentPermissions = {}, isMultipleAction = false) {
      let currentUser = userAddons && userAddons.current_user ? userAddons.current_user : "";
      if (userCurrentPermissions && userCurrentPermissions.is_department) {
        if (isMultipleAction && Array.isArray(action)) {
          let count = 0;
          action.forEach(function (item) {
            if (userCurrentPermissions[addon] && userCurrentPermissions[addon][item]) {
              count++;
            }
          });
          if (count > 0) {
            return true;
          } else {
            return false;
          }
        } else {
          if (action == "all") {
            return true;
          } else {
            if (userCurrentPermissions[addon] && userCurrentPermissions[addon][action]) {
              return true;
            } else {
              return false;
            }
          }
        }
      } else {
        if (currentUser) {
          if (action != null) {
            return true;
          } else {
            if (currentUser[addon] && currentUser[addon].is_permission) {
              return true;
            } else {
              return false;
            }
          }
        }
      }
    },

    getRouteLink(routeName, params = {}, query = {}, isReload = true) {
      let vm = this;
      let team_id = parseInt(vm.$route.params.team_id);
      params.team_id = team_id;
      if (isReload) {
        let route = vm.$router.resolve({
          name: routeName,
          params: params,
          query: query
        });
        return route.href;
      } else {
        vm.$router.push({
          name: routeName,
          params: params,
          query: query
        });
      }
    },

    getUrlEM(emServiceSlug, id = null) {
      let team_id = parseInt(this.$route.params.team_id);
      if (id) {
        return EM_BASE_URL + "/team/" + team_id + "/" + emServiceSlug + "/" + id;
      }
      return EM_BASE_URL + "/team/" + team_id + "/" + emServiceSlug;
    },

    redirectAppUrl(appName, teamId) {
      if (!this.appInfo.length) {
        return "/team/" + teamId + "/launchpad"
      }

      let apps = {};
      this.appInfo.forEach(app => {
        apps[app.app_slug] = app.url
      });

      let appUrl = apps[appName] + "/team/" + teamId;

      switch (appName) {
        case "cem":
          return appUrl + "/process";
        case "education":
          return appUrl + "/list-student";
        case "hrm":
          return appUrl + "/process";
        case "crm":
          return appUrl + "/customers";
        default:
          return "/team/" + teamId + "/launchpad";
      }
    },

    downloadFile(id) {
      let vm = this;
      vm.$store
        .dispatch(FILE_DOWNLOAD, id)
        .then(response => {
          let data = response.data.data;
          if (data) {
            var element = document.createElement("a");
            element.setAttribute("href", data.download);
            element.setAttribute("download", data.original_name ? data.original_name : data.name);
            element.style.display = "none";
            element.click();
          }
        });
    },

    reloadTooltip() {
      // eslint-disable-next-line no-undef
      $("body").tooltip({
        selector: '[data-toggle="m-tooltip"]',
        template: '<div class="m-tooltip tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
      });
    },

    checkDistanceScroll() {
      let distance = 200;
      if (window.innerWidth <= 768) {
        distance = 100;
      }
      return distance;
    },

    elementScrollTop(elId) {
      let distance = this.checkDistanceScroll();
      let element = document.getElementById(elId);
      this.elementScrollVertical(element, 'top', 0, distance, 10);
    },

    elementScrollBottom(elId) {
      let distance = this.checkDistanceScroll();
      let element = document.getElementById(elId);
      this.elementScrollVertical(element, 'bottom', 0, distance, 10);
    },

    elementScrollRight(elId) {
      let element = document.getElementById(elId);
      let distance = this.checkDistanceScroll();
      this.elementScrollHorizontal(element, 'right', 0, distance, 10);
    },

    elementScrollLeft(elId) {
      let element = document.getElementById(elId);
      let distance = this.checkDistanceScroll();
      this.elementScrollHorizontal(element, 'left', 0, distance, 10);
    },

    elementScrollVertical(element, direction, speed, distance, step) {
      let scrollAmount = 0;
      let elementTimer = setInterval(function () {
        if (direction == 'top') {
          element.scrollTop -= step;
        } else {
          element.scrollTop += step;
        }
        scrollAmount += step;
        if (scrollAmount >= distance) {
          window.clearInterval(elementTimer);
        }
      }, speed);
    },

    elementScrollHorizontal(element, direction, speed, distance, step) {
      let scrollAmount = 0;
      let elementTimer = setInterval(function () {
        if (direction == 'left') {
          element.scrollLeft -= step;
        } else {
          element.scrollLeft += step;
        }
        scrollAmount += step;
        if (scrollAmount >= distance) {
          window.clearInterval(elementTimer);
        }
      }, speed);
    },

  }
});
