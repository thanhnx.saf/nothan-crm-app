import InfiniteLoading from 'vue-infinite-loading';

import EmailApi from '@/services/customer-care'
import Loading from '@/components/loading/LoadingTable.vue'
import utils from '@/utils/utils'

export default {
  components: {
    Loading,
    InfiniteLoading
  },
  data() {
    return {
      defaultSelectedTimes: 1,
      items: [],
      selectedItems: [],
      numberOfSelected: 0,
      page: 0,
      isLoading: false,
      options: utils.continuousNumberArray(10)
    }
  },
  created() {
    this.loadEmails()
  },
  methods: {
    toggleSelect(isSelect, item) {
      item.isSelected = isSelect
      this.getSelectedItems(item)
      // this.getNumberOfSelected(item, item.selectedTimes)
    },

    getSelectedItems(item) {
      const codes = this.selectedItems.map(t => t.code)
      const index = codes.indexOf(item.code)
      if (index === -1) {
        this.selectedItems.push(item)
      } else {
        this.selectedItems.splice(index, 1)
      }
      this.numberOfSelected = this.selectedItems.length
    },

    // getNumberOfSelected(item, number = 1) {
    //   const codes = this.selectedItems.map(t => t.code)
    //   const index = codes.indexOf(item.code)
    //   if (index === -1) {
    //     this.numberOfSelected -= item.selectedTimes
    //     return
    //   }
    //   this.selectedItems[index].selectedTimes = parseInt(number)
    //   this.numberOfSelected = this.selectedItems.map(t => t.selectedTimes).reduce((a, b) => a + b, 0)
    // },

    loadEmails($state) {
      if (!this.page) {
        this.isLoading = true
      }
      this.page++
      EmailApi.emailTemplateList({
        page: this.page
      }).then(res => {
        this.isLoading = false
        const data = res.data && res.data.data
        if ($state && !data.length) {
          $state.complete()
          return
        }
        this.items = [...this.items, ...data]
        this.items.map(t => {
          t.selectedTimes = 1
          return t
        })
        if ($state) {
          $state.loaded()
        }
      })
    },

    reset() {
      this.items.forEach(item => {
        item.isSelected = false
        item.selectedTimes = 1
      })
      this.selectedItems = []
      this.numberOfSelected = 0
    },

    submit() {
      this.$emit('onUpdateEmailContents', this.selectedItems)
      setTimeout(() => {
        this.reset()
      }, 100)
    }
  },
}