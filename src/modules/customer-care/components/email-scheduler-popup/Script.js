export default {
  data() {
    return {
      selectOptions: {
        periodic: 'periodic',
        flexible: 'flexible'
      },
      selectedOption: ''
    }
  },
  methods: {
    reloadPage() {
      return window.location.reload();
    },
    onNext() {
      if (!this.selectedOption) return
      this.$router.push({
        name: 'emailSchedulerAdd'
      })
      this.reset()
    },
    reset() {
      this.selectedOption = ''
    }
  },
}
