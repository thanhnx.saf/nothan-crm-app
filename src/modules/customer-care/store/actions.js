import customerCareAPI from "@/services/customer-care";
import {
  CustomerCareVM
} from "../transformers/index";
import {
  EMAIL_TEMPLATE_LIST,
  EMAIL_TEMPLATE_ADD,
  EMAIL_TEMPLATE_DETAILS,
  EMAIL_TEMPLATE_UPDATE,
  EMAIL_SCHEDULER_LIST,
  EMAIL_SCHEDULER_ADD,
  EMAIL_SCHEDULER_DETAIL
} from "@/types/actions.type";

import {
  SET_EMAIL_TEMPLATE_LIST,
  SET_EMAIL_TEMPLATE_DETAILS,
  SET_EMAIL_SCHEDULER_LIST,
  SET_EMAIL_SCHEDULER_DETAILS
} from "@/types/mutations.type";

export const actions = {

  async [EMAIL_TEMPLATE_LIST]({
    commit
  }, params) {
    try {
      let response = await customerCareAPI.emailTemplateList(params);
      if (response.data.data) {
        commit(SET_EMAIL_TEMPLATE_LIST, response.data.data);
      }
      return response;
    } catch (e) {

      return e;
    }
  },

  async [EMAIL_TEMPLATE_ADD](context, params) {
    try {
      let response = await customerCareAPI.emailTemplateAdd(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EMAIL_SCHEDULER_LIST]({
    commit
  }, params) {
    try {
      let response = await customerCareAPI.emailSchedulerList(params);
      if (response.data.data) {
        commit(SET_EMAIL_SCHEDULER_LIST, CustomerCareVM.listEmailSchedulerVM.transform(response.data.data));
      }
      return response;
    } catch (e) {

      return e;
    }
  },

  async [EMAIL_SCHEDULER_ADD](context, params) {
    try {
      let response = await customerCareAPI.emailSchedulerAdd(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EMAIL_TEMPLATE_UPDATE](context, params) {
    try {
      let response = await customerCareAPI.emailTemplateUpdate(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EMAIL_SCHEDULER_DETAIL]({
    commit
  }, params) {
    try {
      let response = await customerCareAPI.emailSchedulerDetails(params);
      if (response.data.data) {
        commit(SET_EMAIL_SCHEDULER_DETAILS, CustomerCareVM.showVM.transform(response.data.data));
      }
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EMAIL_TEMPLATE_DETAILS]({
    commit
  }, id) {
    try {
      let response = await customerCareAPI.emailTemplateDetails(id);
      if (response.data.data) {
        commit(SET_EMAIL_TEMPLATE_DETAILS, CustomerCareVM.emailTemplateDetailsVM.transform(response.data.data));
      }
      return response;
    } catch (e) {
      return e;
    }
  },
}