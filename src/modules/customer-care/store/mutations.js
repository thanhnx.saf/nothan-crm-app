import {
  SET_EMAIL_TEMPLATE_LIST,
  SET_EMAIL_TEMPLATE_DETAILS,

  SET_EMAIL_SCHEDULER_LIST,
  SET_EMAIL_SCHEDULER_DETAILS
} from "@/types/mutations.type";

export const mutations = {
  [SET_EMAIL_TEMPLATE_LIST](state, emailTemplateList) {
    state.emailTemplateList = emailTemplateList;
  },
  [SET_EMAIL_TEMPLATE_DETAILS](state, emailTemplateDetails) {
    state.emailTemplateDetails = emailTemplateDetails;
  },
  [SET_EMAIL_SCHEDULER_LIST](state, emailSchedulerList) {
    state.emailSchedulerList = emailSchedulerList;
  },
  [SET_EMAIL_SCHEDULER_DETAILS](state, emailSchedulerDetails) {
    state.emailSchedulerDetails = emailSchedulerDetails;
  },
}