export default {
  transform(data) {
    let transform = {};
    transform = {
      id: data.id,
      name: data.name,
      name_cached: data.name,
      title: data.title,
      title_cached: data.title,
      content: data.content,
      content_cached: data.content,
      code: data.code,
      created_at: data.created_at,
      created_by: data.created_by,
      updated_at: data.updated_at,
      updated_by: data.updated_by,
    };
    return transform;
  }
}