/* eslint-disable no-undef */

const DAY_TYPE = 1;
const WEEK_TYPE = 2;
const MONTH_TYPE = 3;
const YEAR_TYPE = 4;

export default {
  transform(data) {
    let transform = {};
    let repeatTimeValue = data.repeat_time_value;
    let repeatTimeType = data.repeat_time_type;

    let startOn = '';
    if (data.start_on) {
      startOn = "1970-01-01 " + data.start_on;
      startOn = moment(startOn, 'YYYY-MM-DD hh:mm:ss').format();
    }

    let startDate = '';

    if (data.start_date) {
      startDate = data.start_date;
      startDate = moment(startDate, 'YYYY-MM-DD hh:mm:ss').format();
    }

    let repeat_time = {
      type: 0,
      value: '',
      name: 'Khác'
    }

    if (repeatTimeType === DAY_TYPE && repeatTimeValue === 1) {
      repeat_time = {
        type: DAY_TYPE,
        value: 1,
        name: '1 ngày'
      }
    } else if (repeatTimeType === WEEK_TYPE && repeatTimeValue === 1) {
      repeat_time = {
        type: WEEK_TYPE,
        value: 1,
        name: '1 tuần'
      }
    } else if (repeatTimeType === MONTH_TYPE && repeatTimeValue === 1) {
      repeat_time = {
        type: MONTH_TYPE,
        value: 1,
        name: '1 tháng'
      }
    } else if (repeatTimeType === YEAR_TYPE && repeatTimeValue === 1) {
      repeat_time = {
        type: YEAR_TYPE,
        value: 1,
        name: '1 năm'
      }
    }

    let periodicalDateTimes = JSON.parse(data.periodical_date_times);
    let endDate = periodicalDateTimes && periodicalDateTimes.length > 0 ? periodicalDateTimes[periodicalDateTimes.length - 1] : '';

    transform = {
      id: data.id,
      name: data.name,
      is_active: data.is_active,
      filter_id: data.filter_id,
      email_template_ids: JSON.parse(data.email_template_ids),
      periodical_date_times: periodicalDateTimes,
      start_date: startDate,
      start_on: startOn,
      repeat_time: repeat_time,
      repeat_time_type: repeatTimeType,
      repeat_time_value: repeatTimeValue,
      end_date: endDate,
      created_by: data.created_by,
      updated_by: data.updated_by,
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
      templates_schedules: data.templates_schedules,
      send_status: data.send_status
    };

    return transform;
  },
}