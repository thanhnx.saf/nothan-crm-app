/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import {
  EMAIL_SCHEDULER_DETAIL
} from "@/types/actions.type";
import EmailSchedulerShowFilter from "../../../components/email-scheduler-show-filter/EmailSchedulerShowFilter.vue"
import EmailSchedulerEmailList from '../../../components/email-scheduler-email-list/EmailSchedulerEmailList.vue'

const DAY_TYPE = 1;
const WEEK_TYPE = 2;
const MONTH_TYPE = 3;
const YEAR_TYPE = 4;

export default {
  components: {
    EmailSchedulerShowFilter,
    EmailSchedulerEmailList
  },
  data() {
    return {
      isLoader: false,
      emailSchedulerId: parseInt(this.$route.params.email_scheduler_id),
      repeatTimes: [{
        type: DAY_TYPE,
        name: 'Ngày'
      }, {
        type: WEEK_TYPE,
        name: 'Tuần'
      }, {
        type: MONTH_TYPE,
        name: 'Tháng'
      }, {
        type: YEAR_TYPE,
        name: 'Năm'
      }],
      repeatTimeDefault: [{
        type: DAY_TYPE,
        value: 1,
        name: '1 ngày'
      }, {
        type: WEEK_TYPE,
        value: 1,
        name: '1 tuần'
      }, {
        type: MONTH_TYPE,
        value: 1,
        name: '1 tháng'
      }, {
        type: YEAR_TYPE,
        value: 1,
        name: '1 năm'
      }, {
        type: 0,
        value: '',
        name: 'Khác'
      }]
    }
  },
  mounted() {
    this.loadAsync()
    const {
      team_id,
      email_scheduler_id
    } = this.$route.params
    this.$bus.$on('onEditEmailScheduler', () => {
      this.$router.push({
        name: 'emailSchedulerEdit',
        params: {
          team_id,
          email_scheduler_id
        }
      })
    })
  },
  computed: {
    ...mapGetters(['currentTeam', 'emailSchedulerDetails'])
  },
  methods: {
    async getEmailSchedulerDetails() {
      this.isLoader = true;
      await this.$store.dispatch(EMAIL_SCHEDULER_DETAIL, this.emailSchedulerId).then(() => {
        this.isLoader = false
      })
    },

    async loadAsync() {
      await this.getEmailSchedulerDetails()
      let {
        email_template_ids,
        templates_schedules,
        send_status
      } = this.emailSchedulerDetails
      this.$refs.emailSchedulerEmailList.getEmails(email_template_ids, templates_schedules, send_status)
    }
  }
}