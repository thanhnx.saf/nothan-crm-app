/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import {
  EMAIL_TEMPLATE_ADD,
  CUSTOMER_GET_ATTRIBUTES
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR,
  ID_BASE_URL
} from "@/types/config";
import EmailTemplatePreview from '../../../components/email-template-preview/EmailTemplatePreview.vue'

const customMessagesAddNewEmailTemplate = {
  custom: {
    'name': {
      max: "Giới hạn 250 ký tự."
    },
    'title': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'content': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 60000 ký tự."
    }
  }
};

export default {
  components: {
    EmailTemplatePreview
  },
  computed: {
    ...mapGetters(['currentTeam', 'prefixAttributeCodeCustomer']),
    isDisabled() {
      if (!this.emailTemplate.title || !this.emailTemplate.content || this.errors.any() || this.isLoader || this.isEmptyContent) {
        return true;
      }
      return false;
    },
    countContent() {
      let countContent = this.emailTemplate.content ? this.emailTemplate.content.length : 0;
      return countContent;
    },
    classTextCountContent() {
      let countContent = this.emailTemplate.content ? this.emailTemplate.content.length : 0;
      if (countContent > 0 && countContent < 48000) {
        return 'nt-success';
      }
      if (countContent >= 48000 && countContent < 59000) {
        return 'nt-warning';
      }
      return 'nt-danger';
    }
  },
  watch: {
    emailTemplate: {
      handler(after) {
        if (after && after.content) {
          let content = after.content.replace(/(<\/?(?:img)[^>]*>)|<[^>]+>/ig, '$1');
          content = content ? content.replace(/ /g, '') : '';
          if (content) {
            this.isEmptyContent = false;
          } else {
            this.isEmptyContent = true;
          }
        }
      },
      deep: true
    }
  },
  mounted() {
    this.$validator.localize('en', customMessagesAddNewEmailTemplate);
    document.getElementById("m_header").classList.add("nt-modal-full-page-change-header");
    this.getAttributes();
    this.addEventListener()
  },
  data() {
    return {
      idpOrganizationInfo: isNaN(parseInt(this.$route.params.team_id)) ? ID_BASE_URL + '/choose-organization' : ID_BASE_URL + '/team/' + parseInt(this.$route.params.team_id) + "/info",
      isLoader: false,
      isLoaderAttribute: false,
      server_errors: '',
      emailTemplateId: '',
      isEmptyContent: false,
      attributeCodeCustomer: '',
      emailTemplate: {
        name: '',
        title: '',
        content: ''
      },
      editorOptions: {
        modules: {
          toolbar: {
            container: [
              [{
                'header': [1, 2, 3, 4, 5, 6, false]
              }],
              ['bold', 'italic', 'underline', 'strike'],
              [{
                'color': []
              }, {
                'background': []
              }],
              [{
                'list': 'ordered'
              }, {
                'list': 'bullet'
              }, {
                align: ''
              }, {
                align: 'center'
              }, {
                align: 'right'
              }, {
                align: 'justify'
              }],
              [{
                'indent': '-1'
              }, {
                'indent': '+1'
              }],
              ['image', 'link'],
            ],
            handlers: {
              image: () => {},
              indent: (indent) => {
                let tag = window.getSelection().anchorNode.parentElement
                if (tag.tagName.toLowerCase() === 'li') {
                  tag = tag.parentElement
                  tag.style.paddingLeft = tag.style.paddingLeft || '3rem'
                }
                let paddingLeft = tag.style.paddingLeft
                paddingLeft = parseFloat(paddingLeft.split('rem')[0])
                if (indent === '+1') {
                  tag.style.paddingLeft = paddingLeft ? (paddingLeft + 1.5) >= 10 ? 9 : (paddingLeft + 1.5) + 'rem' : '1.5rem'
                } else {
                  if ((paddingLeft - 1.5) <= 2 && ['ol', 'ul'].indexOf(tag.tagName.toLowerCase()) !== -1) {
                    tag.style.paddingLeft = '3rem'
                  } else {
                    tag.style.paddingLeft = paddingLeft ? (paddingLeft - 1.5) + 'rem' : '0rem'
                  }
                }
                this.quillTriggerChange()
              },
              align: (align) => {
                let tag = window.getSelection().anchorNode.parentElement
                tag.style.textAlign = align || 'left'
                tag.style.display = "block"
                this.quillTriggerChange()
              },
            }
          }
        }
      },
      contentMax: 60000
    }
  },
  methods: {
    quillTriggerChange() {
      const quill = this.$refs.vueEditorAddNewEmailTemplateContent.quill
      const format = quill.getFormat(0, 1)
      quill.formatText(0, 1, format);
    },

    addEventListener() {
      const backdrop = document.getElementById('backdrop')
      const insertBox = document.getElementById('quillInsertBox')
      const quillInput = document.getElementById('quillInput')
      const qlImage = document.getElementsByClassName('ql-image')

      for (let i = 0; i < qlImage.length; i++) {
        qlImage[i].addEventListener('click', (ev) => {
          backdrop.classList.remove('d-none')
          backdrop.classList.add('d-block')
          insertBox.classList.remove('d-none')
          insertBox.classList.add('d-flex')
          insertBox.style.top = `${ev.layerY}px`
          insertBox.style.left = `${ev.layerX}px`
          insertBox.style.transform = 'translate(-50%, 20px)'
          quillInput.focus()
        })
      }
    },

    onDismiss() {
      const backdrop = document.getElementById('backdrop')
      const insertBox = document.getElementById('quillInsertBox')

      backdrop.classList.remove('d-block')
      backdrop.classList.add('d-none')
      insertBox.classList.remove('d-flex')
      insertBox.classList.add('d-none')
    },

    onAddImage() {
      const quillInput = document.getElementById('quillInput')
      const quill = this.$refs.vueEditorAddNewEmailTemplateContent.quill
      const index = quill.getSelection(true).index
      quill.pasteHTML(index, `<img src="${quillInput.value}">`)
      quillInput.value = ''
      this.onDismiss()
    },

    updateEmailFrame() {
      this.$refs.emailTemplatePreview.getEmailFrame()
    },

    insertPrefixAttributeCodeForEmailContent() {
      let vm = this;
      if (vm.attributeCodeCustomer) {
        vm.$refs.vueEditorAddNewEmailTemplateContent.quill.insertText(
          vm.$refs.vueEditorAddNewEmailTemplateContent.quill.getSelection(true).index,
          `{{ ${vm.attributeCodeCustomer} }}`
        );
      }

      vm.attributeCodeCustomer = "";
    },

    getAttributes() {
      let vm = this;
      vm.isLoaderAttribute = true;
      vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then(() => {
        vm.isLoaderAttribute = false;
      });
    },

    addNewEmailTemplate(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.emailTemplate.name = vm.emailTemplate.name ? vm.emailTemplate.name : vm.emailTemplate.title;
          vm.emailTemplate.team_id = vm.currentTeamId;
          vm.isLoader = true;
          vm.$store.dispatch(EMAIL_TEMPLATE_ADD, vm.emailTemplate).then((response) => {
            vm.isLoader = false;

            if (response.data.data && response.data.data.status) {
              vm.emailTemplateId = response.data.data.id;
              vm.$snotify.success('Thêm mới mẫu email thành công.', TOAST_SUCCESS);
              vm.$router.push({
                name: 'emailTemplateDetails',
                params: {
                  team_id: vm.$route.params.team_id,
                  email_template_id: vm.emailTemplateId
                }
              });
              return;
            }

            if (response.data.data && !response.data.data.status) {
              vm.$snotify.error('Thêm mới mẫu email thất bại.', TOAST_ERROR);
              return;
            }

            if (response.data.status_code && response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              vm.$snotify.error('Thêm mới mẫu email thất bại.', TOAST_ERROR);
              return;
            }
          }).catch(() => {});
        }
      });
    }
  }
}