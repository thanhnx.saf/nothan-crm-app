/* eslint-disable no-undef */
import UserObjectLoading from "@/components/loading/UserObject"
import Pagination from "@/components/pagination/Pagination.vue"
import {
  mapGetters
} from "vuex";
import {
  EMAIL_TEMPLATE_LIST
} from "@/types/actions.type";

export default {
  components: {
    UserObjectLoading,
    Pagination
  },
  created() {
    this.reloadTooltip();
    this.$bus.$on('addNewEmailTemplate', () => {
      this.$router.push({
        name: 'emailTemplateAdd',
        params: {
          team_id: this.$route.params.team_id
        }
      });
    });
  },
  mounted() {
    this.getEmailTemplateList();
  },
  computed: {
    ...mapGetters(['currentTeam', 'emailTemplateList'])
  },
  data() {
    return {
      isLoader: false,
      isLoaderList: false,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {},
    }
  },
  methods: {
    getEmailTemplateList() {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      if (vm.currentParams.per_page) {
        delete vm.currentParams.per_page;
      }
      vm.isLoaderList = true;
      vm.$store.dispatch(EMAIL_TEMPLATE_LIST, vm.currentParams).then(response => {
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.isLoaderList = false;
      }).catch(() => {});
    },
    getPagination() {
      this.getEmailTemplateList();
    },
    addNewSuccess() {
      this.getEmailTemplateList();
    }
  }
}