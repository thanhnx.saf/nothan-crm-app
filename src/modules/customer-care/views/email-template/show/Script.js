/* eslint-disable no-undef */
import {
  EMAIL_TEMPLATE_DETAILS
} from "@/types/actions.type";
import {
  mapGetters
} from "vuex";
import EmailTemplatePreview from '../../../components/email-template-preview/EmailTemplatePreview.vue'
import UserObjectLoading from "@/components/loading/UserObject"
export default {
  components: {
    EmailTemplatePreview,
    UserObjectLoading
  },
  data() {
    return {
      isLoader: false,
      emailTemplateId: isNaN(parseInt(this.$route.params.email_template_id)) ? 1 : parseInt(this.$route.params.email_template_id)
    }
  },
  created() {
    this.$bus.$on('editEmailTemplate', () => {
      this.$router.push({
        name: 'emailTemplateEdit',
        params: {
          team_id: this.$route.params.team_id,
          email_template_id: this.emailTemplateId
        }
      });
    });
  },
  computed: {
    ...mapGetters(['currentTeam', 'emailTemplateDetails'])
  },
  mounted() {
    this.getEmailTemplateDetails();
  },
  methods: {
    getEmailTemplateDetails() {
      let vm = this;
      vm.isLoader = true;
      vm.$store.dispatch(EMAIL_TEMPLATE_DETAILS, vm.emailTemplateId).then(() => {
        vm.isLoader = false;
      }).catch(() => {});
    },
  }
}