/* eslint-disable no-undef */
import draggable from 'vuedraggable'

import helpers from "@/utils/utils";
import {
  mapGetters
} from "vuex";
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE
} from "@/types/const";

import {
  SET_CUSTOMERS_DETAILS_BASIC_INFO,
  SET_CUSTOMERS_DETAILS_BUYER_INFO,
  SET_CUSTOMERS_DETAILS_USER_INFO
} from '@/types/mutations.type'

import {
  PERMISSION_UPDATE_CUSTOMER_GENERAL,
  PERMISSION_UPDATE_CUSTOMER_BUYER,
  PERMISSION_UPDATE_CUSTOMER_END_USER
} from "@/types/const";

import UserObjectLoading from "@/components/loading/UserObject";
export default {
  inject: ['$validator'],
  components: {
    "user-object-loading": UserObjectLoading,
    draggable
  },
  props: {
    is_reorder: {
      type: Boolean,
      default: false
    },
    attributes: {
      type: Array,
      default: []
    },
    server_errors: {
      type: [Array, Object, String],
      default: []
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["detailCustomer", "listUsers", "userCheckAddOn", "userCurrentPermissions"]),
  },
  created() {
    let vm = this

    vm.$bus.$on('checkValidateDetailsCustomer', ($event) => {
      if ($event) {
        vm.detailCustomer.attributes.forEach((attribute) => {
          vm.editAttributes[attribute.attribute_code] = true
        })

        setTimeout(() => {
          vm.$validator.validateAll(vm.scope)
          vm.$forceUpdate()
        }, 100)
      }
    })

    vm.$bus.$on('resetUpdateDetailsCustomer', ($event) => {
      if ($event) {
        vm.editAttributes = {}
        vm.$store.commit(SET_CUSTOMERS_DETAILS_BASIC_INFO)
        vm.$store.commit(SET_CUSTOMERS_DETAILS_BUYER_INFO)
        vm.$store.commit(SET_CUSTOMERS_DETAILS_USER_INFO)
        vm.$forceUpdate()
      }
    })
  },
  watch: {
    server_errors: {
      handler(after) {
        this.editAttributes = {}
        for (var error in after) {
          this.editAttributes[error] = true
        }
        this.$forceUpdate()
      },
      deep: true
    },
    errors: {
      handler(after) {
        if (after.items.length) {
          this.editAttributes = {}
          after.items.forEach((item) => {
            this.editAttributes[item.field] = true
          })
          this.$forceUpdate()
        }
      },
      deep: true
    }
  },

  data() {
    return {
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
      editAttributes: {},
      scope: 'editCustomer',

      PERMISSION_UPDATE_CUSTOMER_GENERAL: PERMISSION_UPDATE_CUSTOMER_GENERAL,
      PERMISSION_UPDATE_CUSTOMER_BUYER: PERMISSION_UPDATE_CUSTOMER_BUYER,
      PERMISSION_UPDATE_CUSTOMER_END_USER: PERMISSION_UPDATE_CUSTOMER_END_USER
    }
  },
  methods: {
    onChange() {
      this.$emit('on_change', true)
    },

    showUpdate(attribute) {
      let vm = this
      vm.editAttributes[attribute.attribute_code] = true
      vm.$forceUpdate()
      setTimeout(() => {
        $('#' + attribute.attribute_code).focus()
      }, 100)
    },

    checkValidateAttribute(attribute_code) {
      this.$validator.validateAll(this.scope).then(() => {
        if (this.errors.items.length) {
          this.errors.items.forEach((item) => {
            if (item.field === attribute_code) {
              return true
            }
          })
        }
      })
      if (this.server_errors[attribute_code]) {
        return true
      }
      return false
    },

    hideUpdate(attribute) {
      let vm = this
      let isError = vm.checkValidateAttribute(attribute.attribute_code)
      if (!isError) {
        vm.editAttributes[attribute.attribute_code] = false
        vm.$forceUpdate()
      }
    },

    changeDataInputType(attribute, event) {
      let vm = this;
      let keyCode = event && event.keyCode ? event.keyCode : ''

      if (keyCode === 13) {
        let isError = vm.checkValidateAttribute(attribute.attribute_code)

        if (!isError) {
          vm.editAttributes[attribute.attribute_code] = false
          vm.$forceUpdate()
          vm.$bus.$emit('updateDetailsCustomer', true)
        }
      }

      // Set is change attribute
      attribute.attribute_value = attribute.attribute_value ? attribute.attribute_value : '';
      attribute.is_change_attribute_value = false

      if (attribute.data_type_id == vm.isMultipleSelect ||
        attribute.data_type_id == vm.isTag ||
        attribute.data_type_id == vm.isCheckboxMultiple ||
        attribute.data_type_id == vm.isUserMultiple) {
        if (attribute.attribute_value_cache) {
          if (attribute.attribute_value) {
            if (attribute.attribute_value.join() != attribute.attribute_value_cache.join()) {
              attribute.is_change_attribute_value = true
            }
          } else if (attribute.attribute_value.length <= 0) {
            attribute.is_change_attribute_value = true
          }

        } else {
          if (attribute.attribute_value && attribute.attribute_value.length > 0) {
            attribute.is_change_attribute_value = true
          }
        }
      } else if (attribute.data_type_id == vm.isNumber) {
        let attribute_value = attribute.attribute_value && attribute_value !== "0,00" ? parseFloat(attribute.attribute_value).toFixed(2).replace(".", ",") : '';
        if (attribute.attribute_value != attribute.attribute_value_cache) {
          attribute.is_change_attribute_value = true
        }
      } else {
        if (attribute.attribute_value != attribute.attribute_value_cache) {
          attribute.is_change_attribute_value = true
        }
      }

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute.attribute_code]) {
          vm.server_errors[attribute.attribute_code] = ''
        }
      }, 500)
    }
  }
}