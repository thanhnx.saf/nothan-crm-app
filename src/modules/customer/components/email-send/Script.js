/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import {
  TOAST_SUCCESS
} from "@/types/config";
import {
  CHECK_BEFORE_SEND_EMAIL_CUSTOMERS,
  SEND_EMAIL_CUSTOMERS
} from "@/types/actions.type";
import EmailApi from '@/services/customer-care'
import InfiniteLoading from 'vue-infinite-loading';
import Loading from '@/components/loading/LoadingTable.vue'
import EmailTemplatePreview from '@/modules/customer-care/components/email-template-preview/EmailTemplatePreview.vue'
import {
  ID_BASE_URL
} from "@/types/config";

const customMessagesSendMailCustomer = {
  custom: {
    'title': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'content': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 60000 ký tự."
    }
  }
};

export default {
  components: {
    EmailTemplatePreview,
    Loading,
    InfiniteLoading
  },

  mounted() {
    this.$validator.localize('en', customMessagesSendMailCustomer);
    this.addEventListener()
  },

  created() {
    let vm = this;
    vm.$bus.$on('emailSendCustomers', (data) => {
      if (data) {
        vm.customerIds = []
        let currentParams = data
        vm.currentParams = currentParams
        vm.isShowModal = true
        if (data && data.customers.length) {
          vm.customerIds = data.customers.map((item) => item.id)
        }
        vm.checkBeforeComposeEmail()
        vm.loadEmailTemplateList()
      }
    })
  },

  computed: {
    ...mapGetters(['currentTeam', 'prefixAttributeCodeCustomer']),
    isDisabled() {
      if (!this.emailTemplate.title || !this.emailTemplate.content || this.errors.any() || this.isLoader || this.isEmptyContent) {
        return true;
      }
      return false;
    },
    countContent() {
      let countContent = this.emailTemplate.content ? this.emailTemplate.content.length : 0;
      return countContent;
    },
    classTextCountContent() {
      let countContent = this.emailTemplate.content ? this.emailTemplate.content.length : 0;
      if (countContent > 0 && countContent < 48000) {
        return 'nt-success';
      }
      if (countContent >= 48000 && countContent < 59000) {
        return 'nt-warning';
      }
      return 'nt-danger';
    }
  },

  watch: {
    emailTemplate: {
      handler(after) {
        if (after && after.content) {
          let content = after.content.replace(/(<\/?(?:img)[^>]*>)|<[^>]+>/ig, '$1');
          content = content ? content.replace(/ /g, '') : '';
          if (content) {
            this.isEmptyContent = false;
          } else {
            this.isEmptyContent = true;
          }
        }
      },
      deep: true
    }
  },

  data() {
    return {
      idpAddonsUrl: ID_BASE_URL + '/team/' + this.$route.params.team_id + '/addons/?upgrade_package=true',
      isLoader: false,
      customerIds: [],
      currentParams: {},
      idpOrganizationInfo: isNaN(parseInt(this.$route.params.team_id)) ? ID_BASE_URL + '/choose-organization' : ID_BASE_URL + '/team/' + parseInt(this.$route.params.team_id) + "/info",
      isLoaderAttribute: false,
      isEmptyContent: false,
      attributeCodeCustomer: '',
      emailTemplate: {
        title: '',
        content: '',
        id: ''
      },
      editorOptions: {
        modules: {
          toolbar: {
            container: [
              [{
                'header': [1, 2, 3, 4, 5, 6, false]
              }],
              ['bold', 'italic', 'underline', 'strike'],
              [{
                'color': []
              }, {
                'background': []
              }],
              [{
                'list': 'ordered'
              }, {
                'list': 'bullet'
              }, {
                align: ''
              }, {
                align: 'center'
              }, {
                align: 'right'
              }, {
                align: 'justify'
              }],
              [{
                'indent': '-1'
              }, {
                'indent': '+1'
              }],
              ['image', 'link'],
            ],
            handlers: {
              image: () => {},
              indent: (indent) => {
                let tag = window.getSelection().anchorNode.parentElement
                if (tag.tagName.toLowerCase() === 'li') {
                  tag = tag.parentElement
                  tag.style.paddingLeft = tag.style.paddingLeft || '3rem'
                }
                let paddingLeft = tag.style.paddingLeft
                paddingLeft = parseFloat(paddingLeft.split('rem')[0])
                if (indent === '+1') {
                  tag.style.paddingLeft = paddingLeft ? (paddingLeft + 1.5) >= 10 ? 9 : (paddingLeft + 1.5) + 'rem' : '1.5rem'
                } else {
                  if ((paddingLeft - 1.5) <= 2 && ['ol', 'ul'].indexOf(tag.tagName.toLowerCase()) !== -1) {
                    tag.style.paddingLeft = '3rem'
                  } else {
                    tag.style.paddingLeft = paddingLeft ? (paddingLeft - 1.5) + 'rem' : '0rem'
                  }
                }
                this.quillTriggerChange()
              },
              align: (align) => {
                let tag = window.getSelection().anchorNode.parentElement
                tag.style.textAlign = align || 'left'
                tag.style.display = "block"
                this.quillTriggerChange()
              },
            }
          }
        }
      },
      contentMax: 60000,
      server_errors: '',
      emailTemplates: [],
      isLoading: false,
      page: 0,
      currentEmailTemplate: {},
      totalCustomers: 0,
      totalCustomerEmails: 0,
      isSendEmail: true,
      isToggleDropdownEmailTemplateList: false,
      isShowModal: false
    };
  },

  methods: {
    checkBeforeComposeEmail() {
      let vm = this;

      let params = {
        team_id: vm.currentTeamId,
        customer_ids: vm.customerIds,
        search: vm.currentParams.search,
        filter: vm.currentParams.filter
      }

      vm.totalCustomers = 0
      vm.totalCustomerEmails = 0
      vm.$store.dispatch(CHECK_BEFORE_SEND_EMAIL_CUSTOMERS, params).then((res) => {
        // True không gửi email đc
        if (res.data.data) {
          vm.isSendEmail = res.data.data.is_send_email
          vm.totalCustomers = res.data.data.total_customers
          vm.totalCustomerEmails = res.data.data.total_customer_emails
          $('#confirmBeforeComposeEmail').modal('show')
        }
      })
    },

    showDropdownEmailTemplateList() {
      let vm = this;
      setTimeout(() => {
        vm.isToggleDropdownEmailTemplateList = true
        $('.nt-form-custom-select-infinite-loading .m-dropdown__dropoff').remove()
        $('.nt-email-template-list-dropdown-backdrop').addClass('show')
      }, 10)
    },

    hideDropdownEmailTemplateList() {
      this.isToggleDropdownEmailTemplateList = false
      $('.nt-form-custom-select-infinite-loading .m-dropdown__dropoff').remove()
      $('.nt-email-template-list-dropdown-backdrop').removeClass('show')
    },

    selectEmailTemplate(emailTemplate) {
      if (Object.entries(emailTemplate).length) {
        this.emailTemplate = {
          title: emailTemplate.title,
          content: emailTemplate.content,
          id: emailTemplate.id
        }
      } else {
        this.resetComposeEmail()
      }
      this.currentEmailTemplate = emailTemplate
      this.hideDropdownEmailTemplateList()
    },

    composeEmail() {
      $('#composeEmail').modal('show')
      $('body').addClass('modal-open-fix')
    },

    closeModal() {
      $('body').removeClass('modal-open-fix')
    },

    resetComposeEmail() {
      this.emailTemplate = {
        title: '',
        content: '',
        id: ''
      }
      this.$validator.reset()
      this.isEmptyContent = false
      this.isShowModal = false
    },

    cancelComposeEmail() {
      this.resetComposeEmail()
      this.currentEmailTemplate = {}
      this.closeModal()
    },

    sendEmail(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          let params = {
            team_id: vm.currentTeamId,
            customer_ids: vm.customerIds,
            email_template: vm.emailTemplate,
            search: vm.currentParams.search,
            filter: vm.currentParams.filter
          }

          vm.totalCustomers = 0
          vm.totalCustomerEmails = 0

          vm.$store.dispatch(SEND_EMAIL_CUSTOMERS, params).then((res) => {
            vm.isLoader = false;
            $('#composeEmail').modal('hide')

            vm.totalCustomers = res.data.data.total_customers
            vm.totalCustomerEmails = res.data.data.total_customer_emails

            // True không gửi email đc
            if (res.data.data.is_send_email) {
              $('#sendEmailOutOfQuantity').modal('show')
              vm.cancelComposeEmail()
              return
            }

            $('#sendEmailSuccess').modal('show')
            vm.cancelComposeEmail()
            vm.$emit('send_email_success', true)
            vm.$snotify.success('Gửi email thành công cho ' + vm.totalCustomerEmails + ' khách hàng.', TOAST_SUCCESS)
            return
          })
        }
      });
    },

    loadEmailTemplateList($state) {
      if (!this.page) {
        this.isLoading = true
      }
      this.page++
      EmailApi.emailTemplateList({
        page: this.page
      }).then(res => {
        this.isLoading = false
        const data = res.data && res.data.data
        if (!data.length) {
          if ($state) {
            $state.complete()
          }
          return
        }
        this.emailTemplates = [...this.emailTemplates, ...data]
        if ($state) {
          $state.loaded()
        }
      })
    },

    updateEmailFrame() {
      this.$refs.emailTemplatePreview.getEmailFrame()
    },

    quillTriggerChange() {
      const quill = this.$refs.vueEditorAddNewEmailTemplateContent.quill
      const format = quill.getFormat(0, 1)
      quill.formatText(0, 1, format);
    },

    addEventListener() {
      const backdrop = document.getElementById('backdrop')
      const insertBox = document.getElementById('quillInsertBox')
      const quillInput = document.getElementById('quillInput')
      const qlImage = document.getElementsByClassName('ql-image')

      for (let i = 0; i < qlImage.length; i++) {
        qlImage[i].addEventListener('click', (ev) => {
          backdrop.classList.remove('d-none')
          backdrop.classList.add('d-block')
          insertBox.classList.remove('d-none')
          insertBox.classList.add('d-flex')
          insertBox.style.top = `${ev.layerY}px`
          insertBox.style.left = `${ev.layerX}px`
          insertBox.style.transform = 'translate(-50%, 20px)'
          quillInput.focus()
        })
      }
    },

    onDismiss() {
      const backdrop = document.getElementById('backdrop')
      const insertBox = document.getElementById('quillInsertBox')

      backdrop.classList.remove('d-block')
      backdrop.classList.add('d-none')
      insertBox.classList.remove('d-flex')
      insertBox.classList.add('d-none')
    },

    onAddImage() {
      const quillInput = document.getElementById('quillInput')
      const quill = this.$refs.vueEditorAddNewEmailTemplateContent.quill
      const index = quill.getSelection(true).index
      quill.pasteHTML(index, `<img src="${quillInput.value}">`)
      quillInput.value = ''
      this.onDismiss()
    },

    insertPrefixAttributeCodeForEmailContent() {
      let vm = this;
      if (vm.attributeCodeCustomer) {
        vm.$refs.vueEditorAddNewEmailTemplateContent.quill.insertText(
          vm.$refs.vueEditorAddNewEmailTemplateContent.quill.getSelection(true).index,
          `{{ ${vm.attributeCodeCustomer} }}`
        );
      }

      vm.attributeCodeCustomer = "";
    },
  }
}