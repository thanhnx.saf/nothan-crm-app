import {
  mapGetters
} from "vuex";
import {
  CUSTOMER_GET_CONTACT,
} from "@/types/actions.type";
import AddNewCustomerContact from '../modals/add-new-customer-contact/AddNewCustomerContact';
import DeleteCustomerContact from '../modals/delete-customer-contact/DeleteCustomerContact';
import ContactView from '../modals/contact-view/ContactView';
import {
  PERMISSION_ADD_CONTACT,
  PERMISSION_UPDATE_CONTACT,
  PERMISSION_DELETE_CONTACT
} from "@/types/const";

export default {
  name: "ShowInfoContacts",
  components: {
    AddNewCustomerContact: AddNewCustomerContact,
    DeleteCustomerContact: DeleteCustomerContact,
    ContactView: ContactView,
  },
  data() {
    return {
      PERMISSION_ADD_CONTACT: PERMISSION_ADD_CONTACT,
      PERMISSION_UPDATE_CONTACT: PERMISSION_UPDATE_CONTACT,
      PERMISSION_DELETE_CONTACT: PERMISSION_DELETE_CONTACT,

      team_id: parseInt(this.$route.params.team_id),
      customer_id: parseInt(this.$route.params.customer_id),
      params: {
        customer_id: parseInt(this.$route.params.customer_id),
      },
      contact: [],
      isPropDataToContactView: false,
    }
  },
  created() {
    let vm = this;
    vm.getContactByCustomer(vm.params);
  },
  mounted() {},
  computed: {
    ...mapGetters(["customerContacts", "userCheckAddOn", "userCurrentPermissions", "detailCustomer"]),
  },

  methods: {
    getContactByCustomer(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_CONTACT, params).then(() => {}).catch((error) => {
        console.log(error)
      });
    },
    getDetailContact(contact) {
      let vm = this;
      vm.contact = contact;
    },
    getDetailContactAndAssign(contact) {
      let vm = this;
      vm.contact = contact;
      vm.$bus.$emit('assignObject', vm.contact)
    },
    deleteContactSuccess(boolean) {
      let vm = this;
      if (boolean) {
        vm.getContactByCustomer(vm.params);
        vm.$bus.$emit('resetContactViewAfterDeleteSuccess', true);

      }
    },
    getCustomerLinkDetail(contact) {
      let vm = this;
      let customer_id = contact.customer_id;
      let team_id = vm.team_id;
      if (customer_id) {
        window.open('/team/' + team_id + '/customers/' + customer_id, '_blank');
      }
    },
    addNewContactCustomerIsSuccess(boolean) {
      let vm = this;
      if (boolean) {
        vm.getContactByCustomer(vm.params);
      }

    },
    updateCustomerContactIsSuccess(boolean) {
      let vm = this;
      if (boolean) {
        vm.getContactByCustomer(vm.params);
      }
    }
  }

}