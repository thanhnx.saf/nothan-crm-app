import AttributeWithDataTypeDetailsUpdate from "../attribute-details-update/AttributeWithDataTypeDetailsUpdate.vue";

import {
   SET_CUSTOMERS_DETAILS_BUYER_INFO,
   SET_CUSTOMERS_DETAILS_BASIC_INFO,
   SET_CUSTOMERS_DETAILS_USER_INFO
} from '@/types/mutations.type'

export default {
  inject: ['$validator'],
  props: {
    server_errors: {
      type: [Array, Object, String],
      default: {}
    }
  },
  name: "InfoCustomerBuy",
  components: {
    AttributeWithDataTypeDetailsUpdate
  },
  created() {
    this.listenEventBus()
    this.$store.commit(SET_CUSTOMERS_DETAILS_BUYER_INFO)
    this.$store.commit(SET_CUSTOMERS_DETAILS_BASIC_INFO)
    this.$store.commit(SET_CUSTOMERS_DETAILS_USER_INFO)
  },
  mounted() {
    this.getFreezeeData()
  },
  computed: {
    isReorder() {
      if (this.$route.query.reorder === 'true' || this.$route.query.reorder === true) {
        return true
      }
      return false
    },
    buyerCustomerInfo: {
      get: function () {
        return this.$store.state.customer.buyerCustomerInfo
      },
      set: function (value) {
        this.$store.commit(SET_CUSTOMERS_DETAILS_BUYER_INFO, value)
      }
    },
    basicCustomerInfo: {
      get: function () {
        return this.$store.state.customer.basicCustomerInfo
      },
      set: function (value) {
        this.$store.commit(SET_CUSTOMERS_DETAILS_BASIC_INFO, value)
      }
    },
    usedCustomerInfo: {
      get: function () {
        return this.$store.state.customer.usedCustomerInfo
      },
      set: function (value) {
        this.$store.commit(SET_CUSTOMERS_DETAILS_USER_INFO, value)
      }
    },
  },
  data() {
    return {
      freezeeData: []
    }
  },
  methods: {
    getFreezeeData() {
      const infos = this.$store.state.customer.usedCustomerInfo
      infos.forEach(info => {
        this.freezeeData.push(Object.assign({}, info))
      })
    },

    listenEventBus() {
      this.$bus.$on('customerDetailsCancelReorder', () => {
        const datas = []
        this.freezeeData.forEach(data => {
          datas.push(Object.assign({}, data))
        })
        this.usedCustomerInfo = datas
      })
    },

    onChange() {
      this.$forceUpdate()
    }
  }
}