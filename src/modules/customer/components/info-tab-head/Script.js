/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";
import {
  mapState
} from "vuex";

import helpers from "@/utils/utils";
import {
  TOAST_ERROR
} from "@/types/config";
import {
  PERMISSION_UPDATE_CUSTOMER_BASE,
  IS_PRIVATE_FILE
} from "@/types/const";
import {
  FILE_CREATE_AVATAR,
  CUSTOMER_UPDATE,
  CUSTOMER_DETAIL_GET_ALL_DEAL,
  CUSTOMER_GET_CONTACT,
  GET_ACCOUNT_EDU_ONLINE_STATUS
} from "@/types/actions.type";
import CreateAccountCoponent from "@/modules/education/components/modals/account-create/CreateAccount"
import CreateAccountErrorComponent from "@/modules/education/components/modals/account-create-error/CreateAccountError"
import AuthenticateImage from "@/components/authenticateImage/AuthenticateImage.vue";
const messageUploadFile = {
  custom: {
    add_file: {
      size: "Dung lượng file không được vượt quá 10 MB",
      mimes: "File không đúng định dạng"
    },
  }
};
const UPDATE_CUSTOMER_BASE_TYPE = 1;
// const UPDATE_CUSTOMER_GENERAL_TYPE = 2;
// const UPDATE_CUSTOMER_BUYER_TYPE = 3;
// const UPDATE_CUSTOMER_END_USER_TYPE = 4;

export default {
  inject: ['$validator'],
  props: {
    loading_upload: {
      type: Boolean,
      default: false
    },
    server_errors: {
      type: [Array, Object, String],
      default: []
    }
  },
  components: {
    CreateAccountCoponent,
    CreateAccountErrorComponent,
    AuthenticateImage
  },
  created() {
    let vm = this;
    let params = {
      customer_id: parseInt(this.$route.params.customer_id),
    };
    vm.getContactByCustomer(params);
    if (vm.detailCustomer) {
      let data = vm.detailCustomer;
      let customerType = helpers.getAttributeOption(helpers.getAttributeByAttrCode(data, 'customer_type'), helpers.getAttributeByAttrCodeAndVariableName(data, 'customer_type', 'attribute_value'), 'option_value');

      const customerTypeNot = [
        "Khách hàng chưa mua và chưa sử dụng",
        "Khách hàng đã mua",
        "Khách hàng chỉ mua"

      ];

      if (customerTypeNot.includes(customerType)) {
        vm.isShowActionCreateAccountEdu = false;
      }
    }

    vm.$bus.$on('resetUpdateDetailsCustomer', ($event) => {
      if ($event) {
        vm.editAttributes = {}
        vm.$forceUpdate()
      }
    })
  },
  mounted() {
    let vm = this;
    vm.$validator.localize("en", messageUploadFile);
    vm.getCustomerDetailListDeal(vm.customerId);

    vm.showIconNextPrev();
    // vm.getAccoutEduOnlineStatus();

  },
  watch: {
    loading_upload: {
      handler(after) {
        let vm = this;
        if (after) {
          if (vm.loadAvatar) {
            vm.$emit("after_updated", "avatar");
            vm.loadAvatar = false;
          } else if (vm.updateLoading) {
            vm.$emit("after_updated", "level");
            vm.updateLoading = false;
          }
        }
      },
      deep: true
    },
    errors: {
      handler(after) {
        if (after.has('add_file_scope.add_file')) {
          $("#warning_upload_avatar").modal("show");
        }
        if (after.items.length) {
          after.items.forEach((item) => {
            this.editAttributes[item.field] = true
          })
          this.$forceUpdate()
        }
      },
      deep: true
    },
    server_errors: {
      handler(after) {
        for (var error in after) {
          this.editAttributes[error] = true
        }
        this.$forceUpdate()
      },
      deep: true
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["detailCustomer", "detailCustomerAvatar", "detailCustomerType", "detailCustomerContactMain", "customerDetailListDeal", "userCheckAddOn", "userCurrentPermissions"]),
    ...mapState({
      mainContact: state => state.customer.contact_main,
      // accountEduOnline: state => state.education.accountEduOnline,
    }),
    customerLinkDetail() {
      let vm = this;
      let customer_id = vm.mainContact.customer_id;
      let team_id = parseInt(vm.$route.params.team_id);
      if (customer_id) {
        return '/team/' + team_id + '/customers/' + customer_id;
      }
    },
    tabHeadAttributeCustomerInfo() {
      let attributeDefault = ['full_name', 'phone', 'email', 'address', 'birthday_set_to_date', 'gender']
      return this.detailCustomer.attributes.filter(t => attributeDefault.indexOf(t.attribute_code) !== -1)
    }
  },
 
  data() {
    return {
      PERMISSION_UPDATE_CUSTOMER_BASE: PERMISSION_UPDATE_CUSTOMER_BASE,

      selectItemIndex: 0,
      customerAvatarId: 0,
      customerAvatar: "",
      media_popup_id: "update_avatar_profile",
      place_using: "customer_tab_head",
      cacheAvatar: "",
      cacheAvatarId: 0,
      loadAvatar: false,
      updateLoading: false,
      mimesAllowed: [
        "image/jpg",
        "image/jpeg",
        "image/png",
        "image/gif",
      ],
      isLoaderTabRight: false,
      customerId: parseInt(this.$route.params.customer_id),
      countDealCustomerBuy: 0,
      countDealTransactionNotSuccessCustomerBuy: 0,
      isCreatedMultiple: false,
      studentName: "",
      selectedIds: [parseInt(this.$route.params.customer_id)],
      isCreateByDetailCustomer: true,
      accountEduOnline: {},
      isLoadingEduOnline: false,
      isShowActionCreateAccountEdu: true,
      editAttributes: {},
      scope: 'editCustomer'
    };
  },
  methods: {
    getContactByCustomer(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_CONTACT, params).then(() => {}).catch((error) => {
        console.log(error)
      });
    },
    getCustomerDetailListDeal(customer_id) {
      let vm = this;
      vm.isLoaderTabRight = true;
      vm.$store.dispatch(CUSTOMER_DETAIL_GET_ALL_DEAL, {
        customer_id: customer_id
      }).then((response) => {
        if (response.data.data) {
          vm.getCountDealCustomerBuy();
          vm.isLoaderTabRight = false;
        }
      });
    },

    getCountDealCustomerBuy() {
      let vm = this;
      vm.countDealCustomerBuy = 0;
      vm.countDealTransactionNotSuccessCustomerBuy = 0;

      if (vm.customerDetailListDeal && vm.customerDetailListDeal.length > 0) {
        vm.customerDetailListDeal.forEach(function (deal) {
          if (deal.customer_id == vm.detailCustomer.id) {
            vm.countDealCustomerBuy++;
            if (helpers.getAttributeByAttrCodeAndVariableName(deal, 'total_transaction_amount', 'value') != helpers.getAttributeByAttrCodeAndVariableName(deal, 'total_paid_amount', 'value')) {
              vm.countDealTransactionNotSuccessCustomerBuy++;
            }
          }
        });
      }
    },

    getCountAge(time) {
      let dateTimeParse = Date.parse(time);
      if (dateTimeParse) {
        let year = (new Date(dateTimeParse)).getFullYear();
        let currentYear = (new Date()).getFullYear();
        if (year < currentYear) {
          return currentYear - year;
        } else {
          return '';
        }
      }
    },
    modalChangeLevel(index) {
      let vm = this;
      vm.selectItemIndex = index;
      $('#change_level').modal('show');
    },
    showPopupAvatar() {
      let vm = this;
      vm.cacheAvatarId = vm.customerAvatarId;
      vm.cacheAvatar = vm.customerAvatar;
      $("#uploadAvatarInput").click();
    },
    handleFileUpload(scope) {
      let vm = this;
      let files = vm.$refs.add_file.files;
      let errorSize = 0;
      if (files.length > 0) {
        let image = new Image();
        image.src = window.URL.createObjectURL(files[0]);
        image.onload = function () {
          let width = image.naturalWidth,
            height = image.naturalHeight;
          window.URL.revokeObjectURL(image.src);
          if (width < 100 || height < 100) {
            vm.errors.add({
              field: "add_file",
              msg: "Kích thước tối thiểu của ảnh phải là 100x100",
              scope: scope
            });
            errorSize++;
          } else if (width > 2000 || height > 2000) {
            vm.errors.add({
              field: "add_file",
              msg: "Kích thước tối đa của ảnh là 2000x2000",
              scope: scope
            });
            errorSize++;
          }
          vm.$validator.validateAll().then(() => {
            if (!vm.errors.any()) {
              if (errorSize === 0) {
                let filesAppend = [];
                for (let i = 0; i < files.length; i++) {
                  let file = files[i];
                  filesAppend.push(file);
                }
                vm.uploadFile(filesAppend);
              }
            }
          });
        };
      }
    },

    uploadFile(filesAppend) {
      let vm = this;
      let filesLength = filesAppend ? filesAppend.length : 0;
      console.log(filesAppend, "filesAppend");
      if (filesAppend && filesLength > 0) {
        let formData = new FormData();
        formData.append('files', filesAppend[0]);
        formData.append('customer_id', vm.detailCustomer.id);
        formData.append("is_publish", IS_PRIVATE_FILE);

        vm.loadAvatar = true;
        vm.createAvatar(formData).then(response => {
          if (response.data.data) {
            vm.$refs.add_file.value = "";
            vm.$validator.reset();
            let file = response.data.data;
            if (vm.detailCustomer.attributes && vm.detailCustomer.attributes.length > 0) {
              let attributeAvatar = {};
              vm.detailCustomer.attributes.forEach(function (attribute) {
                if (attribute.attribute_code == 'avatar') {
                  attribute.attribute_value = file.id;
                  attribute.is_attribute_option = 0;
                  attribute.is_change_attribute_value = true;
                  attributeAvatar = attribute;
                }
              });
              let params = vm.setParams(attributeAvatar);
              params.data_mail = [];
              params.type_mail = 6;
              vm.updateCustomer(params).then(response => {
                if (response.data.data && response.data.data.status) {
                  vm.customerAvatar = file.path_on_server;
                  vm.customerAvatarId = file.id;
                  vm.$emit("update_success", true);
                } else {
                  vm.$snotify.error('Cập nhật ảnh đại diện khách hàng thất bại.', TOAST_ERROR);
                  if (response.data.data.sort_delete) {
                    $("#messageTrashCustomer").modal('show');
                  }
                }
              }).catch(error => {
                console.log(error);
              });
            }
          }
        }).catch(errors => {
          console.log(errors);
        });
      }
    },

    changeLevel(item) {
      let vm = this;
      let attributeLevel = {};
      if (vm.detailCustomer.attributes && vm.detailCustomer.attributes.length > 0) {
        vm.detailCustomer.attributes.forEach(function (attribute) {
          if (attribute.id == item.attribute_id) {
            attribute.attribute_value = item.id;
            attribute.is_attribute_option = 1;
            attribute.is_change_attribute_value = true;
            attributeLevel = attribute;
          }
        });
      }
      vm.updateLoading = true;
      let params = vm.setParams(attributeLevel);

      params.update_type = [UPDATE_CUSTOMER_BASE_TYPE];
      params.data_mail = [];
      params.type_mail = 6;

      vm.updateCustomer(params).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          if (response.data.errors.option_not_exist || response.data.errors.attribute_not_exist) {
            $('#updateLevelConfirmInfo').modal('show');
          }
        } else {
          if (response.data.data && response.data.data.status) {
            vm.$emit("update_success", true);
          } else {
            vm.$snotify.error('Cập nhật cấp độ khách hàng thất bại.', TOAST_ERROR);
            if (response.data.data.sort_delete) {
              $("#messageTrashCustomer").modal('show');
            }
          }
        }
      }).catch(errors => {
        console.log(errors);
      });
      vm.selectItemIndex = 0;
    },

    setParams(attribute) {
      let vm = this;
      let params = {
        id: vm.detailCustomer.id,
        assigners: vm.detailCustomer.assigners,
        attributes: [attribute],
        // customer_type_id : vm.detailCustomer.customer_type_id,
        is_active: vm.detailCustomer.is_active,
        team_id: vm.detailCustomer.team_id,
        user_id: vm.detailCustomer.user_id
      };
      return params;
    },

    async createAvatar(formData) {
      let vm = this;
      let file = await vm.$store.dispatch(FILE_CREATE_AVATAR, formData);
      return file;
    },

    async updateCustomer(params) {
      let vm = this;
      let response = await vm.$store.dispatch(CUSTOMER_UPDATE, params);
      return response;
    },

    closeModalWarningUploadAvatar() {
      let vm = this;
      vm.$refs.add_file.value = "";
      vm.$validator.reset();
    },

    reloadPage() {
      return window.location.reload();
    },

    showIconNextPrev() {
      var width = $(window).width();
      if (width > 1200) {
        var progress_bar_slider = $('.s--progress-bar__slider .m-wizard--2');

        var progress_bar_slider_inner_width = $('.s--progress-bar__slider .m-wizard--2').innerWidth();
        var progress_bar_slider_full_width = $('.s--progress-bar__slider .m-wizard__steps').innerWidth();

        var progress_bar_slider_active_index = $('.s--progress-bar__slider .m-wizard__steps .m-wizard__step--active').index() + 1;

        var progress_bar_slider_count_items = $('.s--progress-bar__slider .m-wizard__steps .m-wizard__step').length;
        var progress_bar_slider_left_width_item = parseInt(progress_bar_slider_inner_width - progress_bar_slider_full_width) / progress_bar_slider_count_items;

        if (progress_bar_slider_full_width < progress_bar_slider_inner_width) {
          $('.s--progress-bar__slider-navigation').hide();
        } else {

          if (progress_bar_slider_active_index >= progress_bar_slider_count_items - 2) {
            progress_bar_slider.css({
              'left': parseInt(progress_bar_slider_inner_width - progress_bar_slider_full_width) + 'px'
            });
          } else if (progress_bar_slider_active_index > 2) {
            progress_bar_slider.css({
              'left': progress_bar_slider_active_index * progress_bar_slider_left_width_item + 'px'
            });
          }
        }
      }
    },
    createNewAcoountForStudent() {
      let customerEmail = helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'email', 'attribute_value');
      this.studentName = this.helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'full_name', 'attribute_value');

      if (customerEmail) {
        $('#createAccountEducation').modal('show');
      } else {
        $('#createAccountEducationError').modal('show');
      }

    },
    isChangeStatusCreateNewStudent() {
      this.getAccoutEduOnlineStatus();
    },
    getAccoutEduOnlineStatus() {
      let vm = this;
      vm.isLoadingEduOnline = true;

      let params = {
        id: parseInt(this.$route.params.customer_id),
        team_id: parseInt(this.$route.params.team_id)
      };
      vm.$store.dispatch(GET_ACCOUNT_EDU_ONLINE_STATUS, params).then((response) => {
        vm.isLoadingEduOnline = false;
        vm.accountEduOnline = response.data.data;
      }).catch(() => {
        vm.isLoadingEduOnline = false;
      });
    },


    /// Update customer
    showUpdate(attribute) {
      let vm = this
      vm.editAttributes[attribute.attribute_code] = true
      vm.$forceUpdate()
      setTimeout(() => {
        $('#' + attribute.attribute_code).focus()
      }, 100)
    },

    checkValidateAttribute(attribute_code) {
      this.$validator.validateAll(this.scope).then(() => {
        if (this.errors.items.length) {
          this.errors.items.forEach((item) => {
            if (item.field === attribute_code) {
              return true
            }
          })
        }
      })
      if (this.server_errors[attribute_code]) {
        return true
      }
      return false
    },

    hideUpdate(attribute) {
      let vm = this
      let isError = vm.checkValidateAttribute(attribute.attribute_code)
      if (!isError) {
        vm.editAttributes[attribute.attribute_code] = false
        vm.$forceUpdate()
      }
    },

    changeDataInputType(attribute, event) {
      let vm = this;
      let keyCode = event && event.keyCode ? event.keyCode : ''

      if (keyCode === 13) {
        let isError = vm.checkValidateAttribute(attribute.attribute_code)

        if (!isError) {
          vm.editAttributes[attribute.attribute_code] = false
          vm.$forceUpdate()
          vm.$bus.$emit('updateDetailsCustomer', true)
        }
      }

      // Set is change attribute
      attribute.attribute_value = attribute.attribute_value ? attribute.attribute_value : '';
      attribute.is_change_attribute_value = false

      if (attribute.data_type_id == vm.isMultipleSelect ||
        attribute.data_type_id == vm.isTag ||
        attribute.data_type_id == vm.isCheckboxMultiple ||
        attribute.data_type_id == vm.isUserMultiple) {
        if (attribute.attribute_value_cache) {
          if (attribute.attribute_value) {
            if (attribute.attribute_value.join() != attribute.attribute_value_cache.join()) {
              attribute.is_change_attribute_value = true
            }
          } else if (attribute.attribute_value.length <= 0) {
            attribute.is_change_attribute_value = true
          }

        } else {
          if (attribute.attribute_value && attribute.attribute_value.length > 0) {
            attribute.is_change_attribute_value = true
          }
        }
      } else if (attribute.data_type_id == vm.isNumber) {
        let attribute_value = attribute.attribute_value && attribute_value !== "0,00" ? parseFloat(attribute.attribute_value).toFixed(2).replace(".", ",") : '';
        if (attribute.attribute_value != attribute.attribute_value_cache) {
          attribute.is_change_attribute_value = true
        }
      } else {
        if (attribute.attribute_value != attribute.attribute_value_cache) {
          attribute.is_change_attribute_value = true
        }
      }

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute.attribute_code]) {
          vm.server_errors[attribute.attribute_code] = ''
        }
      }, 500)
    }
  }
}

var width = $(window).width();
if (width > 1200) {
  $('body').delegate('.s--progress-bar__slider-navigation .next', 'click', function () {
    var progress_bar_slider = $('.s--progress-bar__slider .m-wizard--2');
    var progress_bar_slider_inner_width = progress_bar_slider.innerWidth();
    var progress_bar_slider_full_width = progress_bar_slider[0].scrollWidth;
    var progress_bar_slider_left = parseInt(progress_bar_slider.css('left'));

    var progress_bar_slider_max_width_hidden = parseInt(progress_bar_slider_full_width - progress_bar_slider_inner_width);

    var width_progress_bar__slider_next = $('.s--progress-bar__slider .m-wizard__steps .m-wizard__step').innerWidth();

    if (progress_bar_slider_max_width_hidden > Math.abs(progress_bar_slider_left)) {
      var set_progress_bar_slider_left = parseInt(progress_bar_slider_left - width_progress_bar__slider_next);
      if (Math.abs(set_progress_bar_slider_left) > progress_bar_slider_max_width_hidden) {
        set_progress_bar_slider_left = "-" + progress_bar_slider_max_width_hidden;
      }
      progress_bar_slider.css({
        'left': set_progress_bar_slider_left + 'px'
      });
    }

    return false;
  });

  $('body').delegate('.s--progress-bar__slider-navigation .prev', 'click', function () {
    var progress_bar_slider = $('.s--progress-bar__slider .m-wizard--2');
    var progress_bar_slider_left = parseInt(progress_bar_slider.css('left'));

    var width_progress_bar__slider_next = $('.s--progress-bar__slider .m-wizard__steps .m-wizard__step').innerWidth();

    if (Math.abs(progress_bar_slider_left) > 0) {

      var set_progress_bar_slider_left = parseInt(progress_bar_slider_left + width_progress_bar__slider_next);
      if (Math.abs(progress_bar_slider_left) < width_progress_bar__slider_next) {
        set_progress_bar_slider_left = 0;
      }

      progress_bar_slider.css({
        'left': set_progress_bar_slider_left + 'px'
      });
    }

    return false;
  });
}