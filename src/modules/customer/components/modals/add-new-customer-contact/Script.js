/* eslint-disable no-undef */
import { mapState } from "vuex";
import { mapGetters } from "vuex";

import {
  CUSTOMER_GET_ALL_CUSTOMER_CONTACT_BY_TEAM,
  CUSTOMER_ADD_CONTACT
} from "@/types/actions.type";
import helpers from "@/utils/utils";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

// const notifications = {
//   add: {
//     message: {
//       title: "Thêm mới thành công",
//       content: "Thêm mới liên hệ thành công"
//     },
//     type: "success",
//     isShow: false
//   }
// };

const customMessagesCustomerContactAddModal = {
  custom: {
    full_name: {
      required: "Vui lòng nhập tên người liên hệ.",
      max: "Tên người liên hệ có độ dài tối đa là 250 ký tự"
    },
    email: {
      regex: "Email không hợp lệ.",
      max: "Email có độ dài tối đa là 250 ký tự"
    },
    phone: {
      regex: "Số điện thoại không hợp lệ."
    },
    address: {
      max: "Địa chỉ có độ dài tối đa là 250 ký tự"
    },
    note: {
      max: "Ghi chú có độ dài tối đa là 1025 ký tự"
    }
  }
};

export default {
  components: {},
  mounted() {
    let vm = this;
    vm.$validator.localize("en", customMessagesCustomerContactAddModal);
    vm.hiddenInfoCustomerTop();
  },
  data() {
    return {
      team_id: parseInt(this.$route.params.team_id),
      customer_id: parseInt(this.$route.params.customer_id),
      contactErrors: [],
      customerContact: {
        full_name: "",
        phone: "",
        email: "",
        address: "",
        note: "",
        relationship: "",
        is_main_contact: false,
        id: "", //customer_id of list customer
        created_by: parseInt(this.$route.params.customer_id) //current id customer
      },
      isSelectContactInListCustomer: false,
      customerContactAddModalIsLoader: false,
      customerContactAddModalIsSuccess: false,
      customerContactMainIsExists: false,
      customerContactIdSuccess: null,
      relationshipContact: [
        {
          relationship_title: 'Bố'
        },
        {
          relationship_title: 'Mẹ'
        },
        {
          relationship_title: 'Ông'
        },
        {
          relationship_title: 'Bà'
        },
        {
          relationship_title: 'Cô/Dì'
        },
        {
          relationship_title: 'Chú'
        },
        {
          relationship_title: 'Bác'
        },
        {
          relationship_title: 'Anh/Chị'
        },
        {
          relationship_title: 'Em'
        },
        {
          relationship_title: 'Công ty'
        },
        {
          relationship_title: 'Nhân sự'
        },
        {
          relationship_title: 'Khác'
        },
      ],
      isContactExist: false,
      lengthCustomerContact: "",
      customerContactOfTeam: [],
      isPhoneExist: false,
      isEmailExist: false,
      isBtnLoading: false,
      isDuplicatePhoneNumberWithCurrentCustomer: false,
      isDuplicateEmailWithCurrentCustomer: false,
      testABC: false

    };
  },
  watch: {
    customerContactOfTeam: {
      handler(after) {
        this.lengthCustomerContact = after.length;
      }
    },

    customerContact: {
      handler(after) {
        let vm = this;


        if (after.phone) {
          after.phone = helpers.replacePhone(after.phone);
        }

        if (after.phone && vm.checkInputDuplicateInListContact(vm.customerContacts, after.phone, 'phone') === true) {
          vm.isBtnLoading = true;
          vm.contactErrors['phone'] = true;
        } else {
          vm.isBtnLoading = false;
          vm.contactErrors['phone'] = false;
        }

        if (after.email && vm.checkInputDuplicateInListContact(vm.customerContacts, after.email, 'email') === true) {
          vm.isBtnLoading = true;
          vm.contactErrors['email'] = true;
        } else {
          vm.isBtnLoading = false;
          vm.contactErrors['email'] = false;
        }


      },
      deep: true
    },

  },
  created() {
    let vm = this;
    let params = {
      team_id: vm.team_id,
      customer_id: vm.customer_id,
    };
    vm.getAllCustomerByTeam(params);
  },
  computed: {
    customerLinkDetail() {
      let customer_id = this.customerContact.id;
      let team_id = this.team_id;
      if (customer_id) {
        return '/team/' + team_id + '/customers/' + customer_id;
      }
    },
    helpers() {
      return helpers;
    },
    ...mapState({
      mainContact: state => state.customer.contact_main,

    }),
    ...mapGetters(["customerContacts", "detailCustomer"]),
    listCustomerEmail() {
      return this.customerContactOfTeam.filter(object => object.email);
    },
    customerContactPhone() {
      return this.customerContactOfTeam.filter(contact => contact.phone !== null);
    },
    currentCustomerPhone() {
      return helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'phone', 'attribute_value').replace(/ /g, "")
    },
    currentCustomerEmail() {
      return helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'email', 'attribute_value')
    },
    currentCustomerFullName() {
      return helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'full_name', 'attribute_value')
    },
    currentCustomerLink() {
      let vm = this;
      let customer_id = vm.customer_id;
      let team_id = vm.team_id;
      return '/team/' + team_id + '/customers/' + customer_id;
    },

  },

  methods: {
    inputChangePhone($event) {
      let vm = this;
      let inputPhone = $event ? $event.replace(/ /g, "") : "";
      vm.customerContact.phone = $event;
      vm.isDuplicatePhoneNumberWithCurrentCustomer = !!(vm.currentCustomerPhone && inputPhone && vm.currentCustomerPhone === inputPhone);

    },
    inputChangeEmail($event) {
      let vm = this;
      vm.customerContact.email = $event ? $event : "";
      vm.isDuplicateEmailWithCurrentCustomer = !!(vm.currentCustomerEmail && $event && vm.currentCustomerEmail === $event);
    },
    checkInputDuplicateInListContact(dataArrays, dataInput, input_type = '') {
      let statusDuplicate = false;
      if (dataInput && dataArrays && input_type === 'phone') {
        dataArrays.forEach(function (contact) {
          if ((contact.phone && helpers.replacePhone(contact.phone)) === dataInput) {
            return statusDuplicate = true
          }
        })
      }

      if (dataInput && dataArrays && input_type === 'email') {
        dataArrays.forEach((contact) => {
          if (contact.email === dataInput) {
            return statusDuplicate = true
          }
        })
      }
      return statusDuplicate;

    },
    onChangeInputName($event) {
      let vm = this;
      vm.customerContact.full_name = $event;
    },
    /*onChangeInputEmail($event) {
        let vm = this;
        vm.customerContact.email = $event;
        if ( vm.contactErrors['email']  ) {
            vm.contactErrors['email'] = false;
        }
    },
    onChangeInputPhone($event) {
        let vm = this;
        vm.customerContact.phone = $event;
        if ( vm.contactErrors['phone'] ) {
            vm.contactErrors['phone'] = false;
        }
    },*/
    onSearchBlur() {
      let vm = this;
      let params = {
        team_id: vm.team_id,
        customer_id: vm.customer_id,
      };
      vm.getAllCustomerByTeam(params);
    },

    onSearchFullName(search) {
      let vm = this;
      let searchText = search.trim();
      let params = {
        customer_id: vm.customer_id,
        full_name: searchText
      };

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.getAllCustomerByTeam(params);
      }, 500);
    },
    onSearchPhone(search) {
      let vm = this;
      let searchText = search ? search.trim().replace(/ /g, "") : "";
      let params = {
        customer_id: vm.customer_id,
        phone: searchText
      };

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.getAllCustomerByTeam(params);
      }, 500);
    },
    onSearchEmail(search) {
      let vm = this;
      let params = {
        customer_id: vm.customer_id,
        email: search
      };

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.getAllCustomerByTeam(params);
      }, 500);
    },
    checkContactIsExitsInCustomer(contactSelected) {
      let vm = this;
      var statusDuplicate = false;

      vm.customerContacts.forEach((contact) => {
        if (contact.customer_id === contactSelected.id) {
          return statusDuplicate = true;
        }
      });
      return statusDuplicate;
    },

    setSelected(selectedObject) {
      let vm = this;
      /*If size of Object = 1. Contact is not customer*/
      /*If size of Object > 1. Contact is customer*/
      let sizeOfObject = Object.keys(selectedObject).length;

      if (sizeOfObject === 1) {

        let keyOfSelectedObject = Object.keys(selectedObject)[0];

        if (keyOfSelectedObject === 'relationship_title') {
          vm.customerContact.relationship = selectedObject.relationship_title;
        }
        if (keyOfSelectedObject === 'full_name') {
          vm.customerContact.full_name = selectedObject.full_name;
        }
        if (keyOfSelectedObject === 'email') {
          vm.customerContact.email = selectedObject.email;
        }
        if (keyOfSelectedObject === 'phone') {
          vm.customerContact.phone = selectedObject.phone;
        }

      } else {
        vm.checkContactIsExitsInCustomer(selectedObject);
        vm.isContactExist = vm.checkContactIsExitsInCustomer(selectedObject) === true;
        vm.customerContactMainIsExists = false;
        vm.isSelectContactInListCustomer = true;
        vm.customerContact.full_name = selectedObject.full_name;
        vm.customerContact.phone = selectedObject.phone;
        vm.customerContact.email = selectedObject.email;
        vm.customerContact.address = selectedObject.address;
        vm.customerContact.is_main_contact = false;
        vm.customerContact.avatar_url = selectedObject.avatar_url;
        vm.customerContact.id = selectedObject.id;

        $('.alert').show();
      }
    },
    getAllCustomerByTeam(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ALL_CUSTOMER_CONTACT_BY_TEAM, params).then((response) => {
        if (response.data.data) {

          let listCustomerByTeam = response.data.data;
          if (params.full_name) {
            listCustomerByTeam.unshift({
              full_name: params.full_name
            });
          }

          let customers = [];
          if (listCustomerByTeam && listCustomerByTeam.length > 0) {
            listCustomerByTeam.forEach(function (customer) {
              if (!customer.deleted_at) {
                customers.push(customer);
              }
            });
          }

          vm.customerContactOfTeam = customers;
        }

      }).catch((error) => {
        console.log(error);
      });
    },

    hiddenInfoCustomerTop() {
      $('#alert-user-contact').hide();
    },
    closeInfoCustomer() {
      let vm = this;
      vm.resetCustomerContact();
      vm.hiddenInfoCustomerTop();
      vm.isSelectContactInListCustomer = false;
      $('.alert').hide();

    },
    resetCustomerContact() {
      let vm = this;
      vm.isContactExist = false;
      vm.isPhoneExist = false;
      vm.isEmailExist = false;
      vm.isBtnLoading = false;
      vm.isDuplicatePhoneNumberWithCurrentCustomer = false;
      vm.isDuplicateEmailWithCurrentCustomer = false;
      vm.customerContactMainIsExists = false;
      vm.customerContact = {
        full_name: "",
        phone: "",
        relationship: "",
        email: "",
        address: "",
        note: "",
        id: "",
        is_main_contact: false
      };
      let params = {
        team_id: vm.team_id,
        customer_id: vm.customer_id,
      };
      vm.getAllCustomerByTeam(params);
      vm.$validator.reset();
    },
    checkExistsCustomerContactMain() {
      let vm = this;
      let current_contact_id = vm.customerContact.id;
      let mainContactId = vm.mainContact ? vm.mainContact.id : 0;

      vm.customerContactMainIsExists = !vm.customerContactMainIsExists;

      if ((current_contact_id !== mainContactId) && vm.mainContact && vm.customerContactMainIsExists) {
        vm.customerContactMainIsExists = true;
      } else {
        vm.customerContactMainIsExists = false;
      }
    },
    addNewCustomerContact(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isBtnLoading = true;
          let paramsVModel = this.customerContact;
          let paramsRequestServer = {
            full_name: paramsVModel.full_name,
            phone: paramsVModel.phone ? paramsVModel.phone.replace(/ /g, "") : paramsVModel.phone,
            relationship: paramsVModel.relationship,
            email: paramsVModel.email,
            address: paramsVModel.address,
            note: paramsVModel.note,
            is_main_contact: (paramsVModel.is_main_contact === true) ? paramsVModel.is_main_contact = 1 : 0,
            created_by: parseInt(this.$route.params.customer_id), //ID Customer
            customer_id: paramsVModel.id, //ID Contact
          };

          vm.$store.dispatch(CUSTOMER_ADD_CONTACT, paramsRequestServer).then((response) => {
            vm.isBtnLoading = false;

            if (response.status === 422) {
              vm.contactErrors = response.data.errors;
              if (vm.contactErrors.sort_delete) {
                $("#messageTrashCustomer").modal('show');
              }
              vm.$snotify.error('Thêm mới liên hệ thất bại.', TOAST_ERROR);
              return
            }

            if (response.data.data) {
              vm.$emit('addNewContactCustomerIsSuccess', true);
              vm.$snotify.success('Thêm mới liên hệ thành công.', TOAST_SUCCESS);
              vm.closeInfoCustomer();
              $(".s--modal .modal-body").animate({ scrollTop: 0 }, 600);
            }
          }).catch((error) => {
            vm.isBtnLoading = false;
            vm.$snotify.error('Thêm mới liên hệ thất bại.', TOAST_ERROR);
            console.log(error)
          });
        }
      });
    },
  }
};
