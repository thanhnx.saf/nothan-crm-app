/* eslint-disable no-undef */
import { mapState } from "vuex";
import { mapGetters } from "vuex";

import { PERMISSION_UPDATE_CONTACT } from "@/types/const";
import {
  CUSTOMER_GET_ALL_CUSTOMER_CONTACT_BY_TEAM,
  CUSTOMER_UPDATE_CONTACT
} from "@/types/actions.type";
import helpers from "@/utils/utils";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

// const notifications = {
//   add: {
//     message: {
//       title: "Thêm mới thành công",
//       content: "Thêm mới liên hệ thành công"
//     },
//     type: "success",
//     isShow: false
//   }
// };

const customMessagesCustomerContactAddModal = {
  custom: {
    full_name: {
      required: "Vui lòng nhập tên người liên hệ.",
      max: "Tên người liên hệ có độ dài tối đa là 250 ký tự"
    },
    email: {
      regex: "Email không hợp lệ.",
      max: "Email có độ dài tối đa là 250 ký tự"
    },
    phone: {
      regex: "Số điện thoại không hợp lệ."
    },
    address: {
      max: "Địa chỉ có độ dài tối đa là 250 ký tự"
    },
    note: {
      max: "Ghi chú có độ dài tối đa là 1025 ký tự"
    }
  }
};

export default {
  props: ["contact", "isPropDataToContactView"],
  components: {},
  mounted() {
    let vm = this;
    vm.$validator.localize("en", customMessagesCustomerContactAddModal);
    vm.hiddenInfoCustomerTop();
  },
  data() {
    return {
      PERMISSION_UPDATE_CONTACT: PERMISSION_UPDATE_CONTACT,

      team_id: parseInt(this.$route.params.team_id),
      customer_id: parseInt(this.$route.params.customer_id),
      paramsGetAllCustomerByTeam: {
        team_id: parseInt(this.$route.params.team_id),
        customer_id: parseInt(this.$route.params.customer_id),
      },
      contactErrors: [],
      customerContact: {
        full_name: "",
        phone: "",
        email: "",
        address: "",
        note: "",
        relationship: "",
        is_main_contact: false,
        customer_id: "", //customer_id of list customer
        id: "", //customer_id of list customer
        created_by: parseInt(this.$route.params.customer_id) //current id customer
      },
      customerContactCache: {},
      isSelectContactInListCustomer: false,
      customerContactAddModalIsLoader: false,
      customerContactAddModalIsSuccess: false,
      customerContactMainIsExists: false,
      customerContactIdSuccess: null,
      relationshipContact: [
        {
          relationship_title: 'Bố'
        },
        {
          relationship_title: 'Mẹ'
        },
        {
          relationship_title: 'Ông'
        },
        {
          relationship_title: 'Bà'
        },
        {
          relationship_title: 'Cô/Dì'
        },
        {
          relationship_title: 'Chú'
        },
        {
          relationship_title: 'Bác'
        },
        {
          relationship_title: 'Anh/Chị'
        },
        {
          relationship_title: 'Em'
        },
        {
          relationship_title: 'Công ty'
        },
        {
          relationship_title: 'Nhân sự'
        },
        {
          relationship_title: 'Khác'
        },
      ],
      isContactExist: false,
      lengthCustomerContact: "",
      customerContactOfTeam: [],
      isPhoneExist: false,
      isEmailExist: false,
      statusCustomerIdIsExist: false,
      isBtnLoading: false,
      isChange: false,
      isDuplicateEmailWithCurrentCustomer: false,
      isDuplicatePhoneNumberWithCurrentCustomer: false,

    };
  },
  watch: {
    customerContactOfTeam: {
      handler(after) {
        this.lengthCustomerContact = after.length;
      }
    },

    customerContact: {
      handler(after) {
        let vm = this;
        let countChange = 0;
        let customerContactCache = vm.customerContactCache;
        let phoneCache = customerContactCache.phone ? helpers.replacePhone(customerContactCache.phone) : "";

        if (after.full_name !== customerContactCache.full_name) {
          countChange++
        } else if ((after.is_main_contact !== customerContactCache.is_main_contact) || (customerContactCache.is_main_contact === 1 && after.is_main_contact === false)) {
          countChange++
        } else if (after.phone !== phoneCache) {
          countChange++
        } else if (after.email !== customerContactCache.email) {
          countChange++
        } else if (after.address !== customerContactCache.address) {
          countChange++
        } else if (after.relationship !== customerContactCache.relationship) {
          countChange++
        } else {
          if (after.note !== customerContactCache.note) {
            countChange++
          }
        }

        vm.isChange = !!countChange;

        if (after && after.customer_id) {
          $('.alert').show();
          vm.statusCustomerIdIsExist = true;
        } else {

          vm.statusCustomerIdIsExist = false;
        }

        if (after && after.phone) {
          after.phone = helpers.replacePhone(after.phone);
        }

      },
      deep: true
    },
    contact: {
      handler(after) {
        let vm = this;
        if (after && after.customer_id) {
          $('.alert').show();
          vm.statusCustomerIdIsExist = true;
        } else {
          vm.statusCustomerIdIsExist = false;
        }
      }
    },

  },
  computed: {
    customerLinkDetail() {
      let customer_id = this.contact.customer_id;
      let team_id = parseInt(this.$route.params.team_id);
      if (customer_id) {
        return '/team/' + team_id + '/customers/' + customer_id;
      }
    },
    helpers() {
      return helpers;
    },
    ...mapGetters(["customerContacts", "detailCustomer", "userCheckAddOn", "userCurrentPermissions"]),
    ...mapState({
      mainContact: state => state.customer.contact_main,

    }),
    listCustomerEmail() {
      return this.customerContactOfTeam.filter(object => object.email);
    },
    customerContactPhone() {
      return this.customerContactOfTeam.filter(contact => contact.phone !== null);
    },
    currentCustomerPhone() {
      let phone = helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'phone', 'attribute_value');
      return phone ? phone.replace(/ /g, "") : "";
    },
    currentCustomerEmail() {
      return helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'email', 'attribute_value')
    },
    currentCustomerFullName() {
      return helpers.getAttributeByAttrCodeAndVariableName(this.detailCustomer, 'full_name', 'attribute_value')
    },
    currentCustomerLink() {
      let vm = this;
      let customer_id = vm.customer_id;
      let team_id = vm.team_id;
      return '/team/' + team_id + '/customers/' + customer_id;
    },
  },
  created() {
    let vm = this;
    vm.getAllCustomerByTeam(vm.paramsGetAllCustomerByTeam);

    vm.$bus.$on('assignObject', ($event) => {
      vm.customerContact = Object.assign({}, $event);
      vm.customerContactCache = Object.assign({}, $event);
    });
    vm.$bus.$on('resetContactViewAfterDeleteSuccess', ($event) => {
      if ($event) {
        vm.closeInfoCustomer();
      }
    })
  },

  methods: {
    changeInputPhone($event) {
      let vm = this;

      let inputPhone = "";

      if ($event && typeof $event === 'object') {
        inputPhone = $event.phone;
      } else {
        inputPhone = $event
      }

      vm.customerContact.phone = inputPhone ? helpers.replacePhone(inputPhone) : "";
      let phoneCache = vm.customerContactCache ? vm.customerContactCache.phone : "";
      vm.isBtnLoading = false;
      vm.contactErrors['phone'] = false;

      phoneCache = phoneCache ? helpers.replacePhone(phoneCache) : "";

      if (phoneCache && inputPhone && phoneCache != inputPhone) {
        if (inputPhone && vm.checkInputDuplicateInListContact(vm.customerContacts, inputPhone, 'phone') === true) {
          vm.isBtnLoading = true;
          vm.contactErrors['phone'] = true;
        } else {
          vm.isBtnLoading = false;
          vm.contactErrors['phone'] = false;
        }
      }
      if (!phoneCache && inputPhone) {
        if (inputPhone && vm.checkInputDuplicateInListContact(vm.customerContacts, inputPhone, 'phone') === true) {
          vm.isBtnLoading = true;
          vm.contactErrors['phone'] = true;
        } else {
          vm.isBtnLoading = false;
          vm.contactErrors['phone'] = false;
        }
      }

      if (vm.currentCustomerPhone && inputPhone && vm.currentCustomerPhone === inputPhone.replace(/ /g, "")) {
        vm.isDuplicatePhoneNumberWithCurrentCustomer = true;
      } else {
        vm.isDuplicatePhoneNumberWithCurrentCustomer = false;
      }

    },
    changeInputEmail($event) {
      let vm = this;
      let inputEmail = "";

      if ($event && typeof $event === 'object') {
        inputEmail = $event.email;
      } else {
        inputEmail = $event
      }
      vm.customerContact.email = inputEmail ? inputEmail : "";
      let emailCache = vm.customerContactCache ? vm.customerContactCache.email : "";
      vm.isBtnLoading = false;
      vm.contactErrors['email'] = false;

      if (emailCache && inputEmail && emailCache != inputEmail) {
        if (inputEmail && vm.checkInputDuplicateInListContact(vm.customerContacts, inputEmail, 'email') === true) {
          vm.isBtnLoading = true;
          vm.contactErrors['email'] = true;
        } else {
          vm.isBtnLoading = false;
          vm.contactErrors['email'] = false;
        }
      }

      if (!emailCache && inputEmail) {
        if (inputEmail && vm.checkInputDuplicateInListContact(vm.customerContacts, inputEmail, 'email') === true) {
          vm.isBtnLoading = true;
          vm.contactErrors['email'] = true;
        } else {
          vm.isBtnLoading = false;
          vm.contactErrors['email'] = false;
        }
      }

      if (vm.currentCustomerEmail && inputEmail && vm.currentCustomerEmail === inputEmail) {

        vm.isDuplicateEmailWithCurrentCustomer = true;
      } else {

        vm.isDuplicateEmailWithCurrentCustomer = false;
      }

    },
    checkInputDuplicateInListContact(dataArrays, dataInput, input_type = '') {
      let statusDuplicate = false;
      if (dataInput && dataArrays && input_type === 'phone') {
        dataArrays.forEach(function (contact) {
          if (contact.phone && helpers.replacePhone(contact.phone) == dataInput) {
            return statusDuplicate = true
          }
        })
      }

      if (dataInput && dataArrays && input_type === 'email') {
        dataArrays.forEach((contact) => {
          if (contact.email === dataInput) {
            return statusDuplicate = true
          }
        })
      }
      return statusDuplicate;

    },
    onChangeInputName($event) {
      let vm = this;
      vm.customerContact.full_name = $event;
    },
    checkIsContactIsCustomer(contact) {
      let statusExist = false;
      if (contact && contact.customer_id && contact.customer_id !== '') {
        statusExist = true;
      }
      return statusExist;
    },
    contactViewUpdate() {
      let vm = this;
      vm.customerContact = Object.assign({}, vm.contact);
      vm.customerContactCache = Object.assign({}, vm.contact);
      $('#editCustomerContact').modal('show');
    },
    onSearchBlur() {
      let vm = this;
      vm.getAllCustomerByTeam(vm.paramsGetAllCustomerByTeam);
    },

    onSearchFullName(search) {
      let vm = this;
      let searchText = search.trim();
      let params = {
        customer_id: vm.customer_id,
        full_name: searchText
      };

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.getAllCustomerByTeam(params);
      }, 500);
    },
    onSearchPhone(search) {
      let vm = this;
      let searchText = search ? search.trim().replace(/ /g, "") : "";
      let params = {
        customer_id: vm.customer_id,
        phone: searchText
      };

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.getAllCustomerByTeam(params);
      }, 500);
    },
    onSearchEmail(search) {
      let vm = this;
      let params = {
        customer_id: vm.customer_id,
        email: search
      };

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.getAllCustomerByTeam(params);
      }, 500);
    },
    checkContactIsExitsInCustomer(contactSelected) {
      let vm = this;
      var statusDuplicate = false;

      vm.customerContacts.forEach((contact) => {
        if (contact.customer_id === contactSelected.id) {
          return statusDuplicate = true;
        }
      });
      return statusDuplicate;
    },

    setSelected(selectedObject) {
      let vm = this;
      /*If size of Object = 1. Contact is not customer*/
      /*If size of Object > 1. Contact is customer*/
      let sizeOfObject = Object.keys(selectedObject).length;

      if (sizeOfObject === 1) {

        let keyOfSelectedObject = Object.keys(selectedObject)[0];

        if (keyOfSelectedObject === 'relationship_title') {
          vm.customerContact.relationship = selectedObject.relationship_title;
        }
        if (keyOfSelectedObject === 'full_name') {
          vm.customerContact.full_name = selectedObject.full_name;
        }
        if (keyOfSelectedObject === 'email') {
          vm.customerContact.email = selectedObject.email;
        }
        if (keyOfSelectedObject === 'phone') {
          vm.customerContact.phone = selectedObject.phone;
        }

      } else {

        vm.checkContactIsExitsInCustomer(selectedObject);
        vm.isContactExist = vm.checkContactIsExitsInCustomer(selectedObject) === true;
        vm.isSelectContactInListCustomer = true;
        vm.customerContactMainIsExists = false;
        vm.isSelectContactInListCustomer = true;
        vm.customerContact.full_name = selectedObject.full_name;
        vm.customerContact.phone = selectedObject.phone;
        vm.customerContact.email = selectedObject.email;
        vm.customerContact.address = selectedObject.address;
        vm.customerContact.is_main_contact = false;
        vm.customerContact.avatar_url = selectedObject.avatar_url;
        vm.customerContact.customer_id = selectedObject.id;
        $('.alert').show();
      }
    },
    getAllCustomerByTeam(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ALL_CUSTOMER_CONTACT_BY_TEAM, params).then((response) => {
        if (response.data.data) {
          let listCustomerByTeam = response.data.data;
          if (params.full_name) {
            listCustomerByTeam.unshift({
              full_name: params.full_name
            });
          }
          vm.customerContactOfTeam = listCustomerByTeam
        }
      }).catch((error) => {
        console.log(error);
      });
    },

    hiddenInfoCustomerTop() {
      $('#alert-user-contact').hide();
    },
    closeInfoCustomer() {
      let vm = this;
      vm.resetCustomerContact();
      $('.alert').hide()

    },
    resetCustomerContact() {
      let vm = this;
      vm.isSelectContactInListCustomer = false;
      vm.isContactExist = false;
      vm.isPhoneExist = false;
      vm.isEmailExist = false;
      vm.isBtnLoading = false;
      vm.isChange = false;
      vm.statusCustomerIdIsExist = false;
      vm.customerContactMainIsExists = false;
      vm.customerContact = {
        full_name: "",
        phone: "",
        relationship: "",
        email: "",
        address: "",
        note: "",
        id: "",
        is_main_contact: false
      };
      vm.getAllCustomerByTeam(vm.paramsGetAllCustomerByTeam);
      vm.$validator.reset();
      $('.alert').hide();

    },
    resetErrorCheckIsExistPhoneAndEmail(type = '') {
      let vm = this;
      if (type === 'phone' && vm.contactErrors['phone']) {
        vm.contactErrors['phone'] = false;
        vm.customerContact.phone = '';
      }
      if (type === 'email' && vm.contactErrors['email']) {
        vm.contactErrors['email'] = false;
        vm.customerContact.email = '';
      }
    },
    checkExistsCustomerContactMain() {
      let vm = this;
      let current_contact_id = vm.customerContact.id;
      let mainContactId = vm.mainContact ? vm.mainContact.id : 0;

      vm.customerContactMainIsExists = !vm.customerContactMainIsExists;

      vm.customerContactMainIsExists = !!((current_contact_id !== mainContactId) && vm.mainContact && vm.customerContactMainIsExists)

    },
    updateCustomerContact(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isBtnLoading = true;
          let paramsVModel = this.customerContact;
          let paramsRequestServer = {
            customer_id: paramsVModel.customer_id ? paramsVModel.customer_id : "", //ID contact is customer,
            id: vm.customerContactCache.id, //ID of list contact,
            created_by: vm.customer_id, //ID current customer,
            team_id: vm.team_id,
            full_name: paramsVModel.full_name,
            phone: paramsVModel.phone ? paramsVModel.phone.replace(/ /g, "") : paramsVModel.phone,
            relationship: paramsVModel.relationship,
            email: paramsVModel.email,
            address: paramsVModel.address,
            note: paramsVModel.note,
            is_main_contact: (paramsVModel.is_main_contact === true || paramsVModel.is_main_contact === 1) ? 1 : 0
          };

          vm.$store.dispatch(CUSTOMER_UPDATE_CONTACT, paramsRequestServer).then((response) => {
            vm.isBtnLoading = false;

            if (response.status === 422) {
              vm.contactErrors = response.data.errors;
              if (vm.contactErrors.sort_delete) {
                $("#messageTrashCustomer").modal('show');
              }
              vm.$snotify.error('Cập nhật liên hệ thất bại.', TOAST_ERROR);
              return
            }

            if (response.data.data) {
              $('#editCustomerContact').modal('hide');
              vm.$emit('updateCustomerContactIsSuccess', true);
              vm.$snotify.success('Cập nhật liên hệ thành công.', TOAST_SUCCESS);
              vm.closeInfoCustomer();
              $(".s--modal .modal-body").animate({ scrollTop: 0 }, 600);
            }
          }).catch((error) => {
            vm.isBtnLoading = false;
            vm.$snotify.error('Cập nhật liên hệ thất bại.', TOAST_ERROR);
            console.log(error)
          });
        }
      });
    },

  }
};
