/* eslint-disable no-undef */
import { CUSTOMER_DELECT_CONTACT } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  name: "DeleteCustomerContact",
  props: ["contact"],
  methods: {
    deleteContact() {
      let vm = this;
      let params = {
        team_id: parseInt(this.$route.params.team_id),
        customer_id: parseInt(this.$route.params.customer_id),
        contact_id: vm.contact.id,
      };

      vm.$store.dispatch(CUSTOMER_DELECT_CONTACT, params).then((response) => {
        if (response.status === 422) {
          let contactErrors = response.data.errors;
          if (contactErrors.sort_delete) {
            $("#messageTrashCustomer").modal('show');
          }
          vm.$snotify.error('Xóa liên hệ thất bại.', TOAST_ERROR);
          return;
        }

        if (response.data.data && response.data.data.success) {
          vm.$snotify.success('Xóa liên hệ thành công.', TOAST_SUCCESS);
          vm.$emit('deleteContactSuccess', true);
        }
      }).catch((error) => {
        vm.$snotify.error('Xóa liên hệ thất bại.', TOAST_ERROR);
        console.log(error);
      });
    }
  }
}
