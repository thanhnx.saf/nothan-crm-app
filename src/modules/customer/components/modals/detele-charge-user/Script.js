/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import {
  CUSTOMER_REMOVE_ASSIGNERS
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";

const customerMessageAssigners = {
  custom: {
    'usersSelected': {
      required: 'Không được để trống trường này.',
    },
  }
};

export default {
  props: {
    customer_checked: {
      type: Array,
      required: true
    },
    pagination: {
      type: Object,
      required: true
    },
    all_assigner_in_team: {
      type: Array,
      required: true
    }
  },

  data() {
    return {
      isSuccess: false,
      usersSelected: [],
      listAssignerObj: this.all_assigner_in_team,
      totalSuccess: 0,
      isLoader: false
    }
  },

  watch: {
    customer_checked: {
      handler() {
        let vm = this;
        let listAssignerIds = [];
        vm.listAssignerObj = [];

        if (vm.customer_checked && vm.customer_checked.length > 0) {
          vm.customer_checked.forEach(function (customer) {
            let assginersCustomer = customer.assigners;
            if (assginersCustomer) {
              assginersCustomer.forEach(function (assigner) {
                listAssignerIds[assigner] = assigner;
              })
            }
          });

          if (listAssignerIds && listAssignerIds.length > 0) {
            listAssignerIds.forEach(function (assignerId) {
              vm.listUsers.find(function (user) {
                if (user.id == assignerId) {
                  if (!vm.listAssignerObj.includes(user)) {
                    vm.listAssignerObj.push(user);
                  }
                }
              });
            });
          }
        } else {
          vm.listAssignerObj = vm.all_assigner_in_team;
        }
      },
      deep: true
    }
  },

  mounted() {
    let vm = this;
    vm.$validator.localize('en', customerMessageAssigners);
  },

  computed: {
    helpers() {
      return helpers;
    },
    listCustomer() {
      return this.customer_checked;
    },
    ...mapGetters(["listUsers", "currentTeam", "currentUser"]),
  },

  methods: {
    updateAssigner(scope) {
      let vm = this;

      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          let newAssignerSendMail = {};
          newAssignerSendMail['data'] = [];
          vm.isLoader = true;

          vm.listUsers.forEach(function (user) {
            vm.usersSelected.forEach(function (userId) {
              if (user.id === userId) {
                // Convert data send mail
                newAssignerSendMail['data'].push({
                  'email': user.email,
                  'user_name': user.full_name,
                  'team_name': vm.currentTeam.name,
                  'managment_name': vm.currentUser.full_name,
                  'customer_count': vm.customer_checked && vm.customer_checked.length > 0 ? vm.customer_checked.length : vm.pagination.total_item
                });
              }
            });
          });

          let params = {
            team_id: vm.currentTeamId,
            user_ids: vm.usersSelected,
            customer_ids: vm.customer_checked.map(customer => customer.id),
            data_mail: newAssignerSendMail['data'],
            type_mail: 9
          }

          vm.$store.dispatch(CUSTOMER_REMOVE_ASSIGNERS, params).then((response) => {
            if (response.data.data.status) {
              vm.$snotify.success('Xóa người phụ trách thành công.', TOAST_SUCCESS);
              vm.$emit('remove_assigners_success', true);
              $('#deleteMultipleChargeUser').modal('hide');
              $('#messageDeleteMultipleChargeUserSuccess').modal('show');
              vm.resetForm();
              vm.totalSuccess = response.data.data.total;
              vm.isLoader = false;
            } else {
              vm.$snotify.error('Xoá người phụ trách thất bại.', TOAST_ERROR);
            }
          }).catch(error => {
            vm.$snotify.error('Xoá người phụ trách thất bại.', TOAST_ERROR);
            console.log(error);
          });

          vm.resetForm();
        }
      })
    },
    cancelUpdate() {
      let vm = this;
      vm.isSuccess = false;
      vm.resetForm();
    },
    resetForm() {
      let vm = this;
      vm.usersSelected = [];
      vm.$validator.reset();
    }
  },
}