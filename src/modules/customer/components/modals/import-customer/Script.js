import {
  mapState
} from "vuex"
import {
  mapGetters
} from "vuex"

import {
  CUSTOMER_IMPORT,
  DOWNLOAD_EXCEL_DEFAULT,
  DOWNLOAD_EXCEL_FILE,
  CUSTOMER_IMPORT_CHECK_FILE_EXCEL
} from "@/types/actions.type"
import helpers from '@/utils/utils'

export default {
  computed: {
    ...mapGetters(["attributeCustomerFull", "getDataExportExcel", "currentTeam"]),
    ...mapState({}),
    helpers() {
      return helpers
    },
  },
  data() {
    return {
      countRowSuccess: null,
      customerFile: null,
      customerFileUpload: null,
      customerImportCheckDataResult: 1,
      customerImportErrorStatus: null,
      customerImportIsLastStep: false,
      customerImportIsLoader: false,
      customerImportIsStep2: false,
      customerImportIsSuccess: false,
      customerImportSkipping: false,
      customerImportType: 1,
      customerImportUploadMaxSize: 10 * 1024 * 1024, // 10Mb
      customerUploadOnFileChangeEdit: false,
      downloadFileExcelKey: '',
      incorrectFileFormat: false,
      isDisabled: false,
      isImportMaxRow: false,
      isImportMaxSize: false,
      isImportMinRow: false,
      isServerInsertData: true,
      messageError: '',
      totalRow: null,
    }
  },
  methods: {
    downloadFileExcelError() {
      this.$store.dispatch(DOWNLOAD_EXCEL_FILE, this.downloadFileExcelKey).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', 'KetQua_Import_KhongDat.xlsx')
        document.body.appendChild(link)
        link.click()
      }).catch(error => {
        console.log(error)
      })
    },
    downloadFileDefault() {
      let teamId = this.currentTeamId
      return this.$store.dispatch(DOWNLOAD_EXCEL_DEFAULT, teamId).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', 'Mau_Import_KhachHang.xlsx')
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
      }).catch(error => {
        console.log(error)
      })
    },

    customerUploadOnFileChange() {
      let file = this.$refs.uploadFile.files[0]

      if (file) {
        this.customerFileUpload = file
        this.customerCheckUploadFile()
      }
    },

    async customerCheckUploadFile() {
      let file = this.customerFileUpload
      this.downloadFileExcelKey = ''
      this.incorrectFileFormat = false
      this.isImportMaxRow = false
      this.isImportMinRow = false
      this.isImportMaxSize = false
      this.isDisabled = false
      this.countRowSuccess = null
      this.totalRow = null
      this.customerUploadOnFileChangeEdit = false

      this.messageError = ''
      this.customerImportIsLoader = true

      let fileName = file.name
      let fileType = fileName.replace(/^.*\./, '')

      this.customerFile = {
        name: fileName,
        type: fileType,
        size: file.size
      }

      if (this.customerFile && this.customerFile.type != "xlsx") {
        this.messageError = "Không thể tải lên các tập tin thuộc loại này. Hệ thống chỉ hỗ trợ tập tin xlsx."
        this.customerImportIsLoader = false
        this.customerImportIsLastStep = false
        this.customerImportIsSuccess = false
        this.customerImportIsStep2 = false
        this.customerImportType = 1
        this.customerImportErrorStatus = null
        this.customerImportSkipping = false
        this.customerImportCheckDataResult = 1
        this.isDisabled = true
        return
      }

      if (this.customerFile && this.customerFile.size > this.customerImportUploadMaxSize) {
        this.messageError = "File excel tải lên có dung lượng vượt quá 10Mb."
        this.isImportMaxSize = true

        this.customerImportIsLoader = false
        this.customerImportIsLastStep = false
        this.customerImportIsSuccess = false
        this.customerImportIsStep2 = false
        this.customerImportType = 1
        this.customerImportErrorStatus = null
        this.customerImportSkipping = false
        this.customerImportCheckDataResult = 1
        this.isDisabled = true
        return
      }     

      let formData = new FormData()
      formData.append('file', file);
      formData.append('team_id', this.currentTeamId);

      await this.$store.dispatch(CUSTOMER_IMPORT_CHECK_FILE_EXCEL, formData).then(response => {
        this.customerImportIsLoader = false
        if (response.status == 405) {
          this.$bus.$emit('checkAuthorAddCustomer', ' ')
          this.isDisabled = true
          return
        }

        let data = response.data.data
        if (data) {
          this.countRowSuccess = data.countSuccess
          this.totalRow = data.total
          this.downloadFileExcelKey = data.data

          if (data.status == false) {
            this.isDisabled = true
            this.customerImportIsLastStep = false
            this.customerImportIsSuccess = false
            this.customerImportIsStep2 = false
            this.customerImportType = 1
            this.customerImportErrorStatus = null
            this.customerImportSkipping = false

            if (data.code == 2) {
              this.messageError = "File excel tải lên không đúng định dạng các trường thông tin (số lượng trường, tên trường). <br /> Vui lòng chỉnh sửa đầy đủ và chính xác các trường theo file excel mẫu."
              this.incorrectFileFormat = true
              return
            } else if (data.code == 3) {
              this.messageError = "File excel tải lên vượt quá 500 dòng dữ liệu."
              this.isImportMaxRow = true
              return
            } else if (data.code == 4) {
              this.messageError = "File excel tải lên không có dữ liệu."
              this.isImportMinRow = true
              return
            }
            // Server chưa code
            // else if(data.code == 5){
            //     this.messageError = "File excel tải lên có dung lượng vượt quá 10Mb."
            //     this.isImportMaxSize = true
            //     return
            // }
          } else {
            this.isDisabled = false
          }
        }
      }).catch(error => {
        console.log(error)
      })
    },

    customerUploadOnFileEdit() {
      let file = ""
      if (this.customerImportErrorStatus == 1) {
        file = this.$refs.uploadFileOne.files[0]
      } else if (this.customerImportErrorStatus == 3) {
        file = this.$refs.uploadFileTwo.files[0]
      }

      if (file) {
        let fileName = file.name
        let fileType = fileName.replace(/^.*\./, '')

        this.customerFile = {
          name: fileName,
          type: fileType,
          size: file.size
        }

        this.customerFileUpload = file

        this.customerUploadOnFileChangeEdit = true
      }
    },

    customerUploadOnFileEditSubmit() {
      this.customerCheckUploadFile().then(() => {
        this.customerImportCheckErrorStatus()
      })
    },

    customerImportCheckDataResultFunction() {
      this.customerImportSkipping = false
      this.$refs.uploadFile.value = ''
      this.customerFile = null
    },

    customerImportStep2() {
      this.customerImportIsStep2 = true
      this.customerImportIsLoader = false
      this.$refs.uploadFile.value = ''
      this.customerFile = null

      this.customerImportCheckErrorStatus()
    },

    customerImportCheckErrorStatus() {
      this.customerImportErrorStatus = null

      if (this.countRowSuccess != undefined && this.totalRow != undefined && this.countRowSuccess != null && this.totalRow != null) {
        if (this.countRowSuccess == 0) {
          this.customerImportErrorStatus = 3
        } else if (this.countRowSuccess < this.totalRow) {
          this.customerImportErrorStatus = 1
          this.customerImportCheckDataResult = 1
        } else {
          this.customerImportErrorStatus = 2
        }
      }
    },

    customerUploadBackStep() {
      this.customerImportIsLoader = false
      this.customerImportIsStep2 = false
      this.customerImportErrorStatus = null
      this.customerImportSkipping = false
      this.customerImportCheckDataResult = 1
      this.customerFile = null
      this.customerFileUpload = null
      this.messageError = ''
      this.countRowSuccess = null
      this.totalRow = null
      this.downloadFileExcelKey = ''
      this.customerUploadOnFileChangeEdit = false
      this.$refs.uploadFile.value = ''
    },

    customerImportFile() {

      this.customerImportIsLoader = true

      let params = {
        team_id: this.currentTeamId,
        key: this.downloadFileExcelKey,
      }

      this.$store.dispatch(CUSTOMER_IMPORT, params).then((response) => {
        if (response.data.data.success) {
          this.customerImportIsLoader = false
          this.customerImportIsLastStep = false
          this.customerImportIsSuccess = true
        }
      }).catch(error => {
        console.log(error)
      })
    },

    customerUploadSuccessCloseModal() {
      window.location.reload()
    },

    customerUploadCloseModal() {
      this.customerFile = null
      this.customerImportIsLoader = false
      this.customerImportIsLastStep = false
      this.customerImportIsSuccess = false
      this.customerImportIsStep2 = false
      this.customerImportType = 1
      this.customerImportErrorStatus = null
      this.customerImportSkipping = false
      this.customerImportCheckDataResult = 1
      this.customerFileUpload = null
      this.messageError = ''
      this.countRowSuccess = null
      this.totalRow = null
      this.downloadFileExcelKey = ''
      this.customerUploadOnFileChangeEdit = false
      this.$refs.uploadFile.value = ''
    },
  },
}