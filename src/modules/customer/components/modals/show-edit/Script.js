/* eslint-disable no-undef */
import {
  mapGetters,
  mapState
} from "vuex";

import helpers from "@/utils/utils";
import AttributeWithDataType from "../../attribute/AttributeWithDataType";
import {
  CUSTOMER_UPDATE,
  CUSTOMER_CANCEL_UPDATE
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";
//import EditWarning from '@/components/modules/warning/EditWarning';
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE,
  // CUSTOMER
  CUSTOMER_DETAIL_BOX_GENERAL,
  CUSTOMER_DETAIL_BOX_BUY,
  CUSTOMER_DETAIL_BOX_USE
} from "@/types/const";
import {
  PERMISSION_UPDATE_CUSTOMER_BASE,
  PERMISSION_UPDATE_CUSTOMER_GENERAL,
  PERMISSION_UPDATE_CUSTOMER_BUYER,
  PERMISSION_UPDATE_CUSTOMER_END_USER
} from "@/types/const";

// const UPDATE_CUSTOMER_BASE_TYPE = 1;
const UPDATE_CUSTOMER_GENERAL_TYPE = 2;
// const UPDATE_CUSTOMER_BUYER_TYPE = 3;
// const UPDATE_CUSTOMER_END_USER_TYPE = 4;

export default {
  components: {
    "attribute-with-data-type": AttributeWithDataType,
    /*'edit-warning': EditWarning,*/
  },
  mounted() {
    // $('.btn-customer-edit').click(function () {

    // });
  },
  created() {
    let vm = this;
    vm.$bus.$on('customerEdit', ($event) => {
      if ($event) {
        $("#editCustomer").modal('show');
        vm.thenCustomer();
      }
    });
  },
  watch: {
    detailCustomer: {
      handler(after) {
        if (after) {
          let vm = this;
          let countChange = 0;
          vm.thenCustomer();

          if (after.attributes && after.attributes.length > 0) {
            after.attributes.forEach(function (item) {
              item.attribute_value = item.attribute_value == null || item.attribute_value == undefined ? "" : item.attribute_value;
              if (item.data_type_id == vm.isMultipleSelect || item.data_type_id == vm.isTag || item.data_type_id == vm.isCheckboxMultiple || item.data_type_id == vm.isUserMultiple) {
                if (item.attribute_value_cache) {
                  if (item.attribute_value) {
                    if (item.attribute_value.join() != item.attribute_value_cache.join()) {
                      countChange++;
                    }
                  } else if (item.attribute_value.length <= 0) {
                    countChange++;
                  }

                } else {
                  if (item.attribute_value && item.attribute_value.length > 0) {
                    countChange++;
                  }
                }
              } else if (item.data_type_id == vm.isNumber) {

                let attribute_value = item.attribute_value && attribute_value !== "0,00" ? parseFloat(item.attribute_value).toFixed(2).replace(".", ",") : '';
                if (item.attribute_value != item.attribute_value_cache) {
                  // if ( attribute_value != attribute_value_cache && item.attribute_value != attribute_value_cache) {
                  countChange++;
                }
              } else {
                if (item.attribute_value != item.attribute_value_cache) {
                  countChange++;
                }
              }
            });
          }

          if (after.assigners_cache && after.assigners_cache.length > 0) {
            if (after.assigners && after.assigners.length > 0) {
              if (after.assigners.join() != after.assigners_cache.join()) {
                countChange++;
              }
            } else if (after.assigners.length <= 0) {
              countChange++;
            }
          } else {
            if (after.assigners && after.assigners.length > 0) {
              countChange++;
            }
          }

          if (countChange) {
            vm.isChange = true;
          } else {
            vm.isChange = false;
          }
        }
      },
      deep: true
    },
    errors: {
      handler(after) {
        let vm = this;
        if (after.any()) {
          vm.isChange = true;
        }
      },
      deep: true
    }
  },
  computed: {
    ...mapGetters(["detailCustomer", "getErrors", "listUsers", "userCheckAddOn", "userCurrentPermissions"]),
    ...mapState({
      currentTeam: state => state.organization.currentTeam,
    }),
    helpers() {
      return helpers;
    },
  },
  data() {
    return {
      PERMISSION_UPDATE_CUSTOMER_BASE: PERMISSION_UPDATE_CUSTOMER_BASE,
      PERMISSION_UPDATE_CUSTOMER_GENERAL: PERMISSION_UPDATE_CUSTOMER_GENERAL,
      PERMISSION_UPDATE_CUSTOMER_BUYER: PERMISSION_UPDATE_CUSTOMER_BUYER,
      PERMISSION_UPDATE_CUSTOMER_END_USER: PERMISSION_UPDATE_CUSTOMER_END_USER,

      attributeInfoBasic: [],
      attributeInfoGeneral: [],
      attributeInfoBuy: [],
      attributeInfoUse: [],

      server_errors: '',
      isSuccess: false,
      isLoader: false,
      assigners: [],
      messageErrorServer: '',
      isChange: false,
      isCancel: false,
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,

      CUSTOMER_DETAIL_BOX_GENERAL: CUSTOMER_DETAIL_BOX_GENERAL,
      CUSTOMER_DETAIL_BOX_BUY: CUSTOMER_DETAIL_BOX_BUY,
      CUSTOMER_DETAIL_BOX_USE: CUSTOMER_DETAIL_BOX_USE,

      isChangeDataToServer: false
    }
  },
  methods: {
    thenCustomer() {
      let vm = this;

      if (vm.detailCustomer.attributes && vm.detailCustomer.attributes.length > 0) {
        vm.attributeInfoBasic = [];
        vm.attributeInfoGeneral = [];
        vm.attributeInfoBuy = [];
        vm.attributeInfoUse = [];

        vm.detailCustomer.attributes.forEach(function (item) {
          if (!item.is_hidden || item.is_hidden == 0) {
            if (item.show_position == vm.CUSTOMER_DETAIL_BOX_GENERAL) {
              let attributeInfoBasic = ['full_name', 'phone', 'email', 'address'];
              if (attributeInfoBasic.indexOf(item.attribute_code) !== -1) {
                if (vm.accessible(vm.userCheckAddOn, 'customer', vm.PERMISSION_UPDATE_CUSTOMER_BASE, vm.userCurrentPermissions)) {
                  item.update_type = item.show_position ? item.show_position : 1;
                  vm.attributeInfoBasic.push(item);
                }
              } else {
                let hideAttribute = ['latest_note', 'sku', 'avatar', 'customer_type', 'level', 'total_product_when_enduser', 'total_price_when_buyer', 'total_product_when_buyer', 'total_payment_amount'];
                if (hideAttribute.indexOf(item.attribute_code) !== -1) {
                  return;
                }
                if (vm.accessible(vm.userCheckAddOn, 'customer', vm.PERMISSION_UPDATE_CUSTOMER_GENERAL, vm.userCurrentPermissions)) {
                  item.update_type = item.show_position ? item.show_position : 1;
                  vm.attributeInfoGeneral.push(item);
                }
              }
            } else if (item.show_position == vm.CUSTOMER_DETAIL_BOX_BUY && vm.accessible(vm.userCheckAddOn, 'customer', vm.PERMISSION_UPDATE_CUSTOMER_BUYER, vm.userCurrentPermissions)) {
              item.update_type = item.show_position ? item.show_position : 1;
              vm.attributeInfoBuy.push(item);
            } else if (item.show_position == vm.CUSTOMER_DETAIL_BOX_USE && vm.accessible(vm.userCheckAddOn, 'customer', vm.PERMISSION_UPDATE_CUSTOMER_END_USER, vm.userCurrentPermissions)) {
              item.update_type = item.show_position ? item.show_position : 1;
              vm.attributeInfoUse.push(item);
            }
          }
        });
      }
    },

    reset() {
      let vm = this;

      vm.detailCustomer.attributes = vm.detailCustomer.attributes_cache;
      vm.detailCustomer.assigners = vm.detailCustomer.assigners_cache;

      vm.$store.dispatch(CUSTOMER_CANCEL_UPDATE, vm.detailCustomer);
      // vm.$store.dispatch(CUSTOMER_SHOW_DETAIL, vm.detailCustomer.id);
      vm.thenCustomer();

      vm.resetEditCustomer();
    },

    resetEditCustomer() {
      let vm = this;
      vm.isSuccess = false;
      vm.isChangeDataToServer = false;
      vm.$validator.reset();
      vm.server_errors = '';
      vm.messageErrorServer = '';
      $(".s--modal .modal-body").animate({
        scrollTop: 0
      }, 0);
      $('.s--portlet-collapse').removeClass('m-portlet--collapse');
      $('.s--portlet-collapse .m-form').hide();
      $('body').find('.s--portlet-info-contact .m-form').show();
      $('body').find('.s--portlet-info-basic').addClass('m-portlet--collapse');
      $('body').find('.s--portlet-info-custom').addClass('m-portlet--collapse');
    },

    update(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;

          // Get data send mail
          let assignersSendMail = [];

          if (vm.detailCustomer.assigners_cache && vm.detailCustomer.assigners_cache.length > 0) {
            vm.detailCustomer.assigners.forEach(function (assigners_id) {
              if (vm.detailCustomer.assigners >= vm.detailCustomer.assigners_cache) {
                if ($.inArray(assigners_id, vm.detailCustomer.assigners_cache) == -1) {
                  assignersSendMail.push(assigners_id);
                }
              }
            });
          } else {
            vm.detailCustomer.assigners.forEach(function (id) {
              assignersSendMail.push(id);
            });
          }

          let newAssignerSendMail = {};
          newAssignerSendMail['data'] = [];

          let customerName = "";

          if (vm.detailCustomer.attributes && vm.detailCustomer.attributes.length > 0) {
            vm.detailCustomer.attributes.forEach(function (item) {
              if (item.attribute_code == "full_name") {
                customerName = item.attribute_value;
              }
            });
          }

          vm.listUsers.forEach(function (item) {
            assignersSendMail.forEach(function (userId) {
              if (item.id === userId) {
                newAssignerSendMail['data'].push({
                  email: item.email,
                  user_name: item.full_name,
                  customer_name: customerName,
                  team_name: vm.currentTeam.name
                });
              }
            });
          });

          let detailCustomer = vm.detailCustomer;
          detailCustomer = vm.checkUpdatePushToServe(detailCustomer);

          detailCustomer.data_mail = newAssignerSendMail['data'];
          detailCustomer.type_mail = 6;

          vm.$store.dispatch(CUSTOMER_UPDATE, detailCustomer).then(response => {

            if (response.data.status_code && response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              vm.isLoader = false;
              vm.isChange = true;

              if (vm.server_errors["attribute_not_exist"] || vm.server_errors["option_not_exist"]) {
                vm.isChangeDataToServer = true;
                $(".s--modal .modal-body").animate({
                  scrollTop: 0
                }, 300);
              } else {
                vm.isChangeDataToServer = false;
                vm.$snotify.error('Cập nhật khách hàng thất bại.', TOAST_ERROR);
                setTimeout(function () {
                  $(".m-form .m-form__group.has-danger").each(function () {
                    $(".m-form .m-form__group.has-danger").parent().parent().parent().collapse('show');
                  });
                  let scroll_top = $(".m-form .m-form__group.has-danger").offset().top;
                  if (scroll_top >= 70) {
                    $(".modal-body").animate({
                      scrollTop: scroll_top - 70
                    }, 300);
                  }
                }, 100);
              }


            } else {
              if (response.data.data && response.data.data.status) {
                vm.isChange = false;
                vm.isSuccess = true;
                vm.isChangeDataToServer = false;
                vm.server_errors = '';
                vm.$emit("update_success", true);
                vm.$snotify.success('Cập nhật khách hàng thành công.', TOAST_SUCCESS);
                vm.isLoader = false;
                $(".s--modal .modal-body").animate({
                  scrollTop: 0
                }, 300);

                // if(newAssignerSendMail && newAssignerSendMail['data'] && newAssignerSendMail['data'].length > 0){
                //     vm.$store.dispatch(CUSTOMER_SEND_MAIL_ASSIGNERS, newAssignerSendMail);
                // }
              } else {
                if (response.data.data.sort_delete) {
                  vm.$snotify.error('Cập nhật khách hàng thất bại.', TOAST_ERROR);
                  $("#messageTrashCustomer").modal('show');
                }
              }
            }

            setTimeout(function () {
              vm.isSuccess = false;
            }, 5000);
          }).catch(() => {
            vm.isLoader = false;
            vm.isChange = true;
            // vm.messageErrorServer = 'Lỗi, vui lòng thử lại sau.';
            $(".s--modal .modal-body").animate({
              scrollTop: 0
            }, 300);
            vm.$snotify.error('Cập nhật khách hàng thất bại.', TOAST_ERROR);
            // vm.server_errors = error.data.errors;
          });
        } else {
          $(".m-form .m-form__group.has-danger").each(function () {
            $(this).parent().parent().parent().collapse('show');
          });
          let scroll_top = $(".m-form .m-form__group.has-danger").offset().top;
          if (scroll_top >= 70) {
            $(".modal-body").animate({
              scrollTop: scroll_top - 70
            }, 300);
          }
        }
      });
    },

    convertNumberDecimal(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        newValue = parseFloat(newValue.replace(",", "."));
      }
      return newValue;
    },

    checkUpdatePushToServe(data) {
      let vm = this;
      let newData = {};
      newData.id = data.id;
      newData.user_id = data.user_id;
      newData.team_id = data.team_id;
      newData.is_active = data.is_active;
      newData['attributes'] = [];
      newData['assigners'] = data.assigners;
      newData.update_type = [];

      if (data.attributes && data.attributes.length > 0) {
        data.attributes.forEach(function (item) {
          if (!item.is_hidden || item.is_hidden == 0) {
            item.attribute_value = item.attribute_value == null || item.attribute_value == undefined ? "" : item.attribute_value;

            item.is_attribute_option = 0;

            let hideAttribute = ['latest_note', 'sku', 'avatar', 'customer_type', 'level', 'total_product_when_enduser', 'total_price_when_buyer', 'total_product_when_buyer', 'total_payment_amount'];
            if (hideAttribute.indexOf(item.attribute_code) !== -1) {
              return;
            }

            if (item.data_type_id == vm.isSelect || item.data_type_id == vm.isSelectFind || item.data_type_id == vm.isRadio || item.data_type_id == vm.isMultipleSelect || item.data_type_id == vm.isCheckboxMultiple || item.data_type_id == vm.isTag) {
              item.is_attribute_option = 1
            }

            // Convert data
            if (item.data_type_id == vm.isPhone) {
              item.attribute_value = item.attribute_value ? item.attribute_value.replace(/ /g, "") : '';
            }
            if (item.data_type_id == vm.isDate) {
              item.attribute_value = item.attribute_value ? helpers.formatDateTime(item.attribute_value) : '';
            }
            if (item.data_type_id == vm.isDateTime) {
              item.attribute_value = item.attribute_value ? helpers.formatDateTime(item.attribute_value, true) : '';
            }
            if (item.data_type_id == vm.isNumber) {
              item.attribute_value = item.attribute_value ? vm.convertNumberDecimal(item.attribute_value) : '';
            }

            newData['attributes'].push(item);
          }
        });
      }

      // check data change
      let dataChange = [];
      let updateType = {};
      let convertUpdateType = [];

      if (data.attributes && data.attributes.length > 0) {
        data.attributes.forEach(function (item) {
          item.attribute_value = item.attribute_value == null || item.attribute_value == undefined ? "" : item.attribute_value;
          if (item.data_type_id == vm.isMultipleSelect || item.data_type_id == vm.isTag || item.data_type_id == vm.isCheckboxMultiple || item.data_type_id == vm.isUserMultiple) {
            if (item.attribute_value_cache) {
              if (item.attribute_value) {
                if (item.attribute_value.join() != item.attribute_value_cache.join()) {
                  dataChange.push(item);
                }
              } else if (item.attribute_value.length <= 0) {
                dataChange.push(item);
              }

            } else {
              if (item.attribute_value && item.attribute_value.length > 0) {
                dataChange.push(item);
              }
            }
          } else if (item.data_type_id == vm.isNumber) {
            let attribute_value = item.attribute_value && attribute_value !== "0,00" ? parseFloat(item.attribute_value).toFixed(2).replace(".", ",") : '';

            if (item.attribute_value != item.attribute_value_cache) {
              dataChange.push(item);
            }
          } else {
            if (item.attribute_value != item.attribute_value_cache) {
              dataChange.push(item);
            }
          }
        });
      }

      if (data.assigners_cache && data.assigners_cache.length > 0) {
        if (data.assigners && data.assigners.length > 0) {
          if (data.assigners.join() != data.assigners_cache.join()) {
            if (!updateType[UPDATE_CUSTOMER_GENERAL_TYPE]) {
              updateType[UPDATE_CUSTOMER_GENERAL_TYPE] = UPDATE_CUSTOMER_GENERAL_TYPE;
            }
          }
        } else if (data.assigners.length <= 0) {
          if (!updateType[UPDATE_CUSTOMER_GENERAL_TYPE]) {
            updateType[UPDATE_CUSTOMER_GENERAL_TYPE] = UPDATE_CUSTOMER_GENERAL_TYPE;
          }
        }
      } else {
        if (data.assigners && data.assigners.length > 0) {
          if (!updateType[UPDATE_CUSTOMER_GENERAL_TYPE]) {
            updateType[UPDATE_CUSTOMER_GENERAL_TYPE] = UPDATE_CUSTOMER_GENERAL_TYPE;
          }
        }
      }

      if (dataChange && dataChange.length > 0) {
        dataChange.forEach(function (item) {
          if (!updateType[item.update_type]) {
            updateType[item.update_type] = item.update_type;
          }
        });
      }

      if (Object.keys(updateType).length > 0) {
        Object.keys(updateType).forEach(function (type) {
          convertUpdateType.push(type);
        });
      }

      newData.update_type = convertUpdateType;

      return newData;
    },

    showCollapse(event) {
      let thisElement = event.currentTarget;

      let icon = $(thisElement).children(".m-portlet__head").children("div").children(".m-portlet__head-tools").children(".m-portlet__nav").children(".m-portlet__nav-item").children(".m-portlet__nav-link--icon").children(".s--icon-arrow");
      if (icon.hasClass("la-angle-down")) {
        icon.removeClass("la-angle-down");
        icon.addClass("la-angle-up");
      } else {
        icon.removeClass("la-angle-up");
        icon.addClass("la-angle-down");
      }
    },

    isWarningSubmit(isSubmit) {
      let vm = this;
      if (isSubmit) {
        vm.isChange = false;
        vm.isCancel = false;
        vm.resetEditCustomer();
        $('#edit_customer').modal("hide");
        $('body').removeClass('modal-open-fix');
      }
    },

    isWarningCancel(isCancel) {
      let vm = this;
      if (isCancel) {
        vm.isChange = true;
        vm.isCancel = false;
      }
    },
  }
};