/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import { CUSTOMER_TRASH } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  props: {
    selected_ids: {
      type: Array,
      required: true,
      default: []
    },
    total_deleted: {
      type: Number,
      required: true,
      default: 0
    },
    is_deleted_multiple: {
      type: Boolean,
      default: false
    },
    customer_name: {
      type: String,
      default: ''
    },
  },

  data() {
    return {
    }
  },

  watch: {
  },

  mounted() {
  },

  computed: {
    ...mapGetters(["currentTeam"]),
  },

  methods: {
    closeModal() {
      $('.dropdown-backdrop').removeClass('show')
    },
    trash() {
      let vm = this;
      let params = {};
      params.team_id = vm.currentTeamId;
      params.customer_id = vm.selected_ids;

      vm.$store.dispatch(CUSTOMER_TRASH, params).then((response) => {
        if (response.data.data.success) {
          vm.$snotify.success('Xóa tạm khách hàng thành công.', TOAST_SUCCESS);
          vm.$emit('is_change', true)
          vm.closeModal()
        }
      }).catch(() => {
        vm.$snotify.error('Xóa tạm khách hàng thất bại.', TOAST_ERROR);
      });
    }
  },
}
