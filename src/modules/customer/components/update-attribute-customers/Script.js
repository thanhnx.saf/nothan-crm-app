/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex"

import {
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_DATE,
  IS_DATE_TIME,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER_MULTIPLE
} from "@/types/const"

import {} from "@/types/config"

import {
  CHECK_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS,
  UPDATE_ATTRIBUTE_CUSTOMERS,
  CHECK_VALIDATE_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS,
  USER_GET_LIST_BY_TEAM
} from "@/types/actions.type"

import helpers from '@/utils/utils'

import GetAttributeByDataTypeId from "../get-attribute-by-data-type-id/GetAttributeByDataTypeId.vue";

export default {
  components: {
    GetAttributeByDataTypeId
  },

  mounted() {},

  created() {
    let vm = this;
    vm.$bus.$on('updateAttributeCustomers', (data) => {
      if (data) {
        vm.customerIds = []
        let currentParams = data
        vm.currentParams = currentParams
        if (data && data.customers.length) {
          vm.customerIds = data.customers.map((item) => item.id)
        }
        vm.checkBeforeUpdateAttributeCustomers()
      }
    })
  },

  computed: {
    ...mapGetters(['currentTeam']),
    helpers() {
      return helpers
    },
    labelCurrentAttribute() {
      let attributeDataTypeId = this.currentAttribute && this.currentAttribute.data_type ? this.currentAttribute.data_type.id : ''
      if (attributeDataTypeId && [IS_TEXT, IS_TEXT_AREA, IS_EMAIL, IS_PHONE, IS_NUMBER, IS_PRICE].includes(attributeDataTypeId)) {
        return "Nhập"
      }
      return "Chọn"
    },
    isDisabled() {
      if (this.isLoader || this.errors.any() || !this.currentAttribute || this.isLoaderCheckBeforeValidate) {
        return true
      }

      return false
    }
  },

  data() {
    return {
      isLoader: false,
      customerIds: [],
      currentParams: {},
      totalCustomers: 0,
      currentAttribute: '',
      scope: 'updateAttributeCustomers',
      server_errors: '',
      attributes: [],
      isLoaderChangeAttribute: false,
      isLoaderCheckBeforeValidate: false,
      isReloadPage: false
    };
  },

  methods: {
    async getAttributeAndTotalCustomer() {
      let vm = this
      let params = {
        team_id: vm.currentTeamId,
        customer_ids: vm.customerIds,
        search: vm.currentParams.search,
        filter: vm.currentParams.filter
      }

      vm.totalCustomers = 0
      vm.attributes = []

      await vm.$store.dispatch(CHECK_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS, params).then((res) => {
        if (res.data.data) {
          vm.totalCustomers = res.data.data.total_customers
          vm.attributes = res.data.data.attributes
          vm.attributes.forEach((attribute) => {
            if (attribute.data_type_id === IS_MULTIPLE_SELECT ||
              attribute.data_type_id === IS_CHECKBOX_MULTIPLE ||
              attribute.data_type_id === IS_TAG ||
              attribute.data_type_id === IS_USER_MULTIPLE
            ) {
              attribute.attribute_value = attribute.attribute_value ? attribute.attribute_value : []
            }
          })

        }
      })
    },

    async checkBeforeUpdateAttributeCustomers() {
      await this.getAttributeAndTotalCustomer()

      this.isReloadPage = false
      if (this.totalCustomers === 0) {
        $('#updateAttributeCustomers').modal('hide')
        $('#confirmUpdateAttributeCustomers').modal('hide')
        $('#messageUpdateAttributeCustomersError').modal('show')
        this.isReloadPage = true
        return
      }

      $('#updateAttributeCustomers').modal('show')
    },

    changeSelectAttribute() {
      this.isLoader = true

      setTimeout(() => {
        this.isLoader = false
        this.server_errors = ''
        this.$bus.$emit('changeUpdateAttributeCustomers', true)
      }, 300)
    },

    cancelUpdateAttributeCustomers() {
      this.isLoader = false
      this.currentAttribute = ''
      this.$validator.reset()
      this.server_errors = ''
      this.$forceUpdate()
    },

    async checkValidateBeforeUpdateAttributeCustomers(scope) {
      let vm = this
      await vm.$validator.validateAll(scope).then(async () => {
        if (!vm.errors.any()) {
          let params = this.setParamsUpdate()

          vm.isLoaderCheckBeforeValidate = true

          await vm.$store.dispatch(CHECK_VALIDATE_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS, params).then(async (res) => {
            if (res.data.data.is_send) {

              await vm.getAttributeAndTotalCustomer()

              vm.isReloadPage = false
              if (vm.totalCustomers === 0) {
                $('#updateAttributeCustomers').modal('hide')
                $('#confirmUpdateAttributeCustomers').modal('hide')
                $('#messageUpdateAttributeCustomersError').modal('show')
                vm.isReloadPage = true
                return
              }

              $('#updateAttributeCustomers').modal('hide')
              $('#confirmUpdateAttributeCustomers').modal('show')
            } else {
              vm.server_errors = res.data.data.errors
            }
            vm.isLoaderCheckBeforeValidate = false
          })

        }
      })
    },

    setParamsUpdate() {
      let vm = this
      // Convert attribute value
      if (vm.currentAttribute.data_type_id === IS_PHONE) {
        vm.currentAttribute.attribute_value = vm.currentAttribute.attribute_value ? vm.currentAttribute.attribute_value.replace(/ /g, "") : ''
      }

      if (vm.currentAttribute.data_type_id === IS_DATE) {
        vm.currentAttribute.attribute_value = vm.currentAttribute.attribute_value ? vm.helpers.formatDateTime(vm.currentAttribute.attribute_value) : ''
      }

      if (vm.currentAttribute.data_type_id === IS_DATE_TIME) {
        vm.currentAttribute.attribute_value = vm.currentAttribute.attribute_value ? vm.helpers.formatDateTime(vm.currentAttribute.attribute_value, true) : ''
      }

      if (vm.currentAttribute.data_type_id === IS_NUMBER) {
        vm.currentAttribute.attribute_value = vm.currentAttribute.attribute_value ? vm.helpers.convertNumberDecimal(vm.currentAttribute.attribute_value) : ''
      }

      let attribute = {
        attribute_id: vm.currentAttribute.id,
        attribute_code: vm.currentAttribute.attribute_code,
        attribute_value: vm.currentAttribute.attribute_value ? vm.currentAttribute.attribute_value : ''
      }

      let params = {
        team_id: vm.currentTeamId,
        customer_ids: vm.customerIds,
        search: vm.currentParams.search,
        filter: vm.currentParams.filter,
        attribute: attribute
      }
      return params
    },

    async updateAttributeCustomers() {
      let vm = this

      let params = this.setParamsUpdate()

      vm.isLoader = true

      await vm.getAttributeAndTotalCustomer()

      vm.isReloadPage = false
      if (vm.totalCustomers === 0) {
        $('#updateAttributeCustomers').modal('hide')
        $('#confirmUpdateAttributeCustomers').modal('hide')
        $('#messageUpdateAttributeCustomersError').modal('show')
        vm.isReloadPage = true
        return
      }

      await vm.$store.dispatch(UPDATE_ATTRIBUTE_CUSTOMERS, params).then((res) => {
        vm.isLoader = false;

        if (res.data.data.is_send) {
          $('#confirmUpdateAttributeCustomers').modal('hide')
          vm.cancelUpdateAttributeCustomers()
          vm.$emit('update_attribute_customers_success', true)
          return
        }

        $('#confirmUpdateAttributeCustomers').modal('hide')
        $('#messageUpdateAttributeCustomersError').modal('show')
        vm.cancelUpdateAttributeCustomers()
        return
      })
    },

    async reloadGetListAttribute(isSuccess) {
      if (isSuccess) {
        await this.$store.dispatch(USER_GET_LIST_BY_TEAM)
        this.isLoader = false
        this.currentAttribute = ''
        this.$validator.reset()
        this.getAttributeAndTotalCustomer()
        this.$forceUpdate()
      }
    },

    reloadPage(isReload) {
      if (isReload) {
        return window.location.reload()
      }
    }
  }
}