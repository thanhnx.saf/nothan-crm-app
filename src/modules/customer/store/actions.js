import customerAPI from "@/services/customer";
import FileAPI from "@/services/file";
import {
  CustomerVM
} from "../transformers/index";
import {
  FileVM
} from "@/modules/file/transformers/index";
import {
  CUSTOMER_GET_ALL,
  CUSTOMER_GET_ATTRIBUTES,
  CUSTOMER_GET_USER_ATTRIBUTES,
  CUSTOMER_IMPORT_CHECK_ATTRIBUTES_UNIQUE,
  CUSTOMER_SHOW_DETAIL,
  CUSTOMER_IMPORT,
  CUSTOMER_UPDATE,
  CUSTOMER_CANCEL_UPDATE,
  CUSTOMER_CREATE,
  CUSTOMER_ADD_NEW_CHECK_EXIST,
  CUSTOMER_SEND_MAIL_ASSIGNERS,
  CUSTOMER_UPDATE_ASSIGNERS,
  CUSTOMER_REMOVE_ASSIGNERS,
  CUSTOMER_GET_ALL_ASSIGNER_IN_TEAM,
  CUSTOMER_GET_FILTERS,
  CUSTOMER_GET_FILTER_ATTRIBUTES,
  CUSTOMER_FILTER_CREATE,
  CUSTOMER_FILTER_UPDATE,
  CUSTOMER_FILTER_DELETE,
  CUSTOMER_GET_DATA_FOR_EXCEL,
  CUSTOMER_SEND_MAIL_ADD_MULTIPLE_ASSIGNERS,
  CUSTOMER_SEND_MAIL_DELETE_MULTIPLE_ASSIGNERS,
  CUSTOMER_GET_ATTRIBUTE_OPTION,
  CUSTOMER_SAVE_ATTRIBUTE_OPTION,
  CUSTOMER_GET_ATTRIBUTE_DATA_TYPE,
  CUSTOMER_CREATE_ATTRIBUTE,
  CUSTOMER_ADD_ATTRIBUTE_CHECK_EXIST,
  CUSTOMER_DELETE_ATTRIBUTE,
  CUSTOMER_UPDATE_ATTRIBUTE,
  CUSTOMER_SHOW,
  DOWNLOAD_EXCEL_DEFAULT,
  DOWNLOAD_EXCEL_FILE,
  DOWNLOAD_EXCEL_FILE_CACHE,
  CUSTOMER_GET_CONTACT,
  CUSTOMER_DELECT_CONTACT,
  CUSTOMER_GET_ALL_CUSTOMER_CONTACT_BY_TEAM,
  CUSTOMER_ADD_CONTACT,
  CUSTOMER_UPDATE_CONTACT,
  CUSTOMER_IMPORT_CHECK_FILE_EXCEL,
  GET_CONFIG_CUSTOMER_ATTRIBUTE_TABLE,
  CUSTOMER_TRASH,
  CUSTOMER_RESTORE,
  CUSTOMER_CHECK_HAS_DEAL_BEFORE_DELETE,
  GET_CUSTOMER_FLEXIBLE_REPORT,
  GET_CUSTOMER_CHARGE_PERSON_REPORT,
  EXPORT_CUSTOMER_FLEXIBLE_REPORT,
  EXPORT_CUSTOMER_CHARGE_PERSON_REPORT,
  SAVE_CUSTOMER_CONFIG_REPORT_FOR_USER,
  GET_CUSTOMER_CONFIG_REPORT_FOR_USER,
  CUSTOMER_GET_TOP_HARD_REPORT,
  CUSTOMER_GET_INFO_HARD_CHART,
  CUSTOMER_UPDATE_LEVEL,
  CHANGE_TABLE_ATTRIBUTE_CUSTOMER,
  GET_COUNT_CUSTOMER_BY_FILTER,
  GET_CONFIG_CUSTOMER,
  CHECK_BEFORE_SEND_EMAIL_CUSTOMERS,
  SEND_EMAIL_CUSTOMERS,
  CHECK_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS,
  UPDATE_ATTRIBUTE_CUSTOMERS,
  CHECK_VALIDATE_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS,
  GET_TOTAL_CUSTOMERS_PHONE_NUMBER,
  VALIDATE_SMS_SEND_CUSTOMERS,
  SEND_SMS_CUSTOMERS,
  GET_LIST_FILE_CONVERT_TABLE,
  LIST_FILE_IMPORT,
  DOWNLOAD_FILE_IMPORT
} from "@/types/actions.type";
import {
  SET_CUSTOMERS,
  SET_CUSTOMER_ATTRIBUTES,
  SET_CUSTOMER_USER_ATTRIBUTES,
  SET_DETAIL_CUSTOMER,
  SET_DETAIL_CUSTOMER_AVATAR,
  SET_CONTACT_MAIN_AND_LEVEL,
  SET_ERROR,
  SET_CUSTOMER_FILTERS,
  SET_CUSTOMER_FILTER_ATTRIBUTES,
  SET_CUSTOMER_ATTRIIBUTE_OPTIONS,
  SET_CUSTOMER_ATTRIBUTE_DATA_TYPE,
  SET_CUSTOMER_CONTACTS,
  SET_CUSTOMER_CONTACTS_OF_TEAM,
  SET_CUSTOMER_FILTERS_CREATE,
  SET_CUSTOMER_FILTERS_UPDATE,
  SET_CUSTOMER_FILTERS_DELETE,
  SET_CUSTOMER_TOP_HARD_REPORT,
  SET_CUSTOMER_CONFIG_ATTRIBUTE_TABLE,
  SET_CUSTOMER_CONFIG,
} from "@/types/mutations.type";
import {ASK_UPLOAD_FILE, CHECK_FILE_CONVERT} from "../../../types/actions.type";

export const actions = {
  async [GET_COUNT_CUSTOMER_BY_FILTER](ontext, params) {
    try {
      let response = await customerAPI.getCountCustomerByFilter(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_ALL]({
    commit
  }, params) {
    try {
      let customers = await customerAPI.getAll(params);
      if (customers.data.data) {
        commit(SET_CUSTOMERS, CustomerVM.listVM.transform(customers.data.data, customers.data.meta));
      }
      return customers;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e;
    }
  },

  async [DOWNLOAD_EXCEL_DEFAULT](context, team_id) {
    try {
      return await customerAPI.dowloadDefault(team_id);
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_IMPORT_CHECK_FILE_EXCEL](context, params) {
    try {
      return await customerAPI.importCheckFileExcel(params);
    } catch (e) {
      return e;
    }
  },

  async [DOWNLOAD_EXCEL_FILE_CACHE](context, param) {
    try {
      return await customerAPI.dowloadFileAfterChangeCache(param);
    } catch (e) {
      return e;
    }
  },

  async [DOWNLOAD_EXCEL_FILE](context, key) {
    try {
      return await customerAPI.dowloadFileAfterChange(key);
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_ATTRIBUTES]({
    commit
  }) {
    try {
      let customerAttributes = await customerAPI.getAttributes();
      commit(SET_CUSTOMER_ATTRIBUTES, CustomerVM.attributeVM.transform(customerAttributes.data.data));
      return customerAttributes;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e.response;
    }
  },

  async [CUSTOMER_GET_USER_ATTRIBUTES]({
    commit
  }) {
    try {
      let userAttributes = await customerAPI.getAttributeByUser();
      commit(SET_CUSTOMER_USER_ATTRIBUTES, CustomerVM.userAttributeVM.transform(userAttributes.data.data));
      return userAttributes;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e.response;
    }
  },
  async [CUSTOMER_UPDATE]({
    commit
  }, params) {
    params = CustomerVM.updateVM.transform(params);
    try {
      let customerUpdate = await customerAPI.update(params.id, params);
      return customerUpdate;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e.response;
    }
  },

  [CUSTOMER_CANCEL_UPDATE]({
    commit
  }, params) {
    commit(SET_DETAIL_CUSTOMER, CustomerVM.showVM.transform(params));
    commit(SET_CONTACT_MAIN_AND_LEVEL, CustomerVM.showVM.transform(params));
    return CustomerVM.showVM.transform(params);
  },

  async [CUSTOMER_SEND_MAIL_ASSIGNERS]({
    commit
  }, params) {
    try {
      let customerSendMailAssigners = await customerAPI.sendMailAssignersCustomer(params);
      return customerSendMailAssigners;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e.response;
    }
  },

  async [CUSTOMER_SEND_MAIL_ADD_MULTIPLE_ASSIGNERS]({
    commit
  }, params) {
    try {
      let customerSendMailAssigners = await customerAPI.sendMailAddMultipleAssigners(params);
      return customerSendMailAssigners;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e.response;
    }
  },

  async [CUSTOMER_SEND_MAIL_DELETE_MULTIPLE_ASSIGNERS]({
    commit
  }, params) {
    try {
      let customerSendMailAssigners = await customerAPI.sendMailDeleteMultipleAssigners(params);
      return customerSendMailAssigners;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e.response;
    }
  },

  async [CUSTOMER_GET_FILTERS]({
    commit
  }, params) {
    try {
      let customerFilters = await customerAPI.getFilters(params);
      if (customerFilters.data.data) {
        commit(SET_CUSTOMER_FILTERS, CustomerVM.filterVM.transform(customerFilters.data.data));
      }
      return customerFilters;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e
    }
  },

  async [CUSTOMER_GET_FILTER_ATTRIBUTES]({
    commit
  }, filterId) {
    try {
      let customerFilterAttributes = await customerAPI.getAttributesByFilter(filterId);
      if (customerFilterAttributes.data.data) {
        commit(SET_CUSTOMER_FILTER_ATTRIBUTES, CustomerVM.filterAttributeVM.transform(customerFilterAttributes.data.data));
      }
      return customerFilterAttributes;
    } catch (e) {
      commit(SET_ERROR, e.response);
      return e
    }
  },

  async [CUSTOMER_FILTER_CREATE]({
    commit
  }, params) {
    try {
      let createCustomerFilter = await customerAPI.createFilter(params);
      if (createCustomerFilter.data.data) {
        commit(SET_CUSTOMER_FILTERS_CREATE, CustomerVM.createFilterVM.transform(createCustomerFilter.data.data));
      }
      return createCustomerFilter;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_FILTER_UPDATE]({
    commit
  }, params) {
    try {
      let updateCustomerFilter = await customerAPI.updateFilter(params);
      if (updateCustomerFilter.data.data) {
        commit(SET_CUSTOMER_FILTERS_UPDATE, CustomerVM.updateFilterVM.transform(updateCustomerFilter.data.data));
      }
      return updateCustomerFilter;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_FILTER_DELETE]({
    commit
  }, params) {
    try {
      let filter_id = params.filter_id;
      delete params.filter_id;
      let deleteCustomerFilter = customerAPI.deleteFilter(filter_id, params);
      commit(SET_CUSTOMER_FILTERS_DELETE, filter_id);
      return deleteCustomerFilter;
    } catch (e) {
      return e;
    }
  },

  updateAttributebyUser(params) {
    return new Promise((resolve, reject) => {
      customerAPI.updateAttributebyUser(params).then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },

  async [CUSTOMER_SHOW_DETAIL]({
    commit
  }, id) {
    try {
      let customer = await customerAPI.show(id);

      let imageId = 0;
      customer.data.data.attributes.forEach(function (attribute) {
        if (attribute.attribute_code == "avatar" && attribute.attribute_value != "") {
          imageId = attribute.attribute_value;
        }
      })
      if (imageId > 0) {
        let image = await FileAPI.show(imageId);
        commit(SET_DETAIL_CUSTOMER_AVATAR, FileVM.ShowVM.transform(image.data.data));
      }
      let customerData = customer.data.data;

      commit(SET_DETAIL_CUSTOMER, CustomerVM.showVM.transform(customerData));
      commit(SET_CONTACT_MAIN_AND_LEVEL, CustomerVM.showVM.transform(customerData));
      return customer;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_SHOW](context, params) {
    try {
      let customer = await customerAPI.showCustomer(params);
      return customer;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_CONTACT]({
    commit
  }, params) {
    try {
      let listContacts = await customerAPI.getContacts(params);
      commit(SET_CUSTOMER_CONTACTS, listContacts.data.data);
      return listContacts;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_DELECT_CONTACT](context, params) {
    try {
      return await customerAPI.deleteContact(params);
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_ALL_CUSTOMER_CONTACT_BY_TEAM]({
    commit
  }, params) {
    try {
      let customerContactOfTeam = await customerAPI.getAllCustomerContacts(params);

      let customers = [];
      if (customerContactOfTeam && customerContactOfTeam.data.data && customerContactOfTeam.data.data.length > 0) {
        customerContactOfTeam.data.data.forEach(function (customer) {
          if (!customer.deleted_at) {
            customers.push(customer);
          }
        });
      }

      commit(SET_CUSTOMER_CONTACTS_OF_TEAM, customers);
      return customerContactOfTeam;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_ADD_CONTACT](context, params) {
    try {
      return await customerAPI.createContact(params);
    } catch (e) {
      return e.response;
    }
  },


  async [CUSTOMER_UPDATE_CONTACT](context, params) {
    try {
      return await customerAPI.updateContact(params);
    } catch (e) {
      return e.response;
    }
  },

  delete(customerId) {
    return new Promise((resolve, reject) => {
      customerAPI.delete(customerId).then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },

  async [CUSTOMER_TRASH](context, params) {
    try {
      return await customerAPI.multipleDelete(params);
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_CHECK_HAS_DEAL_BEFORE_DELETE](context, params) {
    try {
      return await customerAPI.checkCustomerHasDealBeforeDelete(params);
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_RESTORE](context, params) {
    try {
      return await customerAPI.multipleRestore(params);
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_CREATE](context, params) {
    try {
      params = CustomerVM.createVM.transform(params);
      let customerCreated = await customerAPI.create(params);
      return customerCreated;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_ADD_NEW_CHECK_EXIST](context, params) {
    try {
      let exist = await customerAPI.checkExists(params);
      return exist;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_ATTRIBUTE_DATA_TYPE]({
    commit
  }) {
    try {
      let attributeDataType = await customerAPI.getAllAttributeDataType();
      commit(SET_CUSTOMER_ATTRIBUTE_DATA_TYPE, CustomerVM.listAttributeDataType.transform(attributeDataType.data.data));
      return attributeDataType;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_CREATE_ATTRIBUTE](context, params) {
    params = CustomerVM.createAttribute.transform(params);
    try {
      let createAttribute = await customerAPI.createAttribute(params);
      return createAttribute;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_ADD_ATTRIBUTE_CHECK_EXIST](context, params) {
    try {
      let data = await customerAPI.checkExistsCreateAttribute(params);
      return data;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_UPDATE_ATTRIBUTE](context, params) {
    params = CustomerVM.updateAttribute.transform(params);
    try {
      let updateAttribute = await customerAPI.updateAttribute(params.id, params);
      return updateAttribute;
    } catch (e) {
      return e;
    }
  },

  getCustomerPrefix({
    commit
  }, params) {
    return new Promise((resolve, reject) => {
      customerAPI.getCustomerPrefix(params).then(response => {
        commit("getCustomerPrefix", CustomerVM.prefixVM.transform(response.data.data));
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },

  updateCustomerPrefix({
    commit
  }, params) {
    return new Promise((resolve, reject) => {
      customerAPI.updateCustomerPrefix(params).then(response => {
        commit("updateCustomerPrefix", CustomerVM.prefixVM.transform(response.data.data));
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },

  exportToExcel(params) {
    return new Promise((resolve, reject) => {
      customerAPI.exportToExcel(params).then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },

  async [CUSTOMER_SAVE_ATTRIBUTE_OPTION](context, params) {
    try {
      params = CustomerVM.createAttributeOptions.transform(params);
      let create = await customerAPI.createAttributeOptions(params);
      return create;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_IMPORT_CHECK_ATTRIBUTES_UNIQUE](context, params) {
    try {
      let attributesUnique = await customerAPI.importCheckAttributesUnique(params);
      // if(attributesUnique.data.data){
      //     commit(SET_CUSTOMER_IMPORT_CHECK_ATTRIBUTE_UNIQUE, attributesUnique.data.data);
      // }
      return attributesUnique;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_IMPORT](context, params) {
    try {
      let customerImport = await customerAPI.import(params);
      return customerImport;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_UPDATE_ASSIGNERS](context, params) {
    try {
      let response = await customerAPI.assign(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_REMOVE_ASSIGNERS](context, params) {
    try {
      let response = await customerAPI.removeAssign(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_ALL_ASSIGNER_IN_TEAM]() {
    try {
      let response = await customerAPI.getAllAssignerInTeam();
      return response;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_DATA_FOR_EXCEL](context, params) {
    try {
      let customers = await customerAPI.getAll(params);
      return customers;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_GET_ATTRIBUTE_OPTION]({
    commit
  }, attributeId) {
    try {
      let attributeOptions = await customerAPI.getAttributeOptions(attributeId);
      commit(SET_CUSTOMER_ATTRIIBUTE_OPTIONS, CustomerVM.listAttributeOptionsVM.transform(attributeOptions.data.data));
      return attributeOptions;
    } catch (e) {
      return e;
    }
  },

  async [CUSTOMER_DELETE_ATTRIBUTE](context, params) {
    try {
      let response = await customerAPI.deleteAttributeById(params);
      return response;
    } catch (e) {
      return e;
    }
  },
  async [CUSTOMER_UPDATE_LEVEL](context, params) {
    try {
      let response = await customerAPI.updateLevelCustomer(params);
      return response;
    } catch (e) {
      return e;
    }
  },


  async [CHANGE_TABLE_ATTRIBUTE_CUSTOMER](context, params) {
    try {
      let response = await customerAPI.changeCustomerAttributeTable(params)
      return response;
    } catch (e) {
      return e;
    }
  },

  /***
   ***** REPORTS
   ***/
  async [CUSTOMER_GET_TOP_HARD_REPORT]({
    commit
  }, team_id) {
    try {
      let response = await customerAPI.getInfoTopHardChart(team_id);
      commit(SET_CUSTOMER_TOP_HARD_REPORT, response.data.data);
      return response;
    } catch (e) {
      console.log(e)
      return e;
    }
  },

  async [CUSTOMER_GET_INFO_HARD_CHART](context, params) {
    try {
      let response = await customerAPI.getInfoHardChart(params);
      return response;
    } catch (e) {
      console.log(e)
      return e;
    }
  },

  async [GET_CUSTOMER_FLEXIBLE_REPORT](context, params) {
    try {
      let response = await customerAPI.getCustomerFlexibleReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_CUSTOMER_CHARGE_PERSON_REPORT](context, params) {
    try {
      let response = await customerAPI.getCustomerChargePersonReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EXPORT_CUSTOMER_FLEXIBLE_REPORT](context, params) {
    try {
      let response = await customerAPI.exportCustomerFlexibleReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [EXPORT_CUSTOMER_CHARGE_PERSON_REPORT](context, params) {
    try {
      let response = await customerAPI.exportCustomerChargePersonReport(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [SAVE_CUSTOMER_CONFIG_REPORT_FOR_USER](context, params) {
    try {
      let response = await customerAPI.saveConfigCustomerAttributeReportForUser(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_CUSTOMER_CONFIG_REPORT_FOR_USER](context, params) {
    try {
      let response = await customerAPI.getConfigCustomerAttributeReportForUser(params);
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_CONFIG_CUSTOMER_ATTRIBUTE_TABLE]({
    commit
  }, params) {
    try {
      let response = await customerAPI.getConfigCustomerAttributeTable(params);
      commit(SET_CUSTOMER_CONFIG_ATTRIBUTE_TABLE, CustomerVM.configAttributeTableVM.transform(response.data.data));
      return response;
    } catch (e) {
      return e;
    }
  },

  async [GET_CONFIG_CUSTOMER]({
    commit
  }, params) {
    try {
      let res = await customerAPI.getConfigCustomerAttributeTable(params);
      const data = res.data && res.data.data
      if (data) {
        commit(SET_CUSTOMER_CONFIG, data)
      }
      return res;
    } catch (e) {
      return e;
    }
  },

  async [CHECK_BEFORE_SEND_EMAIL_CUSTOMERS](context, params) {
    try {
      let res = await customerAPI.checkBeforeSendEmailCustomers(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [SEND_EMAIL_CUSTOMERS](context, params) {
    try {
      let res = await customerAPI.sendEmailCustomers(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [CHECK_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS](context, params) {
    try {
      let res = await customerAPI.checkBeforeUpdateAttributeCustomers(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [UPDATE_ATTRIBUTE_CUSTOMERS](context, params) {
    try {
      let res = await customerAPI.updateAttributeCustomers(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [CHECK_VALIDATE_BEFORE_UPDATE_ATTRIBUTE_CUSTOMERS](context, params) {
    try {
      let res = await customerAPI.checkValidateBeforeUpdateAttributeCustomers(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [GET_TOTAL_CUSTOMERS_PHONE_NUMBER](context, params) {
    try {
      let res = await customerAPI.getTotalCustomersPhoneNumber(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [VALIDATE_SMS_SEND_CUSTOMERS](context, params) {
    try {
      let res = await customerAPI.validateSmsSendCustomers(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [SEND_SMS_CUSTOMERS](context, params) {
    try {
      let res = await customerAPI.sendSmsCustomers(params);
      return res;
    } catch (e) {
      return e;
    }
  },

  async [GET_LIST_FILE_CONVERT_TABLE](context, params) {
    try {
      return await customerAPI.getListFileConvertTable(params)
    } catch(e) {
      return e;
    }
  },

  async [LIST_FILE_IMPORT](context, params) {
    try {
      return await customerAPI.importListFileConvert(params)
    } catch(e) {
      return e;
    }
  },

  async [DOWNLOAD_FILE_IMPORT] (context, params) {
    try {
      return await customerAPI.downloadFileImport(params)
    } catch (e) {
      return e;
    }
  },

  async [CHECK_FILE_CONVERT] (context, params) {
    try {
      return await customerAPI.checkFileConvert(params)
    } catch(e) {
      return e;
    }
  },

  async [ASK_UPLOAD_FILE](context, params) {
    try {
      return await customerAPI.uploadFileFix(params);
    } catch (e) {
      return e;
    }
  },
}
