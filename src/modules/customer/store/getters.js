/* eslint-disable no-undef */
import helpers from "@/utils/utils";

export const getters = {
  customerContacts(state) {
    return state.contacts;
  },
  customerContactOfTeam(state) {
    return state.customer_contact_of_team;
  },
  attributeCustom(state) {
    return state.attributes.filter(function (attribute) {
      if (!attribute.is_default_attribute && !attribute.external_attribute) {
        return attribute;
      }
    })
  },

  listCustomers(state) {
    return state.list;
  },

  attributeCustomerFull(state) {
    let attributeInfoGeneralNotAttribute = [{
        id: "user_id",
        attribute_code: "user_id",
        attribute_id: "user_id",
        attribute_name: "Tạo bởi",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "user_id",
        data_type: {
          id: "user_id"
        }
      },
      {
        id: "created_at",
        attribute_code: "created_at",
        attribute_id: "created_at",
        attribute_name: "Tạo lúc",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "created_at",
        data_type: {
          id: "created_at"
        }
      },
      {
        id: "updated_at",
        attribute_code: "updated_at",
        attribute_id: "updated_at",
        attribute_name: "Lần chỉnh sửa cuối",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "updated_at",
        data_type: {
          id: "updated_at"
        }
      }
    ];
    let attributes = [];
    state.attributes.forEach((attribute) => {
      if (attribute.attribute_code === 'avatar' || attribute.attribute_code === 'guid') {
        return;
      }
      attributes.push(attribute);
    });
    return attributes.concat(attributeInfoGeneralNotAttribute);
  },

  prefixAttributeCodeCustomer(state) {
    let attributes = state.attributes;
    let newAttribute = [];
    attributes.forEach((attribute) => {
      if (attribute.attribute_code == "charge_person" || attribute.attribute_code == "avatar") {
        return;
      }
      attribute.prefix_attribute_code = "kh_" + attribute.attribute_code;
      newAttribute.push(attribute);
    });
    return newAttribute;
  },

  attributeFullForFilter(state) {
    let attributeInfoGeneralNotAttribute = [{
        id: "user_id",
        attribute_code: "user_id",
        attribute_id: "user_id",
        attribute_name: "Tạo bởi",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "user_id",
        data_type: {
          id: "user_id"
        }
      },
      {
        id: "created_at",
        attribute_code: "created_at",
        attribute_id: "created_at",
        attribute_name: "Tạo lúc",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "created_at",
        data_type: {
          id: "created_at"
        }
      },
      {
        id: "updated_at",
        attribute_code: "updated_at",
        attribute_id: "updated_at",
        attribute_name: "Lần chỉnh sửa cuối",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "updated_at",
        data_type: {
          id: "updated_at"
        }
      }
    ];
    let fullAttributes = state.attributes.concat(attributeInfoGeneralNotAttribute);
    let attributesForFilter = [];
    if (fullAttributes && fullAttributes.length > 0) {
      fullAttributes.forEach(function (item) {
        if (item.attribute_code === 'guid'){
          return
        }
        if ($.inArray(item.attribute_code, ["sku", "full_name", "level", "customer_type", "total_price_when_buyer", "total_product_when_enduser", "total_product_when_buyer", "total_payment_amount"]) > -1) {
          item.is_auto_gen = true;
        } else {
          item.is_auto_gen = false;
        }
        attributesForFilter.push(item);
      });
    }
    attributesForFilter.sort(function (a, b) {
      if (helpers.convertLanguages(a.attribute_name) < helpers.convertLanguages(b.attribute_name)) {
        return -1;
      }
      if (helpers.convertLanguages(a.attribute_name) > helpers.convertLanguages(b.attribute_name)) {
        return 1;
      }
      return 0;
    });
    return attributesForFilter;
  },

  attributeAddNew(state) {
    let customerAttributes = ["full_name", "phone", "email", "address", "birthday_set_to_date", "note", "total_price_when_buyer", "total_product_when_buyer", "total_product_when_enduser", "total_payment_amount", 'guid'];
    let newAttributes = [];
    customerAttributes.forEach(function (item) {
      state.attributes.forEach(function (attribute) {
        if (attribute.attribute_code == item) {
          newAttributes.push(attribute);
        }
      })
    });
    return newAttributes;
  },

  attributeCustomerInProductDeal(state) {
    let customerAttributes = ["full_name", "phone", "email", "sku"];
    let newAttributes = [];

    customerAttributes.forEach(function (item) {
      state.attributes.forEach(function (attribute) {
        if (attribute.attribute_code == item) {
          newAttributes.push(attribute);
        }
      })
    });
    return newAttributes;
  },

  attributeCustomerUsingReports(state) {
    let attributeIds = [8, 9, 10, 11, 13, 14, 15, 16];
    let newAttributes = [];

    state.attributes.forEach(function (attribute) {
      attributeIds.forEach(function (id) {
        if (attribute.data_type_id == id && attribute.is_hidden == 0) {
          newAttributes.push(attribute);
        }
      });

      if (attribute.attribute_code == 'charge_person') {
        newAttributes.push(attribute);
      }
    });

    return newAttributes;
  },

  getDataExportExcel(state) {
    return state.listAttributeExportExcel;
  },

  detailCustomer(state) {
    return state.customer;
  },

  detailCustomerContactMain(state) {
    return state.contact_main ? state.contact_main : false;
  },

  detailCustomerAvatar(state) {
    return state.customer_avatar ? state.customer_avatar : false;
  },

  detailCustomerType(state) {
    return state.customer_type ? state.customer_type : false;
  },

  getErrors(state) {
    return state.errors;
  },

  getCustomerFilters(state) {
    return state.filters;
  },

  getCustomerAttributeOptions(state) {
    return state.attribute_options;
  },

  attributeDataType(state) {
    return state.attribute_data_type;
  },

  filterAttributesCustomer(state) {
    return state.filter_attributes ? state.filter_attributes : [];
  }
}