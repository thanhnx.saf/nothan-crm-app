import {
  SET_CUSTOMERS,
  SET_CUSTOMER_ATTRIBUTES,
  SET_CUSTOMER_USER_ATTRIBUTES,
  SET_CUSTOMER_IMPORT_CHECK_ATTRIBUTE_UNIQUE,
  SET_DETAIL_CUSTOMER,
  SET_DETAIL_CUSTOMER_AVATAR,
  SET_CUSTOMER_TYPE,
  SET_CUSTOMER_UPDATED,
  SET_CONTACT_MAIN_AND_LEVEL,
  SET_CUSTOMER_FILTER_ATTRIBUTES,
  SET_CUSTOMER_ATTRIIBUTE_OPTIONS,
  SET_ERROR,
  SET_CUSTOMER_FILTERS,
  SET_CUSTOMER_ATTRIBUTE_DATA_TYPE,
  SET_CUSTOMER_CONTACTS,
  SET_CUSTOMER_CONTACTS_OF_TEAM,
  SET_CUSTOMER_FILTERS_CREATE,
  SET_CUSTOMER_FILTERS_UPDATE,
  SET_CUSTOMER_FILTERS_DELETE,
  SET_CUSTOMER_TOP_HARD_REPORT,
  SET_CUSTOMER_CONFIG_ATTRIBUTE_TABLE,
  SET_CUSTOMER_CONFIG,
  SET_CUSTOMERS_DETAILS_BASIC_INFO,
  SET_CUSTOMERS_DETAILS_BUYER_INFO,
  SET_CUSTOMERS_DETAILS_USER_INFO,
} from "@/types/mutations.type"
import helpers from "@/utils/utils";
import {
  IS_USER_MULTIPLE,
  IS_USER,
  IS_DATE
} from '@/types/const'

export const mutations = {
  [SET_CUSTOMERS](state, customers) {
    state.list = customers;
  },

  create() {
    //state.list.unshift(customer);
  },

  [SET_CUSTOMER_ATTRIBUTES](state, attributes) {
    let countBase = 0;
    let countAttrDefault = state.user_attributes.length > 0 ? state.user_attributes.filter(() => state.user_attributes.is_default_attribute == 1).length : 0;
    let userId = state.user_attributes.length > 0 ? state.user_attributes[0].user_id : 0;

    if (state.user_attributes.length > 0) {
      let result = [];
      let listAttrInUser = [];
      state.user_attributes.forEach(function (uAttr) {
        attributes.forEach(function (attr) {
          if (uAttr.attribute_code == attr.attribute_code) {
            attr.is_show = uAttr.is_show;
            attr.sort_order = uAttr.sort_order;
            attr.user_id = uAttr.user_id;
            attr.external_attribute = uAttr.external_attribute;
            attr.code_external_attribute = uAttr.code_external_attribute;
            result.push(attr);
            listAttrInUser.push(attr.id);
          }
        })
      });
    }

    attributes.forEach(function (item) {
      if (item.attribute_id == "charge_person") {
        countBase++;
      }
    })
    if (countBase > 0) {
      let countCharge = 0;
      attributes.forEach(function (item) {
        if (item.attribute_id == "charge_person") {
          countCharge++;
        }
      })
      if (countCharge <= 0) {
        let temp_data_charge = {
          id: "charge_person",
          attribute_code: "charge_person",
          attribute_id: "charge_person",
          attribute_name: "Người phụ trách",
          external_attribute: 1,
          code_external_attribute: "charge_person",
          is_show: 0,
          is_hidden: 0,
          sort_order: countAttrDefault + 1,
          user_id: userId,
          data_type: {
            id: "charge_person"
          }
        };
        state.user_attributes.push(temp_data_charge);
        attributes.push(temp_data_charge);
      }
    } else {
      let temp_data_charge = {
        id: "charge_person",
        attribute_code: "charge_person",
        attribute_id: "charge_person",
        attribute_name: "Người phụ trách",
        is_show: 0,
        is_hidden: 0,
        external_attribute: 1,
        code_external_attribute: "charge_person",
        sort_order: countAttrDefault + 1,
        user_id: userId,
        data_type: {
          id: "charge_person"
        }
      };
      state.user_attributes.push(temp_data_charge);
      attributes.push(temp_data_charge);
    }
    state.attributes = attributes;
  },

  [SET_CUSTOMER_USER_ATTRIBUTES](state, attributes) {
    state.user_attributes = attributes;
  },

  [SET_CUSTOMER_FILTERS](state, filters) {
    state.filters = filters;
  },

  [SET_CUSTOMER_FILTER_ATTRIBUTES](state, filter_attributes) {
    state.filter_attributes = filter_attributes;
  },

  [SET_CUSTOMER_FILTERS_UPDATE](state, filter) {
    let newFilters = [];
    state.filters.forEach(function (item) {
      if (item.id === filter.id) {
        item = filter;
      }
      newFilters.push(item);
    })
    state.filters = newFilters;
  },

  [SET_CUSTOMER_FILTERS_CREATE](state, filter) {
    if (state.filters.length > 0) {
      state.filters.push(filter);
    }
  },

  [SET_CUSTOMER_FILTERS_DELETE](state, filterId) {
    state.filters.forEach(function (item, index) {
      if (item.id === filterId) {
        state.filters.splice(index, 1);
      }
    })
  },

  [SET_DETAIL_CUSTOMER](state, customer) {
    state.customer = customer;


  },

  [SET_DETAIL_CUSTOMER_AVATAR](state, file) {
    state.customer_avatar = file;
  },

  [SET_CUSTOMER_CONTACTS_OF_TEAM](state, contacts) {
    state.customer_contact_of_team = contacts;
  },

  [SET_CUSTOMER_CONTACTS](state, contacts) {
    state.contacts = contacts;
    if (contacts && contacts.length > 0) {
      contacts.every(function (contact) {
        if (contact.is_main_contact === 1 || contact.is_main_contact === true) {
          state.contact_main = contact;
        } else {
          state.contact_main = null
        }
      })
    }
  },

  updateContact(state, contact) {
    if (contact.is_main_contact == 1 || contact.is_main_contact == true) {
      state.contact_main = contact;
    }
  },

  [SET_CUSTOMER_ATTRIBUTE_DATA_TYPE](state, attributeDataType) {
    state.attribute_data_type = attributeDataType;
  },

  createAttribute(state, attribute) {
    state.attributes.push(attribute);
  },

  getCustomerPrefix(state, prefix) {
    state.prefix = prefix;
  },

  updateCustomerPrefix(state, prefix) {
    state.prefix = prefix;
  },

  [SET_CUSTOMER_ATTRIIBUTE_OPTIONS](state, attribute_options) {
    state.attribute_options = attribute_options;
  },
  [SET_CUSTOMER_IMPORT_CHECK_ATTRIBUTE_UNIQUE](state, duplicates) {
    state.importErrors.duplicates = duplicates;
  },
  [SET_CUSTOMER_TYPE](state, customerType) {
    state.customer_type = customerType;
  },
  [SET_CUSTOMER_UPDATED](state, customer) {
    state.customer = customer;
  },
  [SET_CUSTOMER_TOP_HARD_REPORT](state, report) {
    state.top_report = report;
  },

  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_CONTACT_MAIN_AND_LEVEL](state, customer) {
    if (customer.contacts && customer.contacts.length > 0) {
      customer.contacts.forEach(function (contact) {
        if (contact.is_main_contact == 1 || contact.is_main_contact == true) {
          state.contact_main = contact;
        }
      })
    }
    let attribute = "";
    if (Object.keys(customer).length > 0) {
      let attributes = customer.attributes;
      if (attributes.length > 0) {
        attribute = attributes.find(function (item) {
          return item.attribute_code === "level";
        })
      }
      let levels = attribute["attribute_options"];
      let sortLevels = levels.sort(function (before, after) {
        return before.sort_order - after.sort_order;
      });
      state.customer.levels = sortLevels;
      let currentLevelValue = attribute["attribute_value"];
      state.customer.currentLevelValue = currentLevelValue;
      let currentLevel = "";
      sortLevels.forEach(function (item, index) {
        item.level = index + 1;
        if (item.id == currentLevelValue) {
          currentLevel = item;
        }
      });
      state.customer.currentLevel = currentLevel;
    }
  },
  [SET_CUSTOMER_CONFIG_ATTRIBUTE_TABLE](state, attribute_customer_by_user) {
    state.list_attributes_config_by_user = attribute_customer_by_user;
  },

  [SET_CUSTOMER_CONFIG](state, customerConfigs) {
    state.customerConfigs = customerConfigs;
  },

  [SET_CUSTOMERS_DETAILS_BASIC_INFO](state, data) {
    if (data) {
      let convertAttributeData = []
      data.forEach((item) => {
        convertAttributeData[item.attribute_code] = item.sort_order
      })

      state.customer.attributes.forEach(attribute => {
        attribute.sort_order = convertAttributeData[attribute.attribute_code]
      })
      // state.basicCustomerInfo = data
      // return
    }
    const details = state.customer
    const configs = state.customerConfigs
    if (!details.attributes) {
      return []
    }
    // const showAtts = ['sku', 'customer_group', 'customer_resources', 'cmnd_mst', 'birthday_set_to_date', 'facebook', 'note', 'khu_vuc', 'nganh_nghe', 'trinh_do']
    const hiddenAtts = ['guid', 'full_name', 'phone', 'email', 'address', 'birthday_set_to_date', 'customer_type', 'gender', 'total_price_when_buyer', 'total_product_when_enduser', 'level', 'avatar', 'total_product_when_buyer', 'total_payment_amount', 'latest_note']
    const basicInfo = details.attributes.filter(t => t.show_position === 1 && !t.is_hidden && hiddenAtts.indexOf(t.attribute_code) === -1)

    basicInfo.push(...[{
      attribute_code: 'nt_assigners',
      attribute_name: 'Người phụ trách',
      attribute_value: details.assigners,
      data_type: {
        id: IS_USER_MULTIPLE
      }
    }, {
      attribute_code: 'nt_creator_id',
      attribute_name: 'Tạo bởi',
      attribute_value: details.user_id,
      data_type: {
        id: IS_USER
      }
    }, {
      attribute_code: 'nt_created_at',
      attribute_name: 'Tạo lúc',
      attribute_value: details.created_at,
      data_type: {
        id: IS_DATE
      }
    }, {
      attribute_code: 'nt_updated_at',
      attribute_name: 'Cập nhật lúc',
      attribute_value: details.updated_at,
      data_type: {
        id: IS_DATE
      }
    }])

    state.basicCustomerInfo = helpers.getCustomerInfo(basicInfo, configs)
  },

  [SET_CUSTOMERS_DETAILS_BUYER_INFO](state, data) {
    if (data) {
      let convertAttributeData = []
      data.forEach((item) => {
        convertAttributeData[item.attribute_code] = item.sort_order
      })

      state.customer.attributes.forEach(attribute => {
        attribute.sort_order = convertAttributeData[attribute.attribute_code]
      })
      // state.buyerCustomerInfo = data
      // return
    }
    const details = state.customer
    const configs = state.customerConfigs
    if (!details.attributes) {
      return []
    }
    const buyerInfo = details.attributes.filter(t => t.show_position === 2 && !t.is_hidden)
    state.buyerCustomerInfo = helpers.getCustomerInfo(buyerInfo, configs)
  },

  [SET_CUSTOMERS_DETAILS_USER_INFO](state, data) {
    if (data) {
      let convertAttributeData = []
      data.forEach((item) => {
        convertAttributeData[item.attribute_code] = item.sort_order
      })

      state.customer.attributes.forEach(attribute => {
        attribute.sort_order = convertAttributeData[attribute.attribute_code]
      })
      // state.usedCustomerInfo = data
      // return
    }
    const details = state.customer
    const configs = state.customerConfigs
    if (!details.attributes) {
      return []
    }
    const userInfo = details.attributes.filter(t => t.show_position === 3 && !t.is_hidden)
    state.usedCustomerInfo = helpers.getCustomerInfo(userInfo, configs)
  },
}