export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformCustomerAttributes(item));
    });

    return new_data;
  },

  transformCustomerAttributes(data) {
    let transform = {}

    transform = {
      id: data.id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      is_show: data.is_show,
      default: data.default,
      sort_order: data.sort_order
    };

    return transform;
  }
}
