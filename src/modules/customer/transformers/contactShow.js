export default {
  transform(data) {
    let transform = {};
    transform = {
      id: data.id,
      customer_id: data.customer_id,
      full_name: data.full_name,
      phone: data.phone,
      email: data.email,
      address: data.address,
      note: data.note,
      is_main_contact: data.is_main_contact,
      created_at: data.created_at,
      updated_at: data.updated_at,
    };

    return transform;
  }
}
