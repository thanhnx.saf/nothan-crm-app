export default {
  transform(data) {
    let transform = {};

    transform = {
      team_id: data.team_id,
      user_id: data.user_id,
      is_active: data.is_active,
      customer_type_id: data.customer_type_id,
      assigners: data.assigners,
      attributes: this.attributes(data.attributes),
      contacts: data.contacts
    };

    return transform;
  },

  attributes(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformAttribute(item));
    });

    return new_data;
  },

  transformAttribute(data) {
    let transform = {}

    transform = {
      id: data.id,
      attribute_value: data.attribute_value ? data.attribute_value : '',
    };

    return transform;
  }
}
