export default {
  transform(data) {
    let transform = {};

    transform = {
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      data_type_id: data.data_type_id,
      is_hidden: data.is_hidden,
      is_required: data.is_required,
      is_unique: data.is_unique,
      is_multiple_value: data.is_multiple_value,
      attribute_options: data.attribute_options && data.attribute_options.length > 0 ? data.attribute_options : [],
      show_position: data.show_position,
      team_id: data.team_id
    };

    return transform;
  }
}