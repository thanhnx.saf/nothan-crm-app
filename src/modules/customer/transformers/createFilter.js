export default {
  transform(data) {
    let transform = {};
    transform = {
      id: data.id,
      name: data.name,
      name_cache: data.name,
      user_id: data.user_id,
      team_id: data.team_id,
      status: data.status,
      status_cache: data.status,
      created_at: data.created_at,
      updated_at: data.updated_at
    };
    return transform;
  },

}
