export default {
  transform(data) {
    let transform = {};
    transform = {
      id: data.id,
      customer_type_name: data.customer_type_name,
      team_id: data.team_id,
      created_at: data.created_at,
      updated_at: data.updated_at,
    };
    return transform;
  }
}
