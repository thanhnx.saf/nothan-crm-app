/* eslint-disable no-undef */
export default {
  transform(data, meta) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformCustomer(item, meta));
    });


    return new_data;
  },

  transformCustomer(data, meta) {
    let transform = {};

    transform = {
      id: data.id,
      is_active: data.is_active,
      customer_type_id: data.customer_type_id,
      assigners: data.assigners,
      contacts: data.contacts,
      attributes: this.transformAttributes(data.attributes),
      in_page: meta.current_page,
      user_id: data.user_id,
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
    };

    return transform;
  },

  transformAttributes(attributes) {
    let newAttributes = [];
    let vm = this;
    if (attributes && attributes.length > 0) {
      attributes.forEach(function (attribute) {
        let value = attribute.value;
        let attribute_id = attribute.attribute_id;
        let customer_id = attribute.customer_id;
        let id = attribute.id;

        if (attribute.is_multiple_value == 1) {
          let newValue = [];
          if (vm.checkLengthObject(attribute) > 1) {
            Object.keys(attribute).forEach(function (index) {
              if (attribute[index] instanceof Object == true) {
                attribute_id = attribute[index].attribute_id;
                customer_id = attribute[index].customer_id;
                id = attribute[index].id;
                newValue.push(attribute[index].value);
              }
            });
          } else {
            newValue.push(attribute.value);
          }
          value = newValue;
        }

        let tempAttribute = {
          id: id,
          attribute_id: attribute_id,
          customer_id: customer_id,
          value: value
          // value : this.transformValueAttribute(attribute.value)
        };

        newAttributes.push(tempAttribute);
      })
    }

    return newAttributes;
  },

  checkLengthObject(data) {
    var count = 0;

    if (Object.keys(data) && Object.keys(data).length > 0) {
      Object.keys(data).forEach(function (index) {
        if (data[index] instanceof Object == true) {
          count++;
        }
      });
    }

    return count;
  },

  transformValueAttribute(value) {
    let newValue = "";
    if ($.isNumeric(value)) {
      newValue = parseInt(value);
    } else if (value == "0") {
      newValue = "";
    } else if (value == "0000-00-00 00:00:00") {
      newValue = "";
    } else {
      newValue = value;
    }
    return newValue;
  }
}
