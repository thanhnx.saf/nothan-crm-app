export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformItem(item));
    });

    return new_data;
  },

  transformItem(data) {
    let transform = {};

    transform = {
      id: data.id,
      data_type_name: data.data_type_name,
    };

    return transform;
  }
}