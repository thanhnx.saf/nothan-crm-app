export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformItem(item));
    });

    return new_data;
  },

  transformItem(data) {
    let transform = {};

    transform = {
      id: data.id,
      count: data.count,
      option_value: data.option_value,
      option_value_cache: data.option_value ? data.option_value : '',
      parent_id: data.parent_id,
      sort_order: data.sort_order,
      is_default: data.is_default,
      is_drag: data.is_drag,
      note: data.note
    };

    return transform;
  }
}
