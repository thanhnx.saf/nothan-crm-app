export default {
  transform(data) {
    let vm = this;
    let transform = {};

    transform = {
      attribute_id: data.attribute_id,
      attribute_options: vm.attributeOptions(data.attribute_options),
    };

    return transform;
  },

  attributeOptions(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformItem(item));
    });

    return new_data;
  },

  transformItem(data) {
    let transform = {};

    transform = {
      id: data.id,
      count: data.count,
      option_value: data.option_value,
      option_value_cache: data.option_value ? data.option_value : '',
      parent_id: data.parent_id,
      sort_order: data.new_sort_order,
    };

    return transform;
  }
}