export default {
  transform(data) {
    let transform = {};

    transform = {
      id: data.id,
      attribute_id: data.attribute_id,
      count: data.count,
      option_value: data.option_value,
      parent_id: data.parent_id,
      sort_order: data.sort_order,
    };

    return transform;
  },

}