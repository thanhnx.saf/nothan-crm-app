/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";
import {
  mapState
} from "vuex";

import TopStats from '@/modules/report/components/stats/TopStats.vue';
import HardChart from '@/modules/report/components/chart-hard/HardChart.vue';
import CustomerReports from '@/modules/report/views/customer/CustomerReports.vue';
import helpers from "@/utils/utils";
import {
  CUSTOMER_GET_TOP_HARD_REPORT,
  CUSTOMER_GET_INFO_HARD_CHART,
  CUSTOMER_GET_ATTRIBUTES,
  SAVE_CUSTOMER_CONFIG_REPORT_FOR_USER,
  GET_CUSTOMER_CONFIG_REPORT_FOR_USER,
  USER_CURRENT
} from '@/types/actions.type'

export default {
  name: "report",
  components: {
    TopStats,
    HardChart,
    CustomerReports
  },
  mounted() {
    let vm = this;

    vm.getAttributeConfigForUser();

    let params = {
      team_id: vm.team_id,
    };

    let oneDay = 24 * 60 * 60 * 1000;
    let dateCreateOrganization = this.currentTeam && this.currentTeam.created_at ? new Date(this.currentTeam.created_at) : '';
    let currentDate = new Date();
    let diffDays = dateCreateOrganization ? Math.round(Math.abs((currentDate.getTime() - dateCreateOrganization.getTime()) / (oneDay))) : '';

    if (diffDays < 14 && dateCreateOrganization) {
      vm.hardChart.start_time[0] = dateCreateOrganization;
      vm.hardChart.start_time[1] = currentDate;
    }

    vm.getTopHardReports(vm.team_id);
    vm.loadDataByDateRange(params);
  },
  watch: {
    currentTeam: {
      handler() {
        let vm = this;
        let params = {
          team_id: vm.team_id,
        };
        let oneDay = 24 * 60 * 60 * 1000;
        let dateCreateOrganization = new Date(vm.currentTeam.created_at);
        let currentDate = new Date();
        let diffDays = Math.round(Math.abs((currentDate.getTime() - dateCreateOrganization.getTime()) / (oneDay)));

        if (diffDays < 14) {
          vm.hardChart.start_time[0] = dateCreateOrganization;
          vm.hardChart.start_time[1] = currentDate;
        }
        vm.loadDataByDateRange(params);
      }
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["attributeCustomerUsingReports", "currentUser"]),
    ...mapState({
      currentTeam: state => state.organization.currentTeam,
      // server_errors: state => state.organization.createErrors
    }),
  },
  data() {
    return {
      isCurrentTeam: false,
      isLoader: false,
      loadingDataChart: false,
      team_id: parseInt(this.$route.params.team_id),
      hardChart: {
        start_time: [],
      },
      chart: null,
      chartId: 'report_sales_main_chart',
      chartType: 'line',
      chartName: 'Báo cáo khách hàng',
      chartOptions: {
        responsive: true,
        pan: {
          enabled: true,
          mode: 'x',
        },
        zoom: {
          enabled: true,
          mode: 'x',
        },
        responsiveAnimationDuration: 5,
        aspectRatio: 30,
        maintainAspectRatio: false,
        title: {
          display: false
        },
        legend: {
          display: false,
          position: 'top'
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          titleFontColor: "#838CAB",
          titleFontStyle: "normal",
          titleMarginBottom: 7,
          xPadding: 25,
          yPadding: 15,
          bodyFontColor: "#fff",
          bodyFontStyle: "bold",
          footerFontStyle: 'normal',
          caretSize: 6,
          footerFontColor: '#69BE31',
          callbacks: {
            label: function (tooltipItem) {
              if (tooltipItem.yLabel !== 0) {
                return tooltipItem.yLabel;
              } else {
                return '0'
              }
            },
            title: function (tooltipItems, data) {

              let tooltipIndex = tooltipItems[0].index;
              let newData = data.labelsX[tooltipIndex];

              return newData;
            },

          },
          custom: function (tooltip) {
            if (!tooltip) return;
            // disable displaying the color box;
            tooltip.displayColors = false;
          },
          labelTextColor: function () {
            return '#f00';
          }
        },
        scales: {
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Tăng trưởng khách hàng theo thời gian'
            },
            ticks: {
              beginAtZero: true
            }
          }],
          xAxes: [{
            position: 'bottom',
            ticks: {
              min: -1,
              max: 8,
              stepSize: 1,
              fixedStepSize: 1,
            }
          }],
        }
      },
      chartData: {
        labelsX: [],
        labels: [],
        datasets: [{
          label: "T3 - 19/11/2019",
          data: [],
          fill: false,
          backgroundColor: "#2A94DB",
          borderColor: "#2A94DB",
          lineTension: 0,
          // pointBorderColor: "rgba(255, 0, 0,1)",
          pointBackgroundColor: "#fff",
          pointBorderWidth: 7,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "#2A94DB",
          pointHoverBorderColor: "#BFD5E3",
          pointHoverBorderWidth: 2,
          pointRadius: 2,
          pointHitRadius: 1,
        }]
      },
      topReports: [{
          current_value: '',
          value_description: "Tổng số khách hàng",
          value_type: "number",
          styles: {
            icon: '<i class="icon-shopping-cart-1"></i>',
            color: "#69BE31",
            background: "#EAF6E2",
            fontSize: "25px"
          }
        },
        {
          current_value: '',
          value_description: "Khách hàng chỉ mua",
          value_type: "number",
          styles: {
            icon: '<i class="icon-shopping-cart-1"></i>',
            color: "#B82E45",
            background: "#F5E1E4",
            fontSize: "25px"
          }
        },
        {
          current_value: '',
          value_description: "Khách hàng chỉ sử dụng",
          value_type: "number",
          styles: {
            icon: '<i class="icon-dollar-2"></i>',
            color: "#9F36BE",
            background: "#F1E2F6",
            fontSize: "25px"

          }
        },
        {
          current_value: '',
          value_description: "Khách hàng vừa mua vừa sử dụng",
          value_type: "number",
          styles: {
            icon: '<i class="icon-dollar-2"></i>',
            color: "#FF8A00",
            background: "#FFE0BC",
            fontSize: "25px"

          }
        },
      ],
      attributesReportCheckedIds: [],
      flexibleReportData: [],
    }
  },
  methods: {
    ChangeDateTimeSelect(data) {
      let vm = this;
      vm.hardChart = data;
      vm.loadDataByDateRange();
    },
    loadDataByDateRange(params) {
      let vm = this;
      vm.loadingDataChart = true;

      params = {
        team_id: vm.team_id,
        from: vm.hardChart.start_time[0] ? helpers.formatDateTime(vm.hardChart.start_time[0]) : '',
        to: vm.hardChart.start_time[1] ? helpers.formatDateTime(vm.hardChart.start_time[1]) : ''
      };

      vm.$store.dispatch(CUSTOMER_GET_INFO_HARD_CHART, params).then((response) => {
        vm.loadingDataChart = false;

        if (response.data.data) {
          let data = response.data.data;

          vm.chartData.labelsX = data.date_date_full;
          vm.chartData.labels = data.data_date;
          vm.chartData.datasets[0].data = data.date_value;
          vm.loadChart();

        }
      }).catch((error) => {
        console.log(error)
      });
    },
    loadChart() {
      let vm = this;
      vm.isLoaderChart = false;


      if (vm.chart) {
        vm.chart.destroy();
      }
      if (document.getElementById(vm.chartId)) {
        vm.chart = new Chart(document.getElementById(vm.chartId), {
          type: vm.chartType,
          data: vm.chartData,
          options: vm.chartOptions,
        });
      }
    },
    getTopHardReports(team_id) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_TOP_HARD_REPORT, team_id).then((response) => {
        if (response.data.data) {
          vm.isLoader = false;
          let infoHard = response.data.data;
          vm.topReports[0]['current_value'] = infoHard.total;
          vm.topReports[1]['current_value'] = infoHard.total_only_buyer;
          vm.topReports[2]['current_value'] = infoHard.total_only_used;
          vm.topReports[3]['current_value'] = infoHard.total_used_and_buyer;
        }
      }).catch((error) => {
        console.log(error);
      })
    },
    async getAttributes() {
      let vm = this;
      await vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then(() => {
        vm.attributeReady = true;
      }).catch(error => {
        console.log(error);
      });
    },
    checkedAttributeShowHide() {
      let vm = this;
      vm.isLoader = true;
      vm.saveAttributeConfigForUser();
    },
    saveAttributeConfigForUser() {
      let vm = this;

      let params = {
        team_id: vm.currentTeamId,
        user_id: vm.currentUser.id,
        ids: vm.attributesReportCheckedIds
      };

      vm.$store.dispatch(SAVE_CUSTOMER_CONFIG_REPORT_FOR_USER, params).then((response) => {
        if (response.data.data) {
          vm.isLoader = false;
        }
      });
    },

    async getAttributeConfigForUser() {
      let vm = this;

      await vm.getAttributes()

      let params = {
        team_id: vm.currentTeam && vm.currentTeam.team_id ? vm.currentTeam.team_id : vm.team_id
      }

      if (!vm.currentUser) {
        await vm.$store.dispatch(USER_CURRENT)
      }

      params.user_id = vm.currentUser.id

      await vm.$store.dispatch(GET_CUSTOMER_CONFIG_REPORT_FOR_USER, params).then((response) => {
        if (response.data.data) {
          let attributeIds = response.data.data;
          vm.attributesReportCheckedIds = [];

          if (attributeIds) {
            for (var attributeKey in attributeIds) {
              if (!attributeIds.hasOwnProperty(attributeKey)) {
                continue;
              } else {
                vm.attributesReportCheckedIds.push(attributeIds[attributeKey]);
              }
            }
          }
        }
      })
    },
    // saveAttributeConfigForUser() {
    //   let vm = this;

    //   let attributesReportCheckedIds = [];
    //   let attributesReportCheckedIdsObj = {};

    //   if (vm.attributesReportCheckedIds && vm.attributesReportCheckedIds.length > 0) {

    //     vm.attributesReportCheckedIds.forEach(function (id) {
    //       if (!attributesReportCheckedIdsObj[id]) {
    //         attributesReportCheckedIdsObj[id] = id;
    //       }
    //     });

    //     if (Object.keys(attributesReportCheckedIdsObj).length > 0) {
    //       Object.keys(attributesReportCheckedIdsObj).forEach(function (id) {
    //         attributesReportCheckedIds.push(id);
    //       });
    //     }

    //     attributesReportCheckedIds.sort(function (a, b) {
    //       return a - b;
    //     });
    //   }

    //   vm.attributesReportCheckedIds = attributesReportCheckedIds;

    //   let params = {
    //     team_id: vm.currentTeamId,
    //     user_id: vm.currentUser.id,
    //     ids: vm.attributesReportCheckedIds
    //   };

    //   vm.$store.dispatch(SAVE_CUSTOMER_CONFIG_REPORT_FOR_USER, params).then((response) => {
    //     if (response.data.data) {
    //       vm.isLoader = false;
    //     }
    //   });
    // },
    // getAttributeConfigForUser() {
    //   let vm = this;

    //   vm.getAttributes().then((response) => {
    //     if (vm.currentTeam && vm.currentTeamId && vm.currentUser && vm.currentUser.id) {
    //       let params = {
    //         team_id: vm.currentTeamId,
    //         user_id: vm.currentUser.id,
    //       };

    //       vm.$store.dispatch(GET_CUSTOMER_CONFIG_REPORT_FOR_USER, params).then((response) => {
    //         let attributesReportCheckedIdsObj = {};
    //         let attributesReportCheckedIds = [];

    //         if (response.data.data) {
    //           let attributeIds = response.data.data;
    //           vm.attributesReportCheckedIds = [];

    //           if (attributeIds) {
    //             for (var attributeKey in attributeIds) {
    //               if (!attributeIds.hasOwnProperty(attributeKey)) {
    //                 continue;
    //               } else {
    //                 if (!attributesReportCheckedIdsObj[attributeIds[attributeKey]]) {
    //                   attributesReportCheckedIdsObj[attributeIds[attributeKey]] = attributeIds[attributeKey];
    //                 }
    //               }
    //             }

    //             if (Object.keys(attributesReportCheckedIdsObj).length > 0) {
    //               Object.keys(attributesReportCheckedIdsObj).forEach(function (id) {
    //                 attributesReportCheckedIds.push(id);
    //               });
    //             }

    //             attributesReportCheckedIds.sort(function (a, b) {
    //               return a - b;
    //             });

    //             vm.attributesReportCheckedIds = attributesReportCheckedIds;
    //           }
    //         }
    //       }).catch(error => {
    //         console.log(error);
    //       });
    //     }
    //   });
    // }
  }
}