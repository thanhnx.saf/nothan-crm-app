/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex"

import {
  GET_LIST_FILE_CONVERT_TABLE,
  LIST_FILE_IMPORT,
  DOWNLOAD_FILE_IMPORT,
  CHECK_FILE_CONVERT,
  DOWNLOAD_EXCEL_FILE,
  DEAL_GET_RESULT_FILE,
  CUSTOMER_IMPORT,
  DEAL_IMPORT_DEAL,
  ASK_UPLOAD_FILE
} from "@/types/actions.type"
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config"
export default {
  data() {
    return {
      isActive: true,
      filesUpload: [],
      file: '',
      tableInfo: [],
      errorFileKey: [],
      errorFileCode: [],
      fileValidate: [],
      staticFile: [],
      fileConvert: ''
    }
  },
  mounted() {
    this.getListFileConvertTable()
  },
  computed: {
    ...mapGetters(['currentTeam', 'prefixAttributeCodeCustomer', 'deals']),
  },
  methods: {
    handleFileUpload() {
      this.file = this.$refs.customFile.files[0]
      if (this.file) {
        this.isActive = false
      }
    },
    handleFileImportFix() {
      this.fileConvert = this.$refs.fileImport.files[0]
    },
    submitFile() {
      const formData = new FormData()
      formData.append('file', this.file)
      formData.append('team_id', this.currentTeam.team_id)
      $('#btn-submit').html('<i class="fa fa-spinner"></i> Chuyển đổi ...')
      this.isActive = true
      this.$store.dispatch(LIST_FILE_IMPORT, formData)
        .then(response => {
          if (response.data.data.success) {
            $('#btn-submit').html('Chuyển đổi')
            this.isActive = false
            this.$snotify.success('Chuyển đồi file thành công', TOAST_SUCCESS)
            this.getListFileConvertTable()
          }
        })
        .catch(e => {
          $('#btn-submit').html('Chuyển đổi')
          this.isActive = false
          this.$snotify.error('Chuyển đổi file thất bại', TOAST_ERROR)
          this.isActivated = true
          console.log(e)
        })
    },

    getListFileConvertTable() {
      this.$store.dispatch(GET_LIST_FILE_CONVERT_TABLE, this.currentTeam.team_id)
        .then(response => {
          this.tableInfo = response.data.data
          // this.tableInfo.forEach((item, index) => {
          //   if(!this.fileValidate[item.id]) {
          //     this.fileValidate[item.id] = {
          //       code: 0,
          //       key: '',
          //       is_import: 0,
          //       file_error: 0
          //     }
          //     this.staticFile[item.id] = {
          //       totalSuccess: '',
          //       totalAll: ''
          //     }
          //   }
          // })
        }).catch(e => {
          console.log(e)
        })
    },

    downloadFileConvert(id, name) {
      return this.$store.dispatch(DOWNLOAD_FILE_IMPORT, id).then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', name)
        document.body.appendChild(link)
        link.click()
        document.body.removeChild(link)
      }).catch(error => {
        console.log(error)
      })
    },

    checkFileConvert(id) {
      $('#btnCheck' + id).html('<i class="fa fa-spinner"></i> Kiểm tra ...')
      $('#btnCheck' + id).prop('disabled', true)

      this.$store.dispatch(CHECK_FILE_CONVERT, id).then((response) => {
        $('#btnCheck' + id).html('Kiểm tra')
        $('#btnCheck' + id).prop('disabled', false)

        //this.errorFileCode = response.data.data.code
        //this.errorFileKey.key = response.data.data.data
        //console.log(this.errorFileCode, this.errorFileKey)
        // let is_import = 0
        let data = response.data.data
        // if(data.status) {
        //   is_import = 1
        //   if(data.countSuccess < data.total) {
        //     this.fileValidate[id].file_error = 1
        //   }
        // }
        // this.fileValidate[id].code       = data.code
        // this.fileValidate[id].key        = data.data
        // this.fileValidate[id].is_import  = is_import
        // this.staticFile[id].totalSuccess = data.countSuccess
        // this.staticFile[id].totalAll     = data.total
        // this.$forceUpdate()

        this.getListFileConvertTable()

        if (!data.status) {
          alert('File không đúng định dạng hoặc không có bản ghi, hoặc file lớn hơn 500 bản ghi.')
        }
      }).catch(e => {
        $('#btnCheck' + id).html('Kiểm tra')
        $('#btnCheck' + id).prop('disabled', false)

        console.log(e)
      })
    },

    downloadFileError(id, type, name, key) {
      let url = ''
      let file_name = 'KhongDat_' + name
      let params = ''
      if (type === 'customer') {
        url = DOWNLOAD_EXCEL_FILE
        params = key
      } else { // deal
        url = DEAL_GET_RESULT_FILE
        params = {
          key: key
        }
      }

      this.$store.dispatch(url, params)
        .then(response => {
          let urlRes = window.URL.createObjectURL(new Blob([response.data]))
          let link = document.createElement('a')
          link.href = urlRes

          link.setAttribute('download', file_name)
          document.body.appendChild(link)
          link.click()
        }).catch(e => {
          console.log(e)
        })
    },

    importFile(id, type, key) {
      let url = ''
      let params = ''
      $('#btn-import-' + id).prop('disabled', true)
      $('#btn-import-' + id).html('<i class="fa fa-spinner"></i> Importing ...')
      if (type === 'customer') {
        url = CUSTOMER_IMPORT
        params = {
          team_id: this.currentTeam.team_id,
          key: key,
        }
      } else { // deal
        url = DEAL_IMPORT_DEAL
        params = {
          data_key: key
        }
      }

      this.$store.dispatch(url, params)
        .then(() => {
          $('#btn-import-' + id).prop('disabled', false)
          $('#btn-import-' + id).html('Import')
          this.$snotify.success('Import Success.', TOAST_SUCCESS)
        }).catch(e => {
          $('#btn-import-' + id).prop('disabled', false)
          $('#btn-import-' + id).html('Import')
          this.$snotify.error('Import Failed.', TOAST_ERROR)
          console.log(e)
        })
    },

    btnUploadFileClick() {
      $('#btnUploadFileFixed').prop('disabled', true)
      $('#btnUploadFileFixed').html('<i class="fa fa-spinner"></i> Uploading ...')

      const formData = new FormData()
      formData.append('file', this.fileConvert)

      this.$store.dispatch(ASK_UPLOAD_FILE, formData)
        .then(response => {
          $('#btnUploadFileFixed').prop('disabled', false)
          $('#btnUploadFileFixed').html('Upload')

          $('#fileImport').val('')
          this.fileConvert = ''
          $('#uploadFileModal').modal('hide')

          if (response.data.data.success) {
            this.$snotify.success('Upload file thành công', TOAST_SUCCESS)
          } else {
            this.$snotify.error('Upload file thất bại', TOAST_ERROR)
          }
        })
        .catch(e => {
          $('#btnUploadFileFixed').prop('disabled', false)
          $('#btnUploadFileFixed').html('Upload')

          this.$snotify.error('Upload file thất bại', TOAST_ERROR)
          console.log(e)
        })
    },
  }
}