import draggable from "vuedraggable"
import VueLadda from "vue-ladda"
import { findIndex } from 'lodash'

import {
  DEAL_GET_DEAL_ATTRIBUTE,
  DEAL_GET_CUSTOMER_ATTRIBUTE,
  DEAL_GET_PRODUCT_ATTRIBUTE,
  DEAL_EXPORT_ATTRIBUTE,
  DEAL_DEFAULT_ATTRIBUTE,
} from "@/types/actions.type";

const CodeName = {
  Customer: "Khách hàng",
  Deal: "Giao dịch",
  Product: "Sán phẩm"
};

export default {
  components: {
    draggable,
    VueLadda
  },
  data() {
    return {
      isLoading: false,
      codeName: CodeName,
      isLoadingExport: false,
      isLoadingData: false,
      attributes: [
        {
          //     name: CodeName.Customer,
          //     color: 'orange',
          //     data: []
          // }, {
          name: CodeName.Deal,
          color: "green",
          data: [],
          url: [
            "/team/",
            this.$route.params.team_id,
            "/settings/information/deal"
          ].join("")
          // }, {
          //     name: CodeName.Product,
          //     color: 'blue',
          //     data: []
        }
      ],
      selectedData: []
    };
  },
  mounted() {
    const team_id = parseInt(this.$route.params.team_id);
    this.getDealDefaultAttribute(team_id);
    this.getDealAttribute(team_id);
    // this.getCustomerAttribute(team_id);
    // this.getProductAttribute(team_id);
  },
  methods: {
    getDealDefaultAttribute(team_id) {
      this.isLoading = true;
      this.$store
        .dispatch(DEAL_DEFAULT_ATTRIBUTE, { team_id: team_id })
        .then(res => {
          this.isLoading = false;
          const data = res.data && res.data.data;
          this.setDefaultAttributes(data, CodeName.Deal);
        });
    },

    getCustomerAttribute(team_id) {
      this.$store
        .dispatch(DEAL_GET_CUSTOMER_ATTRIBUTE, { team_id: team_id })
        .then(res => {
          const data = res.data && res.data.data;
          this.setData(data, CodeName.Customer);
        });
    },

    getDealAttribute(team_id) {
      this.isLoadingData = true;
      this.$store
        .dispatch(DEAL_GET_DEAL_ATTRIBUTE, { team_id: team_id })
        .then(res => {
          this.isLoadingData = false;
          const data = res.data && res.data.data;
          this.setData(data, CodeName.Deal);
        });
    },

    getProductAttribute(team_id) {
      this.$store
        .dispatch(DEAL_GET_PRODUCT_ATTRIBUTE, { team_id: team_id })
        .then(res => {
          const data = res.data && res.data.data;
          this.setData(data, CodeName.Product);
        });
    },

    setData(data, codeName) {
      if (!data) return;
      const index = findIndex(this.attributes, { name: codeName });
      const color = this.attributes[index].color;
      data.map(att => {
        att.color = color;
        att.uid = Math.random()
          .toString(36)
          .slice(-5);
      });
      this.attributes[index].data = data;
    },

    setDefaultAttributes(data, codeName) {
      if (!data) return;
      const index = findIndex(this.attributes, { name: codeName });
      const color = this.attributes[index].color;
      data.forEach(att => {
        att.color = color;
        att.keepLocal = true;
        att.uid = Math.random()
          .toString(36)
          .slice(-5);
        att.class = "can-disabled disabled";
        this.selectedData.push(att);
      });
    },

    // onClone(att) {
    //   console.log(att)
    //   return {
    //     ...att,
    //     uid: att.multiple
    //       ? Math.random()
    //           .toString(36)
    //           .slice(-5)
    //       : att.uid
    //   };
    // },

    onChange(event) {
      if (event.added) {
        let elm = event.added.element;
        if (!elm) return;
        if (elm.multiple) {
          const index = findIndex(this.selectedData, elm);
          const uid = Math.random()
            .toString(36)
            .slice(-5);
          elm.class = elm.class ? elm.class + ' stable' : 'stable'
          this.selectedData[index] = Object.assign({}, { ...elm, uid });
        } else {
          elm.isDisabled = true;
          this.$forceUpdate();
        }
      }
      this.moveToEnd();
    },

    moveToEnd() {
      const tempArray = this.selectedData.filter(v => v.id === 'deal_payment_history')
      this.selectedData = this.selectedData.filter(v => v.id !== 'deal_payment_history')
      this.selectedData.push(...tempArray)
    },

    exportExcel() {
      this.isLoadingExport = true;
      const atts = this.selectedData.filter(t => !t.keepLocal);
      let tempStore = [];
      atts.forEach(att => {
        if (att.multiple) {
          const index = findIndex(tempStore, {
            key: att.color + att.attribute_name
          });
          if (index === -1) {
            tempStore.push({
              key: att.color + att.attribute_name,
              position: 1
            });
            att.position = 1;
          } else {
            tempStore[index].position += 1;
            att.position = tempStore[index].position;
          }
        }
      });
      const data = {
        team_id: this.$route.params.team_id,
        attributes: atts
      };
      this.$store.dispatch(DEAL_EXPORT_ATTRIBUTE, data).then(res => {
        this.isLoadingExport = false;
        const url = window.URL.createObjectURL(new Blob([res.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "Export_Excel.xlsx");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        tempStore = [];
      });
    },

    removeAtt(data) {
      const att = this.attributes.filter(t => t.color === data.color)[0];
      if (!att) return;
      att.data.filter(t => t.id === data.id)[0].isDisabled = false;
      const index = findIndex(this.selectedData, data);
      this.selectedData.splice(index, 1);
    }
  }
};
