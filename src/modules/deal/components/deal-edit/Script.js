/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import {
  PRODUCT_GET_ALL_BY_TEAM,
  DEAL_UPDATE,
  DEAL_GET_ATTRIBUTES,
  DEAL_GET_USER_ATTRIBUTES,
  DEAL_GET_DETAIL,
  CUSTOMER_GET_ALL,
  CUSTOMER_GET_ATTRIBUTES,
  CUSTOMER_GET_USER_ATTRIBUTES,
  CUSTOMER_SHOW_DETAIL,
  CUSTOMER_GET_CONTACT
} from "@/types/actions.type";
import {
  PERMISSION_UPDATE_DEAL,
  PERMISSION_UPDATE_PRODUCT_DEAL,
  PERMISSION_UPDATE_DEAL_EXTRA,
  PERMISSION_SHOW_CUSTOMER
} from "@/types/const";
import AddNewCustomer from "@/modules/customer/components/modals/add-new-customer/AddNewCustomer.vue";
import {
  TOAST_ERROR
} from "@/types/config";
import AttributeWithDataType from "../attribute/AttributeWithDataType.vue";
import MessageChangeData from "../modals/message-change/MessageChangeData.vue";

const customMessageOrder = {
  custom: {
    'order_note': {
      max: "Giới hạn 1025 ký tự."
    }
  }
};

export default {
  components: {
    "attribute-with-data-type": AttributeWithDataType,
    "add-new-customer": AddNewCustomer,
    MessageChangeData: MessageChangeData
  },

  created() {
    $(document).ready(function () {
      $("#m_header").css("display", "none");
      $("#app-body").addClass("nt-none-padding-top");
      $("#app-body").removeClass("m-body");
      $("#app-body-subheader").css("display", "none");
    });
  },

  mounted() {
    let vm = this;
    vm.getShowDeal();
    vm.getProductsByTeam();
    vm.getDealAttributeByUser();
    vm.getAll();
    vm.getAttributeByUser();
    vm.$validator.localize('en', customMessageOrder);
  },

  data() {
    return {
      PERMISSION_SHOW_CUSTOMER: PERMISSION_SHOW_CUSTOMER,
      PERMISSION_UPDATE_DEAL: PERMISSION_UPDATE_DEAL,
      PERMISSION_UPDATE_PRODUCT_DEAL: PERMISSION_UPDATE_PRODUCT_DEAL,
      PERMISSION_UPDATE_DEAL_EXTRA: PERMISSION_UPDATE_DEAL_EXTRA,
      deal: {
        customer_id: 0,
        customer_name: "",
        order_date: "",
        attributes: [],
        products: [],
        assigners: [],
        product_discount_type: "percent",
        discount: {
          type: "percent",
          value: 0,
          total_discount: 0
        },
        total: 0,
        total_after_discount: 0,
        vat_type: 0,
        vat_amount: 0,
        created_at: "",
        receiver_name: "",
        user_id: 0
      },
      typingTimer: null,
      server_errors: '',
      isLoadProduct: false,
      productSelected: 0,
      isLoader: false,
      listAttributesDefault: [],
      listAttributesExtend: [],
      listProducts: [],
      noteDeal: "",
      dealId: parseInt(this.$route.params.deal_id),
      listProductsStatus: [{
          id: 1,
          name: "Chưa giao"
        },
        {
          id: 2,
          name: "Đã giao"
        }
      ],
      isDealSuccess: false,
      isDetailDealReady: false,
      isCustomerContactReady: false,
      dataReady: false,
      attributeReady: false,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      loadSearch: false,
      isSearchCustomer: false,
      currentParams: {},
      attributeNoPaymentTime: false,
      attributeNoPaymentTimeCache: false,
      isLoadMoreData: false,
      currentCustomerInDeal: {},
      listVAT: [],
      isLoadSelect: false,
      productUnitDisabled: ['Khóa', 'Buổi', 'Tháng']
    }
  },
  computed: {
    isScreenSmall() {
      let viewportWidth = $(window).width();
      if (viewportWidth <= 768) {
        return true;
      }
      return false;
    },
    isChange() {
      let vm = this;
      let count = 0;
      if (vm.compareArray(vm.deal.assigners, vm.deal.assigners_cache) == false) {
        count++;
      }
      if (vm.compareArray(vm.deal.attributes, vm.deal.attributes_cache) == false) {
        count++;
      }
      if (JSON.stringify(vm.deal.discount) !== JSON.stringify(vm.deal.discount_cache)) {
        count++;
      }
      if (vm.deal.order_date !== vm.deal.order_date_cache) {
        count++;
      }
      if (vm.deal.product_discount_type !== vm.deal.product_discount_type_cache) {
        count++;
      }
      if (vm.compareArray(vm.deal.products, vm.deal.products_cache) == false) {
        count++;
      }
      if (count > 0) {
        return true;
      } else {
        return false;
      }
    },

    isDataLoadingSuccess() {
      let vm = this;
      if (vm.isDealSuccess && vm.isDetailDealReady && vm.dataReady && vm.attributeReady && vm.currentUser && vm.currentTeam) {
        $(document).ready(function () {
          $("#m_header").css("display", "none");
          $("#app-body").addClass("nt-none-padding-top");
          $("#app-body").removeClass("m-body");
          $("#app-body-subheader").css("display", "none");
          let height = $(window).height();
          $('.nt-dealpage-content').css("height", parseFloat(height) - 118 + "px");

          $(window).resize(function () {
            let height = $(window).height();
            $('.nt-dealpage-content').css("height", parseFloat(height) - 118 + "px");
          })
        })
        return true;
      } else {
        return false;
      }
    },

    listCustomersData() {
      let vm = this;
      let items = [];
      vm.listCustomers.forEach(function (item) {
        if (!item.deleted_at) {
          let tempData = {
            customer_id: item.id,
            customer_name: "",
            customer_code: "",
            customer_email: "",
            customer_phone: ""
          };

          item.attributes.forEach(function (itemAttribute) {
            vm.attributeCustomerInProductDeal.forEach(function (attribute) {
              if (itemAttribute.attribute_id == attribute.id) {
                if (attribute.attribute_code == "full_name") {
                  tempData["customer_name"] = itemAttribute.value;
                }
                if (attribute.attribute_code == "sku") {
                  tempData["customer_code"] = itemAttribute.value;
                }
                if (attribute.attribute_code == "email") {
                  tempData["customer_email"] = itemAttribute.value;
                }
                if (attribute.attribute_code == "phone") {
                  tempData["customer_phone"] = itemAttribute.value;
                }
              }
            })
          })

          items.push(tempData);
        }
        // if (item.id !== vm.deal.customer_id) {
        // }
      })
      // let currentCustomerTemp = {
      //     customer_id: vm.deal.customer_id,
      //     customer_name: "",
      //     customer_code: "",
      //     customer_email: "",
      //     customer_phone: ""
      // };
      // vm.detailCustomer.attributes.forEach(function (attribute) {
      //     if (attribute.attribute_code == "full_name") {
      //         currentCustomerTemp["customer_name"] = attribute.attribute_value;
      //     }
      //     if (attribute.attribute_code == "sku") {
      //         currentCustomerTemp["customer_code"] = attribute.attribute_value;
      //     }
      //     if (attribute.attribute_code == "email") {
      //         currentCustomerTemp["customer_email"] = attribute.attribute_value;
      //     }
      //     if (attribute.attribute_code == "phone") {
      //         currentCustomerTemp["customer_phone"] = attribute.attribute_value;
      //     }
      // })
      // vm.currentCustomerInDeal = currentCustomerTemp;
      return items;
    },

    listEditAttributes() {
      let vm = this;
      let disableAttributes = ["customer_code", "customer_name", "customer_email", "transaction_status", "reason_for_cancellation", "total_paid_amount", "order_payment"];
      let firstShow = [];
      let afterShow = [];
      if (vm.detailDeal.attributes && vm.detailDeal.attributes.length > 0) {
        vm.detailDeal.attributes.forEach(function (attribute) {
          if ($.inArray(attribute.attribute_code, disableAttributes) == -1) {
            vm.attributeEditDeal.forEach(function (attrEdit) {
              if (attribute.attribute_code == 'customer_name') {
                vm.deal.customer_name = attribute.attribute_value;
              }
              if (attrEdit.id == attribute.id && $.inArray(attribute.attribute_code, disableAttributes) == -1) {
                if (attribute.attribute_code == 'base_total_transaction_amount') {
                  vm.deal.total = attribute.attribute_value != "" || parseFloat(attribute.attribute_value) > 0 ? attribute.attribute_value : 0;
                }
                if (attribute.attribute_code == 'total_transaction_amount') {
                  vm.deal.total_after_discount = attribute.attribute_value != "" || parseFloat(attribute.attribute_value) > 0 ? attribute.attribute_value : 0;
                }
                if (attribute.attribute_code == 'order_note') {
                  vm.noteDeal = attribute.attribute_value;
                }
                if (attribute.attribute_code == 'order_date') {
                  vm.deal.order_date = attribute.attribute_value;
                }
                if (attribute.attribute_code == 'vat_type') {
                  vm.listVAT = attribute.attribute_options;
                  vm.deal.vat_type = attribute.attribute_value;
                }
                if (attribute.attribute_code == 'vat_amount') {
                  vm.deal.vat_amount = attribute.attribute_value;
                }
                if (attribute.data_type_id === 13 && (!attribute.attribute_value || attribute.attribute_value && attribute.attribute_value.length <= 0 || attribute.attribute_value === '')) {
                  attribute.attribute_value = [];
                }
                attribute.data_type = attrEdit.data_type;
                attribute.attribute_options = attrEdit.attribute_options;
              }
            })
            if (attribute.is_default_attribute) {
              firstShow.push(attribute);
            } else {
              afterShow.push(attribute);
            }
          }
        })
      }
      let attributeResults = [];
      $.merge(attributeResults, firstShow);
      let extendAttributes = [{
          id: "user_id",
          attribute_code: "user_id",
          attribute_id: "user_id",
          attribute_name: "Người tạo",
          attribute_value: vm.detailDeal.created_at,
          is_hidden: 0,
          is_show: 0,
          external_attribute: 1,
          is_auto_gen: true,
          code_external_attribute: "user_id",
          data_type: {
            id: 8
          },
          sort_order: 0
        },
        {
          id: "created_at",
          attribute_code: "created_at",
          attribute_id: "created_at",
          attribute_name: "Ngày tạo",
          attribute_value: vm.detailDeal.user_id,
          is_hidden: 0,
          is_show: 0,
          external_attribute: 1,
          is_auto_gen: true,
          code_external_attribute: "created_at",
          data_type: {
            id: 2
          }
        }
      ];
      $.merge(attributeResults, extendAttributes);
      $.merge(attributeResults, afterShow);
      vm.deal.attributes = attributeResults;
      return attributeResults;
    },
    ...mapGetters(["detailDeal", "listCustomers", "attributeCustomerInProductDeal", "attributeEditDeal", "productByTeams", "currentTeam", "currentUser", "detailCustomer", "userCheckAddOn", "userCurrentPermissions"]),
  },
  watch: {
    isDataLoadingSuccess: {
      handler() {
        let vm = this;
        vm.thenInDealsProducts();
        vm.thenDeals();
      },
      deep: true
    },
    deal: {
      handler() {
        let vm = this;
        let discountValue = vm.clearFormatNumber(vm.deal.discount.value);
        if (discountValue > 0) {
          let price = 0;
          if (vm.deal.discount.type == 'percent') {
            price = (vm.deal.total * (100 - parseFloat(discountValue))) / 100;
          } else if (vm.deal.discount.type == 'amount') {
            if (parseFloat(discountValue) > vm.deal.total) {
              price = 0;
            } else {
              price = vm.deal.total - parseFloat(discountValue);
            }
          }
          vm.deal.discount.total_discount = Math.round(vm.deal.total - price);
          let vatValue = 0;
          let vatOption = vm.listVAT.find(function (item) {
            return item.id === vm.deal.vat_type;
          });
          vatValue = vatOption ? (Math.round(price) * parseInt(vatOption.option_value.replace("%", "").trim())) / 100 : 0;
          vm.deal.vat_amount = vatValue;
          let totalAfterVAT = Math.round(price) + Math.round(vatValue);
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = totalAfterVAT;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
              if (item.attribute_code == 'vat_type') {
                item.attribute_value = vatOption.id;
              }
              if (item.attribute_code == 'vat_amount') {
                item.attribute_value = vatValue;
              }
            })
          }
          vm.deal.total_after_discount = Math.round(totalAfterVAT);
        } else {
          vm.deal.discount.total_discount = 0;
          let vatValue = 0;
          let vatOption = vm.listVAT.find(function (item) {
            return item.id === vm.deal.vat_type;
          });
          let price = vm.deal.total;
          vatValue = vatOption ? (Math.round(price) * parseInt(vatOption.option_value.replace("%", "").trim())) / 100 : 0;
          vm.deal.vat_amount = Math.round(vatValue);
          let totalAfterVAT = Math.round(price) + Math.round(vatValue);
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = totalAfterVAT;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
              if (item.attribute_code == 'vat_type') {
                item.attribute_value = vatOption ? vatOption.id : 0;
              }
              if (item.attribute_code == 'vat_amount') {
                item.attribute_value = vatValue;
              }
            })
          }
          vm.deal.total_after_discount = Math.round(totalAfterVAT);
        }
      },
      deep: true
    },
    noteDeal: {
      handler(after) {
        let vm = this;
        vm.deal.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == 'order_note') {
            attribute.attribute_value = after;
          }
        })
      },
      deep: true
    }
  },
  methods: {
    thenInDealsProducts() {
      let vm = this;
      vm.deal.products = [];
      vm.deal.products_cache = [];
      let disableAttributes = ["customer_code", "customer_name", "customer_email", "transaction_status", "reason_for_cancellation", "total_paid_amount", "order_payment"];
      if (vm.detailDeal.products && vm.detailDeal.products.length > 0) {
        let products = [];
        let key = 1;
        vm.detailDeal.products.forEach(function (product) {
          let tempProduct = {
            id: product.id,
            product_id: product.product_id,
            product_code: product.product_code,
            product_name: product.product_name,
            product_type: product.product_type,
            product_unit: product.product_unit,
            quantity: vm.convertNumberDecimalShow(product.quantity),
            discount: vm.formatNumber(parseFloat(product.discount)),
            product_price: parseFloat(product.price),
            sale_price: parseFloat(product.sale_price),
            total_price: parseFloat(product.total_price),
            status: product.is_delivered,
            is_disabled: product.is_disabled,
            customer: {
              customer_id: product.customer_id,
              customer_name: product.customer_name,
              customer_code: product.customer_code,
              customer_email: product.customer_email,
              customer_phone: product.customer_phone
            },
            sort_order: key
          };
          products.push(tempProduct);
          key++;
        })
        vm.deal.products = products;
      }
      if (vm.detailDeal.deal_discount && Object.keys(vm.detailDeal.deal_discount).length > 0) {
        let discount = {
          id: vm.detailDeal.deal_discount.id,
          deal_id: vm.detailDeal.deal_discount.deal_id,
          type: vm.detailDeal.deal_discount.type,
          value: vm.formatNumber(parseFloat(vm.detailDeal.deal_discount.value)),
          total_discount: parseFloat(vm.detailDeal.deal_discount.total_discount)
        };
        vm.deal.discount = discount;
      }
      vm.deal.assigners_cache = vm.detailDeal.assigners_deal_cache && vm.detailDeal.assigners_deal_cache.length > 0 ? vm.detailDeal.assigners_deal_cache : [];
      if (vm.detailDeal.attributes_cache && vm.detailDeal.attributes_cache.length > 0) {
        let resultAttributesCache = [];
        vm.detailDeal.attributes_cache.forEach(function (attribute) {
          vm.attributeEditDeal.forEach(function (attrEdit) {
            if (attribute.attribute_code == 'customer_name') {
              vm.deal.customer_name = attribute.attribute_value;
            }
            if (attrEdit.id == attribute.id && $.inArray(attribute.attribute_code, disableAttributes) == -1) {
              if (attribute.attribute_code == 'base_total_transaction_amount') {
                vm.deal.total = attribute.attribute_value;
                vm.deal.total_cache = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'total_transaction_amount') {
                vm.deal.total_after_discount = attribute.attribute_value;
                vm.deal.total_after_discount_cache = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_note') {
                vm.noteDeal = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_date') {
                vm.deal.order_date = attribute.attribute_value;
                vm.deal.order_date_cache = attribute.attribute_value;
              }
              attribute.data_type = attrEdit.data_type;
              attribute.attribute_options = attrEdit.attribute_options;
              resultAttributesCache.push(attribute);
            }
          })
        })
        vm.deal.attributes_cache = resultAttributesCache;
      }
      if (vm.detailDeal.products_cache && vm.detailDeal.products_cache.length > 0) {
        let products_cache = [];
        let key = 1;
        vm.detailDeal.products_cache.forEach(function (product) {
          let tempProduct = {
            id: product.id,
            product_id: product.product_id,
            product_code: product.product_code,
            product_name: product.product_name,
            product_type: product.product_type,
            product_unit: product.product_unit,
            quantity: vm.convertNumberDecimalShow(product.quantity),
            discount: vm.formatNumber(parseFloat(product.discount)),
            product_price: parseFloat(product.price),
            sale_price: parseFloat(product.sale_price),
            total_price: parseFloat(product.total_price),
            status: product.is_delivered,
            customer: {
              customer_id: product.customer_id,
              customer_name: product.customer_name,
              customer_code: product.customer_code,
              customer_email: product.customer_email,
              customer_phone: product.customer_phone
            },
            sort_order: key
          };
          products_cache.push(tempProduct);
          key++;
        })
        vm.deal.products_cache = products_cache;
      }
      if (vm.detailDeal.deal_discount_cache && Object.keys(vm.detailDeal.deal_discount_cache).length > 0) {
        let discount = {
          id: vm.detailDeal.deal_discount.id,
          deal_id: vm.detailDeal.deal_discount.deal_id,
          type: vm.detailDeal.deal_discount.type,
          value: vm.formatNumber(parseFloat(vm.detailDeal.deal_discount.value)),
          total_discount: parseFloat(vm.detailDeal.deal_discount.total_discount)
        };
        vm.deal.discount_cache = discount;
      }
      vm.deal.product_discount_type_cache = vm.detailDeal.deal_type_cache;
      vm.deal.customer_id = vm.detailDeal.customer_id;
      vm.deal.product_discount_type = vm.detailDeal.deal_type;
      vm.deal.created_at = vm.detailDeal.created_at;
      vm.deal.assigners = vm.detailDeal.assigners_deal && vm.detailDeal.assigners_deal.length > 0 ? vm.detailDeal.assigners_deal : [];
    },

    thenProducts() {
      let vm = this;
      let key = 1;
      vm.productByTeams.forEach(function (product) {
        if (!product.deleted_at) {
          product.quantity = 1;
          product.discount = 0;
          product.product_price = product.product_price == "" || product.product_price == null ? product.product_price = 0 : parseFloat(product.product_price);
          product.sale_price = product.product_price;
          product.total_price = product.product_price;
          product.sort_order = key;
          vm.listProducts.push(product);
          key++;
        }
      })
    },

    thenDeals(attributes = []) {
      let vm = this;
      vm.listAttributesDefault = [];
      vm.listAttributesExtend = [];
      let forAttributes = [];
      if (attributes && attributes.length > 0) {
        forAttributes = attributes;
      } else {
        forAttributes = vm.listEditAttributes;
      }
      forAttributes.forEach(function (attribute) {
        if (attribute.is_hidden == 0 && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'base_total_transaction_amount' && attribute.attribute_code != 'vat_type' && attribute.attribute_code != 'vat_amount' && attribute.is_default_attribute) {
          vm.listAttributesDefault.push(attribute);
        } else if ((attribute.is_hidden == 1 && attribute.is_default_attribute || !attribute.is_default_attribute && attribute.is_hidden == 0 || attribute.external_attribute) && attribute.attribute_code != 'order_note' && attribute.attribute_code != 'customer_phone' && attribute.attribute_code != 'customer_email' && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'base_total_transaction_amount' && attribute.attribute_code != 'vat_type' && attribute.attribute_code != 'vat_amount') {
          vm.listAttributesExtend.push(attribute);
        }
      })
    },

    getShowDeal() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_DETAIL, vm.dealId).then(response => {
        if (response.data.data) {
          if (vm.accessible(vm.userCheckAddOn, 'customer', vm.PERMISSION_SHOW_CUSTOMER, vm.userCurrentPermissions)) {
            vm.$store.dispatch(CUSTOMER_SHOW_DETAIL, response.data.data.customer_id).then(responseCustomer => {
              if (responseCustomer.data.data) {
                vm.isDetailDealReady = true;
                vm.setCurrentCustomerInDeal(responseCustomer.data.data)
                let params = {
                  customer_id: response.data.data.customer_id,
                };
                vm.getAllContactByCustomer(params);
              }
            }).catch(error => {
              console.log(error);
            });
          } else {
            vm.isDetailDealReady = true;
            let params = {
              customer_id: response.data.data.customer_id,
            };
            vm.getAllContactByCustomer(params);
          }
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getAllContactByCustomer(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_CONTACT, params).then((response) => {
        if (response.data.data) {
          vm.isCustomerContactReady = true;
          //console.log(response.data.data, '123');
        }
      }).catch((error) => {
        console.log(error)
      });
    },

    getDealAttributes() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(() => {
        vm.isDealSuccess = true;
      }).catch(error => {
        console.log(error);
      });
    },

    getDealAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_USER_ATTRIBUTES).then(() => {
        vm.getDealAttributes();
      }).catch(error => {
        console.log(error);
      });
    },

    getProductsByTeam() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_GET_ALL_BY_TEAM, {
        team_id: vm.currentTeamId
      }).then(response => {
        if (response.data.data) {
          vm.thenProducts();
          vm.isLoadProduct = true;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getAll(type = "all") {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      vm.$store.dispatch(CUSTOMER_GET_ALL, vm.currentParams).then(response => {
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.oldCurrentPage = response.data.meta.current_page;
        vm.dataReady = true;
        if (type == "load_more") {
          vm.isLoadMoreData = false;
        }
        if (vm.loadSearch) {
          vm.isSearchCustomer = true;
        } else {
          vm.isSearchCustomer = false;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    loadMoreData(status) {
      let vm = this;
      if (status) {
        if (parseInt(vm.pagination.per_page) < vm.pagination.total_item) {
          vm.isLoadMoreData = true;
          let perPage = parseInt(vm.pagination.per_page) + 10;
          vm.currentParams.per_page = perPage;
          vm.getAll("load_more");
        }
      }
    },

    getAttributes() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then(() => {
        vm.attributeReady = true;
      }).catch(error => {
        console.log(error);
      });
    },

    getAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_USER_ATTRIBUTES).then(() => {
        vm.getAttributes();
      }).catch(error => {
        console.log(error);
      });
    },

    customerSearch(value) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        let searchText = value.trim();
        if (searchText && searchText.length > 1) {
          vm.pagination.current_page = 1;
          vm.currentParams.search = searchText;
          vm.getAll();
          vm.loadSearch = true;
        } else {
          if (vm.currentParams.search) {
            vm.pagination.current_page = 1;
            delete vm.currentParams.search;
          }
          if (!searchText || (searchText && searchText.length == 0)) {
            vm.getAll();
            vm.loadSearch = false;
          }
        }
      }, 500);
    },

    selectTypeDiscount(event) {
      let vm = this;
      vm.deal.products.forEach(function (item) {
        item.discount = 0;
        item.sale_price = item.product_price;
        item.total_price = item.product_price * parseInt(String(item.quantity).replace(/[^\d]/g, ""));
      })
      vm.deal.product_discount_type = event.target.value;
      vm.getTotalPrice();
    },

    selectTypeAdditionalDiscount(event) {
      let vm = this;
      vm.deal.discount.value = 0;
      vm.deal.total_after_discount = vm.deal.total;
      vm.deal.discount.total_discount = 0;
      vm.deal.discount.type = event.target.value;
    },

    setOnlyNumberInInput(event) {
      let keyCode = event.keyCode;
      if (!((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 106) || keyCode == 8 || keyCode == 188 || keyCode == 37 || keyCode == 39)) {
        event.preventDefault();
      }
    },

    changeQuantityProduct(product, index) {
      let vm = this;
      let stringQuantity = String(product.quantity);
      if (product.quantity == "" || vm.convertNumberDecimal(stringQuantity) < 0 || isNaN(vm.convertNumberDecimal(stringQuantity))) {
        product.quantity = 0;
      }
      let currentQuantity = vm.convertNumberDecimal(stringQuantity);
      let price = parseFloat(product.sale_price) * currentQuantity;
      vm.deal.products[index].total_price = Math.round(price);
      vm.getTotalPrice();
    },

    changeDiscount(product, type, index) {
      let vm = this;
      let salePrice = 0;
      let price = 0;
      let discount = 0;
      if (product.discount == "" || product.discount < 0 || isNaN(product.discount)) {
        product.discount = 0;
      }
      let currentDiscount = vm.clearFormatNumber(product.discount);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          salePrice = 0;
          discount = 100;
        } else {
          discount = product.discount;
          salePrice = (parseFloat(product.product_price) * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > product.product_price) {
          discount = product.product_price;
          salePrice = 0;
        } else {
          discount = product.discount;
          salePrice = product.product_price - currentDiscount;
        }
      }
      vm.deal.products[index].discount = discount;
      price = salePrice * vm.clearFormatNumber(product.quantity);
      vm.deal.products[index].sale_price = Math.round(salePrice);
      vm.deal.products[index].total_price = Math.round(price);
      vm.getTotalPrice();
    },

    getTotalPrice() {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        let totalPrice = 0;
        vm.deal.products.forEach(function (product) {
          totalPrice = totalPrice + parseFloat(product.total_price);
        })
        vm.deal.total = Math.round(totalPrice);
      } else {
        vm.deal.total = 0;
      }
    },

    changeAdditionalDiscount(type) {
      let vm = this;
      let price = 0;
      let valueOut = 0;
      if (vm.deal.discount.value == "" || vm.deal.discount.value < 0 || isNaN(vm.deal.discount.value)) {
        vm.deal.discount.value = 0;
      }
      let currentDiscount = vm.clearFormatNumber(vm.deal.discount.value);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          valueOut = 100;
          price = 0;
        } else {
          valueOut = vm.deal.discount.value;
          price = (parseFloat(vm.deal.total) * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > vm.deal.total) {
          price = 0;
          valueOut = vm.deal.total;
        } else {
          valueOut = vm.deal.discount.value;
          price = vm.deal.total - currentDiscount;
        }
      }
      vm.deal.discount.value = valueOut;
      let newPrice = Math.round(price);
      let newTotalDiscount = Math.round(vm.deal.total - newPrice);
      vm.deal.discount.total_discount = Math.round(newTotalDiscount);
      vm.deal.total_after_discount = Math.round(newPrice);
    },

    reset() {
      let vm = this;
      location.href = "/team/" + vm.currentTeamId + "/deals/" + vm.dealId;
    },

    closeProductFromTable(sort_order) {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        if (vm.deal.products.length > 1) {
          vm.deal.products.forEach(function (item, index) {
            if (item.sort_order == sort_order) {
              vm.deal.products.splice(index, 1);
            }
          })
        } else {
          vm.deal.products = [];
        }
        vm.getTotalPrice();
        vm.isLoadSelect = true;
      }
    },

    setCurrentCustomerInDeal(detailCustomer) {
      let vm = this
      let customerId = detailCustomer.id
      let customerName = ''
      let customerCode = ''
      let customerEmail = ''
      let customerPhone = ''

      detailCustomer.attributes.forEach(function (attribute) {
        if (attribute.attribute_code == "full_name") {
          customerName = attribute.attribute_value
        }
        if (attribute.attribute_code == "sku") {
          customerCode = attribute.attribute_value
        }
        if (attribute.attribute_code == "email") {
          customerEmail = attribute.attribute_value
        }
        if (attribute.attribute_code == "phone") {
          customerPhone = attribute.attribute_value
        }
      })

      vm.currentCustomerInDeal = {
        customer_id: customerId,
        customer_name: customerName,
        customer_code: customerCode,
        customer_phone: customerPhone,
        customer_email: customerEmail
      }
    },

    selectProductInDeal() {
      let vm = this;
      if (vm.listProducts && vm.listProducts.length > 0 && vm.productSelected > 0) {
        let totalPrice = 0;
        // vm.currentCustomerInDeal = {
        //   customer_id: "",
        //   customer_name: "",
        //   customer_code: "",
        //   customer_phone: "",
        //   customer_email: ""
        // };
        vm.setCurrentCustomerInDeal(vm.detailCustomer)

        vm.listProducts.forEach(function (product) {
          if (product.id == vm.productSelected) {
            let newProduct = {
              id: "",
              product_id: product.id,
              discount: product.discount,
              product_code: product.product_code,
              product_type: product.product_type,
              product_name: product.product_name,
              product_price: product.product_price,
              quantity: product.quantity,
              sale_price: product.sale_price,
              sort_order: vm.deal.products && vm.deal.products.length >= 0 ? vm.deal.products.length + 1 : 0,
              status: 1,
              customer: vm.currentCustomerInDeal,
              total_price: product.total_price,
              product_unit: product.product_unit,
              is_add_new: true
            };
            totalPrice = product.product_price;
            vm.deal.products.push(newProduct);
          }
        })
        vm.deal.total = parseFloat(vm.deal.total) + parseFloat(totalPrice);
        vm.productSelected = 0;
      }
    },

    formatNumber(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        newValue = newValue.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
      } else {
        newValue = parseFloat(newValue);
      }
      return newValue;
    },

    convertNumberDecimalShow(value = "") {
      let newValue = String(parseFloat(value));
      if (newValue.length > 0) {
        if (newValue.indexOf(".") > -1) {
          let originalValue = newValue.substring(0, newValue.indexOf("."));
          let decimalValue = newValue.substring(newValue.indexOf(".") + 1, newValue.length);
          let originValueConvert = originalValue.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
          let decimalValueConvert = "";
          if (parseInt(decimalValue.charAt(2)) >= 5) {
            if (parseInt(decimalValue.charAt(1)) >= 9 && parseInt(decimalValue.charAt(0)) >= 9) {
              originalValue = parseFloat(originValueConvert) + 1;
              decimalValueConvert = 0;
            } else {
              decimalValueConvert = parseInt(decimalValue.substring(0, 2)) + 1;
            }
          } else {
            decimalValueConvert = decimalValue.substring(0, 2);
          }
          if (parseFloat(decimalValueConvert) > 0) {
            newValue = originValueConvert + "," + decimalValueConvert;
          } else {
            newValue = originValueConvert;
          }
        }
      }
      return newValue;
    },

    convertNumberDecimal(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        if (newValue.indexOf(",") > -1) {
          let originalValue = newValue.substring(0, newValue.indexOf(","));
          let decimalValue = newValue.substring(newValue.indexOf(",") + 1, newValue.length);
          let decimalValueConvert = "";
          if (parseInt(decimalValue.charAt(2)) >= 5) {
            if (parseInt(decimalValue.charAt(1)) >= 9 && parseInt(decimalValue.charAt(0)) >= 9) {
              originalValue = parseFloat(originalValue) + 1;
            } else {
              decimalValueConvert = parseInt(decimalValue.substring(0, 2)) + 1;
            }
          } else {
            decimalValueConvert = decimalValue.substring(0, 2);
          }
          newValue = parseFloat(originalValue + "." + decimalValueConvert);
        } else {
          newValue = parseFloat(newValue);
        }
      }
      return newValue;
    },

    clearFormatNumber(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        if (newValue.indexOf(",") > -1) {
          let originalValue = newValue.substring(0, newValue.indexOf(","));
          originalValue = String(parseFloat(originalValue));
          let decimalValue = newValue.substring(newValue.indexOf(",") + 1, newValue.length);
          let originValueConvert = parseFloat(originalValue.replace(/[^\d]/g, ""));
          let decimalValueConvert = "";
          if (parseInt(decimalValue.charAt(2)) >= 5) {
            if (parseInt(decimalValue.charAt(1)) >= 9 && parseInt(decimalValue.charAt(0)) >= 9) {
              originValueConvert = parseFloat(originValueConvert) + 1;
              decimalValueConvert = 0;
            } else {
              decimalValueConvert = parseInt(decimalValue.substring(0, 2)) + 1;
            }
          } else {
            decimalValueConvert = decimalValue.substring(0, 2);
          }
          newValue = originValueConvert + "." + decimalValueConvert;
          newValue = parseFloat(newValue);
        } else {
          newValue = parseFloat(newValue.replace(/[^\d]/g, ""));
        }
      }
      return newValue;
    },

    formatNumberBlur(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {
              if (String(product.quantity).indexOf(",") > -1) {
                let originalValue = String(product.quantity).substring(0, String(product.quantity).indexOf(","));
                originalValue = String(parseFloat(originalValue));
                let decimalValue = String(product.quantity).substring(String(product.quantity).indexOf(",") + 1, String(product.quantity).length);
                let originValueConvert = originalValue.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
                let decimalValueConvert = "";
                if (parseInt(decimalValue.charAt(2)) >= 5) {
                  if (parseInt(decimalValue.charAt(1)) >= 9 && parseInt(decimalValue.charAt(0)) >= 9) {
                    originValueConvert = parseFloat(originValueConvert) + 1;
                    decimalValueConvert = 0;
                  } else {
                    decimalValueConvert = parseInt(decimalValue.substring(0, 2)) + 1;
                  }
                } else {
                  decimalValueConvert = decimalValue.substring(0, 2);
                }
                let newValue = "";
                if (parseFloat(decimalValueConvert) > 0) {
                  newValue = originValueConvert + "," + decimalValueConvert;
                } else {
                  newValue = originValueConvert;
                }
                product.quantity = newValue;
              } else {
                product.quantity = String(parseFloat(product.quantity)).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
              }
            } else if (type === 'discount') {
              product.discount = String(parseFloat(product.discount)).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = String(parseFloat(vm.deal.discount.value)).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
      }
    },

    clearFormatNumberFocus(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {
              if (String(product.quantity).indexOf(",") > -1) {
                let originalValue = String(product.quantity).substring(0, String(product.quantity).indexOf(","));
                let decimalValue = String(product.quantity).substring(String(product.quantity).indexOf(",") + 1, String(product.quantity).length);
                let originValueConvert = originalValue.replace(".", "");
                let decimalValueConvert = decimalValue.replace(".", "");
                let newValue = originValueConvert + "," + decimalValueConvert;
                product.quantity = newValue;
              } else {
                product.quantity = String(product.quantity).replace(".", "");
              }
            } else if (type === 'discount') {
              product.discount = vm.clearFormatNumber(product.discount);
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = vm.clearFormatNumber(vm.deal.discount.value);
      }
    },

    switchCollapseDeal(event) {
      let current = event.currentTarget;
      let arrow = $(current).parent(".m--header-collapse").children(".btn-collapse").children(".icon");
      if ($(arrow).hasClass("icon-down-arrow-1")) {
        $(arrow).removeClass("icon-down-arrow-1");
        $(arrow).addClass("icon-up-arrow-1");
      } else {
        $(arrow).removeClass("icon-up-arrow-1");
        $(arrow).addClass("icon-down-arrow-1");
      }
    },

    updateDeal() {
      let vm = this;
      vm.$validator.validateAll("editDeal").then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          // let dealProduct = vm.deal.products;
          // dealProduct.filter(detail => {
          //   if (detail.product_type === 'Dịch vụ') {
          //     return detail.quantity = 1
          //   }
          // });

          // Check giao dịch 0 đồng chuyển trạng thái thành toán thành đã thanh toán hết
          if (vm.deal.total_after_discount == 0) {
            if (vm.deal.attributes && vm.deal.attributes.length > 0) {
              vm.deal.attributes.forEach(function (attribute) {
                if (attribute.attribute_code == "order_payment_status") {
                  if (attribute.attribute_options && attribute.attribute_options.length > 0) {
                    attribute.attribute_options.forEach(function (option) {
                      if (option.option_value == "Thanh toán hết") {
                        attribute.attribute_value = option.id;
                      }
                    })
                  }

                }
              });
            }
          }

          vm.$store.dispatch(DEAL_UPDATE, {
            id: vm.detailDeal.id,
            params: vm.deal
          }).then(response => {
            if (response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              if (vm.server_errors.not_permission_update_deal) {
                $("#policyErrors").modal("show");
              } else if (vm.server_errors.edu_not_permission_update_deal) {
                $("#policyErrorsEdu").modal("show");
              }
              vm.$snotify.error('Cập nhật giao dịch thất bại.', TOAST_ERROR);
            } else if (response.data.data) {
              if (response.data.data.sort_delete) {
                if (response.data.data.reload) {
                  $("#messageChangeData").modal('show');
                  vm.$snotify.error('Cập nhật giao dịch thất bại.', TOAST_ERROR);
                }
              } else {
                if (response.data.data.status) {
                  localStorage.setItem('UpdateDealSuccess', true);
                  return window.location.href = "/team/" + vm.currentTeamId + "/deals/" + vm.dealId;
                  // vm.$snotify.success('Cập nhật giao dịch thành công.', TOAST_SUCCESS);
                  // vm.$router.push({
                  //   name: 'DealProductInfo',
                  //   params: {
                  //     team_id: vm.currentTeamId,
                  //     deal_id: vm.dealId
                  //   }
                  // });
                  // $("#m_header").css("display", "block");
                  // $("#app-body").removeClass("nt-none-padding-top");
                  // $("#app-body").addClass("m-body");
                  // $("#app-body-subheader").css("display", "block");
                } else {
                  if (response.data.data.code && response.data.data.code === 'not_exist') {
                    $("#server_change_data").modal("show");
                    vm.$snotify.error('Cập nhật giao dịch thất bại.', TOAST_ERROR);
                  } else {
                    vm.$snotify.error('Cập nhật giao dịch thất bại.', TOAST_ERROR);
                  }
                }
              }
            }
            vm.isLoader = false;
          }).catch(() => {
            vm.isLoader = false;
            vm.$snotify.error('Cập nhật giao dịch thất bại.', TOAST_ERROR);
          });
        }
      });
    },

    redirectToDetail() {
      let vm = this;
      vm.$router.push({
        name: 'DealProductInfo',
        params: {
          team_id: vm.currentTeamId,
          deal_id: vm.dealId
        }
      });
      $("#m_header").css("display", "block");
      $("#app-body").removeClass("nt-none-padding-top");
      $("#app-body").addClass("m-body");
      $("#app-body-subheader").css("display", "block");
    },

    compareArray(arr1, arr2) {
      if (!arr1 || !arr2) return false;
      let count = 0;
      arr1.forEach(function (e1, id1) {
        arr2.forEach(function (e2, id2) {
          if (typeof arr1 == 'object' && typeof arr2 == 'object') {
            if (id1 === id2) {
              if (e1.attribute_code === "contact_user_phone" && e2.attribute_code === "contact_user_phone") {
                let value1 = e1.attribute_value.replace(/[^\d]/g, "");
                let value2 = e2.attribute_value.replace(/[^\d]/g, "");
                if (value1 === value2) {
                  count++;
                } else {
                  return;
                }
              } else if (JSON.stringify(e1) === JSON.stringify(e2)) {
                count++;
              } else {
                return;
              }
            } else {
              return;
            }
          } else {
            return false;
          }
        })
      })
      if (arr1.length > arr2.length) {
        if (arr1.length === count) {
          return true;
        } else {
          return false;
        }
      } else {
        if (arr2.length === count) {
          return true;
        } else {
          return false;
        }
      }
    },

    updateValueProductCustomer(product) {
      this.deal.products.forEach((productItem, index) => {
        if (product.index === index) {
          productItem.customer = product.data
        }
      })
      this.$forceUpdate()
    },

    refreshPage() {
      return window.location.reload();
    },

    afterCreateSuccess() {
      let vm = this;
      vm.getAll();
    },

    loadingSelectSuccess() {
      let vm = this;
      vm.isLoadSelect = false;
    },
    reloadPage() {
      return window.location.reload();
    }
  }
}