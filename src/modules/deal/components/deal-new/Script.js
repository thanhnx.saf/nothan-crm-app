/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import {
  PRODUCT_GET_ALL_BY_TEAM,
  DEAL_CREATE,
  DEAL_CREATE_ADD_STATUS,
  DEAL_GET_ATTRIBUTES,
  DEAL_GET_USER_ATTRIBUTES,
  CUSTOMER_SHOW_DETAIL,
  CUSTOMER_GET_ALL,
  CUSTOMER_GET_ATTRIBUTES,
  CUSTOMER_GET_USER_ATTRIBUTES,
  CUSTOMER_GET_CONTACT
} from "@/types/actions.type";
import AddNewCustomer from "@/modules/customer/components/modals/add-new-customer/AddNewCustomer.vue";
import MessageBackToCustomerById from "../modals/message-back/MessageBackToCustomerById.vue";
import MessageChangeData from "../modals/message-change/MessageChangeData.vue";
import {
  TOAST_ERROR
} from "@/types/config";
import AttributeWithDataType from "../attribute/AttributeWithDataType.vue";
import {
  PERMISSION_SHOW_DEAL
} from "@/types/const";

const customMessageOrder = {
  custom: {
    'order_note': {
      max: "Giới hạn 1025 ký tự."
    }
  }
};

export default {
  components: {
    "attribute-with-data-type": AttributeWithDataType,
    "add-new-customer": AddNewCustomer,
    MessageBackToCustomerById: MessageBackToCustomerById,
    MessageChangeData: MessageChangeData,
  },
  created() {
    $(document).ready(function () {
      $("#m_header").css("display", "none");
      $("#app-body").addClass("nt-none-padding-top");
      $("#app-body").removeClass("m-body");
      $("#app-body-subheader").css("display", "none");
    })
  },
  mounted() {
    let vm = this;
    vm.getShowCustomer();
    vm.getProductsByTeam();
    vm.getDealAttributeByUser();
    vm.getAll();
    vm.getAttributeByUser();
    vm.$validator.localize('en', customMessageOrder);
  },
  data() {
    return {
      PERMISSION_SHOW_DEAL: PERMISSION_SHOW_DEAL,
      deal: {
        customer_id: 0,
        customer_name: "",
        order_date: "",
        attributes: [],
        products: [],
        assigners: [],
        product_discount_type: "percent",
        discount: {
          type: "percent",
          value: 0,
          total_discount: 0
        },
        total: 0,
        vat_amount: 0,
        vat_type: 0,
        total_after_discount: 0
      },
      typingTimer: null,
      server_errors: '',
      isLoadProduct: false,
      productSelected: 0,
      isLoader: false,
      listAttributesDefault: [],
      listAttributesExtend: [],
      listProducts: [],
      noteDeal: "",
      customerId: parseInt(this.$route.params.customer_id),
      listProductsStatus: [{
          id: 1,
          name: "Chưa giao"
        },
        {
          id: 2,
          name: "Đã giao"
        }
      ],
      isDealSuccess: false,
      isCustomerSuccess: false,
      isCustomerContactReady: false,
      dataReady: false,
      attributeReady: false,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      loadSearch: false,
      isSearchCustomer: false,
      currentParams: {},
      isLoadMoreData: false,
      currentCustomerInDeal: {},
      listVAT: [],
      isLoadSelect: false
    }
  },
  computed: {
    isScreenSmall() {
      let viewportWidth = $(window).width();
      if (viewportWidth <= 768) {
        return true;
      }
      return false;
    },
    isDataLoadingSuccess() {
      let vm = this;
      if (vm.isDealSuccess && vm.isCustomerSuccess && vm.dataReady && vm.attributeReady && vm.currentUser && vm.currentTeam && vm.isCustomerContactReady) {
        $(document).ready(function () {
          $("#m_header").css("display", "none");
          $("#app-body").addClass("nt-none-padding-top");
          $("#app-body").removeClass("m-body");
          $("#app-body-subheader").css("display", "none");
          let height = $(window).height();
          $('.nt-dealpage-content').css("height", parseFloat(height) - 118 + "px");

          $(window).resize(function () {
            let height = $(window).height();
            $('.nt-dealpage-content').css("height", parseFloat(height) - 118 + "px");
          })

          $("#addNewCustomer .modal-footer .m-btn--custom.modal-close").addClass("nt-button");
          $("#addNewCustomer .modal-footer .m-btn--custom.modal-close").addClass("nt-button-secondary");
          $("#addNewCustomer .modal-footer .m-btn--custom.btn-primary").addClass("nt-button");
          $("#addNewCustomer .modal-footer .m-btn--custom.btn-primary").addClass("nt-button-warning");
        })
        vm.thenDeals();
        return true;
      } else {
        return false;
      }
    },

    listCustomersData() {
      let vm = this;
      let items = [];
      vm.listCustomers.forEach(function (item) {
        if (!item.deleted_at) {
          let tempData = {
            customer_id: item.id,
            customer_name: "",
            customer_code: "",
            customer_email: "",
            customer_phone: ""
          };
          item.attributes.forEach(function (itemAttribute) {
            vm.attributeCustomerInProductDeal.forEach(function (attribute) {
              if (itemAttribute.attribute_id == attribute.id) {
                if (attribute.attribute_code == "full_name") {
                  tempData["customer_name"] = itemAttribute.value;
                }
                if (attribute.attribute_code == "sku") {
                  tempData["customer_code"] = itemAttribute.value;
                }
                if (attribute.attribute_code == "email") {
                  tempData["customer_email"] = itemAttribute.value;
                }
                if (attribute.attribute_code == "phone") {
                  tempData["customer_phone"] = itemAttribute.value;
                }
              }
            })
          })
          items.push(tempData);
        }
      })
      return items;
    },

    listAddAttributes() {
      let vm = this;
      let attributes = [];
      vm.attributeAddDeal.forEach(function (attribute) {
        if (attribute.attribute_code == "customer_code") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "sku") {
              attribute.attribute_value = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "customer_name") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "full_name") {
              attribute.attribute_value = item.attribute_value;
              vm.deal.customer_name = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "customer_phone") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "phone") {
              attribute.attribute_value = item.attribute_value ? item.attribute_value.replace(/ /g, "") : '';
            }
          })
        } else if (attribute.attribute_code == "customer_email") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "email") {
              attribute.attribute_value = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "transaction_status") {
          attribute.attribute_value = attribute.attribute_options[0].id;
        } else if (attribute.attribute_code == "order_payment_status") {
          attribute.attribute_value = attribute.attribute_options[0].id;
        } else if (attribute.attribute_code == "order_name") {
          let nowDay = new Date();
          let dd = nowDay.getDate();
          let mm = nowDay.getMonth() + 1;
          let yyyy = nowDay.getFullYear();
          if (dd < 10) {
            dd = '0' + dd;
          }
          if (mm < 10) {
            mm = '0' + mm;
          }
          let newDayFormat = dd + '/' + mm + '/' + yyyy;
          let customer = vm.detailCustomer.attributes.find(function (item) {
            return item.attribute_code == "full_name";
          })
          let customerName = customer.attribute_value;
          let orderName = "GD-" + customerName + '-' + newDayFormat;
          attribute.attribute_value = orderName;
        } else if (attribute.attribute_code == "order_date") {
          let nowDate = new Date();
          attribute.attribute_value = nowDate;
          vm.deal.order_date = nowDate;
        } else if (attribute.attribute_code == "vat_type") {
          vm.listVAT = attribute.attribute_options;
          vm.deal.vat_type = attribute.attribute_options[0].id;
        } else if (attribute.attribute_code == "total_transaction_amount") {
          attribute.attribute_value = 0;
        } else if (attribute.attribute_code == "base_total_transaction_amount") {
          attribute.attribute_value = 0;
        } else if (attribute.attribute_code == "total_paid_amount") {
          attribute.attribute_value = 0;
        } else if (attribute.data_type_id === 13) {
          attribute.attribute_value = [];
        } else {
          attribute.attribute_value = '';
        }
        attributes.push(attribute);
      })
      let disableAttributes = ["reason_for_cancellation", "order_payment"];
      let resultAttributes = [];
      attributes.forEach(function (attribute) {
        if ($.inArray(attribute.attribute_code, disableAttributes) == -1) {
          resultAttributes.push(attribute);
        }
      })
      vm.deal.attributes = resultAttributes;
      return resultAttributes;
    },
    ...mapGetters(["listCustomers", "attributeCustomerInProductDeal", "attributeAddDeal", "productByTeams", "currentTeam", "currentUser", "detailCustomer", "userCheckAddOn", "userCurrentPermissions"]),
  },
  watch: {
    deal: {
      handler() {
        let vm = this;
        let discountValue = vm.clearFormatNumber(vm.deal.discount.value);
        if (discountValue > 0) {
          let price = 0;
          if (vm.deal.discount.type == 'percent') {
            price = (vm.deal.total * (100 - parseFloat(discountValue))) / 100;
          } else if (vm.deal.discount.type == 'amount') {
            if (parseFloat(discountValue) > vm.deal.total) {
              price = 0;
            } else {
              price = vm.deal.total - parseFloat(discountValue);
            }
          }
          let newPrice = Math.round(price);
          let newTotalDiscount = Math.round(vm.deal.total - newPrice);
          vm.deal.discount.total_discount = newTotalDiscount;
          let vatValue = 0;
          let vatOption = vm.listVAT.find(function (item) {
            return item.id === vm.deal.vat_type;
          });
          vatValue = vatOption ? (Math.round(price) * parseInt(vatOption.option_value.replace("%", "").trim())) / 100 : 0;
          vm.deal.vat_amount = vatValue;
          let totalAfterVAT = Math.round(price) + Math.round(vatValue);
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = totalAfterVAT;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
              if (item.attribute_code == 'vat_type') {
                item.attribute_value = vatOption.id;
              }
              if (item.attribute_code == 'vat_amount') {
                item.attribute_value = vatValue;
              }
            })
          }
          vm.deal.total_after_discount = Math.round(totalAfterVAT);
        } else {
          vm.deal.discount.total_discount = 0;
          let vatValue = 0;
          let vatOption = vm.listVAT.find(function (item) {
            return item.id === vm.deal.vat_type;
          });
          let price = vm.deal.total;
          vatValue = vatOption ? (Math.round(price) * parseInt(vatOption.option_value.replace("%", "").trim())) / 100 : 0;
          vm.deal.vat_amount = Math.round(vatValue);
          let totalAfterVAT = Math.round(price) + Math.round(vatValue);
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = totalAfterVAT;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
              if (item.attribute_code == 'vat_type') {
                item.attribute_value = vatOption.id;
              }
              if (item.attribute_code == 'vat_amount') {
                item.attribute_value = vatValue;
              }
            })
          }
          vm.deal.total_after_discount = Math.round(totalAfterVAT);
        }
      },
      deep: true
    },
    noteDeal: {
      handler(after) {
        let vm = this;
        vm.deal.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == 'order_note') {
            attribute.attribute_value = after;
          }
        })
      },
      deep: true
    }
  },
  methods: {
    thenProducts() {
      let vm = this;
      vm.productByTeams.forEach(function (product) {
        if (!product.deleted_at) {
          product.quantity = 1;
          product.discount = 0;
          product.product_price = product.product_price == "" || product.product_price == null ? product.product_price = 0 : parseFloat(product.product_price);
          product.sale_price = product.product_price;
          product.total_price = product.product_price;
          vm.listProducts.push(product);
        }
      })
    },
    thenDeals() {
      let vm = this;
      vm.listAttributesDefault = [];
      vm.listAttributesExtend = [];
      vm.listAddAttributes.forEach(function (attribute) {
        if (attribute.is_hidden == 0 && attribute.attribute_code != 'order_code' && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'transaction_status' && attribute.attribute_code != 'base_total_transaction_amount' && attribute.attribute_code != 'order_payment_status' && attribute.attribute_code != 'vat_type' && attribute.attribute_code != 'vat_amount' && attribute.attribute_code != 'total_paid_amount' && attribute.is_default_attribute) {
          vm.listAttributesDefault.push(attribute);
        } else if ((attribute.is_hidden == 1 && attribute.is_default_attribute || !attribute.is_default_attribute && attribute.is_hidden == 0) && attribute.attribute_code != 'order_note' && attribute.attribute_code != 'customer_phone' && attribute.attribute_code != 'customer_email' && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'base_total_transaction_amount' && attribute.attribute_code != 'order_payment_status' && attribute.attribute_code != 'vat_type' && attribute.attribute_code != 'vat_amount' && attribute.attribute_code != 'total_paid_amount') {
          vm.listAttributesExtend.push(attribute);
        }
      })
      if (vm.listAttributesDefault && vm.listAttributesDefault.length > 0) {
        let sortAttributes = ["order_name", "customer_code", "customer_name", "contact_user"];
        let attributesNew = [];
        sortAttributes.forEach(function (code) {
          vm.listAttributesDefault.forEach(function (attribute) {
            if (attribute.attribute_code === code) {
              attributesNew.push(attribute);
            }
          })
        })
        vm.listAttributesDefault = attributesNew;
      }
    },

    setCurrentCustomerInDeal(detailCustomer) {
      let vm = this
      let customerId = detailCustomer.id
      let customerName = ''
      let customerCode = ''
      let customerEmail = ''
      let customerPhone = ''

      detailCustomer.attributes.forEach(function (attribute) {
        if (attribute.attribute_code == "full_name") {
          customerName = attribute.attribute_value
        }
        if (attribute.attribute_code == "sku") {
          customerCode = attribute.attribute_value
        }
        if (attribute.attribute_code == "email") {
          customerEmail = attribute.attribute_value
        }
        if (attribute.attribute_code == "phone") {
          customerPhone = attribute.attribute_value
        }
      })

      vm.currentCustomerInDeal = {
        customer_id: customerId,
        customer_name: customerName,
        customer_code: customerCode,
        customer_phone: customerPhone,
        customer_email: customerEmail
      }
    },

    getShowCustomer() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_SHOW_DETAIL, vm.customerId).then(response => {
        if (response.data.data) {
          vm.setCurrentCustomerInDeal(response.data.data)
          vm.isCustomerSuccess = true;

          let params = {
            customer_id: response.data.data.id,
          };
          vm.getAllContactByCustomer(params);
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getAllContactByCustomer(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_CONTACT, params).then((response) => {
        if (response.data.data) {
          vm.isCustomerContactReady = true;
          //console.log(response.data.data, '123');
        }
      }).catch((error) => {
        console.log(error)
      });
    },

    getDealAttributes() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(() => {
        vm.isDealSuccess = true;
        vm.deal.assigners.push(vm.currentUser.id);
        vm.deal.customer_id = vm.customerId;
      }).catch(error => {
        console.log(error);
      });
    },

    getDealAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_USER_ATTRIBUTES).then(() => {
        vm.getDealAttributes();
      }).catch(error => {
        console.log(error);
      });
    },

    getProductsByTeam() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_GET_ALL_BY_TEAM, {
        team_id: vm.currentTeamId
      }).then(response => {
        if (response.data.data) {
          vm.thenProducts();
          vm.isLoadProduct = true;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getAll(type = "") {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      vm.$store.dispatch(CUSTOMER_GET_ALL, vm.currentParams).then(response => {
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.oldCurrentPage = response.data.meta.current_page;
        vm.dataReady = true;
        if (type == "load_more") {
          vm.isLoadMoreData = false;
        }
        if (vm.loadSearch) {
          vm.isSearchCustomer = true;
        } else {
          vm.isSearchCustomer = false;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    loadMoreData(status) {
      let vm = this;
      if (status) {
        if (parseInt(vm.pagination.per_page) < vm.pagination.total_item) {
          vm.isLoadMoreData = true;
          let perPage = parseInt(vm.pagination.per_page) + 10;
          vm.currentParams.per_page = perPage;
          vm.getAll("load_more");
        }
      }
    },

    getAttributes() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then(() => {
        vm.attributeReady = true;
      }).catch(error => {
        console.log(error);
      });
    },

    getAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_USER_ATTRIBUTES).then(() => {
        vm.getAttributes();
      }).catch(error => {
        console.log(error);
      });
    },

    customerSearch(value) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        let searchText = value.trim();
        if (searchText && searchText.length > 1) {
          vm.pagination.current_page = 1;
          vm.currentParams.search = searchText;
          vm.getAll();
          vm.loadSearch = true;
        } else {
          if (vm.currentParams.search) {
            vm.pagination.current_page = 1;
            delete vm.currentParams.search;
          }
          if (!searchText || (searchText && searchText.length == 0)) {
            vm.getAll();
            vm.loadSearch = false;
          }
        }
      }, 500);
    },

    selectTypeDiscount(event) {
      let vm = this;
      vm.deal.products.forEach(function (item) {
        item.discount = 0;
        item.sale_price = item.product_price;
        item.total_price = item.product_price * parseInt(String(item.quantity).replace(/[^\d]/g, ""));
      })
      vm.deal.product_discount_type = event.target.value;
      vm.getTotalPrice();
    },

    selectTypeAdditionalDiscount(event) {
      let vm = this;
      vm.deal.discount.value = 0;
      vm.deal.total_after_discount = vm.deal.total;
      vm.deal.discount.total_discount = 0;
      vm.deal.discount.type = event.target.value;
    },

    setOnlyNumberInInput(event, product_type = '') {
      let keyCode = event.keyCode;
      let product_type_service = 'Dịch vụ';

      /*Check if Number input === service => Allow only integer input*/
      if (product_type === product_type_service) {
        if (!((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 106) || keyCode == 8 || keyCode == 37 || keyCode == 39)) {
          event.preventDefault();
        }
      } else {
        if (!((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 106) || keyCode == 8 || keyCode == 188 || keyCode == 37 || keyCode == 39)) {
          event.preventDefault();
        }
      }
    },

    changeQuantityProduct(product, index) {
      let vm = this;
      let stringQuantity = String(product.quantity);
      if (product.quantity == "" || vm.convertNumberDecimal(stringQuantity) < 0 || isNaN(vm.convertNumberDecimal(stringQuantity))) {
        product.quantity = 0;
      }
      let currentQuantity = vm.convertNumberDecimal(stringQuantity);
      let price = parseFloat(product.sale_price) * currentQuantity;
      vm.deal.products[index].total_price = Math.round(price);
      vm.getTotalPrice();
    },

    changeDiscount(product, type, index) {
      let vm = this;
      let salePrice = 0;
      let price = 0;
      let discount = 0;
      if (product.discount == "" || product.discount < 0 || isNaN(product.discount)) {
        product.discount = 0;
      }
      let currentDiscount = vm.clearFormatNumber(product.discount);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          salePrice = 0;
          discount = 100;
        } else {
          discount = product.discount;
          salePrice = (parseFloat(product.product_price) * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > product.product_price) {
          discount = product.product_price;
          salePrice = 0;
        } else {
          discount = product.discount;
          salePrice = product.product_price - currentDiscount;
        }
      }
      vm.deal.products[index].discount = discount;
      price = salePrice * vm.clearFormatNumber(product.quantity);
      vm.deal.products[index].sale_price = Math.round(salePrice);
      vm.deal.products[index].total_price = Math.round(price);
      vm.getTotalPrice();
    },

    getTotalPrice() {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        let totalPrice = 0;
        vm.deal.products.forEach(function (product) {
          totalPrice = totalPrice + parseFloat(product.total_price);
        })
        vm.deal.total = Math.round(totalPrice);
      } else {
        vm.deal.total = 0;
      }
    },

    changeAdditionalDiscount(type) {
      let vm = this;
      let price = 0;
      let valueOut = 0;
      if (vm.deal.discount.value == "" || vm.deal.discount.value < 0 || isNaN(vm.deal.discount.value)) {
        vm.deal.discount.value = 0;
      }
      let currentDiscount = vm.clearFormatNumber(vm.deal.discount.value);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          valueOut = 100;
          price = 0;
        } else {
          valueOut = vm.deal.discount.value;
          price = (parseFloat(vm.deal.total) * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > vm.deal.total) {
          price = 0;
          valueOut = vm.deal.total;
        } else {
          valueOut = vm.deal.discount.value;
          price = vm.deal.total - currentDiscount;
        }
      }
      vm.deal.discount.value = valueOut;
      let newPrice = Math.round(price);
      let newTotalDiscount = Math.round(vm.deal.total - newPrice);
      vm.deal.discount.total_discount = Math.round(newTotalDiscount);
      vm.deal.total_after_discount = Math.round(newPrice);
    },

    reset() {
      let vm = this;
      // let attributes = [];
      // vm.attributeAddDeal.forEach(function (attribute) {
      //     if (attribute.attribute_code == "customer_code") {
      //         vm.detailCustomer.attributes.forEach(function (item) {
      //             if (item.attribute_code == "sku") {
      //                 attribute.attribute_value = item.attribute_value;
      //             }
      //         })
      //     } else if (attribute.attribute_code == "customer_name") {
      //         vm.detailCustomer.attributes.forEach(function (item) {
      //             if (item.attribute_code == "full_name") {
      //                 attribute.attribute_value = item.attribute_value;
      //                 vm.deal.customer_name = item.attribute_value;
      //             }
      //         })
      //     } else if (attribute.attribute_code == "customer_phone") {
      //         vm.detailCustomer.attributes.forEach(function (item) {
      //             if (item.attribute_code == "phone") {
      //                 attribute.attribute_value = item.attribute_value ? item.attribute_value.replace(/ /g, "") : '';
      //                 ;
      //             }
      //         })
      //     } else if (attribute.attribute_code == "customer_email") {
      //         vm.detailCustomer.attributes.forEach(function (item) {
      //             if (item.attribute_code == "email") {
      //                 attribute.attribute_value = item.attribute_value;
      //             }
      //         })
      //     } else if (attribute.attribute_code == "transaction_status") {
      //         attribute.attribute_value = attribute.attribute_options[0].id;
      //     } else if (attribute.attribute_code == "order_name") {
      //         let nowDay = new Date();
      //         let dd = nowDay.getDate();
      //         let mm = nowDay.getMonth() + 1;
      //
      //         let yyyy = nowDay.getFullYear();
      //         if (dd < 10) {
      //             dd = '0' + dd;
      //         }
      //         if (mm < 10) {
      //             mm = '0' + mm;
      //         }
      //         let newDayFormat = dd + '/' + mm + '/' + yyyy;
      //         let customer = vm.detailCustomer.attributes.find(function (item) {
      //             return item.attribute_code == "full_name";
      //         })
      //         let customerName = customer.attribute_value;
      //         let orderName = customerName + '-' + newDayFormat;
      //         attribute.attribute_value = orderName;
      //     } else if (attribute.attribute_code == "order_date") {
      //         let nowDate = new Date();
      //         attribute.attribute_value = nowDate;
      //         vm.deal.order_date = nowDate;
      //     } else {
      //         attribute.attribute_value = '';
      //     }
      //     attributes.push(attribute);
      // })
      // vm.deal.assigners.push(vm.currentUser.id);
      // vm.deal.customer_id = vm.detailCustomer.id;
      // let sortAttributes = ["order_name", "order_code", "transaction_status", "customer_name", "customer_code", "order_date", "total_transaction_amount", "order_note", "customer_phone", "customer_email"];
      // let resultAttributes = [];
      // sortAttributes.forEach(function (code) {
      //     attributes.forEach(function (attribute) {
      //         if (attribute.attribute_code == code) {
      //             resultAttributes.push(attribute);
      //         }
      //     })
      // })
      // let newDeal = {
      //     customer_id: 0,
      //     attributes: resultAttributes,
      //     products: [],
      //     assigners: [],
      //     product_discount_type: "percent",
      //     discount: {
      //         type: "percent",
      //         value: 0,
      //         total_discount: 0
      //     },
      //     total: 0,
      //     total_after_discount: 0
      // };
      // vm.deal = newDeal;
      // vm.server_errors = '';
      // vm.productSelected = {};
      // vm.isLoader = false;
      // vm.thenDeals();
      // vm.$validator.reset();
      // vm.noteDeal = "";

      vm.$router.push({
        name: 'InfoBasic',
        params: {
          customer_id: vm.detailCustomer.id,
          team_id: vm.currentTeamId
        }
      });
      $("#m_header").css("display", "block");
      $("#app-body").removeClass("nt-none-padding-top");
      $("#app-body").addClass("m-body");
      $("#app-body-subheader").css("display", "block");
    },

    closeProductFromTable(sort_order) {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        if (vm.deal.products.length > 1) {
          vm.deal.products.forEach(function (item, index) {
            if (item.sort_order == sort_order) {
              vm.deal.products.splice(index, 1);
            }
          })
        } else {
          vm.deal.products = [];
        }
        vm.getTotalPrice();
        vm.isLoadSelect = true;
      }
    },

    selectProductInDeal() {
      let vm = this;
      if (vm.listProducts && vm.listProducts.length > 0 && vm.productSelected > 0) {
        let totalPrice = 0;
        vm.setCurrentCustomerInDeal(vm.detailCustomer)
        vm.listProducts.forEach(function (product) {
          if (product.id === vm.productSelected) {
            let newProduct = {
              id: product.id,
              discount: product.discount,
              product_code: product.product_code,
              product_name: product.product_name,
              product_price: product.product_price,
              product_type: product.product_type,
              quantity: product.quantity,
              sale_price: product.sale_price,
              status: 1,
              customer: vm.currentCustomerInDeal,
              sort_order: vm.deal.products && vm.deal.products.length >= 0 ? vm.deal.products.length + 1 : 0,
              total_price: product.total_price,
              product_unit: product.product_unit
            };
            totalPrice = parseFloat(product.product_price);
            vm.deal.products.push(newProduct);
          }
        })
        vm.deal.total = vm.deal.total + totalPrice;
        vm.productSelected = 0;
      }
    },

    convertNumberDecimal(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        if (newValue.indexOf(",") > -1) {
          let originalValue = newValue.substring(0, newValue.indexOf(","));
          originalValue = String(parseFloat(originalValue));
          let decimalValue = newValue.substring(newValue.indexOf(",") + 1, newValue.length);
          let decimalValueConvert = "";
          if (parseInt(decimalValue.charAt(2)) >= 5) {
            if (parseInt(decimalValue.charAt(1)) >= 9 && parseInt(decimalValue.charAt(0)) >= 9) {
              originalValue = parseFloat(originalValue) + 1;
            } else {
              decimalValueConvert = parseInt(decimalValue.substring(0, 2)) + 1;
            }
          } else {
            decimalValueConvert = decimalValue.substring(0, 2);
          }
          newValue = parseFloat(originalValue + "." + decimalValueConvert);
        } else {
          newValue = parseFloat(newValue);
        }
      }
      return newValue;
    },

    clearFormatNumber(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        if (newValue.indexOf(",") > -1) {
          let originalValue = newValue.substring(0, newValue.indexOf(","));
          originalValue = String(parseFloat(originalValue));
          let originValueConvert = parseFloat(originalValue.replace(/[^\d]/g, ""));
          let decimalValue = newValue.substring(newValue.indexOf(",") + 1, newValue.length);
          let decimalValueConvert = "";
          if (parseInt(decimalValue.charAt(2)) >= 5) {
            if (parseInt(decimalValue.charAt(1)) >= 9 && parseInt(decimalValue.charAt(0)) >= 9) {
              originValueConvert = parseFloat(originValueConvert) + 1;
              decimalValueConvert = 0;
            } else {
              decimalValueConvert = parseInt(decimalValue.substring(0, 2)) + 1;
            }
          } else {
            decimalValueConvert = decimalValue.substring(0, 2);
          }
          newValue = originValueConvert + "." + decimalValueConvert;
          newValue = parseFloat(newValue);
        } else {
          newValue = parseFloat(newValue.replace(/[^\d]/g, ""));
        }
      }
      return newValue;
    },

    formatNumberBlur(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {

              if (String(product.quantity).indexOf(",") > -1) {
                let originalValue = String(product.quantity).substring(0, String(product.quantity).indexOf(","));
                originalValue = String(parseFloat(originalValue));
                let decimalValue = String(product.quantity).substring(String(product.quantity).indexOf(",") + 1, String(product.quantity).length);
                let originValueConvert = originalValue.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
                let decimalValueConvert = "";
                if (parseInt(decimalValue.charAt(2)) >= 5) {
                  if (parseInt(decimalValue.charAt(1)) >= 9 && parseInt(decimalValue.charAt(0)) >= 9) {
                    originValueConvert = parseFloat(originValueConvert) + 1;
                    decimalValueConvert = 0;
                  } else {
                    decimalValueConvert = parseInt(decimalValue.substring(0, 2)) + 1;
                  }
                } else {
                  decimalValueConvert = decimalValue.substring(0, 2);
                }
                let newValue = "";
                if (parseFloat(decimalValueConvert) > 0) {
                  newValue = originValueConvert + "," + decimalValueConvert;
                } else {
                  newValue = originValueConvert;
                }
                product.quantity = newValue;
              } else {
                product.quantity = String(parseFloat(product.quantity)).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
              }
            } else if (type === 'discount') {
              product.discount = String(parseFloat(product.discount)).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = String(parseFloat(vm.deal.discount.value)).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
      }
    },

    clearFormatNumberFocus(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {
              if (String(product.quantity).indexOf(",") > -1) {
                let originalValue = String(product.quantity).substring(0, String(product.quantity).indexOf(","));
                let decimalValue = String(product.quantity).substring(String(product.quantity).indexOf(",") + 1, String(product.quantity).length);
                let originValueConvert = originalValue.replace(".", "");
                let decimalValueConvert = decimalValue.replace(".", "");
                let newValue = originValueConvert + "," + decimalValueConvert;
                product.quantity = newValue;
              } else {
                product.quantity = String(product.quantity).replace(".", "");
              }
            } else if (type === 'discount') {
              product.discount = vm.clearFormatNumber(product.discount);
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = vm.clearFormatNumber(vm.deal.discount.value);
      }
    },

    createDeal() {
      let vm = this;
      vm.$validator.validateAll("addNewDeal").then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          let dealProduct = vm.deal.products;
          dealProduct.filter(detail => {
            if (detail.product_type === 'Dịch vụ' && detail.product_unit === 'Khóa') {
              detail.quantity = 1;
            }
          });

          // Check giao dịch 0 đồng chuyển trạng thái thành toán thành đã thanh toán hết
          if (vm.deal.total_after_discount == 0) {
            if (vm.deal.attributes && vm.deal.attributes.length > 0) {
              vm.deal.attributes.forEach(function (attribute) {
                if (attribute.attribute_code == "order_payment_status") {
                  if (attribute.attribute_options && attribute.attribute_options.length > 0) {
                    attribute.attribute_options.forEach(function (option) {
                      if (option.option_value == "Thanh toán hết") {
                        attribute.attribute_value = option.id;
                      }
                    })
                  }

                }
              });
            }
          }

          vm.$store.dispatch(DEAL_CREATE, vm.deal).then(response => {
            if (response.data.status_code === 422) {
              vm.server_errors = response.data.errors;
              vm.$snotify.error('Thêm mới giao dịch thất bại.', TOAST_ERROR);
            } else if (response.data.data) {
              if (response.data.data.sort_delete) {
                if (response.data.data.reload) {
                  $("#messageChangeData").modal('show');
                } else {
                  $("#messageBackToCustomerId").modal('show');
                }
                vm.$snotify.error('Thêm mới giao dịch thất bại.', TOAST_ERROR);
              } else if (response.data.data.code && response.data.data.code === 'not_exist') {
                $("#server_change_data").modal("show");
                vm.$snotify.error('Thêm mới giao dịch thất bại.', TOAST_ERROR);
              } else {
                vm.$store.dispatch(DEAL_CREATE_ADD_STATUS, true).then(() => {});
                if (vm.accessible(vm.userCheckAddOn, 'deal', vm.PERMISSION_SHOW_DEAL, vm.userCurrentPermissions)) {
                  localStorage.setItem('AddNewDealSuccess', true);
                  return window.location.href = "/team/" + vm.currentTeamId + "/deals/" + response.data.data.id;
                  // vm.$snotify.success('Thêm mới giao dịch thành công.', TOAST_SUCCESS);
                  // vm.$router.push({
                  //   name: 'DealProductInfo',
                  //   params: {
                  //     team_id: vm.currentTeamId,
                  //     deal_id: response.data.data.id
                  //   }
                  // });
                } else {
                  vm.$router.push({
                    name: 'deals',
                    params: {
                      team_id: vm.currentTeamId
                    }
                  });
                }
                $("#m_header").css("display", "block");
                $("#app-body").removeClass("nt-none-padding-top");
                $("#app-body").addClass("m-body");
                $("#app-body-subheader").css("display", "block");
              }
            }
            vm.isLoader = false;
          }).catch(() => {
            vm.isLoader = false;
            vm.$snotify.error('Thêm mới giao dịch thất bại.', TOAST_ERROR);
          });
        }
      });
    },

    afterCreateSuccess() {
      let vm = this;
      vm.getAll();
    },

    loadingSelectSuccess() {
      let vm = this;
      vm.isLoadSelect = false;
    },

    updateValueProductCustomer(product) {
      this.deal.products.forEach((productItem, index) => {
        if (product.index === index) {
          productItem.customer = product.data
        }
      })
      this.$forceUpdate()
    },

    reloadPage() {
      return window.location.reload();
    }
  }
}