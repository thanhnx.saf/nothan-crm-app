import VueLadda from "vue-ladda";

import {
  DEAL_EXPORT_DEFAULT_ATTRIBUTE,
  DEAL_VALIDATE_IMPORT_DEAL,
  DEAL_GET_RESULT_FILE,
  DEAL_IMPORT_DEAL
} from "@/types/actions.type";
import {
  TOAST_ERROR
} from "@/types/config";

const UploadOption = {
  continue: "continue",
  reupload: "reupload"
};

export default {
  components: {
    VueLadda
  },
  data() {
    return {
      dealFile: {},
      reuploadFile: {},
      errorMessage: "",
      steps: [{
          isDone: true,
          icon: "icon-upload",
          name: "Upload File"
        },
        {
          isDone: false,
          icon: "icon-retweet",
          name: "Kiểm tra dữ liệu"
        },
        {
          isDone: false,
          icon: "icon-check",
          name: "Hoàn thành"
        }
      ],
      isDisabledNext: true,
      isLoadingNext: false,
      currentStep: 1,
      numberOfSuccessesImported: 0,
      numberOfDeals: 0,
      uploadOption: UploadOption,
      selectedOption: UploadOption.continue,
      isDisabledUpload: false,
      fileKey: 0
    };
  },
  methods: {
    downloadResult() {
      this.$store.dispatch(DEAL_GET_RESULT_FILE, {
        key: this.fileKey
      }).then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "KetQua_Import_KhongDat.xlsx");
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }).catch(error => console.log(error))
    },

    dealUploadOnFileChange() {
      let vm = this;
      const file = vm.$refs.uploadFile.files[0]

      if (!file) {
        return
      }
      vm.dealFile = file;
      this.dealCheckUploadFile(file)
      this.$refs.uploadFile.value = ''
    },

    onReuploadChange() {
      const vm = this;
      const file = vm.$refs.reuploadFile.files[0]

      if (!file) {
        return
      }
      vm.reuploadFile = file;
      this.dealCheckUploadFile(file)
      this.$refs.reuploadFile.value = ''
    },

    dealCheckUploadFile(file) {
      const fileName = file.name
      if (fileName.split('.').pop() !== 'xlsx') {
        this.errorMessage = 'Không thể tải lên các tập tin thuộc loại này. Hệ thống chỉ hỗ trợ tập tin xlsx.'
        return
      }
      if (file.size >= (10 * Math.pow(1024, 2))) {
        this.errorMessage = 'File excel tải lên có dung lượng vượt quá 10MB.'
        return
      }
      this.isDisabledUpload = true
      this.errorMessage = '';
      let vm = this;
      let formData = new FormData();
      formData.append('file', file);
      formData.append('team_id', vm.$route.params.team_id);
      vm.$store.dispatch(DEAL_VALIDATE_IMPORT_DEAL, formData).then(res => {
        vm.isDisabledUpload = false
        const data = res.data && res.data.data
        if (data && data.status) {
          const data = res.data && res.data.data
          this.numberOfSuccessesImported = data.countSuccess;
          this.isDisabledNext = this.currentStep === 2 ? !this.numberOfSuccessesImported : false
          this.numberOfDeals = data.total;
          this.fileKey = data.data
        } else {
          this.errorMessage = (data && data.message) || res.data.message
        }
      }).catch(error => console.log(error))
    },

    downloadFileDefault() {
      let teamId = this.$route.params.team_id;
      return this.$store
        .dispatch(DEAL_EXPORT_DEFAULT_ATTRIBUTE, teamId)
        .then(response => {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", "Mau_Import_GiaoDich.xlsx");
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        })
        .catch(error => {
          console.log(error);
        });
    },

    onBack() {
      if (this.currentStep === 1) return;
      this.currentStep -= 1;
      this.steps[this.currentStep].isDone = false;
    },

    onNext() {
      if (this.currentStep === this.steps.length) return;
      if (this.currentStep === 2) {
        this.isLoadingNext = true
        this.$store.dispatch(DEAL_IMPORT_DEAL, {
          data_key: this.fileKey
        }).then(res => {
          this.isLoadingNext = false
          const data = res.data && res.data.data
          if (data.status) {
            this.steps[this.currentStep].isDone = true;
            this.currentStep += 1;
          } else {
            this.isDisabledNext = true;
            this.$snotify.error(data.message, TOAST_ERROR);
          }
        }).catch(error => console.log(error))
      } else {
        this.steps[this.currentStep].isDone = true;
        this.currentStep += 1;
        this.isDisabledNext = !this.numberOfSuccessesImported
      }
    },

    onCancel() {
      setTimeout(() => {
        this.currentStep = 1;
        this.errorMessage = "";
        this.selectedOption = "";
        this.dealFile = {};
        this.reuploadFile = {};
        this.isDisabledNext = true;
        this.steps.forEach((step, index) => {
          if (index) step.isDone = false;
        });
        this.$emit('importDealSuccessfully')
      }, 200);
    }
  },
  computed: {
    isHideBack() {
      return this.currentStep === 1 || this.currentStep === this.steps.length;
    }
  }
};