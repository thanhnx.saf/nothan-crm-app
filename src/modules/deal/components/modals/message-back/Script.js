export default {
  data() {
    return {
      customerId: parseInt(this.$route.params.customer_id),
    }
  },

  methods: {
    reloadPage() {
      return window.location.href = "/team/" + this.currentTeamId + "/customers/" + this.customerId;
    }
  },
}
