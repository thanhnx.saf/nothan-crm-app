import { mapGetters } from "vuex";

import {
  PRODUCT_GET_ALL_BY_TEAM,
  DEAL_CREATE,
  DEAL_CREATE_ADD_STATUS
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import AttributeWithDataType from "../../attribute/AttributeWithDataType";

export default {
  components: {
    "attribute-with-data-type": AttributeWithDataType
  },
  mounted() {
    let vm = this;
    vm.getProductsByTeam();
    vm.thenDeals();
  },
  data() {
    return {
      deal: {
        customer_id: 0,
        customer_name: "",
        order_date: "",
        attributes: [],
        products: [],
        assigners: [],
        product_discount_type: "percent",
        discount: {
          type: "percent",
          value: 0,
          total_discount: 0
        },
        total: 0,
        total_after_discount: 0
      },
      typingTimer: null,
      server_errors: '',
      isLoadProduct: false,
      productSelected: 0,
      isLoader: false,
      listAttributesDefault: [],
      listAttributesExtend: [],
      listProducts: [],
      noteDeal: "",
      isCreateSuccess: false,
      dealIdCreated: 0
    }
  },
  computed: {
    listAddAttributes() {
      let vm = this;
      let attributes = [];
      vm.attributeAddDeal.forEach(function (attribute) {
        if (attribute.attribute_code == "customer_code") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "sku") {
              attribute.attribute_value = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "customer_name") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "full_name") {
              attribute.attribute_value = item.attribute_value;
              vm.deal.customer_name = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "customer_phone") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "phone") {
              attribute.attribute_value = item.attribute_value ? item.attribute_value.replace(/ /g, "") : '';
            }
          })
        } else if (attribute.attribute_code == "customer_email") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "email") {
              attribute.attribute_value = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "transaction_status") {
          attribute.attribute_value = attribute.attribute_options[0].id;
        } else if (attribute.attribute_code == "order_name") {
          let nowDay = new Date();
          let dd = nowDay.getDate();
          let mm = nowDay.getMonth() + 1;

          let yyyy = nowDay.getFullYear();
          if (dd < 10) {
            dd = '0' + dd;
          }
          if (mm < 10) {
            mm = '0' + mm;
          }
          let newDayFormat = dd + '/' + mm + '/' + yyyy;
          let customer = vm.detailCustomer.attributes.find(function (item) {
            return item.attribute_code == "full_name";
          })
          let customerName = customer.attribute_value;
          let orderName = customerName + '-' + newDayFormat;
          attribute.attribute_value = orderName;
        } else if (attribute.attribute_code == "order_date") {
          let nowDate = new Date();
          attribute.attribute_value = nowDate;
          vm.deal.order_date = nowDate;
        } else {
          attribute.attribute_value = '';
        }
        attributes.push(attribute);
      })
      vm.deal.assigners.push(vm.currentUser.id);
      vm.deal.customer_id = vm.detailCustomer.id;
      let sortAttributes = ["order_name", "order_code", "transaction_status", "customer_name", "customer_code", "order_date", "total_transaction_amount", "order_note", "customer_phone", "customer_email", "base_total_transaction_amount"];
      let resultAttributes = [];
      sortAttributes.forEach(function (code) {
        attributes.forEach(function (attribute) {
          if (attribute.attribute_code == code) {
            resultAttributes.push(attribute);
          }
        })
      })
      vm.deal.attributes = resultAttributes;
      return resultAttributes;
    },
    ...mapGetters(["detailDeal", "attributeAddDeal", "detailCustomer", "productByTeams", "currentTeam", "currentUser"]),
  },
  watch: {
    deal: {
      handler() {
        let vm = this;
        let discountValue = vm.clearFormatNumber(vm.deal.discount.value);
        if (discountValue > 0) {
          let price = 0;
          if (vm.deal.discount.type == 'percent') {
            price = (vm.deal.total * (100 - parseFloat(discountValue))) / 100;
          } else if (vm.deal.discount.type == 'amount') {
            if (parseFloat(discountValue) > vm.deal.total) {
              price = 0;
            } else {
              price = vm.deal.total - parseFloat(discountValue);
            }
          }
          let newPrice = Math.round(price * 1000) / 1000;
          let newTotalDiscount = Math.round((vm.deal.total - newPrice) * 1000) / 1000;
          vm.deal.discount.total_discount = newTotalDiscount;
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = price;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
            })
          }
          vm.deal.total_after_discount = price;
        } else {
          vm.deal.discount.total_discount = 0;
          vm.deal.total_after_discount = vm.deal.total;
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
            })
          }
        }
      },
      deep: true
    },
    noteDeal: {
      handler(after) {
        let vm = this;
        vm.deal.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == 'order_note') {
            attribute.attribute_value = after;
          }
        })
      },
      deep: true
    }
  },
  methods: {
    thenProducts() {
      let vm = this;
      vm.productByTeams.forEach(function (product) {
        product.quantity = 1;
        product.discount = 0;
        product.product_price = product.product_price == "" || product.product_price == null ? product.product_price = 0 : parseFloat(product.product_price);
        product.sale_price = product.product_price;
        product.total_price = product.product_price;
        vm.listProducts.push(product);
      })
    },
    thenDeals() {
      let vm = this;
      vm.listAttributesDefault = [];
      vm.listAttributesExtend = [];
      vm.listAddAttributes.forEach(function (attribute) {
        if (attribute.is_hidden == 0 && attribute.attribute_code != 'order_code' && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'transaction_status' && attribute.attribute_code != 'base_total_transaction_amount') {
          vm.listAttributesDefault.push(attribute);
        } else if (attribute.is_hidden == 1 && attribute.attribute_code != 'order_note' && attribute.attribute_code != 'customer_phone' && attribute.attribute_code != 'customer_email' && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'base_total_transaction_amount') {
          vm.listAttributesExtend.push(attribute);
        }
      })
    },

    getProductsByTeam() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_GET_ALL_BY_TEAM).then(response => {
        if (response.data.data) {
          vm.thenProducts();
          vm.isLoadProduct = true;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    selectTypeDiscount(event) {
      let vm = this;
      vm.deal.products.forEach(function (item) {
        item.discount = 0;
        item.sale_price = item.product_price;
        item.total_price = item.product_price * parseInt(String(item.quantity).replace(/[^\d]/g, ""));
      })
      vm.deal.product_discount_type = event.target.value;
      vm.getTotalPrice();
    },

    selectTypeAdditionalDiscount(event) {
      let vm = this;
      vm.deal.discount.value = 0;
      vm.deal.total_after_discount = vm.deal.total;
      vm.deal.discount.total_discount = 0;
      vm.deal.discount.type = event.target.value;
    },

    setOnlyNumberInInput(event) {
      let keyCode = event.keyCode;
      if (!((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 106) || keyCode == 8)) {
        event.preventDefault();
      }
    },

    changeQuantityProduct(product, index) {
      let vm = this;
      let stringQuantity = String(parseInt(product.quantity));
      if (stringQuantity.length > 0) {
        product.quantity = stringQuantity.replace(/^0+/, '');
      }
      if (product.quantity == "" || product.quantity < 0 || isNaN(product.quantity)) {
        product.quantity = 0;
      }
      let currentDiscount = vm.clearFormatNumber(product.quantity);
      let price = parseFloat(product.sale_price) * currentDiscount;
      vm.deal.products[index].total_price = price;
      vm.getTotalPrice();
    },

    changeDiscount(product, type, index) {
      let vm = this;
      let salePrice = 0;
      let price = 0;
      let discount = 0;
      let stringDiscount = String(parseInt(product.discount));
      if (stringDiscount.length > 0) {
        product.discount = stringDiscount.replace(/^0+/, '');
      }
      if (product.discount == "" || product.discount < 0 || isNaN(product.discount)) {
        product.discount = 0;
      }
      let currentDiscount = vm.clearFormatNumber(product.discount);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          salePrice = 0;
          discount = 100;
        } else {
          discount = product.discount;
          salePrice = (parseFloat(product.product_price) * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > product.product_price) {
          discount = product.product_price;
          salePrice = 0;
        } else {
          discount = product.discount;
          salePrice = product.product_price - currentDiscount;
        }
      }
      vm.deal.products[index].discount = discount;
      price = salePrice * vm.clearFormatNumber(product.quantity);
      vm.deal.products[index].sale_price = salePrice;
      vm.deal.products[index].total_price = price;
      vm.getTotalPrice();
    },

    getTotalPrice() {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        let totalPrice = 0;
        vm.deal.products.forEach(function (product) {
          totalPrice = totalPrice + product.total_price;
        })
        vm.deal.total = totalPrice;
      }
    },

    changeAdditionalDiscount(type) {
      let vm = this;
      let price = 0;
      let valueOut = 0;
      let stringDiscount = String(parseInt(vm.deal.discount.value));
      if (stringDiscount.length > 0) {
        vm.deal.discount.value = stringDiscount.replace(/^0+/, '');
      }
      if (vm.deal.discount.value == "" || vm.deal.discount.value < 0 || isNaN(vm.deal.discount.value)) {
        vm.deal.discount.value = 0;
      }
      let currentDiscount = vm.clearFormatNumber(vm.deal.discount.value);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          valueOut = 100;
          price = 0;
        } else {
          valueOut = vm.deal.discount.value;
          price = (vm.deal.total * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > vm.deal.total) {
          price = 0;
          valueOut = vm.deal.total;
        } else {
          valueOut = currentDiscount;
          price = vm.deal.total - currentDiscount;
        }
      }
      vm.deal.discount.value = valueOut;
      let newPrice = Math.round(price * 1000) / 1000;
      let newTotalDiscount = Math.round((vm.deal.total - newPrice) * 1000) / 1000;
      vm.deal.discount.total_discount = newTotalDiscount;
      vm.deal.total_after_discount = newPrice;
    },

    reset() {
      let vm = this;
      let attributes = [];
      vm.attributeAddDeal.forEach(function (attribute) {
        if (attribute.attribute_code == "customer_code") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "sku") {
              attribute.attribute_value = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "customer_name") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "full_name") {
              attribute.attribute_value = item.attribute_value;
              vm.deal.customer_name = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "customer_phone") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "phone") {
              attribute.attribute_value = item.attribute_value ? item.attribute_value.replace(/ /g, "") : '';
            }
          })
        } else if (attribute.attribute_code == "customer_email") {
          vm.detailCustomer.attributes.forEach(function (item) {
            if (item.attribute_code == "email") {
              attribute.attribute_value = item.attribute_value;
            }
          })
        } else if (attribute.attribute_code == "transaction_status") {
          attribute.attribute_value = attribute.attribute_options[0].id;
        } else if (attribute.attribute_code == "order_name") {
          let nowDay = new Date();
          let dd = nowDay.getDate();
          let mm = nowDay.getMonth() + 1;

          let yyyy = nowDay.getFullYear();
          if (dd < 10) {
            dd = '0' + dd;
          }
          if (mm < 10) {
            mm = '0' + mm;
          }
          let newDayFormat = dd + '/' + mm + '/' + yyyy;
          let customer = vm.detailCustomer.attributes.find(function (item) {
            return item.attribute_code == "full_name";
          })
          let customerName = customer.attribute_value;
          let orderName = customerName + '-' + newDayFormat;
          attribute.attribute_value = orderName;
        } else if (attribute.attribute_code == "order_date") {
          let nowDate = new Date();
          attribute.attribute_value = nowDate;
          vm.deal.order_date = nowDate;
        } else {
          attribute.attribute_value = '';
        }
        attributes.push(attribute);
      })
      vm.deal.assigners.push(vm.currentUser.id);
      vm.deal.customer_id = vm.detailCustomer.id;
      let sortAttributes = ["order_name", "order_code", "transaction_status", "customer_name", "customer_code", "order_date", "total_transaction_amount", "order_note", "customer_phone", "customer_email"];
      let resultAttributes = [];
      sortAttributes.forEach(function (code) {
        attributes.forEach(function (attribute) {
          if (attribute.attribute_code == code) {
            resultAttributes.push(attribute);
          }
        })
      })
      let newDeal = {
        customer_id: 0,
        attributes: resultAttributes,
        products: [],
        assigners: [],
        product_discount_type: "percent",
        discount: {
          type: "percent",
          value: 0,
          total_discount: 0
        },
        total: 0,
        total_after_discount: 0
      };
      vm.deal = newDeal;
      vm.server_errors = '';
      vm.productSelected = {};
      vm.isLoader = false;
      vm.thenDeals();
      vm.$validator.reset();
      vm.noteDeal = "";
    },

    closeProductFromTable(sort_order) {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        vm.deal.products.forEach(function (item, index) {
          if (item.sort_order == sort_order) {
            vm.deal.products.splice(index, 1);
          }
        })
        vm.getTotalPrice();
      }
    },

    selectProductInDeal() {
      let vm = this;
      if (vm.listProducts && vm.listProducts.length > 0 && vm.productSelected > 0) {
        let totalPrice = 0;
        vm.listProducts.forEach(function (product) {
          if (product.id == vm.productSelected) {
            let newProduct = {
              id: product.id,
              discount: product.discount,
              product_code: product.product_code,
              product_name: product.product_name,
              product_price: product.product_price,
              quantity: product.quantity,
              sale_price: product.sale_price,
              sort_order: vm.deal.products && vm.deal.products.length >= 0 ? vm.deal.products.length + 1 : 0,
              total_price: product.total_price
            };
            totalPrice = product.product_price;
            vm.deal.products.push(newProduct);
          }
        })
        vm.deal.total = vm.deal.total + totalPrice;
        vm.productSelected = 0;
      }
    },

    clearFormatNumber(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        newValue = parseFloat(newValue.replace(/[^\d]/g, ""));
      }
      return newValue;
    },

    formatNumberBlur(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {
              product.quantity = String(product.quantity).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
            } else if (type === 'discount') {
              product.discount = String(product.discount).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = String(vm.deal.discount.value).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
      }
    },

    clearFormatNumberFocus(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {
              product.quantity = parseFloat(String(product.quantity).replace(/[^\d]/g, ""));
            } else if (type === 'discount') {
              product.discount = parseFloat(String(product.discount).replace(/[^\d]/g, ""));
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = parseFloat(String(vm.deal.discount.value).replace(/[^\d]/g, ""));
      }
    },

    createDeal() {
      let vm = this;
      vm.$validator.validateAll("addNewDeal").then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          vm.$store.dispatch(DEAL_CREATE, vm.deal).then(response => {
            if (response.data.data && response.data.data.status) {
              vm.dealIdCreated = response.data.data.id;
              vm.isCreateSuccess = true;
              vm.$snotify.success('Thêm mới giao dịch thành công.', TOAST_SUCCESS);
              vm.$store.dispatch(DEAL_CREATE_ADD_STATUS, true).then(() => {
              });
              vm.reset();
              setTimeout(function () {
                vm.isCreateSuccess = false;
              }, 5000)
            }
            vm.isLoader = false;
          }).catch(() => {
            vm.isLoader = false;
            vm.$snotify.error('Thêm mới giao dịch thất bại.', TOAST_ERROR);
          });
        }
      });
    }
  }
}
