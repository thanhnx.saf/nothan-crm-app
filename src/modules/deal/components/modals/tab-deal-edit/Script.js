/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import {
  PRODUCT_GET_ALL_BY_TEAM,
  DEAL_UPDATE
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import AttributeWithDataType from "../../attribute/AttributeWithDataType";

export default {
  components: {
    "attribute-with-data-type": AttributeWithDataType
  },
  mounted() {
    let vm = this;
    vm.getProductsByTeam();
    vm.thenDeals();
    vm.thenInDealsProducts();
  },
  data() {
    return {
      deal: {
        customer_id: 0,
        customer_name: "",
        order_date: "",
        order_date_cache: "",
        attributes: [],
        attributes_cache: [],
        products: [],
        products_cache: [],
        assigners: [],
        assigners_cache: [],
        product_discount_type: "percent",
        product_discount_type_cache: "",
        discount: {
          id: 0,
          deal_id: 0,
          type: "percent",
          value: 0,
          total_discount: 0
        },
        discount_cache: {},
        total: 0,
        total_after_discount: 0
      },
      isSuccess: false,
      typingTimer: null,
      server_errors: '',
      isLoadProduct: false,
      productSelected: 0,
      isLoader: false,
      listAttributesDefault: [],
      listAttributesExtend: [],
      listProducts: [],
      noteDeal: "",
      typingLoader: false
    }
  },
  computed: {
    listEditAttributes() {
      let vm = this;
      let disableAttributes = ["customer_code", "customer_name", "customer_email", "transaction_status", "reason_for_cancellation"];
      let resultAttributes = [];
      if (vm.detailDeal.attributes && vm.detailDeal.attributes.length > 0) {
        vm.detailDeal.attributes.forEach(function (attribute) {
          vm.attributeEditDeal.forEach(function (attrEdit) {
            if (attribute.attribute_code == 'customer_name') {
              vm.deal.customer_name = attribute.attribute_value;
            }
            if (attrEdit.id == attribute.id && $.inArray(attribute.attribute_code, disableAttributes) == -1) {
              if (attribute.attribute_code == 'base_total_transaction_amount') {
                vm.deal.total = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'total_transaction_amount') {
                vm.deal.total_after_discount = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_note') {
                vm.noteDeal = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_date') {
                vm.deal.order_date = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_payment' && attribute.attribute_value == "") {
                let nowDate = new Date();
                attribute.attribute_value = nowDate;
              }
              attribute.data_type = attrEdit.data_type;
              attribute.attribute_options = attrEdit.attribute_options;
              resultAttributes.push(attribute);
            }
          })
        })
      }
      vm.deal.attributes = resultAttributes;
      return resultAttributes;
    },
    ...mapGetters(["detailDeal", "attributeEditDeal", "productByTeams", "listUsers", "currentTeam"]),
  },
  watch: {
    deal: {
      handler() {
        let vm = this;
        let discountValue = vm.clearFormatNumber(vm.deal.discount.value);
        if (discountValue > 0) {
          let price = 0;
          if (vm.deal.discount.type == 'percent') {
            price = (vm.deal.total * (100 - parseFloat(discountValue))) / 100;
          } else if (vm.deal.discount.type == 'amount') {
            if (parseFloat(discountValue) > vm.deal.total) {
              price = 0;
            } else {
              price = vm.deal.total - parseFloat(discountValue);
            }
          }
          vm.deal.discount.total_discount = vm.deal.total - price;
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = price;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
            })
          }
          vm.deal.total_after_discount = price;
        } else {
          vm.deal.discount.total_discount = 0;
          vm.deal.total_after_discount = vm.deal.total;
          if (vm.deal.attributes && vm.deal.attributes.length > 0) {
            vm.deal.attributes.forEach(function (item) {
              if (item.attribute_code == 'total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
              if (item.attribute_code == 'base_total_transaction_amount') {
                item.attribute_value = vm.deal.total;
              }
            })
          }
        }
      },
      deep: true
    },
    noteDeal: {
      handler(after) {
        let vm = this;
        vm.deal.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == 'order_note') {
            attribute.attribute_value = after;
          }
        })
      },
      deep: true
    }
  },
  methods: {
    thenInDealsProducts() {
      let vm = this;
      let disableAttributes = ["customer_code", "customer_name", "customer_email", "transaction_status", "reason_for_cancellation"];
      if (vm.detailDeal.products && vm.detailDeal.products.length > 0) {
        let products = [];
        let key = 1;
        vm.detailDeal.products.forEach(function (product) {
          let tempProduct = {
            id: product.product_id,
            product_code: product.product_code,
            product_name: product.product_name,
            quantity: vm.formatNumber(parseFloat(product.quantity)),
            discount: vm.formatNumber(parseFloat(product.discount)),
            product_price: parseFloat(product.price),
            sale_price: parseFloat(product.sale_price),
            total_price: parseFloat(product.total_price),
            sort_order: key
          };
          products.push(tempProduct);
          key++;
        })
        vm.deal.products = products;
      }
      if (vm.detailDeal.deal_discount && Object.keys(vm.detailDeal.deal_discount).length > 0) {
        let discount = {
          id: vm.detailDeal.deal_discount.id,
          deal_id: vm.detailDeal.deal_discount.deal_id,
          type: vm.detailDeal.deal_discount.type,
          value: vm.formatNumber(parseFloat(vm.detailDeal.deal_discount.value)),
          total_discount: parseFloat(vm.detailDeal.deal_discount.total_discount)
        };
        vm.deal.discount = discount;
      }
      vm.deal.assigners_cache = vm.detailDeal.assigners_deal_cache;
      if (vm.detailDeal.attributes_cache && vm.detailDeal.attributes_cache.length > 0) {
        let resultAttributesCache = [];
        vm.detailDeal.attributes_cache.forEach(function (attribute) {
          vm.attributeEditDeal.forEach(function (attrEdit) {
            if (attribute.attribute_code == 'customer_name') {
              vm.deal.customer_name = attribute.attribute_value;
            }
            if (attrEdit.id == attribute.id && $.inArray(attribute.attribute_code, disableAttributes) == -1) {
              if (attribute.attribute_code == 'base_total_transaction_amount') {
                vm.deal.total = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'total_transaction_amount') {
                vm.deal.total_after_discount = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_note') {
                vm.noteDeal = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_date') {
                vm.deal.order_date = attribute.attribute_value;
                vm.deal.order_date_cache = attribute.attribute_value;
              }
              if (attribute.attribute_code == 'order_payment' && attribute.attribute_value == "") {
                let nowDate = new Date();
                attribute.attribute_value = nowDate;
              }
              attribute.data_type = attrEdit.data_type;
              attribute.attribute_options = attrEdit.attribute_options;
              resultAttributesCache.push(attribute);
            }
          })
        })
        vm.deal.attributes_cache = resultAttributesCache;
      }
      if (vm.detailDeal.products_cache && vm.detailDeal.products_cache.length > 0) {
        let products_cache = [];
        let key = 1;
        vm.detailDeal.products_cache.forEach(function (product) {
          let tempProduct = {
            id: product.product_id,
            product_code: product.product_code,
            product_name: product.product_name,
            quantity: vm.formatNumber(parseFloat(product.quantity)),
            discount: vm.formatNumber(parseFloat(product.discount)),
            product_price: parseFloat(product.price),
            sale_price: parseFloat(product.sale_price),
            total_price: parseFloat(product.total_price),
            sort_order: key
          };
          products_cache.push(tempProduct);
          key++;
        })
        vm.deal.products_cache = products_cache;
      }
      if (vm.detailDeal.deal_discount_cache && Object.keys(vm.detailDeal.deal_discount_cache).length > 0) {
        let discount = {
          id: vm.detailDeal.deal_discount.id,
          deal_id: vm.detailDeal.deal_discount.deal_id,
          type: vm.detailDeal.deal_discount.type,
          value: vm.formatNumber(parseFloat(vm.detailDeal.deal_discount.value)),
          total_discount: parseFloat(vm.detailDeal.deal_discount.total_discount)
        };
        vm.deal.discount_cache = discount;
      }
      vm.deal.product_discount_type_cache = vm.detailDeal.deal_type_cache;
      vm.deal.customer_id = vm.detailDeal.customer_id;
      vm.deal.product_discount_type = vm.detailDeal.deal_type;
      vm.deal.assigners = vm.detailDeal.assigners_deal;
    },

    thenProducts() {
      let vm = this;
      let key = 1;
      vm.productByTeams.forEach(function (product) {
        product.quantity = 1;
        product.discount = 0;
        product.product_price = product.product_price == "" || product.product_price == null ? product.product_price = 0 : parseFloat(product.product_price);
        product.sale_price = product.product_price;
        product.total_price = product.product_price;
        product.sort_order = key;
        vm.listProducts.push(product);
        key++;
      })
    },

    thenDeals(attributes = []) {
      let vm = this;
      vm.listAttributesDefault = [];
      vm.listAttributesExtend = [];
      let forAttributes = [];
      if (attributes && attributes.length > 0) {
        forAttributes = attributes;
      } else {
        forAttributes = vm.listEditAttributes;
      }
      forAttributes.forEach(function (attribute) {
        if (attribute.is_hidden == 0 && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'base_total_transaction_amount') {
          vm.listAttributesDefault.push(attribute);
        } else if (attribute.is_hidden == 1 && attribute.attribute_code != 'order_note' && attribute.attribute_code != 'customer_phone' && attribute.attribute_code != 'customer_email' && attribute.attribute_code != 'total_transaction_amount' && attribute.attribute_code != 'base_total_transaction_amount') {
          vm.listAttributesExtend.push(attribute);
        }
      })
    },

    getProductsByTeam() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_GET_ALL_BY_TEAM).then(response => {
        if (response.data.data) {
          vm.thenProducts();
          vm.isLoadProduct = true;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    selectTypeDiscount(event) {
      let vm = this;
      vm.deal.products.forEach(function (item) {
        item.discount = 0;
        item.sale_price = item.product_price;
        item.total_price = item.product_price * parseInt(String(item.quantity).replace(/[^\d]/g, ""));
      })
      vm.deal.product_discount_type = event.target.value;
      vm.getTotalPrice();
    },

    selectTypeAdditionalDiscount(event) {
      let vm = this;
      vm.deal.discount.value = 0;
      vm.deal.total_after_discount = vm.deal.total;
      vm.deal.discount.total_discount = 0;
      vm.deal.discount.type = event.target.value;
    },

    setOnlyNumberInInput(event) {
      let keyCode = event.keyCode;
      if (!((keyCode > 47 && keyCode < 58) || (keyCode > 95 && keyCode < 106) || keyCode == 8)) {
        event.preventDefault();
      }
    },

    changeQuantityProduct(product, index) {
      let vm = this;
      let stringQuantity = String(parseInt(product.quantity));
      if (stringQuantity.length > 0) {
        product.quantity = stringQuantity.replace(/^0+/, '');
      }
      if (product.quantity == "" || product.quantity < 0 || isNaN(product.quantity)) {
        product.quantity = 0;
      }
      let currentDiscount = vm.clearFormatNumber(product.quantity);
      let price = parseFloat(product.sale_price) * currentDiscount;
      vm.deal.products[index].total_price = price;
      vm.getTotalPrice();
    },

    changeDiscount(product, type, index) {
      let vm = this;
      let salePrice = 0;
      let price = 0;
      let discount = 0;
      let stringDiscount = String(parseInt(product.discount));
      if (stringDiscount.length > 0) {
        product.discount = stringDiscount.replace(/^0+/, '');
      }
      if (product.discount == "" || product.discount < 0 || isNaN(product.discount)) {
        product.discount = 0;
      }
      let currentDiscount = vm.clearFormatNumber(product.discount);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          salePrice = 0;
          discount = 100;
        } else {
          discount = product.discount;
          salePrice = (parseFloat(product.product_price) * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > product.product_price) {
          discount = product.product_price;
          salePrice = 0;
        } else {
          discount = product.discount;
          salePrice = product.product_price - currentDiscount;
        }
      }
      vm.deal.products[index].discount = discount;
      price = salePrice * vm.clearFormatNumber(product.quantity);
      vm.deal.products[index].sale_price = salePrice;
      vm.deal.products[index].total_price = price;
      vm.getTotalPrice();
    },

    getTotalPrice() {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        let totalPrice = 0;
        vm.deal.products.forEach(function (product) {
          totalPrice = totalPrice + product.total_price;
        })
        vm.deal.total = totalPrice;
      }
    },

    changeAdditionalDiscount(type) {
      let vm = this;
      let price = 0;
      let valueOut = 0;
      let stringDiscount = String(parseInt(vm.deal.discount.value));
      if (stringDiscount.length > 0) {
        vm.deal.discount.value = stringDiscount.replace(/^0+/, '');
      }
      if (vm.deal.discount.value == "" || vm.deal.discount.value < 0 || isNaN(vm.deal.discount.value)) {
        vm.deal.discount.value = 0;
      }
      let currentDiscount = vm.clearFormatNumber(vm.deal.discount.value);
      if (type == 'percent') {
        if (currentDiscount > 100) {
          valueOut = 100;
          price = 0;
        } else {
          valueOut = vm.deal.discount.value;
          price = (vm.deal.total * (100 - currentDiscount)) / 100;
        }
      } else if (type == 'amount') {
        if (currentDiscount > vm.deal.total) {
          price = 0;
          valueOut = vm.deal.total;
        } else {
          valueOut = vm.deal.discount.value;
          price = vm.deal.total - currentDiscount;
        }
      }
      vm.deal.discount.value = valueOut;
      let newPrice = Math.round(price * 1000) / 1000;
      let newTotalDiscount = Math.round((vm.deal.total - newPrice) * 1000) / 1000;
      vm.deal.discount.total_discount = newTotalDiscount;
      vm.deal.total_after_discount = newPrice;
    },

    reset() {
      let vm = this;
      vm.detailDeal.attributes = vm.detailDeal.attributes_cache;
      vm.detailDeal.assigners_deal = vm.detailDeal.assigners_deal_cache;
      vm.detailDeal.deal_discount = vm.detailDeal.deal_discount_cache;
      vm.detailDeal.products = vm.detailDeal.products_cache;
      vm.detailDeal.deal_type = vm.detailDeal.deal_type_cache;
      let disableAttributes = ["customer_code", "customer_name", "customer_email", "transaction_status", "reason_for_cancellation"];
      let resultAttributes = [];
      if (vm.detailDeal.attributes_cache && vm.detailDeal.attributes_cache.length > 0) {
        vm.detailDeal.attributes_cache.forEach(function (attribute) {
          vm.attributeEditDeal.forEach(function (attrEdit) {
            if (attribute.attribute_code == 'customer_name') {
              vm.deal.customer_name = attribute.attribute_value_cache;
            }
            if (attrEdit.id == attribute.id && $.inArray(attribute.attribute_code, disableAttributes) == -1) {
              if (attribute.attribute_code == 'total_transaction_amount') {
                vm.deal.total = attribute.attribute_value_cache;
              }
              if (attribute.attribute_code == 'order_note') {
                vm.noteDeal = attribute.attribute_value_cache;
              }
              if (attribute.attribute_code == 'order_date') {
                vm.deal.order_date = attribute.attribute_value_cache;
              }
              if (attribute.attribute_code == 'order_payment' && attribute.attribute_value_cache == "") {
                let nowDate = new Date();
                attribute.attribute_value = nowDate;
              }
              attribute.attribute_value = attribute.attribute_value_cache;
              attribute.data_type = attrEdit.data_type;
              attribute.attribute_options = attrEdit.attribute_options;
              resultAttributes.push(attribute);
            }
          })
        })
      }
      if (vm.detailDeal.products_cache && vm.detailDeal.products_cache.length > 0) {
        let products = [];
        let key = 1;
        vm.detailDeal.products_cache.forEach(function (product) {
          let tempProduct = {
            id: product.product_id,
            product_code: product.product_code,
            product_name: product.product_name,
            quantity: vm.formatNumber(product.quantity),
            discount: vm.formatNumber(product.discount),
            product_price: parseFloat(product.price),
            sale_price: parseFloat(product.sale_price),
            total_price: parseFloat(product.total_price),
            sort_order: key
          };
          products.push(tempProduct);
          key++;
        })
        vm.deal.products = products;
      }
      if (vm.detailDeal.deal_discount_cache && Object.keys(vm.detailDeal.deal_discount_cache).length > 0) {
        let discount = {
          id: vm.detailDeal.deal_discount_cache.id,
          deal_id: vm.detailDeal.deal_discount_cache.deal_id,
          type: vm.detailDeal.deal_discount_cache.type,
          value: vm.formatNumber(parseFloat(vm.detailDeal.deal_discount_cache.value)),
          total_discount: parseFloat(vm.detailDeal.deal_discount_cache.total_discount)
        };
        vm.deal.discount = discount;
      }
      vm.deal.customer_id = vm.detailDeal.customer_id;
      vm.deal.product_discount_type = vm.detailDeal.deal_type;
      vm.deal.assigners = vm.detailDeal.assigners_deal_cache;
      vm.deal.attributes = resultAttributes;
      vm.thenDeals(resultAttributes);
      $("#boxInfoDeal.collapse").removeClass("show");
      $("#boxInfoProducts.collapse").removeClass("show");
      $("#boxInfoExtendDeal.collapse").removeClass("show");
    },

    closeProductFromTable(index) {
      let vm = this;
      if (vm.deal.products && vm.deal.products.length > 0) {
        vm.deal.products.forEach(function (item, iIndex) {
          if (iIndex == index) {
            vm.deal.products.splice(index, 1);
          }
        })
        vm.getTotalPrice();
      }
    },

    selectProductInDeal() {
      let vm = this;
      if (vm.listProducts && vm.listProducts.length > 0 && vm.productSelected > 0) {
        let totalPrice = 0;
        vm.listProducts.forEach(function (product) {
          if (product.id == vm.productSelected) {
            let newProduct = {
              id: product.id,
              discount: product.discount,
              product_code: product.product_code,
              product_name: product.product_name,
              product_price: product.product_price,
              quantity: product.quantity,
              sale_price: product.sale_price,
              sort_order: vm.deal.products && vm.deal.products.length >= 0 ? vm.deal.products.length + 1 : 0,
              total_price: product.total_price
            };
            totalPrice = product.product_price;
            vm.deal.products.push(newProduct);
          }
        })
        vm.deal.total = vm.deal.total + totalPrice;
        vm.productSelected = 0;
      }
    },

    clearFormatNumber(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        newValue = parseFloat(newValue.replace(/[^\d]/g, ""));
      }
      return newValue;
    },

    formatNumber(value = "") {
      let newValue = String(value);
      if (newValue.length > 0) {
        newValue = newValue.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
      } else {
        newValue = parseFloat(newValue);
      }
      return newValue;
    },

    formatNumberBlur(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {
              product.quantity = String(product.quantity).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
            } else if (type === 'discount') {
              product.discount = String(product.discount).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = String(vm.deal.discount.value).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
      }
    },

    clearFormatNumberFocus(type, index = -1) {
      let vm = this;
      if (type === 'quantity' || type === 'discount') {
        vm.deal.products.forEach(function (product, product_index) {
          if (product_index === index) {
            if (type === 'quantity') {
              product.quantity = parseFloat(String(product.quantity).replace(/[^\d]/g, ""));
            } else if (type === 'discount') {
              product.discount = parseFloat(String(product.discount).replace(/[^\d]/g, ""));
            } else {
              return;
            }
          }
        })
      } else if (type === 'addDiscount') {
        vm.deal.discount.value = parseFloat(String(vm.deal.discount.value).replace(/[^\d]/g, ""));
      }
    },

    switchCollapseDeal(event) {
      let current = event.currentTarget;
      let arrow = $(current).parent(".m--header-collapse").children(".btn-collapse").children(".icon");
      if ($(arrow).hasClass("icon-down-arrow-1")) {
        $(arrow).removeClass("icon-down-arrow-1");
        $(arrow).addClass("icon-up-arrow-1");
      } else {
        $(arrow).removeClass("icon-up-arrow-1");
        $(arrow).addClass("icon-down-arrow-1");
      }
    },

    updateDeal() {
      let vm = this;
      vm.isLoader = true;
      vm.$store.dispatch(DEAL_UPDATE, { id: vm.detailDeal.id, params: vm.deal }).then(response => {
        if (response.data.data && response.data.data.status) {
          vm.$snotify.success('Cập nhật giao dịch thành công.', TOAST_SUCCESS);
          vm.$emit("is_edit_success", true);
        }
        vm.isLoader = false;
      }).catch(() => {
        vm.isLoader = false;
        vm.$snotify.error('Cập nhật giao dịch thất bại', TOAST_ERROR);
      });
    }
  }
}
