/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";
import {
  mapState
} from "vuex";

import helpers from "@/utils/utils";
import {
  DEAL_CREATE_PAYMENT,
  DEAL_GET_DETAIL
} from "@/types/actions.type";

import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";

const customMessageCreatePayment = {
  custom: {
    'paymentDate': {
      required: 'Không được để trống trường này.',
    },
    'paymentPrice': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 20 ký tự.",
    },
    // 'paymentPayer': {
    //     required: 'Không được để trống trường này.',
    //     max: "Giới hạn 250 ký tự."
    // },
    // 'paymentReceiver': {
    //     required: 'Không được để trống trường này.',
    //     max: 'Giới hạn 250 ký tự.',
    // },
    'paymentVoucher': {
      max: 'Giới hạn 250 ký tự.',
    },
    'paymentNote': {
      max: 'Giới hạn 1025 ký tự.',
    },
    'paymentMethod': {
      required: 'Không được để trống trường này.',
    }
  }
};

function regexInputPrice(number) {
  return /^-?[0-9]([0-9]|(?!-))+$/.test(number);
}

function regexInputPriceNotAllow_0(number) {
  return /(^-?|^0*)([1-9]).*([0-9]*|(?!-))*$/.test(number);
}

export default {
  components: {},

  data() {
    return {
      count: 0,
      deal_id: parseInt(this.$route.params.deal_id),
      team_id: parseInt(this.$route.params.team_id),
      newDate: new Date(),
      isLoader: false,
      Payment: {
        date: new Date(),
        price: '',
        // payer: '',
        // receiver: '',
        voucher: '',
        note: '',
        payment_method: '',
      },
      paymentMethod: [{
          method: 'Tiền mặt',
          value: 1
        },
        {
          method: 'Chuyển khoản',
          value: 2
        },
        {
          method: 'Khác',
          value: 3
        }

      ],
      isPricePaymentMax: false,
      isPriceNotStartWith_0: false,
      isRegexPrice: false,
      isBtnLoading: false,
      isDealCompleteOrCancel: false,
      isRemoveNotifyStringZero: false,
      totalPriceRemain: ''
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["detailDeal"]),
    ...mapState({
      totalPriceHasPaid: state => state.deal.totalPriceHasPaid,
      totalPriceDeal: state => state.deal.totalPriceDeal,
    }),
    currentTotalPricePaid() {
      let total = '';
      let priceInput = this.Payment.price;
      let totalPriceHasPaid = this.totalPriceHasPaid;
      totalPriceHasPaid === "" || totalPriceHasPaid === null ? totalPriceHasPaid = 0 : totalPriceHasPaid = this.totalPriceHasPaid;
      if (priceInput.length > 0) {
        /*check priceInput is number*/
        if (isNaN(priceInput)) {
          return total = totalPriceHasPaid
        }
        total = (parseInt(totalPriceHasPaid) + parseInt(priceInput));
        return total;
      } else {
        return total = totalPriceHasPaid
      }
    },
    currentTotalPriceRemain() {
      return this.totalPriceDeal - this.currentTotalPricePaid;

    },
    orderDate() {
      let date = '';
      date = helpers.getAttributeByAttrCodeAndVariableName(this.detailDeal, 'order_date', 'attribute_value');
      return helpers.formatDateTime(date)
    },

  },
  created() {
    this.checkDealHasCompleteOrCancel();
    let vm = this;

    vm.$bus.$on('addNewPayment', ($event) => {
      if ($event) {
        vm.$store.dispatch(DEAL_GET_DETAIL, vm.deal_id).then((response) => {
          if (response.data.data) {
            let totalPaidAmount = helpers.getAttributeByAttrCodeAndVariableName(vm.detailDeal, 'total_paid_amount', 'attribute_value');
            let baseTotalTransactionAmount = helpers.getAttributeByAttrCodeAndVariableName(vm.detailDeal, 'total_transaction_amount', 'attribute_value');
            vm.totalPriceRemain = baseTotalTransactionAmount - totalPaidAmount;

            vm.Payment.price = vm.totalPriceRemain != 0 ? vm.totalPriceRemain : '';

            $('#AddDealPayment').modal('show');
          }
        })
      }
    });
  },
  mounted() {
    let vm = this;
    vm.$validator.localize('en', customMessageCreatePayment);
    vm.$bus.$on('updateFormatValue', ($event) => {
      if ($event) {
        this.Payment.price = $event;
      }
    });
  },

  watch: {
    Payment: {
      handler(after) {
        let self = this;
        let inputPrice = after.price;
        if (inputPrice.length > 0) {
          setTimeout(function () {
            self.isRegexPrice = !regexInputPrice(inputPrice);
            if (!regexInputPriceNotAllow_0(inputPrice)) {
              self.isPriceNotStartWith_0 = true;
              if (self.Payment.price == 0 && self.isRemoveNotifyStringZero === true) {
                self.isRemoveNotifyStringZero = false;
                self.isPriceNotStartWith_0 = false;
                return
              }
              self.isRemoveNotifyStringZero = false;
            } else {
              self.isPriceNotStartWith_0 = false;
              return self.isPriceNotStartWith_0 = false;
            }
          }, 800);
        }
      },
      deep: true
    },
    currentTotalPriceRemain: {
      handler(after) {
        this.count++;
        this.isPricePaymentMax = false;
        if (after < 0) {
          this.isPricePaymentMax = true;
        }
        if (this.count === 1) {
          this.isPricePaymentMax = false;
        }

      },
      deep: true
    },
  },
  methods: {
    addNewPayment(scope) {
      let vm = this;
      vm.isBtnLoading = true;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {

          let newPayment = {
            date: helpers.formatDateTime(vm.Payment.date),
            price: vm.Payment.price,
            // payer: vm.Payment.payer,
            // receiver: vm.Payment.receiver,
            payment_method: vm.Payment.payment_method,
            deal_id: vm.deal_id,
            team_id: vm.team_id,
            note: vm.Payment.note,
            voucher: vm.Payment.voucher,
          };

          if (newPayment.price == 0) {
            vm.isPriceNotStartWith_0 = true;
            vm.isBtnLoading = false;
            return;
          }

          vm.$store.dispatch(DEAL_CREATE_PAYMENT, newPayment).then((response) => {
            if (response.status === 422) {
              if (response.data.errors.not_permission_create_payment) {
                $('#AddDealPayment').modal('hide');
                vm.$bus.$emit('addNewPaymentPolicy', true);
                return;
              }
            } else {
              /* vm.$store.state.deal.totalPriceHasPaid += parseInt(newPayment.price);
              vm.$store.state.deal.totalPriceDeal -= parseInt(newPayment.price);*/

              $('#AddDealPayment').modal('hide');
              vm.$bus.$emit('addNewPaymentSuccess', true);
              vm.getDetailDeal(vm.deal_id);
              vm.cancelAddNewPayment();
              vm.isBtnLoading = false;

              vm.$snotify.success('Thêm thanh toán thành công.', TOAST_SUCCESS);
            }
          }).catch((error) => {
            vm.$snotify.error('Thêm thanh toán thất bại.', TOAST_ERROR);
            vm.isBtnLoading = false;

            console.log(error);
          });
        } else {
          vm.isBtnLoading = false;
        }
      });

    },
    cancelAddNewPayment() {
      let vm = this;
      vm.count = 0;
      vm.isPricePaymentMax = false;
      vm.isRegexPrice = false;
      vm.isBtnLoading = false;
      vm.isPriceNotStartWith_0 = false;
      vm.Payment = {
        date: new Date(),
        price: '',
        // payer: '',
        // receiver: '',
        payment_method: '',
        note: '',
        voucher: '',
      };
      vm.isRemoveNotifyStringZero = true;
      vm.$validator.reset();
    },
    getDetailDeal(dealId) {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_DETAIL, dealId).then(() => {}).catch(error => {
        console.log(error);
      });
    },
    checkDealHasCompleteOrCancel() {
      let vm = this;
      if (vm.detailDeal && vm.detailDeal.transactionStatusCurrent && (vm.detailDeal.transactionStatusCurrent.option_value === 'Hoàn thành' || vm.detailDeal.transactionStatusCurrent.option_value === 'Hủy bỏ')) {

        vm.isDealCompleteOrCancel = true;
      }
    },
  }
}