import helpers from "@/utils/utils";

export default {
  data() {
    return {
      detailPayment: {}
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
  },
  created() {
    this.$bus.$on('detailPayment', ($event) => {
      if ($event) {
        this.detailPayment = $event;
      }
    })
  },
  methods: {
    cancelViewPayment() {
      this.detailPayment = {}
    }
  },
}
