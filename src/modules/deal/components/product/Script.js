import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import { PERMISSION_SHOW_DEAL } from "@/types/const";
import {
  DEAL_GET_ALL_PRODUCT
} from "@/types/actions.type";

export default {
  props: {
    deal_id: {
      type: Number,
      default: null
    },
    customer_id: {
      type: Number,
      default: null
    }
  },
  components: {},
  watch: {
    deal_id: {
      handler(after) {
        let vm = this;
        vm.getDealListProduct(after);
      },
      deep: true
    }
  },
  mounted() {
    let vm = this;
    vm.getDealListProduct(vm.deal_id);
  },
  data() {
    return {
      PERMISSION_SHOW_DEAL: PERMISSION_SHOW_DEAL,
      products: [],
      isLoader: false
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["currentTeam", "userCheckAddOn", "userCurrentPermissions"]),
  },
  methods: {
    getDealListProduct(deal_id) {
      let vm = this;
      vm.isLoader = true;
      vm.$store.dispatch(DEAL_GET_ALL_PRODUCT, { deal_id: deal_id }).then(response => {
        if (response.data.data) {
          vm.products = response.data.data;
          vm.isLoader = false;
        }
      });
    },
  }
}
