/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import GetAllProductByDealId from "../product/GetAllProductByDealId.vue";
import Pagination from "@/components/pagination/Pagination.vue";
import { PERMISSION_ADD_DEAL, PERMISSION_SHOW_DEAL } from "@/types/const";
import {
  DEAL_GET_ALL,
  DEAL_GET_DETAIL,
  DEAL_CREATE_ADD_STATUS,
  DEAL_GET_ATTRIBUTES,
  CUSTOMER_DETAIL_GET_ALL_DEAL
} from "@/types/actions.type";

export default {
  components: {
    GetAllProductByDealId: GetAllProductByDealId,
    pagination: Pagination,
  },
  mounted() {
    let vm = this;
    vm.getDealOfCustomers();
    vm.getDealAttributes();
    vm.getCustomerDetailListDeal(vm.customerId);
  },
  data() {
    return {
      PERMISSION_ADD_DEAL: PERMISSION_ADD_DEAL,
      PERMISSION_SHOW_DEAL: PERMISSION_SHOW_DEAL,

      isDataReady: false,
      isAttributeReady: false,
      dealSelected: null,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {},
      total_price: 0,
      customerId: parseInt(this.$route.params.customer_id),
      loadDataListDealCustomer: false,
      listDealPagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      listDealcurrentParams: {}
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["listCustomerDeals", "attributeDealCustomerFull", "detailCustomer", "currentTeam", "getStatusAddNew", "customerDetailListDeal", "userCheckAddOn", "userCurrentPermissions"]),
  },
  watch: {
    getStatusAddNew: {
      handler(after) {
        let vm = this;
        if (after == true) {
          vm.getDealOfCustomers();
        }
      },
      deep: true
    }
  },
  methods: {
    getCustomerDetailListDeal(customer_id) {
      let vm = this;
      vm.loadDataListDealCustomer = true;

      let currentPage = vm.listDealPagination.current_page > 0 ? vm.listDealPagination.current_page : 1;
      vm.listDealcurrentParams.page = currentPage;
      if (vm.listDealcurrentParams.per_page) {
        delete vm.listDealcurrentParams.per_page;
      }

      vm.listDealcurrentParams.customer_id = customer_id;

      vm.$store.dispatch(CUSTOMER_DETAIL_GET_ALL_DEAL, vm.listDealcurrentParams).then(response => {


        if (response) {
          vm.listDealPagination.current_page = response.data.meta.current_page;
          vm.listDealPagination.total_pages = response.data.meta.last_page;
          vm.listDealPagination.total_item = response.data.meta.total;
          vm.listDealPagination.per_page = response.data.meta.per_page.toString();
        }

        let items = 0;
        if (response.data.meta.current_page < response.data.meta.last_page) {
          items = response.data.meta.per_page;
        } else {
          items = response.data.meta.total - (response.data.meta.per_page * (response.data.meta.last_page - 1));
        }
        vm.listDealPagination.items_in_page = parseInt(items);

        vm.loadDataListDealCustomer = false;
      }).catch(error => {
        console.log(error);
      });
      // vm.$store.dispatch(CUSTOMER_DETAIL_GET_ALL_DEAL, {customer_id: customer_id});
    },

    getPaginationListDealCustomer() {
      let vm = this;
      vm.getCustomerDetailListDeal(vm.customerId);
      $('#table-responsive-data').animate({ scrollLeft: 0 }, 200);
    },

    getDealAttributes() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(() => {
      }).catch(error => {
        console.log(error);
      });
    },

    getDealOfCustomers() {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      vm.currentParams.customer_id = vm.detailCustomer.id;
      vm.$store.dispatch(DEAL_GET_ALL, vm.currentParams).then(response => {
        if (response.data.data) {
          let total = 0;
          response.data.data.forEach(function (item) {
            if (item.attributes && item.attributes.length > 0) {
              item.attributes.forEach(function (attribute) {
                if (attribute.data_type_id == 7 && attribute.attribute_code == "total_transaction_amount") {
                  let valueTotal = attribute.value;
                  if (attribute.value == "" || attribute.value == null) {
                    valueTotal = 0;
                  }
                  total = total + parseFloat(valueTotal);
                }
              })
            }
          });

          vm.$store.dispatch(DEAL_CREATE_ADD_STATUS, false).then(() => {
          });
          vm.total_price = total;
          vm.isDataReady = true;
        }
      }).catch(error => {
        console.log(error);
      });
    },
    getDetailDeal(dealId) {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_DETAIL, dealId).then(() => {
      }).catch(error => {
        console.log(error);
      });
    },
    renderElementClass(value) {
      let elClass = '';

      if (value == 'Mới mở') {
        elClass = 'deal-status-open';
      }
      if (value == 'Hoàn thành') {
        elClass = 'deal-status-success';
      }
      if (value == 'Hủy bỏ') {
        elClass = 'deal-status-cancel';
      }
      return elClass;
    },
    getColorByOption(deal) {
      let vm = this;
      let value = "";
      if (deal && deal.attributes && deal.attributes.length > 0) {
        deal.attributes.forEach(function (item) {
          vm.attributeDealCustomerFull.forEach(function (attribute) {
            if (item.attribute_id == attribute.id && item.attribute_code == "order_payment_status") {
              let option = attribute.attribute_options.find(function (op) {
                return op.id == item.value;
              });
              value = option.option_value;
            }
          })
        });
      }

      if (value == "Chưa thanh toán") {
        return "rgb(184, 46, 69)";
      } else if (value == "Đã thanh toán hết") {
        return "#69BE31";
      } else if (value == "Thanh toán 1 phần") {
        return "#FF8A00";
      } else {
        return "";
      }
    }
  }
}
