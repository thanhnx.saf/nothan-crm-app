/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import {
  DEAL_UPDATE_DELIVERY_STATUS,
  DEAL_GET_DETAIL
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";

export default {
  components: {},
  mounted() {},
  data() {
    return {
      indexItem: null,
      isDisabledUpdateDelivered: false
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["detailDeal", "currentTeam", "userCheckAddOn", "userCurrentPermissions"]),
  },
 
  methods: {
    showModalChangeIsDelivered(index) {
      let vm = this;
      vm.indexItem = index;
      setTimeout(function () {
        $('.s--modal-change-is-delivered').modal('show');
      }, 100);
    },
    updateIsDelivered(product) {
      let vm = this;
      vm.isDisabledUpdateDelivered = true;

      let data = {};
      let status = product.is_delivered;
      data.deal_id = vm.detailDeal.id;
      data.id = product.id;

      data.status = status;
      data.team_id = vm.currentTeamId;

      vm.$store.dispatch(DEAL_UPDATE_DELIVERY_STATUS, data).then(response => {
        if (response.data.status_code == 422) {
          if (response.data.errors) {

            $('.s--modal-change-is-delivered').modal('hide');

            if (response.data.errors.not_permission_create_payment) {
              $("#popupPolicyEditDeliveryProduct").modal("show");
            } else if (response.data.errors.edu_not_permission_update_deal) {
              $("#policyErrorsEdu").modal("show");
            }

            if (product.is_delivered == 1) {
              status = 2;
            } else if (product.is_delivered == 2) {
              status = 1;
            }

          }

        } else if (response.data.data && response.data.data.success) {
          vm.$snotify.success('Cập nhật trạng thái sản phẩm thành công.', TOAST_SUCCESS);
          $('.s--modal-change-is-delivered').modal('hide');
          vm.$bus.$emit('updateIsDelivered', true);
        } else {
          vm.$snotify.error('Cập nhật trạng thái sản phẩm thất bại.', TOAST_ERROR);
        }

        vm.isDisabledUpdateDelivered = false;

        if (vm.detailDeal.products && vm.detailDeal.products.length > 0) {
          vm.detailDeal.products.forEach(function (item) {
            if (item.id == product.id) {
              item.is_delivered = status;
            }
          });
        }
      }).catch(() => {
        vm.$snotify.error('Cập nhật trạng thái sản phẩm thất bại', TOAST_ERROR);
      });
    },
    updateDealDetail(dealId) {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_DETAIL, dealId);
    },
    cancelUpdateIsDelivered(product) {
      let vm = this;
      let status = "";
      if (product.is_delivered == 1) {
        status = 2;
      } else if (product.is_delivered == 2) {
        status = 1;
      }
      if (vm.detailDeal.products && vm.detailDeal.products.length > 0) {
        vm.detailDeal.products.forEach(function (item) {
          if (item.id == product.id) {
            item.is_delivered = status;
          }
        });
      }
    },

    refreshPage() {
      return window.location.reload();
    },
  }
}