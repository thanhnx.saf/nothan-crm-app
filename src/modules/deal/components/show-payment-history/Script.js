/* eslint-disable no-undef */
import {
  mapState
} from "vuex";
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import Pagination from "@/components/pagination/Pagination.vue";
import {
  DEAL_GET_ALL_PAYMENT_HISTORY
} from "@/types/actions.type";
import {
  PERMISSION_CREATE_PAYMENT
} from '@/types/const'
export default {
  components: {
    pagination: Pagination,
  },
  mounted() {
    let vm = this;
    vm.disabledBtnTransactionStatus(vm.detailDeal);
    // $('#popupPolicyAddPayment').modal('show');
  },

  data() {
    return {
      deal_id: parseInt(this.$route.params.deal_id),
      team_id: parseInt(this.$route.params.team_id),
      payment: {},
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {},
      loadDataPaymentHistory: false,
      isDealCompleteOrCancel: false,
      PERMISSION_CREATE_PAYMENT: PERMISSION_CREATE_PAYMENT
    }
  },
  created() {
    let vm = this;
    vm.checkDealHasCompleteOrCancel();
    vm.getAllPaymentHistory();
    this.$bus.$on('addNewPaymentSuccess', ($event) => {
      if ($event) {
        vm.getAllPaymentHistory();
      }
    });

    this.$bus.$on('addNewPaymentPolicy', ($event) => {
      if ($event) {
        $('#popupPolicyAddPayment').modal('show');
      }
    });

    vm.disabledBtnTransactionStatus(vm.detailDeal);
  },
  watch: {
    detailDeal: {
      handler(after) {
        if (after) {
          // Disabled btn edit
          let vm = this;
          vm.disabledBtnTransactionStatus(vm.detailDeal);
          vm.checkDealHasCompleteOrCancel();
        }
      },
      deep: true
    },
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapState({
      payment_history: state => state.deal.payment_history,
      totalPriceHasPaid: state => state.deal.totalPriceHasPaid,
      totalPriceDeal: state => state.deal.totalPriceDeal,
      detailDeal: state => state.deal.detailDeal,
    }),
    ...mapGetters(["detailDeal", "userCheckAddOn", "userCurrentPermissions"]),
    currentTotalPricePaid() {
      return helpers.formatPrice(this.totalPriceHasPaid);
    },
    currentTotalPriceRemain() {
      let priceRemain = this.totalPriceDeal - this.totalPriceHasPaid;
      return helpers.formatPrice(priceRemain);
    },


  },
  methods: {
    disabledBtnTransactionStatus(data) {
      if (data.transactionStatusCurrent && (data.transactionStatusCurrent.option_value == "Hoàn thành" || data.transactionStatusCurrent.option_value == "Hủy bỏ")) {
        $(".btn-deal-edit").addClass('nt-button-top-disabled');
      } else {
        $(".btn-deal-edit").removeClass('nt-button-top-disabled');
      }
    },

    getAllPaymentHistory() {
      let vm = this;
      vm.loadDataPaymentHistory = true;

      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      if (vm.currentParams.per_page) {
        delete vm.currentParams.per_page;
      }

      vm.currentParams.team_id = vm.team_id;
      vm.currentParams.deal_id = vm.deal_id;

      vm.$store.dispatch(DEAL_GET_ALL_PAYMENT_HISTORY, vm.currentParams).then(response => {
        if (response) {
          vm.pagination.current_page = response.data.meta.current_page;
          vm.pagination.total_pages = response.data.meta.last_page;
          vm.pagination.total_item = response.data.meta.total;
          vm.pagination.per_page = response.data.meta.per_page.toString();
          vm.oldCurrentPage = response.data.meta.current_page;
        }

        let items = 0;
        if (response.data.meta.current_page < response.data.meta.last_page) {
          items = response.data.meta.per_page;
        } else {
          items = response.data.meta.total - (response.data.meta.per_page * (response.data.meta.last_page - 1));
        }
        vm.pagination.items_in_page = parseInt(items);

        vm.loadDataPaymentHistory = false;
      }).catch(error => {
        console.log(error);
      });
    },
    viewDetailPayment(payment) {
      if (payment) {
        this.$bus.$emit('detailPayment', payment);
      }
    },
    checkIsNumber(value) {
      return /^\d+\.\d+$/.test(value)
    },
    removeDashInStringAndFormatParseInt(value) {
      if (value) {
        let data = '';
        data = value.replace(/-/g, "");
        return parseInt(data)
      }
    },
    getPaginationHistory() {
      let vm = this;
      vm.loadDataPaymentHistory = true;
      vm.getAllPaymentHistory();
      $('#table-responsive-data').animate({
        scrollLeft: 0
      }, 200);
    },
    checkDealHasCompleteOrCancel() {
      let vm = this;
      if (vm.detailDeal && vm.detailDeal.transactionStatusCurrent && (vm.detailDeal.transactionStatusCurrent.option_value === 'Hoàn thành' || vm.detailDeal.transactionStatusCurrent.option_value === 'Hủy bỏ')) {
        vm.isDealCompleteOrCancel = true;
      }
    },
    addNewPayment() {
      let vm = this;
      vm.$bus.$emit('addNewPayment', true);
    }
  }
}