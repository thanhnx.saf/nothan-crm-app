import {
  SET_DEALS,
  SET_DEAL_ATTRIBUTES,
  SET_DEAL_USER_ATTRIBUTES,
  SET_STATUS_ADD_NEW,
  SET_ALL_PAYMENT_HISTORY,
  SET_DEAL_DETAIL,
  SET_CUSTOMER_DETAIL_DEAL,
  SET_DEAL_FILTERS,
  SET_DEAL_FILTER_ATTRIBUTES,
  SET_DEAL_FILTERS_CREATE,
  SET_DEAL_FILTERS_UPDATE,
  SET_DEAL_FILTERS_DELETE,
  SET_DEAL_ATTRIBUTE_DATA_TYPE
} from "@/types/mutations.type";

export const mutations = {
  [SET_DEALS](state, deals) {
    state.list = deals;
  },

  [SET_CUSTOMER_DETAIL_DEAL](state, deals) {
    state.customer_detail_list_deal = deals;
  },

  [SET_DEAL_ATTRIBUTES](state, attributes) {
    state.attributes = attributes;
    if (state.user_attributes.length > 0) {
      let result = [];
      let listAttrInUser = [];
      state.user_attributes.forEach(function (uAttr) {
        attributes.forEach(function (attr) {
          if (uAttr.attribute_code == attr.attribute_code) {
            attr.is_show = uAttr.is_show;
            attr.sort_order = uAttr.sort_order;
            attr.user_id = uAttr.user_id;
            attr.external_attribute = uAttr.external_attribute;
            attr.code_external_attribute = uAttr.code_external_attribute;
            if (attr.attribute_options && attr.attribute_options.length > 0) {
              attr.is_multiple_value = 1;
            } else {
              attr.is_multiple_value = 0;
            }
            result.push(attr);
            listAttrInUser.push(attr.id);
          }
        })
      });
      let countAttr = state.user_attributes.length;
      let userId = state.user_attributes[0].user_id;

      if (listAttrInUser.length !== attributes.length) {
        let attributeOuts = [];
        attributes.forEach(function (attr) {
          if (listAttrInUser.indexOf(attr.id) === -1) {
            attributeOuts.push(attr);
          }
        })
        attributeOuts.forEach(function (attr) {
          countAttr++;
          let temp_data = {
            attribute_code: attr.attribute_code,
            attribute_id: attr.id,
            attribute_name: attr.attribute_name,
            is_show: 0,
            sort_order: countAttr,
            user_id: userId
          };
          state.user_attributes.push(temp_data);
          attr.is_show = 0;
          attr.sort_order = countAttr;
          attr.user_id = userId;
          result.push(attr);
        })
      }

      let countBase = 0;
      result.forEach(function (item) {
        if (item.attribute_id == "charge_person") {
          countBase++;
        }
      })
      if (countBase > 0) {
        let countCharge = 0;
        result.forEach(function (item) {
          if (item.attribute_id == "charge_person") {
            countCharge++;
          }
        })
        if (countCharge <= 0) {
          let temp_data_charge = {
            id: "charge_person",
            attribute_code: "charge_person",
            attribute_id: "charge_person",
            attribute_name: "Người phụ trách",
            external_attribute: 1,
            code_external_attribute: "charge_person",
            is_show: 1,
            sort_order: 7,
            user_id: userId,
            data_type: {
              id: "charge_person"
            }
          };
          state.user_attributes.push(temp_data_charge);
          result.push(temp_data_charge);
        }
      } else {
        let temp_data_charge = {
          id: "charge_person",
          attribute_code: "charge_person",
          attribute_id: "charge_person",
          attribute_name: "Người phụ trách",
          is_show: 1,
          external_attribute: 1,
          code_external_attribute: "charge_person",
          sort_order: 7,
          user_id: userId,
          data_type: {
            id: "charge_person"
          }
        };
        state.user_attributes.push(temp_data_charge);
        result.push(temp_data_charge);
      }

      result.forEach(function (attr) {
        if (attr.attribute_code == 'charge_person') {
          attr.sort_order = 7;
        }
        if (attr.attribute_code == 'total_transaction_amount') {
          attr.sort_order = 8;
        }
      });
      result.sort((a, b) => parseFloat(a.sort_order) - parseFloat(b.sort_order));
      state.attributes = result;
    }
  },

  [SET_DEAL_USER_ATTRIBUTES](state, attributes) {
    state.user_attributes = attributes;
  },

  [SET_DEAL_DETAIL](state, deal) {
    state.deal = deal;

    /*Get total price has paid*/
    let data = deal;
    let attribute = [];
    if (data) {
      let attribute_codes = ['total_paid_amount', 'total_transaction_amount'];
      let variable_name = 'attribute_value';

      let attributes = data.attributes;

      if (attributes && attributes.length > 0) {
        attributes.forEach(function (item) {
          attribute_codes.forEach(function (attribute_code) {
            if (item.attribute_code === attribute_code) {
              attribute.push(item[variable_name]);

              // attribute = item;
            }
          })

        });
      }
    }
    state.totalPriceDeal = attribute[0];
    state.totalPriceHasPaid = attribute[1];
  },

  [SET_STATUS_ADD_NEW](state, status) {
    state.add_new = status;
  }
  ,
  [SET_ALL_PAYMENT_HISTORY](state, paymentHistory) {
    state.payment_history = paymentHistory;
  },

  [SET_DEAL_FILTERS](state, filters) {
    state.filters = filters;
  },

  [SET_DEAL_FILTER_ATTRIBUTES](state, filterAttributes) {
    state.filter_attributes = filterAttributes;
  },

  [SET_DEAL_FILTERS_CREATE](state, filterCreate) {
    state.filters.push(filterCreate);
  },

  [SET_DEAL_FILTERS_UPDATE](state, filterUpdate) {
    let newFilters = [];
    state.filters.forEach(function (item) {
      if (item.id === filterUpdate.id) {
        item = filterUpdate;
      }
      newFilters.push(item);
    })
    state.filters = newFilters;
  },

  [SET_DEAL_FILTERS_DELETE](state, filterId) {
    state.filters.forEach(function (item, index) {
      if (item.id === filterId) {
        state.filters.splice(index, 1);
      }
    })
  },

  [SET_DEAL_ATTRIBUTE_DATA_TYPE](state, dataType) {
    state.data_types = dataType;
  }
}
