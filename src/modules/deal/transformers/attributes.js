export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformDealAttributes(item));
    });

    return new_data;
  },

  transformDealAttributes(data) {
    let vm = this;
    let transform = {}

    transform = {
      id: data.id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      team_id: data.team_id,
      data_type_id: data.data_type_id,
      is_hidden: data.is_hidden,
      is_required: data.is_required,
      is_default_attribute: data.is_default_attribute,
      default_value: data.default_value,
      is_unique: data.is_unique,
      is_multiple_value: data.is_multiple_value,
      hint: data.hint,
      note: data.note,
      attribute_options: vm.transformAttributeOptions(data.attribute_options),
      sort_order: data.sort_order,
      data_type: data.data_type
    };

    return transform;
  },

  transformAttributeOptions(data) {
    let options = [];
    if (data && data.length > 0) {
      data.forEach(function (option) {
        let temp = {
          attribute_id: option.attribute_id,
          id: option.id,
          option_value: option.option_value,
          parent_id: option.parent_id,
          sort_order: option.sort_order
        };
        options.push(temp);
      })
      options.sort(function (before, after) {
        return before.sort_order - after.sort_order;
      });
    }
    return options;
  }
}
