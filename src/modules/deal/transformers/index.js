import listVM from './list';
import listDealCustomerVM from './listDealCustomer';
import showVM from './show';
import attributeVM from './attributes';
import userAttributeVM from './userAttributes';
import insertVM from './insert';
import updateVM from './update';
import filterVM from './listFilters';
import filterAttributesVM from './listFilterAttributes';
import createFilterVM from './createDealFilter';
import updateFilterVM from './updateDealFilter';
import listAttributeDataTypeVM from "./listAttributeDataType";

export const DealVM = {
  listVM,
  listDealCustomerVM,
  showVM,
  attributeVM,
  userAttributeVM,
  insertVM,
  updateVM,
  filterVM,
  filterAttributesVM,
  createFilterVM,
  updateFilterVM,
  listAttributeDataTypeVM
}
