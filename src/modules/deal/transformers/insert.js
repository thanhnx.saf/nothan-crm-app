import helpers from "@/utils/utils";

export default {
  transform(data) {
    let vm = this;
    let insertData = {
      assigners: data.assigners,
      attributes: vm.transformAttributes(data.attributes),
      customer_id: data.customer_id,
      customer_name: data.customer_name,
      order_date: data.order_date ? helpers.formatDateTime(data.order_date) : "",
      discount: vm.transformDiscount(data.discount),
      product_discount_type: data.product_discount_type,
      products: vm.transformProducts(data.products),
      total: data.total,
      total_after_discount: data.total_after_discount
    };
    return insertData;
  },

  transformDiscount(data) {
    let vm = this;
    let deal = {
      total_discount: data.total_discount,
      type: data.type,
      value: vm.clearFormatNumber(data.value)
    };
    return deal;
  },

  transformProducts(data) {
    let vm = this;
    let products = [];
    data.forEach(function (item) {
      let tempData = {
        discount: vm.clearFormatNumber(item.discount),
        id: item.id,
        product_code: item.product_code,
        product_name: item.product_name,
        product_price: item.product_price,
        product_type: item.product_type,
        customer_id: item.customer.customer_id && typeof item.customer.customer_id !== "undefined" ? item.customer.customer_id : "",
        customer_name: item.customer.customer_name && typeof item.customer.customer_name !== "undefined" ? item.customer.customer_name : "",
        customer_code: item.customer.customer_code && typeof item.customer.customer_code !== "undefined" ? item.customer.customer_code : "",
        customer_phone: item.customer.customer_phone && typeof item.customer.customer_phone !== "undefined" ? item.customer.customer_phone : "",
        customer_email: item.customer.customer_email && typeof item.customer.customer_email !== "undefined" ? item.customer.customer_email : "",
        status: item.status,
        quantity: vm.convertNumberDecimal(String(item.quantity).replace(".", "")),
        sale_price: item.sale_price,
        sort_order: item.sort_order,
        total_price: item.total_price,
        product_unit: item.product_unit
      };
      products.push(tempData);
    });
    return products;
  },

  transformAttributes(data) {
    let vm = this;
    let attributes = [];
    data.forEach(function (item) {
      if (!item.is_hidden || item.is_hidden && item.is_default_attribute) {
        let tempData = {};
        if (item.data_type_id == 2) {
          tempData = {
            id: item.id,
            attribute_code: item.attribute_code,
            attribute_value: item.attribute_value ? helpers.formatDateTime(item.attribute_value) : ""
          };
        } else if (item.data_type_id == 3) {
          tempData = {
            id: item.id,
            attribute_code: item.attribute_code,
            attribute_value: item.attribute_value ? helpers.formatDateTime(item.attribute_value, true) : ""
          };
        } else if (item.data_type_id == 6) {
          tempData = {
            id: item.id,
            attribute_code: item.attribute_code,
            attribute_value: item.attribute_value ? vm.convertNumberDecimal(item.attribute_value) : ""
          };
        } else {
          let attributeValue = "";
          if (!item.attribute_value && (item.attribute_code === "vat_amount" || item.attribute_code === "total_transaction_amount" || item.attribute_code === "base_total_transaction_amount" || item.attribute_code === "total_paid_amount")) {
            attributeValue = 0;
          } else if (item.attribute_value) {
            if (item.attribute_code === "contact_user_phone") {
              attributeValue = item.attribute_value.replace(/[^\d]/g, "");
            } else {
              attributeValue = item.attribute_value;
            }
          }
          if (item.attribute_code === "contact_user") {
            tempData = {
              id: item.id,
              attribute_code: item.attribute_code,
              attribute_value: attributeValue,
              customer_id: item.customer_id,
            };
          } else {
            tempData = {
              id: item.id,
              attribute_code: item.attribute_code,
              attribute_value: attributeValue
            };
          }
        }
        attributes.push(tempData);
      }
    });
    return attributes;
  },

  convertNumberDecimal(value = "") {
    let newValue = String(value);
    if (newValue.length > 0) {
      newValue = parseFloat(newValue.replace(",", "."));
    }
    return newValue;
  },

  clearFormatNumber(value = "") {
    let newValue = String(value);
    if (newValue.length > 0) {
      newValue = parseFloat(newValue.replace(/[^\d]/g, ""));
    }
    return newValue;
  }
}
