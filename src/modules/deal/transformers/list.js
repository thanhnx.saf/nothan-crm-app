/* eslint-disable no-undef */
export default {
  transform(data, meta) {
    let vm = this;
    let responseData = [];

    data.forEach(function (item) {
      responseData.push(vm.transformDeal(item, meta));
    });

    return responseData;
  },

  transformDeal(data, meta) {
    let vm = this;
    let transform = {};

    transform = {
      id: data.id,
      is_active: data.is_active,
      attributes: vm.transformAttributes(data.attributes),
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
      customer: data.customer,
      in_page: meta.current_page,
      customer_id: data.customer_id,
      user_id: data.user_id,
      assigners: data.assigners,
    };
    return transform;
  },

  transformAttributes(data) {
    let attributes = [];
    let issetAttributes = [];
    if (data && data.length > 0) {
      data.forEach(function (item) {
        if (item.data_type_id === 10 || item.data_type_id === 11 || item.data_type_id === 13 || item.data_type_id === 16) {
          if ($.inArray(item.attribute_id + '_' + item.deal_id, issetAttributes) > -1) {
            let newValue = [];
            attributes.forEach(function (attribute) {
              if (item.attribute_id === attribute.attribute_id) {
                if (Array.isArray(attribute.value)) {
                  newValue.push(item.value);
                  $.merge(attribute.value, newValue);
                } else {
                  newValue.push(item.value);
                  newValue.push(attribute.value);
                  attribute.value = newValue;
                }
              }
            })
          } else {
            let newValue = [];
            newValue.push(item.value);
            item.value = newValue;
            attributes.push(item);
            issetAttributes.push(item.attribute_id + '_' + item.deal_id);
          }
        } else {
          attributes.push(item);
        }
      })
    }
    return attributes;
  }
}
