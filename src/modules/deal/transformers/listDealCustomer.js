export default {
  transform(data) {
    let vm = this;
    let responseData = [];

    data.forEach(function (item) {
      responseData.push(vm.transformDeal(item));
    });

    return responseData;
  },

  transformDeal(data) {
    let transform = {};

    transform = {
      id: data.id,
      is_active: data.is_active,
      attributes: data.attributes,
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
      customer: data.customer,
      customer_id: data.customer_id,
      user_id: data.user_id,
      assigners: data.assigners,
    };
    return transform;
  }
}
