/* eslint-disable no-undef */
export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformFilterAttributes(item));
    });

    return new_data;
  },

  transformFilterAttributes(data) {
    let vm = this;
    let transform = {};
    transform = {
      id: data.id,
      filter_id: data.filter_id,
      attribute_id: data.attribute_id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      data_type: data.data_type,
      attribute_values: data.attribute_values,
      external_attribute: data.external_attribute,
      code_external_attribute: data.code_external_attribute,
      attribute_options: data.attribute_options,
      show_position: data.show_position,
      is_auto_gen: vm.thenIsAutoGenerate(data),
      is_disabled: true,
      created_at: data.created_at,
      updated_at: data.updated_at
    };

    return transform;
  },

  thenIsAutoGenerate(item) {
    if ($.inArray(item.attribute_code, ["shipping_status", "product_price", "order_date", "order_code", "single_sale_price", "price_after", "product_code", "product_name", "product_quantity", "product_discount", "order_discount", "amount_must_be_paid", "transaction_status", "reason_for_cancellation", "base_total_transaction_amount", "vat_amount", "vat_type", "total_transaction_amount", "total_paid_amount", "order_payment_status", "customer_code", "customer_name", "user_id", "created_at", "updated_at"]) > -1) {
      return true;
    } else {
      return false;
    }
  }
}
