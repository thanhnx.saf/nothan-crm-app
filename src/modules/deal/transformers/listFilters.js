export default {
  transform(data) {
    let vm = this;
    let new_data = [];
    data.forEach(function (item) {
      new_data.push(vm.transformFilters(item));
    });

    return new_data;
  },

  transformFilters(data) {
    let transform = {};

    transform = {
      id: data.id,
      name: data.name,
      user_id: data.user_id,
      team_id: data.team_id,
      status: data.status,
      created_at: data.created_at,
      updated_at: data.updated_at
    };

    return transform;
  }
}
