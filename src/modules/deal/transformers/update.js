import helpers from "@/utils/utils";

export default {
  transform(data) {
    let vm = this;
    let updateAttributes = vm.transformAttributes(data.attributes);
    let updateData = {
      attributes: updateAttributes.attributes,
      customer_id: data.customer_id,
      customer_name: data.customer_name,
      total: data.total,
      total_cache: data.total_cache,
      total_after_discount: data.total_after_discount,
      total_after_discount_cache: data.total_after_discount_cache,
      type_update: updateAttributes.type
    };
    if (data.products && data.products_cache) {
      let newDataProducts = [];
      data.products.forEach(function (product) {
        let temp = {
          id: product.id,
          product_id: product.product_id,
          product_code: product.product_code,
          product_name: product.product_name,
          product_type: product.product_type,
          quantity: String(product.quantity),
          discount: String(product.discount),
          product_price: product.product_price,
          sale_price: product.sale_price,
          total_price: product.total_price,
          status: product.status,
          customer: product.customer,
          sort_order: product.sort_order
        };
        newDataProducts.push(temp);
      })
      if (!vm.compareArray(newDataProducts, data.products_cache)) {
        updateData.products = vm.transformProducts(data.products);
        updateData.products_cache = vm.transformProducts(data.products_cache);
        updateData.type_update.push(2);
      }
    }
    if (data.discount && data.discount_cache && !vm.compareObject(data.discount, data.discount_cache)) {
      updateData.discount = vm.transformDiscount(data.discount);
      updateData.discount_cache = vm.transformDiscount(data.discount_cache);
      updateData.type_update.push(2);
    }
    if (!vm.compareArray(data.assigners, data.assigners_cache)) {
      updateData.assigners = data.assigners;
      updateData.assigners_cache = data.assigners_cache;
      updateData.type_update.push(3);
    }
    if (data.product_discount_type && data.product_discount_type !== '' && data.product_discount_type !== data.product_discount_type_cache) {
      updateData.product_discount_type = data.product_discount_type;
      updateData.product_discount_type_cache = data.product_discount_type_cache;
      updateData.type_update.push(2);
    }
    if (data.order_date && data.order_date !== '' && data.order_date !== data.order_date_cache) {
      updateData.order_date = data.order_date ? helpers.formatDateTime(data.order_date) : "";
      updateData.order_date_cache = data.order_date_cache ? helpers.formatDateTime(data.order_date_cache) : "";
      updateData.type_update.push(3);
    }
    let typeUpdate = updateData.type_update.filter(vm.distinctValue);
    updateData.type_update = typeUpdate;
    return updateData;
  },

  transformDiscount(data) {
    let vm = this;
    let deal = {
      total_discount: data.total_discount,
      type: data.type,
      value: vm.clearFormatNumber(data.value)
    };
    return deal;
  },

  transformAttributes(data) {
    let attributes = [];
    let typeUpdate = [];
    let vm = this;
    data.forEach(function (item) {
      // console.log(item, 'item');
      if (!item.is_hidden || item.is_hidden && item.is_default_attribute) {
        let tempData = {};
        if (item.data_type_id == 2) {
          tempData = {
            id: item.id,
            attribute_code: item.attribute_code,
            attribute_value: item.attribute_value ? helpers.formatDateTime(item.attribute_value) : ""
          };
          let valueCache = helpers.formatDateTime(item.attribute_value_cache);
          item.attribute_value_cache = valueCache;
        } else if (item.data_type_id == 3) {
          tempData = {
            id: item.id,
            attribute_code: item.attribute_code,
            attribute_value: item.attribute_value ? helpers.formatDateTime(item.attribute_value, true) : ""
          };
          let valueCache = helpers.formatDateTime(item.attribute_value_cache, true);
          item.attribute_value_cache = valueCache;
        } else if (item.data_type_id == 6) {
          tempData = {
            id: item.id,
            attribute_code: item.attribute_code,
            attribute_value: item.attribute_value ? vm.convertNumberDecimal(item.attribute_value) : ""
          };
        } else {
          let attributeValue = "";
          if (!item.attribute_value && item.attribute_code === "vat_amount") {
            attributeValue = 0;
          } else if (item.attribute_value) {
            if (item.attribute_code === "contact_user_phone") {
              attributeValue = item.attribute_value.replace(/[^\d]/g, "");
            } else {
              attributeValue = item.attribute_value;
            }
          }
          if (item.attribute_code === "contact_user") {
            tempData = {
              id: item.id,
              attribute_code: item.attribute_code,
              attribute_value: attributeValue,
              customer_id: item.customer_id,
            };
          } else {
            tempData = {
              id: item.id,
              attribute_code: item.attribute_code,
              attribute_value: attributeValue
            };
          }
        }

        if (item.attribute_value_cache !== tempData.attribute_value) {
          if (item.attribute_code === "order_name" || item.attribute_code === "order_code" || item.attribute_code === "contact_user" || item.attribute_code === "transaction_status") {
            typeUpdate.push(1);
          } else if (item.attribute_code === "total_transaction_amount" || item.attribute_code === "order_note" || item.attribute_code === "base_total_transaction_amount" || item.attribute_code === "vat_amount" || item.attribute_code === "vat_type") {
            typeUpdate.push(2);
          } else {
            typeUpdate.push(3);
          }
          attributes.push(tempData);
        }
      }
    });
    let result = {
      attributes: attributes,
      type: typeUpdate
    };

    return result;
  },

  transformProducts(data) {
    let products = [];
    let vm = this;
    data.forEach(function (item) {
      let tempData = {
        discount: vm.clearFormatNumber(item.discount),
        id: item.id,
        product_id: item.product_id,
        product_code: item.product_code,
        product_name: item.product_name,
        product_price: item.product_price,
        product_type: item.product_type,
        customer_id: item.customer.customer_id && typeof item.customer.customer_id !== "undefined" ? item.customer.customer_id : "",
        customer_name: item.customer.customer_name && typeof item.customer.customer_name !== "undefined" ? item.customer.customer_name : "",
        customer_code: item.customer.customer_code && typeof item.customer.customer_code !== "undefined" ? item.customer.customer_code : "",
        customer_phone: item.customer.customer_phone && typeof item.customer.customer_phone !== "undefined" ? item.customer.customer_phone : "",
        customer_email: item.customer.customer_email && typeof item.customer.customer_email !== "undefined" ? item.customer.customer_email : "",
        status: item.status,
        quantity: vm.convertNumberDecimal(String(item.quantity).replace(".", "")),
        sale_price: item.sale_price,
        sort_order: item.sort_order,
        total_price: item.total_price,
        product_unit: item.product_unit
      };
      products.push(tempData);
    });
    return products;
  },

  distinctValue(value, index, self) {
    return self.indexOf(value) === index;
  },

  convertNumberDecimal(value = "") {
    let newValue = String(value);
    if (newValue.length > 0) {
      newValue = parseFloat(newValue.replace(",", "."));
    }
    return newValue;
  },

  clearFormatNumber(value = "") {
    let newValue = String(value);
    if (newValue.length > 0) {
      newValue = parseFloat(newValue.replace(/[^\d]/g, ""));
    }
    return newValue;
  },

  compareObject(obj1, obj2) {
    if (JSON.stringify(obj1) === JSON.stringify(obj2)) {
      return true;
    } else {
      return false;
    }
  },

  compareArray(arr1, arr2) {
    if (!arr1 || !arr2) return false;
    if (!Array.isArray(arr1) || !Array.isArray(arr2)) return false;
    let count = 0;
    arr1.forEach((e1) => arr2.forEach(e2 => {
      if (typeof arr1 == 'object' && typeof arr2 == 'object') {
        if (JSON.stringify(e1) === JSON.stringify(e2)) {
          count++;
        } else {
          return;
        }
      } else if (typeof arr1 == 'object' && typeof arr2 != 'object' || typeof arr1 != 'object' && typeof arr2 == 'object') {
        return;
      } else {
        if (e1 === e2) {
          count++
        } else {
          return;
        }
      }
    })
    )
    if (arr1.length > arr2.length) {
      if (arr1.length === count) {
        return true;
      } else {
        return false;
      }
    } else {
      if (arr2.length === count) {
        return true;
      } else {
        return false;
      }
    }
  }
}
