export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformUserAttributes(item));
    });

    return new_data;
  },

  transformUserAttributes(data) {
    let transform = {}

    transform = {
      id: data.id,
      attribute_id: data.attribute_id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      external_attribute: data.external_attribute,
      code_external_attribute: data.code_external_attribute,
      sort_order: data.sort_order,
      is_show: data.is_show,
      user_id: data.user_id
    };

    return transform;
  }
}
