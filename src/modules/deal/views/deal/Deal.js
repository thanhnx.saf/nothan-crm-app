/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from '@/utils/utils';
import {
  DEAL_GET_ALL,
  DEAL_GET_ATTRIBUTES,
  DEAL_GET_USER_ATTRIBUTES,
  DEAL_GET_TOTAL_PRICE,
  GET_LIST_DEAL_FILTERS,
  GET_DEAL_FILTER_ATTRIBUTES,
  CUSTOMER_GET_CONTACT,
  EXPORT_LIST_DEAL
} from "@/types/actions.type";
import {
  PERMISSION_EXPORT_DEAL_BASIC,
  PERMISSION_EXPORT_DEAL_PRODUCT
} from "@/types/const";
import Pagination from "@/components/pagination/Pagination.vue";
import Filter from "@/components/filter/Filter.vue";
import ListContact from "@/components/contact/ListContact";
import ImportDeal from "../../components/modals/import-deal/ImportDeal.vue";
import {
  PERMISSION_SHOW_DEAL
} from "@/types/const"
import Pipeline from '../pipeline/Pipeline.vue'

const SEARCH_TIMEOUT_DEFAULT = 10000;

const displayOptions = {
  table: 'table',
  pipeline: 'pipeline'
}

export default {
  components: {
    pagination: Pagination,
    "filter-deal": Filter,
    "listContact": ListContact,
    ImportDeal: ImportDeal,
    Pipeline
  },
  mounted() {
    let vm = this;
    vm.getAll();
    vm.getAttributeByUser();
    vm.getTotalPriceAllDeal();
    vm.getFilters();
    vm.callTooltipAfterLoadData();
  },
  watch: {
    contacts: {
      handler() {},
      deep: true
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    isLoader() {
      let vm = this;
      if (vm.dataReady === true && vm.attributeReady === true) {
        return true;
      } else {
        return false;
      }
    },
    ...mapGetters(["deals", "allAttributes", "listAttributesForDeals", "currentUser", "currentTeam", "listUsers", "getDealFilters", "userCheckAddOn", "userCurrentPermissions"]),
  },
  data() {
    return {
      displayOptions: displayOptions,
      displayType: displayOptions.table,
      PERMISSION_EXPORT_DEAL_BASIC: PERMISSION_EXPORT_DEAL_BASIC,
      PERMISSION_EXPORT_DEAL_PRODUCT: PERMISSION_EXPORT_DEAL_PRODUCT,
      PERMISSION_SHOW_DEAL: PERMISSION_SHOW_DEAL,
      showFilter: false,
      isFilter: false,
      isSearch: false,
      isLoaderDeals: false,
      loadDataDeals: false,
      dealChecked: [],
      dealCheckedPerPage: [],
      dealCheckAllPerPage: [],
      contacts: [],
      oldCurrentPage: 0,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {},
      beforeSearchCache: null,
      dataReady: false,
      attributeReady: false,
      totalPriceAllDeal: 0,
      isAllData: true,
      filterAttributes: [],
      cacheForFilterAttributes: null,
      searchText: '',
      isMinimizeFilter: false
    }
  },
  methods: {
    refresh() {
      this.getAll()
    },
    getAll(type = "all") {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;

      vm.currentParams.page = currentPage;
      vm.currentParams.per_page = 10;
      vm.isLoaderDeals = true;
      vm.$store.dispatch(DEAL_GET_ALL, vm.currentParams).then(response => {
        if (type != "pagination") {
          vm.dealChecked = [];
        }
        if (typeof vm.dealCheckAllPerPage[response.data.meta.current_page] == "undefined") {
          vm.dealCheckAllPerPage[response.data.meta.current_page] = false;
        }
        if (typeof vm.dealCheckedPerPage[response.data.meta.current_page] == "undefined") {
          vm.dealCheckedPerPage[response.data.meta.current_page] = [];
        }
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.oldCurrentPage = response.data.meta.current_page;

        let items = 0;

        if (response.data.meta.current_page < response.data.meta.last_page) {
          items = response.data.meta.per_page;
        } else {
          items = response.data.meta.total - (response.data.meta.per_page * (response.data.meta.last_page - 1));
        }
        vm.pagination.items_in_page = parseInt(items);
        vm.beforeSearchCache = JSON.stringify(response.data.data);
        vm.dataReady = true;
        // vm.getTotalPriceAllDeal();

        if (vm.loadSearch) {
          vm.isSearch = true;
        } else {
          vm.isSearch = false;
        }

        if (vm.currentParams.filter) {
          vm.isFilter = true;
        } else {
          vm.isFilter = false;
        }
        vm.loadDataDeals = false;
        vm.isLoaderDeals = false;

        //let deals = response.data.data;
        //vm.getListContact(deals);
      }).catch(error => {
        console.log(error);
      });
    },
    getListContact(deals) {
      let vm = this;
      if (deals && deals.length) {
        let customers = [];
        deals.forEach(function (deal) {
          let customer_id = deal.customer_id;
          if (customers.indexOf(customer_id) === -1) {
            customers.push(customer_id);
            let params = {
              customer_id: customer_id,
            };
            vm.getAllContactByCustomer(params);
          }
        });
      }
    },
    getTotalPriceAllDeal() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_TOTAL_PRICE).then((response) => {
        if (response.data.data.total) {
          vm.totalPriceAllDeal = response.data.data.total;
        }
      }).catch(error => {
        console.log(error);
      });
    },
    getAttributes() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(() => {
        vm.attributeReady = true;
      }).catch(error => {
        console.log(error);
      });
    },
    getAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_USER_ATTRIBUTES).then(() => {
        vm.getAttributes();
      }).catch(error => {
        console.log(error);
      });
    },
    checkedDeal(deal) {
      let vm = this;
      if (!vm.checkExistValueInChecked(deal)) {
        vm.dealCheckedPerPage[vm.oldCurrentPage].push(deal);
      } else {
        let newChecked = [];
        vm.dealCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
          if (item.id !== deal.id) {
            newChecked.push(item);
          }
        });
        vm.dealCheckedPerPage[vm.oldCurrentPage] = newChecked;
      }
      let itemCheckedInPage = vm.dealCheckedPerPage[vm.oldCurrentPage];
      if (itemCheckedInPage && vm.pagination.items_in_page == itemCheckedInPage.length) {
        vm.dealCheckAllPerPage[vm.oldCurrentPage] = true;
      } else {
        vm.dealCheckAllPerPage[vm.oldCurrentPage] = false;
      }
    },
    checkedAllToggle() {
      let vm = this;

      if (!vm.dealCheckAllPerPage[vm.oldCurrentPage]) {
        let itemIsChecked = [];

        if (vm.dealCheckedPerPage[vm.oldCurrentPage] && vm.dealCheckedPerPage[vm.oldCurrentPage].length > 0) {
          vm.dealCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
            itemIsChecked.push(item.id);
          })
        }
        let newChecked = [];
        vm.deals.forEach(function (deal) {
          if ($.inArray(deal.id, itemIsChecked) == -1) {
            vm.dealChecked.push(deal);
            newChecked.push(deal);
          }
        });
        vm.dealCheckedPerPage[vm.oldCurrentPage] = newChecked;
      } else {
        let newChecked = [];
        vm.dealChecked.forEach(function (deal) {
          if (deal.in_page != vm.pagination.current_page) {
            newChecked.push(deal);
          }
        });
        vm.dealCheckedPerPage[vm.oldCurrentPage] = newChecked;
        vm.dealChecked = newChecked;
      }
    },
    checkExistValueInChecked(deal) {
      let vm = this;
      if (vm.dealCheckedPerPage[vm.oldCurrentPage] && vm.dealCheckedPerPage[vm.oldCurrentPage].length > 0) {
        let exist = vm.dealCheckedPerPage[vm.oldCurrentPage].find(function (item) {
          return item.id == deal.id;
        })
        if (exist && Object.keys(exist).length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },
    getPaginationDeal() {
      let vm = this;
      vm.loadDataDeals = true;
      vm.getAll("pagination");
      $('#table-responsive-data').animate({
        scrollLeft: 0
      }, 200);
    },
    getQuantityItemChecked() {
      let vm = this;
      let result = 0;
      if (vm.dealChecked.length > 0) {
        if (vm.dealChecked.length > vm.pagination.total_item) {
          result = vm.pagination.total_item;
        } else {
          result = vm.dealChecked.length;
        }
      } else {
        result = vm.pagination.total_item;
      }
      return result;
    },
    dealSearch(event) {
      let vm = this;
      let keyCode = event.keyCode;
      let searchText = event.target.value ? event.target.value : '';
      searchText = searchText ? searchText.trim() : '';

      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }

      if (keyCode === 13) {
        vm.dealSearchSetQuery(searchText);
        vm.doneTyping();
      } else {
        vm.clearTimeoutSearch = setTimeout(() => {
          vm.dealSearchSetQuery(searchText);
          vm.doneTyping();
        }, SEARCH_TIMEOUT_DEFAULT);
      }
    },
    dealSearchForClick() {
      let vm = this;
      let searchText = vm.$refs.dealSearch.value ? vm.$refs.dealSearch.value : '';
      searchText = searchText ? searchText.trim() : '';
      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }
      vm.dealSearchSetQuery(searchText);
      vm.doneTyping();
    },
    dealSearchSetQuery(searchText) {
      let vm = this;
      vm.currentParams.page = 1;
      vm.currentParams.search = searchText;
      vm.searchText = searchText;
      vm.dealChecked = [];
      vm.dealCheckAllPerPage = [];
      vm.dealCheckedPerPage = [];
      vm.$refs.dealSearch.blur();
    },
    dealTyping() {
      let vm = this;
      clearTimeout(vm.typingTimer);
    },
    doneTyping() {
      let vm = this;
      if (vm.searchText && vm.searchText.length) {
        vm.pagination.current_page = 1;
        vm.currentParams.search = vm.searchText;
        vm.loadDataDeals = true;
        vm.getAll();
        vm.loadSearch = true;
      } else {
        if (vm.currentParams.search) {
          vm.pagination.current_page = 1;
          delete vm.currentParams.search;
        }
        if (!vm.searchText || (vm.searchText && vm.searchText.length == 0)) {
          vm.loadDataDeals = true;
          vm.getAll();
          vm.loadSearch = false;
        }
      }
    },
    addNewSuccess(isSuccess) {
      let vm = this;
      if (isSuccess) {
        vm.getAll();
      }
    },
    renderElementClass(value) {
      let elClass = '';

      if (value == 'Mới mở') {
        elClass = 'deal-status-open';
      }
      if (value == 'Hoàn thành') {
        elClass = 'deal-status-success';
      }
      if (value == 'Hủy bỏ') {
        elClass = 'deal-status-cancel';
      }
      return elClass;
    },

    getStyleFilter() {
      let vm = this;
      if (!vm.showFilter && vm.isAllData) {
        return '320px';
      } else if (!vm.showFilter && !vm.isAllData) {
        return '70px';
      } else {
        return '320px';
      }
    },

    filterShow(show) {
      let vm = this;
      vm.showFilter = show;
    },

    filterAllData(allData) {
      let vm = this;
      vm.isAllData = allData;
    },

    startFilter(params) {
      let vm = this;
      if (params.filter) {
        vm.pagination.current_page = 1;
        vm.currentParams.filter = params.filter;
      } else if (!params.filter && vm.currentParams.filter) {
        vm.pagination.current_page = 1;
        delete vm.currentParams.filter;
      }
      vm.loadDataDeals = true;
      vm.getAll();
    },

    cancelFilter(status) {
      let vm = this;
      if (status) {
        vm.pagination.current_page = 1;
        delete vm.currentParams.filter;
        vm.loadDataDeals = true;
        vm.getAll();
      }
    },

    showFilterGetId(filter) {
      let vm = this;
      if ($.isNumeric(filter.id)) {
        vm.getFilterAttributes(filter.id, filter.status);
      }
    },

    getFilterAttributes(filterId, status) {
      let vm = this;
      vm.$store.dispatch(GET_DEAL_FILTER_ATTRIBUTES, filterId).then(response => {
        let data = [];
        let cacheParams = [];
        let multiAttributes = [];
        let attributes = [];
        response.data.data.forEach(function (item) {
          vm.checkExistAttributeValueOptions(item);
          let temp = {
            id: item.id,
            filter_id: item.filter_id,
            attribute_id: item.external_attribute ? item.code_external_attribute : item.attribute_id,
            attribute_name: item.attribute_name,
            attribute_code: item.external_attribute ? item.code_external_attribute : item.attribute_code,
            data_type: item.data_type,
            attribute_values: item.attribute_values,
            external_attribute: item.external_attribute,
            code_external_attribute: item.code_external_attribute,
            attribute_options: item.attribute_options,
            show_position: item.show_position,
            is_auto_gen: vm.thenIsAutoGenerate(item),
            is_disabled: true,
            created_at: item.created_at,
            updated_at: item.updated_at
          };
          cacheParams.push(temp);
          if ((item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && (item.attribute_values.id == 6 || item.attribute_values.id == 7 || item.attribute_values.id != 6 && item.attribute_values.id != 7 && Array.isArray(item.attribute_values.attribute_value)) || (item.data_type == 6 || item.data_type == 7) && item.attribute_values.condition == '<>') {
            multiAttributes.push(item);
            attributes.push(temp);
          } else {
            let dataType = vm.getDataType(item);
            let value = {
              attribute_id: item.attribute_values.attribute_id,
              data_type: dataType,
              attribute_value: item.attribute_values.attribute_id == 'charge_person' && status == 0 ? [vm.currentUser.id] : item.attribute_values.attribute_value,
              condition: item.attribute_values.condition,
              dateRange: item.attribute_values.dateRange ? item.attribute_values.dateRange : "",
              errors: item.attribute_values.errors ? item.attribute_values.errors : "",
              id: item.attribute_values.id ? item.attribute_values.id : "",
              rangeDate: item.attribute_values.rangeDate ? item.attribute_values.rangeDate : "",
              type: item.attribute_values.type ? item.attribute_values.type : "",
              numberRange: item.attribute_values.numberRange ? item.attribute_values.numberRange : "",
              error: item.attribute_values.error ? item.attribute_values.error : "",
              filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
            }
            temp.attribute_values = value;
            attributes.push(temp);
            data.push(value);
          }
        })
        if (multiAttributes && multiAttributes.length > 0) {
          let multiParams = vm.getMultipleParams(multiAttributes);
          $.merge(data, multiParams);
        }
        vm.filterAttributes = attributes;
        // Set cache filter
        vm.allAttributes.forEach(function (item) {
          if (attributes && attributes.length > 0) {
            attributes.forEach(function (attribute_filter) {
              if (item.attribute_code == attribute_filter.attribute_code || item.attribute_code == attribute_filter.code_external_attribute) {
                attribute_filter.is_hidden = item.is_hidden;
              }
            });
          }
        });
        vm.cacheForFilterAttributes = JSON.stringify({
          data: attributes
        });
        vm.pagination.current_page = 1;
        vm.currentParams.filter = data;
        vm.loadDataDeals = true;
        vm.getAll();
      }).catch(error => {
        console.log(error);
      });
    },
    checkExistAttributeValueOptions(attribute_filter) {
      // Kiểm tra tồn tại attribute option
      let dataType = [8, 9, 10, 11, 13, 14];
      if (attribute_filter && attribute_filter.data_type && dataType.includes(attribute_filter.data_type)) {

        let count = 0;
        let attributeOptionsIds = [];
        let attributeOptionsObj = {};

        if (attribute_filter.attribute_values.attribute_value && attribute_filter.attribute_values.attribute_value.length > 0) {
          attribute_filter.attribute_options.forEach(function (attribute_option) {
            attributeOptionsIds.push(attribute_option.id);
          });

          attribute_filter.attribute_values.attribute_value.forEach(function (attribute_option_id) {
            if (attributeOptionsIds.includes(attribute_option_id) || attribute_option_id === "empty_value") {
              if (!attributeOptionsObj[attribute_option_id]) {
                attributeOptionsObj[attribute_option_id] = attribute_option_id;
              }
            } else {
              count++;
            }
          })
        }

        if (count > 0) {
          attribute_filter.attribute_values.is_attribute_value_option_not_exist = true;
        } else {
          attribute_filter.attribute_values.is_attribute_value_option_not_exist = false;
        }

        if (Object.keys(attributeOptionsObj).length > 0) {
          attribute_filter.attribute_values.attribute_value = Object.values(attributeOptionsObj).map(function (value) {
            if (value === "empty_value") {
              return value;
            }
            return parseInt(value);
          });
        } else {
          attribute_filter.attribute_values.attribute_value = '';
        }
      }
    },
    getMultipleParams(items) {
      let multiArgs = [];
      items.forEach(function (item) {
        if (Array.isArray(item.attribute_values.attribute_value)) {
          let temp_data = item.attribute_values.attribute_value;
          if (item.attribute_values.id != 7 && item.attribute_values.id != 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_data[0] != 'undefined' && typeof temp_data[1] != 'undefined') {
            if (temp_data[0]) {
              let temp_args_min = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: temp_data[0],
                condition: ">=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              };
              multiArgs.push(temp_args_min);
            }
            if (temp_data[1]) {
              let temp_args_max = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: temp_data[1],
                condition: "<=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              }
              multiArgs.push(temp_args_max);
            }
          }
        } else if (item.attribute_values.rangeDate && Array.isArray(item.attribute_values.rangeDate) && item.attribute_values.rangeDate.length > 0) {
          let temp_range_date = item.attribute_values.rangeDate;
          if (item.attribute_values.id == 7 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) && typeof temp_range_date[0] != 'undefined' && typeof temp_range_date[1] != 'undefined') {
            let dateMin = new Date(temp_range_date[0]);
            let min = dateMin.getDate() + '/' + (dateMin.getMonth() + 1) + '/' + dateMin.getFullYear();
            if (min != '1/1/1970') {
              let temp_args_min = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: min,
                condition: ">=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              };
              multiArgs.push(temp_args_min);
            }
            let dateMax = new Date(temp_range_date[1]);
            let max = dateMax.getDate() + '/' + (dateMax.getMonth() + 1) + '/' + dateMax.getFullYear();
            if (max != '1/1/1970') {
              let temp_args_max = {
                attribute_id: item.attribute_values.attribute_id,
                data_type: "2",
                attribute_code: item.attribute_code,
                attribute_value: max,
                condition: "<=",
                filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
              }
              multiArgs.push(temp_args_max);
            }
          }
        } else if (item.attribute_values.dateRange && Object.keys(item.attribute_values.dateRange).length > 0) {
          if (item.attribute_values.id == 6 && (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at'))) {
            let temp_args_max = {
              attribute_id: item.attribute_values.attribute_id,
              data_type: "2",
              attribute_code: item.attribute_code,
              attribute_value: item.attribute_values.dateRange,
              condition: "=",
              filter_date_type: item.attribute_values.filter_date_type ? item.attribute_values.filter_date_type : ''
            }
            multiArgs.push(temp_args_max);
          }
        } else if (typeof item.attribute_values.attribute_value == 'string' || typeof item.attribute_values.attribute_value == 'object') {
          if (item.data_type == 6 || item.data_type == 7) {
            let temp_data = item.attribute_values.numberRange;
            if (typeof temp_data.min != 'undefined' && typeof temp_data.max != 'undefined') {
              if (temp_data.min) {
                let temp_args_min = {
                  attribute_id: item.attribute_values.attribute_id,
                  data_type: "1",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data.min,
                  condition: ">="
                };
                multiArgs.push(temp_args_min);
              }
              if (temp_data.max) {
                let temp_args_max = {
                  attribute_id: item.attribute_values.attribute_id,
                  data_type: "1",
                  attribute_code: item.attribute_code,
                  attribute_value: temp_data.max,
                  condition: "<="
                }
                multiArgs.push(temp_args_max);
              }
            }
          }
        } else {
          return;
        }
      })
      return multiArgs;
    },

    getDataType(item) {
      if (item.data_type == 2 || item.data_type == 3 || item.external_attribute == 1 && (item.code_external_attribute == 'created_at' || item.code_external_attribute == 'updated_at')) {
        return "2";
      } else if (item.attribute_code == "total_paid_amount") {
        return "3";
      } else {
        return "1";
      }
    },

    thenIsAutoGenerate(item) {
      if ($.inArray(item.attribute_code, ["shipping_status", "product_price", "order_date", "order_code", "single_sale_price", "price_after", "product_code", "product_name", "product_quantity", "product_discount", "order_discount", "amount_must_be_paid", "transaction_status", "reason_for_cancellation", "base_total_transaction_amount", "vat_amount", "vat_type", "total_transaction_amount", "total_paid_amount", "order_payment_status", "customer_code", "customer_name", "user_id", "created_at", "updated_at"]) > -1) {
        return true;
      } else {
        return false;
      }
    },

    getFilters() {
      let vm = this;
      vm.$store.dispatch(GET_LIST_DEAL_FILTERS).then(() => {}).catch(error => {
        console.log(error);
      });
    },

    isResponseSuccess() {
      let vm = this;
      vm.getFilters();
    },

    getAllContactByCustomer(params) {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_CONTACT, params).then((response) => {
        if (response.data.data) {
          //vm.isCustomerContactReady = true;
          //console.log(response.data.data, '123');
          let contacts = response.data.data;
          contacts.forEach(function (contact) {
            vm.contacts[contact.id] = contact.full_name;
          });
          //console.log(vm.contacts);
        }
      }).catch((error) => {
        console.log(error)
      });
    },

    convertNumber(value) {
      let stringValue = String(value);
      if (stringValue && stringValue.length > 0) {
        stringValue = stringValue.replace(".", ",");
        let originalValue = stringValue.substring(0, stringValue.indexOf(","));
        let decimalValue = stringValue.substring(stringValue.indexOf(",") + 1, stringValue.length);
        if (originalValue && originalValue.length > 3) {
          originalValue = originalValue.replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, "$1.");
          stringValue = originalValue + "," + decimalValue;
        }
      }
      return stringValue;
    },

    exportDeal(type) {
      let vm = this;
      let ids = "";
      if (vm.dealChecked && vm.dealChecked.length > 0) {
        let listIds = vm.dealChecked.map(function (el) {
          return el.id;
        });
        ids = listIds.join();
      }
      let params = vm.currentParams;
      params.type = type;
      params.ids = ids;

      let convertFilter = params.filter;
      delete params.filter;

      params.filter = {};
      params.filter.attributes = convertFilter;

      delete params.page;

      vm.$store.dispatch(EXPORT_LIST_DEAL, params).then(response => {
        if (response.data) {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          let currentDate = new Date();
          let time = vm.helpers.formatDateTimeForClient(currentDate);
          link.href = url;

          if (type == 1) {
            link.setAttribute('download', "Danhsach_Giaodich_Rutgon_" + time + '.xlsx');
          } else if (type == 2) {
            link.setAttribute('download', "Danhsach_Giaodich_Cosanpham_" + time + '.xlsx');
          }

          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getClassForTableAttributes(attribute) {
      let classAttribute = attribute.attribute_code;
      if (attribute.data_type_id === 7) {
        classAttribute += " text-right";
      }
      return classAttribute;
    },

    callTooltipAfterLoadData() {
      $("body").tooltip({
        selector: '[data-toggle="m-tooltip"]',
        template: '<div class="m-tooltip tooltip nt-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
      });
    },
  }
}