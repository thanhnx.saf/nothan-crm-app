/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import InfoTabHead from "../../components/show-info-tab/ShowInfoTabHead.vue";
import {
  DEAL_GET_DETAIL,
  DEAL_PRINT_TO_EXCEL,
  DEAL_GET_ATTRIBUTES,
  DEAL_GET_USER_ATTRIBUTES
} from "@/types/actions.type";
import Note from "@/components/note/Note.vue"
import ModalAddNewPayment from "../../components/modals/tab-payment-new/TabAddNewPayment.vue";
import ModalViewPayment from "../../components/modals/tab-payment/TabViewPayment.vue";
import {
  PERMISSION_ADD_NOTE_DEAL
} from "@/types/const";
import {
  TOAST_SUCCESS
} from "@/types/config";

export default {
  components: {
    InfoTabHead: InfoTabHead,
    Note: Note,
    'add-new-payment': ModalAddNewPayment,
    'view-payment': ModalViewPayment,
  },

  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["detailDeal", "currentTeam", "attributeEditDeal", "userCheckAddOn", "userCurrentPermissions"]),
  },

  mounted() {
    let vm = this;
    vm.getShowDeal();
    vm.getDealAttributeByUser();
    $("#m_header").css("display", "block");
    if (!$("#app-body").hasClass("nt-none-padding-top")) {
      $("#app-body").removeClass("nt-none-padding-top");
    }
    if (!$("#app-body").hasClass("m-body")) {
      $("#app-body").addClass("m-body");
    }
    $("#app-body-subheader").css("display", "block");

    let AddNewDealSuccess = localStorage.getItem('AddNewDealSuccess')
    if (AddNewDealSuccess) {
      vm.$snotify.success('Thêm mới giao dịch thành công.', TOAST_SUCCESS)
      setTimeout(() => {
        localStorage.removeItem('AddNewDealSuccess')
      }, 1000)
    }

    let UpdateDealSuccess = localStorage.getItem('UpdateDealSuccess')
    if (UpdateDealSuccess) {
      vm.$snotify.success('Cập nhật giao dịch thành công.', TOAST_SUCCESS)
      setTimeout(() => {
        localStorage.removeItem('UpdateDealSuccess')
      }, 1000)
    }
  },

  created() {
    let vm = this;
    vm.$bus.$on('dealPrintToExcel', ($event) => {
      if ($event) {
        vm.printDetailDealToExcel();
      }
    });

    vm.$bus.$on('getCustomerDetailDeal', ($event) => {
      if ($event) {
        vm.customer = $event;
      }
    });
  },

  watch: {
    detailDeal: {
      handler(after) {
        if (after) {
          // Disabled btn edit
          let vm = this;
          vm.disabledBtnTransactionStatus(after);
        }
      },
      deep: true
    },
  },

  data() {
    return {
      PERMISSION_ADD_NOTE_DEAL: PERMISSION_ADD_NOTE_DEAL,
      isLoaded: false,
      dealId: parseInt(this.$route.params.deal_id),
      customer: {}
    }
  },

  methods: {
    disabledBtnTransactionStatus(data) {
      if (data.transactionStatusCurrent && (data.transactionStatusCurrent.option_value == "Hoàn thành" || data.transactionStatusCurrent.option_value == "Hủy bỏ")) {
        $(".btn-deal-edit").addClass('nt-button-top-disabled');
      } else {
        $(".btn-deal-edit").removeClass('nt-button-top-disabled');
      }
    },

    getShowDeal() {
      let vm = this;
      vm.isLoaded = true;
      vm.$store.dispatch(DEAL_GET_DETAIL, vm.dealId).then(response => {
        if (response.data.data) {
          vm.disabledBtnTransactionStatus(response.data.data);
          vm.isLoaded = false;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getDealAttributes() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(() => {
        vm.isDealSuccess = true;
      }).catch(error => {
        console.log(error);
      });
    },

    getDealAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_USER_ATTRIBUTES).then(() => {
        vm.getDealAttributes();
      }).catch(error => {
        console.log(error);
      });
    },

    printDetailDealToExcel() {
      let vm = this;
      $('.btn-deal-print').attr('disabled', 'disabled');
      $('.btn-deal-print').addClass('m-loader m-loader--secondary m-loader--lg');
      $('.btn-deal-print').css({
        'min-width': '106px',
        'min-height': '32px'
      });
      $('.btn-deal-print').html('');

      let customerAddress = helpers.getAttributeByAttrCodeAndVariableName(vm.customer, 'address', 'attribute_value') ? helpers.getAttributeByAttrCodeAndVariableName(vm.customer, 'address', 'attribute_value') : "";
      let dealCode = helpers.getAttributeByAttrCodeAndVariableName(vm.detailDeal, 'order_code', 'attribute_value');

      let params = {
        deal_id: vm.detailDeal.id,
        team_name: vm.currentTeam && vm.currentTeam.name ? vm.currentTeam.name : "",
        customer_address: customerAddress
      };

      vm.$store.dispatch(DEAL_PRINT_TO_EXCEL, params).then(response => {
        if (response.data && response.data.status_code != 422) {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', dealCode + '.xlsx');
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);

          $('.btn-deal-print').removeAttr('disabled');
          $('.btn-deal-print').removeClass('m-loader m-loader--secondary m-loader--lg');
          $('.btn-deal-print').html('<i class="la la-print"></i> In giao dịch');
        }
      });
    },

    updateSuccess(isSuccess) {
      let vm = this;
      if (isSuccess) {
        vm.$store.dispatch(DEAL_GET_DETAIL, vm.dealId);
      }
    },

    reloadPage() {
      return window.location.reload();
    }
  }
}