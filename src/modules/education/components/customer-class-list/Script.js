/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import {
  GET_LIST_CLASS_OF_CUSTOMER_DETAIL,
  EXPORT_LEARNING_SITUATION
} from "@/types/actions.type";
import Pagination from "@/components/pagination/Pagination.vue";
import LearningSituationCustomerDetail from "../customer-details/LearningSituationCustomerDetail";
import {
  PERMISSION_EXPORT_MEMBER_HISTORY
} from "@/types/const";

const studyTypes = {
  course: 1,
  others: 2
}
const classStatuses = {
  notOpenYet: 'Chưa diễn ra',
  opening: 'Đang diễn ra',
  opened: 'Đã diễn ra',
  closed: 'Đóng'
}

export default {
  components: {
    Pagination: Pagination,
    LearningSituationCustomerDetail: LearningSituationCustomerDetail,
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["currentTeam", "listUsers", "listClassOfCustomerDetail", "detailCustomer", "userCheckAddOn", "userCurrentPermissions"]),
  },
  data() {
    return {
      PERMISSION_EXPORT_MEMBER_HISTORY: PERMISSION_EXPORT_MEMBER_HISTORY,

      customerId: parseInt(this.$route.params.customer_id),
      isLoader: false,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {},
      student: null,
      isLoaderShowLearningSituation: false,
      isLoaderExportLearningSituation: false,
      classStatuses: classStatuses,
    }
  },
  mounted() {
    let vm = this;
    if (vm.customerId) {
      vm.getListClassOfCustomerDetail();
    }

    // if (vm.detailCustomer) {
    //   let customerCurrentType = helpers.getAttributeOption(helpers.getAttributeByAttrCode(vm.detailCustomer, 'customer_type'), helpers.getAttributeByAttrCodeAndVariableName(vm.detailCustomer, 'customer_type', 'attribute_value'), 'option_value');
    //   let customerTypes = ["Khách hàng chưa mua và chưa sử dụng", "Khách hàng đã mua", "Khách hàng chỉ mua"];
    //   if (customerTypes.includes(customerCurrentType)) {
    //     return window.location.href = "/404";
    //   }
    // }
  },
  watch: {
    // detailCustomer: {
    //   handler(after) {
    //     if (after) {
    //       let customerCurrentType = helpers.getAttributeOption(helpers.getAttributeByAttrCode(vm.detailCustomer, 'customer_type'), helpers.getAttributeByAttrCodeAndVariableName(vm.detailCustomer, 'customer_type', 'attribute_value'), 'option_value');
    //       let customerTypes = ["Khách hàng chưa mua và chưa sử dụng", "Khách hàng đã mua", "Khách hàng chỉ mua"];
    //       if (customerTypes.includes(customerCurrentType)) {
    //         return window.location.href = "/404";
    //       }
    //     }
    //   },
    //   deep: true
    // },
  },
  methods: {
    getListClassOfCustomerDetail() {
      let vm = this;
      vm.currentParams.member_id = vm.customerId;
      vm.isLoader = true;

      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      if (vm.currentParams.per_page) {
        delete vm.currentParams.per_page;
      }

      vm.$store.dispatch(GET_LIST_CLASS_OF_CUSTOMER_DETAIL, vm.currentParams).then(response => {
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;

        vm.isLoader = false;
      }).catch(error => {
        console.log(error);
      });
    },
    getPaginationListClassOfCustomerDetail() {
      let vm = this;
      vm.getListClassOfCustomerDetail();
      $('#table-responsive-data').animate({
        scrollLeft: 0
      }, 200);
    },
    getClassState(data) {
      const {
        study_type,
        count_lesson_success,
        total_lesson,
      } = data

      if (count_lesson_success) {
        if (study_type === studyTypes.course && count_lesson_success === total_lesson) {
          data.status = 3
          return {
            name: classStatuses.opened,
            class: 'nt-barge-dropdown-success'
          }
        }
        data.status = 2
        return {
          name: classStatuses.opening,
          class: 'nt-barge-dropdown-warning'
        }
      }
      data.status = 1
      return {
        name: classStatuses.notOpenYet,
        class: 'nt-barge-dropdown-primary'
      }
    },
    // getClassState(start_date, end_date) {
    //   let currentDate = new Date();

    //   let getDate = currentDate.getDate() > 9 ? currentDate.getDate() : "0" + currentDate.getDate();
    //   let getMonth = parseInt(currentDate.getMonth() + 1) > 9 ? parseInt(currentDate.getMonth() + 1) : "0" + parseInt(currentDate.getMonth() + 1);
    //   let getFullYear = currentDate.getFullYear();

    //   currentDate = moment(getFullYear + '-' + getMonth + '-' + getDate + ' 00:00:00', 'YYYY-MM-DD hh:mm:ss');

    //   let currentDateTimestamp = currentDate.valueOf();
    //   let startDateTimestamp = moment(start_date, 'YYYY-MM-DD hh:mm:ss').valueOf();
    //   let endDateTimestamp = moment(end_date, 'YYYY-MM-DD hh:mm:ss').valueOf();

    //   // 86399 23h59p59s
    //   let timestampOneDay = 86399;
    //   endDateTimestamp = endDateTimestamp + timestampOneDay;

    //   let item = {};

    //   if (currentDateTimestamp < startDateTimestamp) {
    //     item.name = "Chưa diễn ra";
    //     item.class = "nt-barge-dropdown-primary";
    //   } else if (currentDateTimestamp > endDateTimestamp) {
    //     item.name = "Đã diễn ra";
    //     item.class = "nt-barge-dropdown-success";
    //   } else if (startDateTimestamp <= currentDateTimestamp <= endDateTimestamp) {
    //     item.name = "Đang diễn ra";
    //     item.class = "nt-barge-dropdown-warning";
    //   }

    //   return item;
    // },
    showModalLearningSituationCustomerDetail(student) {
      let vm = this;

      student.member_id = vm.customerId;

      student.member_code = helpers.getAttributeByAttrCodeAndVariableName(vm.detailCustomer, 'sku', 'attribute_value');
      student.member_name = helpers.getAttributeByAttrCodeAndVariableName(vm.detailCustomer, 'full_name', 'attribute_value');


      vm.student = student;
      vm.isLoaderShowLearningSituation = true;

      setTimeout(function () {
        $('#learningSituationCustomerDetail').modal('show');
      }, 100);
    },
    isLoadingSuccess() {
      let vm = this;
      vm.isLoaderShowLearningSituation = false;
    },
    exportLearningSituation(classEducation) {
      let vm = this;
      vm.isLoaderExportLearningSituation = true;

      let params = {
        class_id: classEducation.class_id,
        member_id: vm.customerId,
        class_member_id: classEducation.class_member_id,
        study_type: classEducation.product_unit
      }

      vm.$store.dispatch(EXPORT_LEARNING_SITUATION, params).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.server_errors = response.data.errors;
          if (vm.server_errors.class_close) {
            $('#policyErrors').modal('show');
          }
        } else {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', 'Tinhhinh_Hoctap.xlsx');
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
        vm.isLoaderExportLearningSituation = false;
      });
    }

  }
}