/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from '@/utils/utils';
import {
  GET_LEARNING_OF_STUDENTS,
  EXPORT_LEARNING_SITUATION
} from "@/types/actions.type";
import Pagination from "@/components/pagination/Pagination.vue";
import {
  PERMISSION_EXPORT_MEMBER_HISTORY
} from "@/types/const";

export default {
  props: {
    student: {
      type: Object
    },
    is_loading: {
      type: Boolean,
      default: false
    }
  },

  components: {
    Pagination: Pagination
  },

  mounted() {
    let vm = this;
    if (vm.listLearningOfStudents) {
      vm.updatePagination(vm.listLearningOfStudents);
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["currentTeam", "classDetail", "listLearningOfStudents", "userCheckAddOn", "userCurrentPermissions"])
  },
  watch: {
    is_loading: {
      handler(after) {
        let vm = this;
        if (after) {
          vm.getLearningOfStudent();
        }
      },
      deep: true
    },

    listLearningOfStudents: {
      handler(after) {
        let vm = this;
        let tempLearnigns = [];
        if (after) {

          if (after.length < vm.student.total_lesson) {
            tempLearnigns = vm.listLearningOfStudents;
            for (let i = (tempLearnigns.length + 1); i <= vm.student.total_lesson; i++) {

              let startTime = "";
              if (vm.student.start_time) {
                startTime = vm.student.start_time;
                startTime = moment(startTime, 'YYYY-MM-DD hh:mm:ss').format();
              }

              let endTime = "";
              if (vm.student.end_time) {
                endTime = vm.student.end_time;
                endTime = moment(endTime, 'YYYY-MM-DD hh:mm:ss').format();
              }

              let tempData = {
                id: "",
                lesson: "Buổi " + i,
                start_time: startTime,
                end_time: endTime,
                total_lesson: vm.student.total_lesson,
                date: "",
                status: 1,
                lesson_id: "",
                member_id: "",
                is_back_soon: "",
                is_checkin_late: "",
                is_checkin: "",
                status_history: "",
                home_work: "",
                note_home_work: "",
                note: "",
                created_at: "",
                updated_at: "",
                checkin_at: "",
                checkin_by: "",
                back_soon_at: "",
                back_soon_by: "",
                checkin_late_at: "",
                checkin_late_by: "",
                check_home_work_at: "",
                check_home_work_by: ""
              };
              tempLearnigns.push(tempData);
            }
          } else {
            tempLearnigns = vm.listLearningOfStudents;
          }
        }
        vm.listTempLearnings = tempLearnigns;
      },
      deep: true
    },

    listTempLearnings: {
      handler(after) {
        let vm = this;
        if (after) {
          vm.updatePagination(after);
        }
      },
      deep: true
    }
  },
  data() {
    return {
      PERMISSION_EXPORT_MEMBER_HISTORY: PERMISSION_EXPORT_MEMBER_HISTORY,

      listLearnings: [],
      listTempLearnings: [],
      loadDataClass: false,
      pagination: {
        current_page: 1,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      productName: "",
      pageNumber: 1,
      perPage: 10,
      isLoader: false,

      isLoaderExportLearningSituation: false
    }
  },
  methods: {
    getLearningOfStudent() {
      let vm = this;
      let params = {
        member_id: vm.student.member_id,
        class_id: vm.student.class_id,
        class_member_id: vm.student.class_member_id,
        study_type: vm.student.product_unit
      };

      vm.$store.dispatch(GET_LEARNING_OF_STUDENTS, params).then(response => {
        let lessons = response.data.data;
        if (lessons) {
          vm.loadDataClass = true;
        }
      }).catch(error => {
        return error;
      })
    },
    getLessonState(date) {
      let currentDate = new Date();
      let item = {};

      if (!date || date == null || date == '') {
        item.name = "Chưa thực hiện";
        item.class = "nt-barge-dropdown-primary";
        return item;
      }

      let getDate = currentDate.getDate() > 9 ? currentDate.getDate() : "0" + currentDate.getDate();
      let getMonth = parseInt(currentDate.getMonth() + 1) > 9 ? parseInt(currentDate.getMonth() + 1) : "0" + parseInt(currentDate.getMonth() + 1);
      let getFullYear = currentDate.getFullYear();

      currentDate = moment(getFullYear + '-' + getMonth + '-' + getDate + ' 00:00:00', 'YYYY-MM-DD hh:mm:ss');

      let currentDateTimestamp = currentDate.valueOf();
      let dateTimestamp = moment(date, 'YYYY-MM-DD hh:mm:ss').valueOf();
      let endDateTimestamp = moment(getFullYear + '-' + getMonth + '-' + getDate + ' 00:00:00', 'YYYY-MM-DD hh:mm:ss').valueOf();

      if (currentDateTimestamp < dateTimestamp) {
        item.name = "Chưa thực hiện";
        item.class = "nt-barge-dropdown-primary";
      } else if (currentDateTimestamp > dateTimestamp) {
        item.name = "Đã thực hiện";
        item.class = "nt-barge-dropdown-success";
      } else if (dateTimestamp <= currentDateTimestamp <= endDateTimestamp) {
        item.name = "Đang thực hiện";
        item.class = "nt-barge-dropdown-warning";
      }

      return item;
    },
    reset() {
      let vm = this;
      vm.$emit("loading_success", true);
      vm.pagination = {
        current_page: 1,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      }
    },
    getValueTime(time, id) {
      let vm = this;
      if (vm.isInt(id)) {
        return vm.helpers.formatTimeInDB(time);
      } else {
        return vm.helpers.formatTime(time);
      }
    },

    isInt(x) {
      let y = parseInt(x, 10);
      return !isNaN(y) && x == y && x.toString() == y.toString();
    },

    updatePagination(students) {
      let vm = this;
      if (students && students.length > 0) {
        // vm.pagination.current_page = 1;
        vm.pagination.per_page = 10;
        vm.pagination.total_item = students.length;
        vm.pagination.total_pages = Math.ceil(students.length / vm.pagination.per_page);
      }

      vm.getPaginationLearningSituation();
    },

    getPaginationLearningSituation() {
      let vm = this;
      let start = (vm.pagination.current_page - 1) * vm.pagination.per_page;
      let end = start + vm.pagination.per_page;

      if (vm.listTempLearnings.length > vm.pagination.per_page) {
        vm.listLearnings = vm.listTempLearnings.slice(start, end);
      } else {
        vm.listLearnings = vm.listTempLearnings;
      }
    },

    exportLearningSituation() {
      let vm = this;
      vm.isLoaderExportLearningSituation = true;
      let params = {
        class_id: vm.student.class_id,
        member_id: vm.student.member_id,
        class_member_id: vm.student.class_member_id,
        study_type: vm.student.product_unit
      }
      vm.$store.dispatch(EXPORT_LEARNING_SITUATION, params).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.server_errors = response.data.errors;
          if (vm.server_errors.class_close) {
            $('#policyErrors').modal('show');
          }
        } else {
          const url = window.URL.createObjectURL(new Blob([response.data]));
          const link = document.createElement('a');
          link.href = url;
          link.setAttribute('download', 'Tinhhinh_Hoctap.xlsx');
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
        vm.isLoaderExportLearningSituation = false;
      });
    }
  }
}