/* eslint-disable no-undef */
import helpers from "@/utils/utils";
import { CUSTOMER_DETAIL_BOX_BUY, CUSTOMER_DETAIL_BOX_GENERAL, CUSTOMER_DETAIL_BOX_USE } from "@/types/const";

export const getters = {
  listClasses(state) {
    return state.list;
  },
  classDetail(state) {
    return state.class;
  },
  classListLesson(state) {
    return state.classListLesson;
  },
  classListStudents(state) {
    return state.classListStudents;
  },
  listClassByProductId(state) {
    return state.listClassByProductId;
  },
  listLearningOfStudents(state) {
    return state.listLearningOfStudents;
  },
  listClassOfCustomerDetail(state) {
    return state.listClassOfCustomerDetail;
  },
  students(state) {
    return state.students;
  },

  getStudentFilters(state) {
    return state.filters;
  },

  filterAttributesStudent(state) {
    return state.filter_attributes ? state.filter_attributes : [];
  },

  attributeStudentFullForFilter(state) {
    let attributeInfoGeneralNotAttribute = [
      {
        id: "user_id",
        attribute_code: "user_id",
        attribute_id: "user_id",
        attribute_name: "Tạo bởi",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "user_id",
        data_type: {
          id: "user_id"
        }
      },
      {
        id: "created_at",
        attribute_code: "created_at",
        attribute_id: "created_at",
        attribute_name: "Tạo lúc",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "created_at",
        data_type: {
          id: "created_at"
        }
      },
      {
        id: "updated_at",
        attribute_code: "updated_at",
        attribute_id: "updated_at",
        attribute_name: "Lần chỉnh sửa cuối",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "updated_at",
        data_type: {
          id: "updated_at"
        }
      }
    ];
    let fullAttributes = state.attributes;
    $.merge(fullAttributes, attributeInfoGeneralNotAttribute);
    let attributesForFilter = [];
    if (fullAttributes && fullAttributes.length > 0) {
      fullAttributes.forEach(function (item) {
        if ($.inArray(item.attribute_code, ["sku", "full_name", "level", "customer_type", "total_price_when_buyer", "total_product_when_enduser", "total_product_when_buyer", "total_payment_amount"]) > -1) {
          item.is_auto_gen = true;
        } else {
          item.is_auto_gen = false;
        }
        attributesForFilter.push(item);
      });
    }

    attributesForFilter.sort(function (a, b) {
      if (helpers.convertLanguages(a.attribute_name) < helpers.convertLanguages(b.attribute_name)) { return -1; }
      if (helpers.convertLanguages(a.attribute_name) > helpers.convertLanguages(b.attribute_name)) { return 1; }
      return 0;
    });
    return attributesForFilter;
  },

  attributeStudentFull(state) {
    let attributeInfoBasic = [];
    let attributeInfo = [];
    let attributeInfoGeneral = [];
    let attributeInfoGeneralNotAttribute = [
      {
        id: "user_id",
        attribute_code: "user_id",
        attribute_id: "user_id",
        attribute_name: "Tạo bởi",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "user_id",
        data_type: {
          id: "user_id"
        }
      },
      {
        id: "created_at",
        attribute_code: "created_at",
        attribute_id: "created_at",
        attribute_name: "Tạo lúc",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "created_at",
        data_type: {
          id: "created_at"
        }
      },
      {
        id: "updated_at",
        attribute_code: "updated_at",
        attribute_id: "updated_at",
        attribute_name: "Lần chỉnh sửa cuối",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "updated_at",
        data_type: {
          id: "updated_at"
        }
      }
    ];
    let educationNotAttribute = [
      {
        id: "username",
        attribute_code: "username",
        attribute_id: "username",
        attribute_name: "Tài khoản",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "username",
        data_type: {
          id: "username"
        }
      },
      {
        id: "e_status",
        attribute_code: "e_status",
        attribute_id: "e_status",
        attribute_name: "Trạng thái",
        is_hidden: 0,
        is_show: 1,
        external_attribute: 1,
        is_auto_gen: true,
        code_external_attribute: "e_status",
        data_type: {
          id: "e_status"
        }
      },

    ];
    let attributeInfoGeneralCustom = [];
    let attributeInfoBuy = [];
    let attributeInfoUse = [];
    let newAttributeExternal = [];
    if (state.attributes && state.attributes.length > 0) {
      state.attributes.forEach(function (item) {
        if ($.inArray(item.attribute_code, ["created_at", "user_id", "updated_at"]) > -1) {
          return;
        }
        if ($.inArray(item.attribute_code, ["sku", "full_name", "level", "customer_type", "total_price_when_buyer", "total_product_when_enduser", "total_product_when_buyer", "total_payment_amount"]) > -1) {
          item.is_auto_gen = true;
        } else {
          item.is_auto_gen = false;
        }
        if (item.show_position == CUSTOMER_DETAIL_BOX_GENERAL) {
          if (item.attribute_code == 'sku' || item.attribute_code == 'full_name') {
            attributeInfoBasic.push(item);
          } else if (item.attribute_code == 'phone' || item.attribute_code == 'email' || item.attribute_code == 'address' || item.attribute_code == 'customer_type' || item.attribute_code == 'gender' || item.attribute_code == 'total_price_when_buyer' || item.attribute_code == 'total_product_when_buyer') {
            attributeInfo.push(item);
          } else {
            if (item.attribute_code == 'total_product_when_enduser' || item.attribute_code == 'total_payment_amount') {
              newAttributeExternal.push(item);
            } else if (item.attribute_code == 'level' || item.attribute_code == 'customer_group' || item.attribute_code == 'customer_resources' || item.attribute_code == 'cmnd_mst' || item.attribute_code == 'birthday_set_to_date' || item.attribute_code == 'facebook' || item.attribute_code == 'note') {
              attributeInfoGeneral.push(item);
            } else {
              attributeInfoGeneralCustom.push(item);
            }
          }
        } else if (item.show_position == CUSTOMER_DETAIL_BOX_BUY) {
          attributeInfoBuy.push(item);
        } else if (item.show_position == CUSTOMER_DETAIL_BOX_USE) {
          attributeInfoUse.push(item);
        } else if (item.external_attribute) {
          newAttributeExternal.push(item);
        }
      });
    }
    return attributeInfoBasic.concat(educationNotAttribute, attributeInfo, newAttributeExternal, attributeInfoGeneral, attributeInfoGeneralNotAttribute, attributeInfoGeneralCustom, attributeInfoBuy, attributeInfoUse);
  },
}
