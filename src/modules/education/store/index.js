import { actions } from './actions';
import { getters } from './getters';
import { mutations } from './mutations';
import { state } from './state';

export const educationStore = {
  actions: actions,
  getters: getters,
  mutations: mutations,
  state: state
}
