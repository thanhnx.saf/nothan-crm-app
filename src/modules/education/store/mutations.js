import {
  SET_EDUCATION,
  SET_CLASS_DETAIL,
  SET_CLASS_LIST_LESSON_GET_ALL,
  SET_CLASS_LIST_STUDENT_GET_ALL,
  SET_CLASS_LIST_BY_PRODUCT_ID,
  SET_LEARNING_OF_STUDENTS,
  SET_LIST_CLASS_OF_CUSTOMER_DETAIL,
  SET_ALL_STUDENT_EDUCATION,
  SET_STUDENT_FILTERS,
  SET_STUDENT_FILTERS_CREATE,
  SET_STUDENT_FILTERS_UPDATE,
  SET_STUDENT_FILTERS_DELETE,
  SET_STUDENT_FILTER_ATTRIBUTES,
  SET_STUDENT_ATTRIBUTES, SET_ACCOUNT_EDU_ONLINE_STATUS
} from "@/types/mutations.type";

export const mutations = {
  [SET_EDUCATION](state, classes) {
    state.list = classes;
  },

  [SET_CLASS_DETAIL](state, classObj) {
    state.class = classObj;
  },

  [SET_CLASS_DETAIL](state, classObj) {
    state.class = classObj;
  },

  [SET_CLASS_LIST_LESSON_GET_ALL](state, classListLesson) {
    state.classListLesson = classListLesson;
  },

  [SET_CLASS_LIST_STUDENT_GET_ALL](state, students) {
    state.classListStudents = students;
  },

  [SET_CLASS_LIST_BY_PRODUCT_ID](state, classes) {
    state.listClassByProductId = classes;
  },

  [SET_LEARNING_OF_STUDENTS](state, learnings) {
    state.listLearningOfStudents = learnings;
  },

  [SET_LIST_CLASS_OF_CUSTOMER_DETAIL](state, listClass) {
    state.listClassOfCustomerDetail = listClass;
  },

  [SET_ALL_STUDENT_EDUCATION](state, students) {
    state.students = students;
  },

  [SET_STUDENT_FILTERS](state, filters) {
    state.filters = filters;
  },

  [SET_STUDENT_FILTER_ATTRIBUTES](state, filter_attributes) {
    state.filter_attributes = filter_attributes;
  },

  [SET_STUDENT_FILTERS_UPDATE](state, filter) {
    let newFilters = [];
    state.filters.forEach(function (item) {
      if (item.id === filter.id) {
        item = filter;
      }
      newFilters.push(item);
    })
    state.filters = newFilters;
  },

  [SET_STUDENT_FILTERS_CREATE](state, filter) {
    if (state.filters.length > 0) {
      state.filters.push(filter);
    }
  },

  [SET_STUDENT_FILTERS_DELETE](state, filterId) {
    state.filters.forEach(function (item, index) {
      if (item.id === filterId) {
        state.filters.splice(index, 1);
      }
    })
  },

  [SET_ACCOUNT_EDU_ONLINE_STATUS](state, response) {
    state.accountEduOnline = response;
  },

  [SET_STUDENT_ATTRIBUTES](state, attributes) {
    let countBase = 0;
    let countAttrDefault = state.user_attributes.length > 0 ? state.user_attributes.filter(() => state.user_attributes.is_default_attribute == 1).length : 0;
    let userId = state.user_attributes.length > 0 ? state.user_attributes[0].user_id : 0;

    if (state.user_attributes.length > 0) {
      let result = [];
      let listAttrInUser = [];
      state.user_attributes.forEach(function (uAttr) {
        attributes.forEach(function (attr) {
          if (uAttr.attribute_code == attr.attribute_code) {
            attr.is_show = uAttr.is_show;
            attr.sort_order = uAttr.sort_order;
            attr.user_id = uAttr.user_id;
            attr.external_attribute = uAttr.external_attribute;
            attr.code_external_attribute = uAttr.code_external_attribute;
            result.push(attr);
            listAttrInUser.push(attr.id);
          }
        })
      });
    }

    attributes.forEach(function (item) {
      if (item.attribute_id == "charge_person") {
        countBase++;
      }
    })
    if (countBase > 0) {
      let countCharge = 0;
      attributes.forEach(function (item) {
        if (item.attribute_id == "charge_person") {
          countCharge++;
        }
      })
      if (countCharge <= 0) {
        let temp_data_charge = {
          id: "charge_person",
          attribute_code: "charge_person",
          attribute_id: "charge_person",
          attribute_name: "Người phụ trách",
          external_attribute: 1,
          code_external_attribute: "charge_person",
          is_show: 1,
          is_hidden: 0,
          sort_order: countAttrDefault + 1,
          user_id: userId,
          data_type: {
            id: "charge_person"
          }
        };
        state.user_attributes.push(temp_data_charge);
        attributes.push(temp_data_charge);
      }
    } else {
      let temp_data_charge = {
        id: "charge_person",
        attribute_code: "charge_person",
        attribute_id: "charge_person",
        attribute_name: "Người phụ trách",
        is_show: 1,
        is_hidden: 0,
        external_attribute: 1,
        code_external_attribute: "charge_person",
        sort_order: countAttrDefault + 1,
        user_id: userId,
        data_type: {
          id: "charge_person"
        }
      };
      state.user_attributes.push(temp_data_charge);
      attributes.push(temp_data_charge);

      // let temp_data_type = {
      //     id: "type_customer",
      //     attribute_code: "type_customer",
      //     attribute_id: "type_customer",
      //     attribute_name: "Loại khách hàng",
      //     external_attribute : 1,
      //     code_external_attribute : "type_customer",
      //     is_show: 1,
      //     sort_order: countAttrDefault + 2,
      //     user_id: userId,
      //     data_type: {
      //         id: "type_customer"
      //     }
      // };
      // state.user_attributes.push(temp_data_type);
      // attributes.push(temp_data_type);
    }
    state.attributes = attributes;

    /*if (state.user_attributes.length > 0) {
        let result = [];
        let listAttrInUser = [];
        state.user_attributes.forEach(function (uAttr) {
            attributes.forEach(function (attr) {
                if (uAttr.attribute_code == attr.attribute_code) {
                    attr.is_show = uAttr.is_show;
                    attr.sort_order = uAttr.sort_order;
                    attr.user_id = uAttr.user_id;
                    attr.external_attribute = uAttr.external_attribute;
                    attr.code_external_attribute = uAttr.code_external_attribute;
                    if(attr.attribute_options && attr.attribute_options.length > 0){
                        attr.is_multiple_value = 1;
                    }else{
                        attr.is_multiple_value = 0;
                    }
                    result.push(attr);
                    listAttrInUser.push(attr.id);
                }
            })
        });
        let countAttr = state.user_attributes.length;
        let userId = state.user_attributes[0].user_id;
        if (listAttrInUser.length !== attributes.length) {
            let attributeOuts = [];
            attributes.forEach(function (attr) {
                if (listAttrInUser.indexOf(attr.id) === -1) {
                    attributeOuts.push(attr);
                }
            })
            attributeOuts.forEach(function (attr) {
                countAttr++;
                let temp_data = {
                    attribute_code: attr.attribute_code,
                    attribute_id: attr.id,
                    attribute_name: attr.attribute_name,
                    is_show: 0,
                    sort_order: countAttr,
                    user_id: userId
                };
                state.user_attributes.push(temp_data);
                attr.is_show = 0;
                attr.sort_order = countAttr;
                attr.user_id = userId;
                result.push(attr);
            })
        }
        let countBase = 0;
        result.forEach(function (item) {
            if (item.attribute_id == "charge_person" || item.attribute_id == "type_customer") {
                countBase++;
            }
        })
        if (countBase > 0) {
            let countCharge = 0;
            let countType = 0;
            result.forEach(function (item) {
                if (item.attribute_id == "charge_person") {
                    countCharge++;
                }
                if (item.attribute_id == "type_customer") {
                    countType++;
                }
            })
            if (countCharge <= 0) {
                let temp_data_charge = {
                    id: "charge_person",
                    attribute_code: "charge_person",
                    attribute_id: "charge_person",
                    attribute_name: "Người phụ trách",
                    external_attribute : 1,
                    code_external_attribute : "charge_person",
                    is_show: 1,
                    sort_order: countAttr + 1,
                    user_id: userId,
                    data_type: {
                        id: "charge_person"
                    }
                };
                state.user_attributes.push(temp_data_charge);
                result.push(temp_data_charge);
            }
            if (countType <= 0) {
                let temp_data_type = {
                    id: "type_customer",
                    attribute_code: "type_customer",
                    attribute_id: "type_customer",
                    attribute_name: "Loại khách hàng",
                    external_attribute : 1,
                    code_external_attribute : "type_customer",
                    is_show: 1,
                    sort_order: countAttr + 2,
                    user_id: userId,
                    data_type: {
                        id: "type_customer"
                    }
                };
                state.user_attributes.push(temp_data_type);
                result.push(temp_data_type);
            }
        } else {
            let temp_data_charge = {
                id: "charge_person",
                attribute_code: "charge_person",
                attribute_id: "charge_person",
                attribute_name: "Người phụ trách",
                is_show: 1,
                external_attribute : 1,
                code_external_attribute : "charge_person",
                sort_order: countAttr + 1,
                user_id: userId,
                data_type: {
                    id: "charge_person"
                }
            };
            state.user_attributes.push(temp_data_charge);
            result.push(temp_data_charge);

            let temp_data_type = {
                id: "type_customer",
                attribute_code: "type_customer",
                attribute_id: "type_customer",
                attribute_name: "Loại khách hàng",
                external_attribute : 1,
                code_external_attribute : "type_customer",
                is_show: 1,
                sort_order: countAttr + 2,
                user_id: userId,
                data_type: {
                    id: "type_customer"
                }
            };
            state.user_attributes.push(temp_data_type);
            result.push(temp_data_type);
        }
        state.attributes = result;
    }*/
  },
}
