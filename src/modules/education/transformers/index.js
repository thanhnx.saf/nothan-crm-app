import listLearningOfStudentsVM from './listLearningOfStudents';
import listClassOfCustomerDetailVM from './listClassOfCustomerDetail';

export const EducationVM = {
    listLearningOfStudentsVM,
    listClassOfCustomerDetailVM
}
