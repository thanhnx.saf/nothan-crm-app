import moment from 'moment';

export default {
    transform(data) {
        let vm = this;
        let responseData = [];

        data.forEach(function(item){
            responseData.push(vm.transformData(item));
        });

        return responseData;
    },

    transformData(data){
        let transform = {};

        let startTime = "";
        let endTime = "";
        let startDate = "";
        let endDate = "";

        if(data.start_time){
            startTime = "1970-01-01 " + data.start_time;
            startTime = moment(startTime, 'YYYY-MM-DD hh:mm:ss').format();
        }

        if(data.end_time){
            endTime = data.end_time;
            endTime = "1970-01-01 " + data.end_time;
            endTime = moment(endTime, 'YYYY-MM-DD hh:mm:ss').format();
        }

        if(data.start_date){
            startDate = data.start_date;
            startDate = moment(startDate, 'YYYY-MM-DD hh:mm:ss').format();
        }

        if(data.end_date){
            endDate = data.end_date;
            endDate = moment(endDate, 'YYYY-MM-DD hh:mm:ss').format();
        }

        transform = {
            id: data.id,
            class_id: data.id,
            name: data.name,
            code: data.code,
            product_id: data.product_id,
            product_name: data.product_name,
            status: data.status,
            class_member_status: data.class_member_status,
            count_lesson_success: data.count_lesson_success ? data.count_lesson_success : 0,
            start_date: startDate,
            end_date: endDate,
            start_time: startTime,
            end_time: endTime,
            address: data.address,
            deal_code: data.deal_code,
            deal_id: data.deal_id,
            is_join: data.class_member_is_join,
            total_lesson: data.total_lesson,
            count_back_soon: data.count_back_soon ? data.count_back_soon : 0,
            count_checkin: data.count_checkin ? data.count_checkin : 0,
            count_checkin_late: data.count_checkin_late ? data.count_checkin_late : 0,
            count_homework: data.count_homework ? data.count_homework : 0, 
            teachers: data.teachers ? data.teachers : [],
            comment: data.comment ? data.comment : "",
            course_name: data.course_name,
            product_unit: data.product_unit,
            study_type: data.study_type,
            class_member_id: data.class_member_id,
            deleted_at: data.deleted_at
        };

        return transform;
    }
}
