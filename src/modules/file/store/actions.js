import FileAPI from "@/services/file";
import { FileVM } from "../transformers/index";
import {
  FILE_CREATE_AVATAR,
  FILE_UPLOAD_NOTE,
  FILE_DOWNLOAD,
  FILE_DETAIL,
  FILE_CREATE,
  FILE_CREATE_MULTIPLE,
  FILE_BY_OBJECT
} from "@/types/actions.type";

export const actions = {
  getAll({ commit }, params) {
    return new Promise((resolve, reject) => {
      FileAPI.getAll(params).then(response => {
        let fileData = response.data.data;

        /*fileData.forEach(function (file) {
            let file_type_id = file.file_type_id;
            
            FileAPI.showTypes(file_type_id).then(response => {
                let fileTypeData = response.data.data;
                let typeObj =  FileVM.TypeVM.transformTypes(fileTypeData);

                file.file_type = typeObj;
            }).catch(error => {
                console.log(error);
            });
        });

        setTimeout(function() {
            commit('getAll', FileVM.ListVM.transform(fileData));
        }, 2000);*/

        commit('getAll', FileVM.ListVM.transform(fileData));

        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  show({ commit }, id) {
    return new Promise((resolve, reject) => {
      FileAPI.show(id).then(response => {
        commit('show', FileVM.ShowVM.transform(response.data.data));
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  download({ commit }, id) {
    return new Promise((resolve, reject) => {
      FileAPI.download(id).then(response => {
        commit('show', FileVM.ShowVM.transform(response.data.data));
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  create({ commit }, params) {
    return new Promise((resolve, reject) => {
      FileAPI.create(params).then(response => {
        if (response.data.data) {
          commit('create', response.data.data);
        }
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },
  unlink({ commit }, params) {
    return new Promise((resolve, reject) => {
      FileAPI.unlink(params).then(response => {
        if (response.data.data) {
          commit('unlink', params);
        }
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },
  delete(file_id) {
    return new Promise((resolve, reject) => {
      FileAPI.delete(file_id).then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },
  multipleDelete(file_ids) {
    return new Promise((resolve, reject) => {
      FileAPI.multipleDelete(file_ids).then(response => {
        resolve(response);
      }).catch(error => {
        reject(error);
      })
    });
  },
  showTypes({ commit }, type_id) {
    return new Promise((resolve) => {
      FileAPI.showTypes(type_id).then(response => {
        commit('showTypes', FileVM.TypeVM.transformTypes(response.data.data));
        resolve(response);
      }).catch(error => {
        console.log(error);
      });
    });
  },
  getAttributesByUser({ commit }, user_id) {
    return new Promise((resolve, reject) => {
      FileAPI.getAttributesByUser(user_id).then(response => {
        commit('getAttributesByUser', response.data.data);
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  pushFileIsChosen({ commit }, files) {
    if (Object.keys(files).length || files.length > 0) {
      commit('pushFileIsChosen', files);
    }
  },
  getFileIsChosen({ commit }) {
    commit('getFileIsChosen');
  },

  async [FILE_CREATE_AVATAR](context, data) {
    try {
      let createdAvatar = await FileAPI.create(data);

      return createdAvatar;
    } catch (e) {
      return e;
    }
  },

  async [FILE_CREATE](context, data) {
    try {
      let fileCreated = await FileAPI.create(data);

      return fileCreated;
    } catch (e) {
      return e;
    }
  },

  async [FILE_CREATE_MULTIPLE](context, data) {
    try {
      let fileMultipleCreated = await FileAPI.createMultiple(data);

      return fileMultipleCreated;
    } catch (e) {
      return e;
    }
  },

  async [FILE_UPLOAD_NOTE](context, params) {
    try {
      let filesUploadNote = await FileAPI.create(params);
      return filesUploadNote;
    } catch (e) {
      return e;
    }
  },

  async [FILE_DOWNLOAD](context, id) {
    try {
      let fileDownload = await FileAPI.download(id);
      return fileDownload;
    } catch (e) {
      return e;
    }
  },

  async [FILE_DETAIL](context, id) {
    try {
      let fileDetail = await FileAPI.show(id);
      return fileDetail;
    } catch (e) {
      return e;
    }
  },

  async [FILE_BY_OBJECT](context, params) {
    try {
      let fileByObject = await FileAPI.byObject(params);
      return fileByObject;
    } catch (e) {
      return e;
    }
  },
}
