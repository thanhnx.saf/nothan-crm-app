export const mutations = {
  getAll(state, files) {
    state.list = files;
  },
  show(state, file) {
    state.file = file;
  },
  showTypes() {

  },
  getAttributesByUser(state, attributes) {
    state.attributesByUser = attributes;
  },
  create(state, file) {
    state.list.push(file);
  },
  unlink(state, params) {
    state.list.filter(function (item) {
      if (item.id == params.id) {
        item.customer_id = 0;
      }
    });
    // state.list.push(file);
  },
  delete() {

  },
  multipleDelete() {

  },
  getAllCustomer() {

  },
  getCustomer() {

  },
  pushFileIsChosen(state, files_is_checked) {
    let keys = Object.keys(files_is_checked);
    keys.forEach(function (key) {
      state.files_is_checked[key] = files_is_checked[key];
    })
  },
  getFileIsChosen(state) {
    return state.files_is_checked;
  }
}
