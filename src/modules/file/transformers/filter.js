export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformFilters(item));
    });

    return new_data;
  },

  transformFilters(data) {
    let transform = {};

    transform = {
      id: data.id,
      name: data.name,
      team_id: data.team_id,
      filter_set_id: data.filter_set_id,
      status: data.status,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
    };

    return transform;
  }
}
