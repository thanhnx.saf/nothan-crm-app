export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformFolders(item));
    });

    return new_data;
  },

  transformFolders(data) {
    let transform = {};

    transform = {
      id: data.id,
      user_id: data.user_id,
      team_id: data.team_id,
      name: data.name,
      description: data.description,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
    };

    return transform;
  }
}
