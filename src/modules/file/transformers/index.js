import ListVM from './list';
import FolderVM from './folder';
import ShowVM from './show';
import TypeVM from './type';
import FilterVM from './filter';

export const FileVM = {
  ShowVM,
  ListVM,
  FolderVM,
  TypeVM,
  FilterVM
}
