export default {
  transform(data) {
    let transform = {};
    transform = {
      id: data.id,
      is_publish: data.is_publish,
      file_type_id: data.file_type_id,
      team_id: data.team_id,
      user_id: data.user_id,
      customer_id: data.customer_id,
      file_folder_id: data.file_folder_id,
      original_name: data.original_name,
      size: data.size,
      type: data.type,
      is_image: data.is_image,
      name: data.name,
      path_on_server: data.path_on_server,
      title: data.title,
      description: data.description,
      download: data.download,
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
    };

    return transform;
  }
}
