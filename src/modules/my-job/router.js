import MyJob from './views/list/List.vue'

export default [{
  name: "MyJobList",
  path: "/my-job",
  component: MyJob,
  meta: {
    breadcrumb: [{
      name: "Công việc của tôi",
    }, {
      name: "Danh sách công việc của tôi",
    }],
  }
}]