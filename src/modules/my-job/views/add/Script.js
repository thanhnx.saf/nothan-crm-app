/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex"

import TaskService from '@/services/task'
import helpers from "@/utils/utils"
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config"

const customMessagesAddNewMyJob = {
  custom: {
    'title': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'desc': {
      max: "Giới hạn 1025 ký tự."
    },
    'deadline_date': {
      required: 'Không được để trống trường này.'
    }
  }
}

export default {
  props: {
    category: {
      type: Object,
      default: () => {},
    },
  },
  mounted() {
    this.getDataAddNewMyJob()
    this.$validator.localize('en', customMessagesAddNewMyJob);
  },
  computed: {
    ...mapGetters(['currentUser']),
    helpers() {
      return helpers
    },
    isDisabled() {
      if (this.errors.any() || this.isLoader) {
        return true
      }
      return false
    }
  },
  data() {
    return {
      addNewMyJob: {},
      server_errors: '',
      isLoader: false,
    }
  },
  methods: {
    getDataAddNewMyJob() {
      this.addNewMyJob = {
        title: '',
        desc: '',
        deadlineDate: new Date(),
        deadlineTime: new Date(),
      }
    },

    reset() {
      this.$validator.reset()
      this.server_errors = ''
    },

    cancelCreate() {
      this.getDataAddNewMyJob()
      this.isLoader = false
      this.reset()
    },

    create(scope) {
      this.isLoader = true
      this.$validator.validateAll(scope).then(() => {
        if (this.errors.any()) {
          this.isLoader = false
          return
        }

        let deadlineDate = this.addNewMyJob.deadlineDate ? moment(this.addNewMyJob.deadlineDate).format('YYYY-MM-DD') : ''
        let deadlineTime = this.addNewMyJob.deadlineTime ? moment(this.addNewMyJob.deadlineTime).format('H:mm:s') : '00:00:00'

        let addNewMyJob = {
          ...this.addNewMyJob,
          deadline: deadlineDate ? deadlineDate + ' ' + deadlineTime : '',
          assigner_id: this.currentUser.id,
          category_id: this.category.id
        }
        
        TaskService.createTask(addNewMyJob).then(res => {
          this.isLoader = false
          if (res.status === 422) {
            this.server_errors = res.data.errors
            this.$snotify.error('Thêm mới công việc thất bại.', TOAST_ERROR)
            return
          }
          res.data.categoryId = this.category.id
          this.$emit('onAddTask', res.data)
          this.$snotify.success('Thêm mới công việc thành công.', TOAST_SUCCESS)
          this.getDataAddNewMyJob()
          this.reset()
          $('#addNewMyJob').modal('hide')
        })
      });
    },

    changeDataInputType(item) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[item]) {
          vm.server_errors[item] = ''
        }
      }, 500)
    }
  }
}