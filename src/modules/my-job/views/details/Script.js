/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";
import {} from "@/types/actions.type"
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config"
import TaskService from '@/services/task'
import helpers from '@/utils/utils'

const customMessagesUpdateMyJob = {
  custom: {
    'title': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'desc': {
      max: "Giới hạn 1025 ký tự."
    },
    'deadline_date': {
      required: 'Không được để trống trường này.'
    }
  }
}

export default {
  computed: {
    ...mapGetters(['currentTeam']),
    helpers() {
      return helpers
    },
    isDisabled() {
      if (this.errors.any() || this.isLoader) {
        return true
      }
      return false
    },
    isCountChange() {
      let count = 0

      if (this.details.title !== this.details.title_cached) {
        count++
      }

      if (this.details.desc !== this.details.desc_cached) {
        count++
      }

      let deadlineDate = this.helpers.formatDateTime(this.details.deadline_date)
      let deadlineDateCached = this.helpers.formatDateTime(this.details.deadline_date_cached)
      let deadlineTime = this.helpers.formatTime(this.details.deadline_time)
      let deadlineTimeCached = this.helpers.formatTime(this.details.deadline_time_cached)
      if (deadlineDate !== deadlineDateCached || deadlineTime !== deadlineTimeCached) {
        count++
      }
      return count
    }
  },

  mounted() {
    this.$validator.localize('en', customMessagesUpdateMyJob)
  },

  created() {
    this.$bus.$on('showMyJobDetails', (details) => {
      this.details = details
      $('#myJobDetails').modal('show')
    })

    this.$bus.$on('showMyJobUpdate', (details) => {
      this.details = details
      this.editDetails = {
        title: true,
        desc: true,
        deadline_date: true,
        deadline_time: true
      }
      $('#myJobDetails').modal('show')
    })
  },

  watch: {
    server_errors: {
      handler(after) {
        for (var error in after) {
          this.editDetails[error] = true
        }
        this.$forceUpdate()
      },
      deep: true
    },

    errors: {
      handler(after) {
        if (after.items.length) {
          after.items.forEach((item) => {
            this.editDetails['deadline_time'] = false
            this.editDetails[item.field] = true

            if (item.field === 'deadline_date') {
              this.editDetails['deadline_time'] = true
            }
          })
          this.$forceUpdate()
        }
      },
      deep: true
    }
  },

  data() {
    return {
      details: {},
      isLoader: false,
      editDetails: {},
      scope: 'updateMyJob',
      server_errors: ''
    }
  },

  methods: {
    isShowExpire(deadline) {
      if (!deadline) {
        return true
      }
      const current = moment().format('YYYY-MM-DD H:mm:s')
      return moment(deadline, 'YYYY-MM-DD H:mm:s').isAfter(current)
    },

    onDeleteTask(task) {
      TaskService.deleteTask(task.id)
      this.$snotify.success('Xóa công việc thành công.', TOAST_SUCCESS)
      $('#myJobDetails').modal('hide')
      this.$emit('onDeleteTaskSuccess', task)
    },

    showUpdate(attribute) {
      let vm = this
      vm.editDetails[attribute] = true
      vm.$forceUpdate()
      setTimeout(() => {
        $('#' + attribute).focus()
      }, 100)
    },

    hideUpdate(attribute) {
      let vm = this
      let isError = vm.checkValidateAttribute(attribute)
      if (!isError) {
        vm.editDetails[attribute] = false
        vm.$forceUpdate()
      }
    },

    checkValidateAttribute(attribute) {
      this.$validator.validateAll(this.scope).then(() => {
        if (this.errors.items.length) {
          this.errors.items.forEach((item) => {
            if (item.field === attribute) {
              return true
            }
          })
        }
      })
      if (this.server_errors[attribute]) {
        return true
      }
      return false
    },

    changeDataInputType(attribute, event) {
      let vm = this;
      let keyCode = event && event.keyCode ? event.keyCode : ''

      if (keyCode === 13) {
        let isError = vm.checkValidateAttribute(attribute)
        if (!isError) {
          vm.editDetails[attribute] = false
          vm.$forceUpdate()
          vm.update()
        }
      }

      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute]) {
          vm.server_errors[attribute] = ''
        }
      }, 500)
    },

    cancelUpdate() {
      this.editDetails = {}
      this.server_errors = ''
      this.$validator.reset()
      $('#myJobDetails').modal('hide')
      this.$emit('cancelOnUpdateTask', this.details)
    },

    update() {
      this.isLoader = true
      this.$validator.validateAll(this.scope).then(() => {
        if (this.errors.any()) {
          this.isLoader = false
          return
        }

        let details = this.details
        let deadlineDate = this.details.deadline_date ? moment(this.details.deadline_date, 'YYYY-MM-DD H:mm:s').format('YYYY-MM-DD') : ''
        let deadlineTime = this.details.deadline_time ? moment(this.details.deadline_time, 'YYYY-MM-DD H:mm:s').format('H:mm:s') : '00:00:00'
        details.deadline = deadlineDate ? deadlineDate + ' ' + deadlineTime : ''

        TaskService.updateTask(details, details.id).then(res => {
          this.isLoader = false

          if (res.status === 422) {
            this.server_errors = res.data.errors
            this.$snotify.error('Cập nhật công việc thất bại.', TOAST_ERROR)
            return
          }

          this.$snotify.success('Cập nhật công việc thành công.', TOAST_SUCCESS)
          this.editDetails = {}
          this.server_errors = ''
          this.$validator.reset()
          $('#myJobDetails').modal('hide')
          this.$emit('onUpdateTask', details)
        })
      })
    }
  }
}