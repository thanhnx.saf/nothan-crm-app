/* eslint-disable no-undef */
import draggable from 'vuedraggable'

import Details from '../details/Details.vue'
import AddNew from '../add/Add.vue'
import TaskService from '@/services/task'
import Loading from '@/components/loading/LoadingTable.vue'
import NoResult from '@/components/no-results/NoResults.vue'

const categoriesNames = {
  todo: 'Chưa thực hiện',
  doing: 'Đang thực hiện',
  done: 'Đã hoàn thành'
}

import {
  TOAST_SUCCESS
} from "@/types/config"

export default {
  components: {
    draggable,
    Details,
    AddNew,
    Loading,
    NoResult,
  },
  data() {
    return {
      settings: {},
      categories: [],
      isLoading: false,
    }
  },
  computed: {
    todoCategory() {
      if (!this.categories.length) {
        return
      }
      return this.categories.filter(t => t.name === categoriesNames.todo)[0]
    },
  },
  created() {
    this.getUserConfig()
    this.eventListener()
  },
 
  methods: {
    eventListener() {
      this.$bus.$on('onAddTask', (task) => {
        if (!task) {
          return
        }
        this.onAddTask(task)
      })
      this.$bus.$on('onUpdateTaskCategoryId', ({
        categoryId,
        task
      }) => {
        const currentCategory = this.categories.filter(t => t.id === task.category_id)[0]
        const currentTasks = currentCategory.tasks
        const ids = currentTasks.map(t => t.id)
        const index = ids.indexOf(task.id)
        currentTasks.splice(index, 1)
        task.category_id = categoryId
        this.onAddTask(task)
      })
    },

    showDetails(details, is_update) {
      details = {
        ...details,
        deadline_date: details.deadline,
        deadline_time: details.deadline,
        title_cached: details.title,
        desc_cached: details.desc,
        deadline_cached: details.deadline,
        deadline_date_cached: details.deadline,
        deadline_time_cached: details.deadline,
      }

      if (is_update) {
        this.$bus.$emit('showMyJobUpdate', details)
      } else {
        this.$bus.$emit('showMyJobDetails', details)
      }
    },

    getUserConfig() {
      this.isLoading = true
      TaskService.getUserConfig().then(res => {
        this.settings = res.data
        this.getCategories()
      })
    },

    getCategories() {
      TaskService.getCategories().then(res => {
        this.isLoading = false
        this.categories = res.data
        this.categories.forEach((category, index) => {
          category.isLoading = true
          const setting = this.settings && this.settings.configs.find(t => t.id === category.id)
          const orders = setting && setting.orders || []
          this.getTasks(category.id, index, orders)
          switch (category.name) {
            case categoriesNames.todo:
              category.borderColor = '#6996D9'
              category.isAllowAdd = true
              break

            case categoriesNames.doing:
              category.borderColor = '#FF8A09'
              break

            case categoriesNames.done:
              category.borderColor = '#69BE31'
              break

            default:
              category.borderColor = '#fff'
              break
          }
        })
      })
    },

    getTasks(categoryId, index, orders = []) {
      TaskService.getTasks(categoryId).then(res => {
        const category = this.categories[index]
        if (orders.length) {
          res.data.map(t => {
            const index = orders.indexOf(t.id)
            t.order = index
            return t
          })
          category.tasks = res.data.sort((a, b) => (a.order > b.order) ? 1 : (a.order < b.order) ? -1 : 0)
        } else {
          category.tasks = res.data
        }
        category.isLoading = false
        this.$forceUpdate()
      })
    },

    onAddTask(task) {
      const category = this.categories.filter(t => t.id === task.category_id)[0]
      const tasks = category.tasks
      tasks.unshift(task)
      this.updateUserConfig()
      this.$forceUpdate()
    },

    getDate(date) {
      return moment(date, 'YYYY-MM-DD H:mm:s').format('YYYY-MM-DD')
    },

    getTime(time) {
      return moment(time, 'YYYY-MM-DD H:mm:s').format('HH:mm')
    },

    isShowExpire(deadline) {
      if (!deadline) {
        return true
      }
      const current = moment().format('YYYY-MM-DD H:mm:s')
      return moment(deadline, 'YYYY-MM-DD H:mm:s').isAfter(current)
    },

    onUpdateCategoryId(id, category) {
      const tasks = category.tasks
      const task = tasks.find(t => t.id === id)
      task.isLoading = true
      this.$forceUpdate()
      TaskService.updateCategoryId(id, category.id).then(() => {
        task.isLoading = false
        task.category_id = category.id
        this.updateUserConfig()
        this.$forceUpdate()
      })
    },

    onSort() {
      this.updateUserConfig()
      this.$forceUpdate()
    },

    onChange(event, category) {
      if (event.added) {
        this.onUpdateCategoryId(event.added.element.id, category)
      }
      if (event.moved) {
        this.onSort()
      }
    },

    updateUserConfig() {
      const configs = []
      this.categories.forEach(category => {
        configs.push({
          id: category.id,
          orders: category.tasks.map(t => t.id)
        })
      })
      if (this.settings.id) {
        TaskService.updateUserConfig({
          configs
        }, this.settings.id)
      } else {
        TaskService.saveUserConfig({
          configs
        }).then(res => {
          this.settings = res.data
        })
      }
    },

    onDeleteTaskSuccess(task) {
      let taskId = task.id
      let taskCategoryId = task.category_id

      this.categories.forEach(category => {
        if (category.id === taskCategoryId) {
          category.tasks.forEach((taskItem, index) => {
            if (taskItem.id === taskId) {
              category.tasks.splice(index, 1)
            }
          })
        }
      })

      this.$forceUpdate()
    },

    cancelOnUpdateTask(task) {
      let taskId = task.id
      let taskCategoryId = task.category_id

      this.categories.forEach(category => {
        if (category.id === taskCategoryId) {
          category.tasks.forEach((taskItem) => {
            if (taskItem.id === taskId) {
              taskItem.title = task.title_cached
              taskItem.desc = task.desc_cached
              taskItem.deadline = task.deadline_cached
            }
          })
        }
      })
      this.$forceUpdate()
    },

    onUpdateTask(task) {
      let taskId = task.id
      let taskCategoryId = task.category_id

      this.categories.forEach(category => {
        if (category.id === taskCategoryId) {
          category.tasks.forEach((taskItem) => {
            if (taskItem.id === taskId) {
              taskItem.title = task.title
              taskItem.desc = task.desc
              taskItem.deadline = task.deadline
            }
          })
        }
      })
      this.$forceUpdate()
    },

    onDeleteTask(task) {
      TaskService.deleteTask(task.id)
      this.onDeleteTaskSuccess(task)
      this.$snotify.success('Xóa công việc thành công.', TOAST_SUCCESS)
    },

    reloadTooltip() {
      $(".nt-col-head-title").tooltip({
        selector: '[data-toggle="m-tooltip"]',
        template: '<div class="m-tooltip tooltip nt-tooltip-category-task-info" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
      });
    },
  }
}