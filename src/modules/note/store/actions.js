import noteAPI from "@/services/note";
import { NoteVM } from "../transformers/index";
import {
  NOTE_CREATE,
  NOTE_GET_ALL_BY_OBJECT_ID
} from "@/types/actions.type";
import {
  SET_NOTE_BY_OBJECT_ID
} from "@/types/mutations.type";

export const actions = {
  async [NOTE_GET_ALL_BY_OBJECT_ID]({ commit }, params) {
    try {
      let getAllNoteByOjectId = await noteAPI.getAllByObjectId(params);
      commit(SET_NOTE_BY_OBJECT_ID, NoteVM.listVM.transform(getAllNoteByOjectId.data.data));
      return getAllNoteByOjectId;
    } catch (e) {
      return e;
    }
  },

  async [NOTE_CREATE](context, params) {
    try {
      let createNote = await noteAPI.create(params);
      return createNote;
    } catch (e) {
      return e;
    }
  },
}
