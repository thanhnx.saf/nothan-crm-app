import listVM from './list';
import showVM from './show';

export const NoteVM = {
  showVM,
  listVM,
};
