export default {
  transform(data) {
    let transform = {};

    transform = {
      id: data.id,
      team_id: data.team_id,
      user_id: data.user_id,
      note: data.note,
      object_id: data.object_id,
      type: data.type,
      created_at: data.created_at,
      update_at: data.update_at,
      files: data.files,
    };

    return transform;
  }
}
