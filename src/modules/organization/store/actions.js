import organizationAPI from "@/services/organization";
import {
  organizationVM
} from "../transformers/index";
import {
  ORGANIZATION_CURRENT,
  ORGANIZATION_GET_ALL
} from "@/types/actions.type";
import {
  SET_ORGANIZATION_CURRENT,
  SET_ORGANIZATIONS,
} from "@/types/mutations.type";

export const actions = {
  async [ORGANIZATION_GET_ALL]({
    commit
  }) {
    try {
      let organizations = await organizationAPI.getAll();
      if (organizations.data.data) {
        commit(SET_ORGANIZATIONS, organizationVM.listVM.transform(organizations.data.data));
      }
      return organizations;
    } catch (e) {
      return e;
    }
  },
  async [ORGANIZATION_CURRENT]({
    commit
  }, teamId) {
    try {
      let response = await organizationAPI.getCurrentTeam(teamId);
      if (response.data.data) {
        commit(SET_ORGANIZATION_CURRENT, organizationVM.teamVM.transform(response.data.data));
      }
      return response;
    } catch (e) {
      return e
    }
  },
}