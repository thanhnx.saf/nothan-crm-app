import {
  SET_ORGANIZATION_CURRENT,
  SET_ORGANIZATIONS,
} from "@/types/mutations.type";

export const mutations = {
  [SET_ORGANIZATIONS](state, organizations) {
    state.list = organizations;
  },
  [SET_ORGANIZATION_CURRENT](state, organization) {
    state.currentTeam = organization;
  },
}
