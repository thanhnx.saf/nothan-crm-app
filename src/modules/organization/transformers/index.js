import createVM from "./create";
import updateVM from "./update";
import showVM from "./show";
import listVM from "./list";
import teamVM from "./team";

export const organizationVM = {
  createVM,
  updateVM,
  showVM,
  listVM,
  teamVM
};
