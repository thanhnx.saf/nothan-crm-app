export default {
  transform(data) {
    let vm = this;
    let newData = [];

    data.forEach(function (item) {
      newData.push(vm.transformItem(item));
    });

    return newData;
  },

  transformItem(data) {
    let transform = {};

    transform = {
      team_id: data.id,
      name: data.name,
      founding_date: data.founding_date,
      user_id: data.user_id,
      business_field_id: data.business_field_id,
      scale: data.scale,
      logo_url: data.logo_url,
      created_at: data.created_at,
      updated_at: data.updated_at,
      deleted_at: data.deleted_at,
      role_name: data.role_name,
      role_id: data.role_id
    };

    return transform;
  }
};
