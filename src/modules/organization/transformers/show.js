export default {
  transform(data) {
    let transform = {};

    transform = {
      team_id: data.id,
      name: data.name,
      business_field_id: data.business_field_id,
      scale: data.scale,
      logo_url: data.logo_url,
      founding_date: data.founding_date,
      user_id: 1
    };

    return transform;
  }
};
