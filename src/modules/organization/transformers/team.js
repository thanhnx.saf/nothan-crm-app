export default {
  transform(data) {
    let transform = {};
    let roleName = "Owner";
    if (data.role_id === 2) {
      roleName = "Admin";
    } else if (data.role_id === 4) {
      roleName = "Member";
    }
    transform = {
      team_id: data.id,
      name: data.name,
      founding_date: data.founding_date,
      user_id: data.user_id,
      business_field_id: data.business_field_id,
      scale: data.scale,
      logo_url: data.logo_url,
      role_id: data.role_id,
      created_at: data.created_at,
      email: data.email,
      phone: data.phone,
      address: data.address,
      website: data.website,
      facebook: data.facebook,
      role_name: roleName,
    };
    return transform;
  }
};
