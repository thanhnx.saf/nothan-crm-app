import { mapGetters } from "vuex";

import helpers from '@/utils/utils';

export default {
  name: "ShowInfoBasic",

  components: {

  },
  mounted() {
    let vm = this;
    vm.getCreatedBy(vm.detailProduct.user_id);
  },
  computed: {
    helpers() {
      return helpers;
    },
    isLoader() {
      let vm = this;
      if (vm.dataReady === true && vm.attributeReady === true) {
        return true;
      } else {
        return false;
      }
    },
    ...mapGetters(["listUsers", "detailProduct"]),
  },
  data() {
    return {
      dataReady: false,
      attributeReady: false,
      productCreatedBy: {},
    }
  },
  methods: {
    getCreatedBy(userId) {
      let vm = this;
      let createdBy = vm.listUsers.find(function (user) {
        return user.id == userId;
      });
      vm.productCreatedBy = createdBy;
    },
  }
}
