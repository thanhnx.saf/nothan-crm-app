/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import {
  FILE_CREATE,
  PRODUCT_UPDATE,
  PRODUCT_DETAIL
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";
import {
  IS_PRIVATE_FILE
} from "@/types/const";
import AuthenticateImage from "@/components/authenticateImage/AuthenticateImage.vue";

export default {
  components: {
    AuthenticateImage
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(['detailProduct', 'detailProductImage'])
  },
  data() {
    return {
      productId: parseInt(this.$route.params.product_id),
      isLoader: false,
      isFileDimensionsAllowedMax: false,
      isFileDimensionsAllowedMin: false,
      isFileTypeAllowed: false,
      isFileSizeAllowed: false,
      mimesAllowed: [
        "image/jpg",
        "image/jpeg",
        "image/png",
        "image/gif",
      ],
      uploadAvatar: '',
      changeFile: '',
      uploadMaxSize: 10 * 1024 * 1024, // 10Mb
    };
  },
  methods: {
    changeImage(event) {
      let vm = this;
      let file = event.target.files[0];
      let reader = new FileReader();
      if (file) {
        vm.isLoader = true;

        vm.isFileDimensionsAllowedMax = false;
        vm.isFileDimensionsAllowedMin = false;
        vm.isFileTypeAllowed = false;
        vm.isFileSizeAllowed = false;

        let fileName = file.name;
        let fileType = fileName.replace(/^.*\./, '').toLowerCase();
        let fileSize = file.size;

        vm.uploadAvatar = {
          file_name: fileName,
          file_type: fileType,
          file_size: fileSize,
        };

        if (vm.uploadAvatar && vm.uploadAvatar.file_type === 'png' || vm.uploadAvatar.file_type === 'jpg' || vm.uploadAvatar.file_type === 'jpeg' || vm.uploadAvatar.file_type === 'gif') {
          vm.isFileTypeAllowed = false;
        } else {
          vm.isFileTypeAllowed = true;
          vm.modalWarningErrors();
          return;
        }

        if (vm.uploadAvatar.file_size && vm.uploadAvatar.file_size > vm.uploadMaxSize) {
          vm.isFileSizeAllowed = true;
          vm.modalWarningErrors();
          return;
        } else {
          vm.isFileSizeAllowed = false;
        }

        let image = new Image();
        image.src = window.URL.createObjectURL(file);

        image.onload = function () {
          let imgWidth = image.naturalWidth;
          let imgHeight = image.naturalHeight;

          let fileDimensions = {
            width: imgWidth,
            height: imgHeight
          };

          vm.uploadAvatar.file_dimensions = fileDimensions;

          if (vm.uploadAvatar.file_dimensions.width < 100 || vm.uploadAvatar.file_dimensions.height < 100) {
            vm.isFileDimensionsAllowedMin = true;
            vm.modalWarningErrors();
            return;
          } else {
            vm.isFileDimensionsAllowedMin = false;
          }

          if (vm.uploadAvatar.file_dimensions.width > 2000 || vm.uploadAvatar.file_dimensions.height > 2000) {
            vm.isFileDimensionsAllowedMax = true;
            vm.modalWarningErrors();
            return;
          } else {
            vm.isFileDimensionsAllowedMax = false;
          }

          reader.readAsDataURL(file);

          vm.changeFile = file;

          vm.uploadFile();
        }
      }
    },

    showPopupAvatar() {
      $("#uploadAvatarInput").click();
    },

    modalWarningErrors() {
      let vm = this;
      $('#warning_upload_file').modal('show');
      vm.isLoader = false;
    },

    async uploadFile() {
      let vm = this;
      let formData = new FormData();
      formData.append('files', vm.changeFile);
      formData.append("is_publish", IS_PRIVATE_FILE);
      let file = await vm.$store.dispatch(FILE_CREATE, formData);

      if (file && file.data.data) {
        vm.detailProduct.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == 'product_image') {
            attribute.attribute_value = file.data.data.id;
          }
        });
      }
      vm.update();
    },

    update() {
      let vm = this;
      let detailProduct = vm.detailProduct;
      let attributes = [];

      if (detailProduct.attributes && detailProduct.attributes.length > 0) {
        detailProduct.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == "product_image") {
            attributes.push(attribute);
          }
        });
      }

      let transformDetailProduct = {};
      transformDetailProduct.id = detailProduct.id;
      transformDetailProduct.attributes = attributes;

      vm.$store.dispatch(PRODUCT_UPDATE, transformDetailProduct).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.$snotify.error('Cập nhật ảnh sản phẩm thất bại.', TOAST_ERROR);
          vm.isLoader = false;
        } else {
          if (response.data.data && response.data.data.status) {
            // vm.$emit("update_success",true);
            vm.getProductDetail();
          } else {
            if (response.data.data.sort_delete) {
              vm.$snotify.error('Cập nhật ảnh sản phẩm thất bại.', TOAST_ERROR);
              $("#messageTrashProduct").modal('show');
            }
          }
        }
      }).catch(() => {
        vm.isLoader = false;
        vm.$snotify.error('Cập nhật ảnh sản phẩm thất bại.', TOAST_ERROR);
      });
    },

    getProductDetail() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_DETAIL, vm.productId).then(response => {
        if (response.data.data) {
          vm.isLoader = false;
          vm.$snotify.success('Cập nhật ảnh sản phẩm thành công.', TOAST_SUCCESS);
        }
      }).catch(error => {
        vm.$snotify.error('Cập nhật ảnh sản phẩm thất bại.', TOAST_ERROR);
        vm.isLoader = false;
        console.log(error);
      });
    }
  }
}