import {
  FILE_DETAIL
} from "@/types/actions.type";

export default {
  props: {
    file_id: {
      type: Number,
      required: true
    },
  },
  created() {
    this.getFileById();
  },
  data() {
    return {
      isLoader: false,
      fileUrl: ""
    }
  },
  methods: {
    getFileById() {
      let vm = this;
      vm.isLoader = true;
      vm.$store.dispatch(FILE_DETAIL, vm.file_id).then(response => {

        vm.fileUrl = response.data.data.path_on_server;
        vm.isLoader = false;
      });
    },
  }
};