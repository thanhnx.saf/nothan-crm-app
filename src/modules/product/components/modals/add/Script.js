/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import {
  FILE_CREATE_AVATAR,
  PRODUCT_CREATE,
  PRODUCT_ADD_NEW_CHECK_EXIST
} from "@/types/actions.type";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";
import {
  IS_SELECT,
  IS_PRIVATE_FILE
} from "@/types/const";

const doneTypingInterval = 500;

export default {
  data() {
    return {
      isSuccess: false,
      isLoader: false,
      product: {
        attributes: [],
        team_id: ''
      },
      isFileDimensionsAllowedMax: false,
      isFileDimensionsAllowedMin: false,
      mimesAllowed: [
        "image/jpg",
        "image/jpeg",
        "image/png",
        "image/gif",
      ],
      uploadAvatar: '',
      changeFile: '',
      nameIsExists: false,
      skuIsExists: false,
      productId: '',
      isLoaderCheckExists: false,
      server_errors: '',
      messageErrorServer: '',
      uploadMaxSize: 10 * 1024 * 1024, // 10Mb
    }
  },

  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["currentUser", "currentTeam", "attributeAddNewProduct"]),
    listAttributeCodesByAllAttributes() {
      let vm = this;
      vm.product.attributes = vm.attributeAddNewProduct;

      return vm.attributeAddNewProduct;
    },
  },

  methods: {
    changeImage(event) {
      let vm = this;
      let file = event.target.files[0];
      let reader = new FileReader();

      reader.onload = function (e) {
        $('#add_product_image_preview').css({
          'background-image': 'url(' + e.target.result + ')'
        });
        $('#add_product_image_preview').addClass('add-product-image-preview');
      }

      if (file) {
        vm.isFileDimensionsAllowedMax = false;
        vm.isFileDimensionsAllowedMin = false;

        let fileName = file.name;
        let fileType = fileName.replace(/^.*\./, '').toLowerCase();
        let fileSize = file.size;

        vm.uploadAvatar = {
          file_name: fileName,
          file_type: fileType,
          file_size: fileSize,
        };

        if (vm.uploadAvatar.file_size > vm.uploadMaxSize) {
          return;
        }

        let image = new Image();
        image.src = window.URL.createObjectURL(file);

        image.onload = function () {
          let imgWidth = image.naturalWidth;
          let imgHeight = image.naturalHeight;

          let fileDimensions = {
            width: imgWidth,
            height: imgHeight
          };

          vm.uploadAvatar.file_dimensions = fileDimensions;

          if (vm.uploadAvatar.file_dimensions.width < 100 || vm.uploadAvatar.file_dimensions.height < 100) {
            vm.isFileDimensionsAllowedMin = true;
          } else {
            vm.isFileDimensionsAllowedMin = false;
          }

          if (vm.uploadAvatar.file_dimensions.width > 2000 || vm.uploadAvatar.file_dimensions.height > 2000) {
            vm.isFileDimensionsAllowedMax = true;
          } else {
            vm.isFileDimensionsAllowedMax = false;
          }
        }

        reader.readAsDataURL(file);

        vm.changeFile = file;
      }
    },

    async uploadFile() {
      let vm = this;
      if (!vm.isFileDimensionsAllowedMax && !vm.isFileDimensionsAllowedMin) {
        let formData = new FormData();
        formData.append('files', vm.changeFile);
        formData.append("is_publish", IS_PRIVATE_FILE);
        let file = await vm.$store.dispatch(FILE_CREATE_AVATAR, formData);

        if (file && file.data.data) {
          vm.product.attributes.forEach(function (attribute) {
            if (attribute.attribute_code == 'product_image') {
              attribute.attribute_value = file.data.data.id;
            }
          });
        }
      }
    },

    createNew(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;
          if (vm.uploadAvatar) {
            vm.uploadFile().then(() => {
              vm.createNewPushToServer();
            });
          } else {
            vm.createNewPushToServer();
          }
        }
      });
    },

    createNewPushToServer() {
      let vm = this;
      vm.convertDataPushToServer();
      vm.product.team_id = vm.currentTeamId;
      vm.$store.dispatch(PRODUCT_CREATE, vm.product).then(response => {
        vm.isLoader = false;
        if (response.data.status_code && response.data.status_code == 422) {
          vm.server_errors = response.data.errors;
          vm.$snotify.error('Thêm mới sản phẩm thất bại.', TOAST_ERROR);
        } else {
          if (response.data.data && response.data.data.status) {
            vm.productId = response.data.data.id;

            vm.isSuccess = true;
            vm.$snotify.success('Thêm mới sản phẩm thành công.', TOAST_SUCCESS);
            $(".s--modal .modal-body").animate({
              scrollTop: 0
            }, 300);
            vm.resetProduct();
            vm.$emit("add_new_success", true);
          }
        }
      }).catch(() => {
        vm.$snotify.error('Thêm mới sản phẩm thất bại.', TOAST_ERROR);
        vm.messageErrorServer = 'Lỗi, vui lòng thử lại sau.';
        $(".s--modal .modal-body").animate({
          scrollTop: 0
        }, 300);
        vm.isLoader = false;
      });
    },

    convertDataPushToServer() {
      let vm = this;
      if (vm.product && vm.product.attributes) {
        vm.product.attributes.forEach(function (attribute) {
          if (attribute.data_type_id == IS_SELECT) {
            attribute.attribute_value = attribute.attribute_value ? parseInt(attribute.attribute_value) : '';
          }
        });
      }
    },

    resetProduct() {
      let vm = this;

      if (vm.product.attributes && vm.product.attributes.length > 0) {
        vm.product.attributes.forEach(function (attribute) {
          if (attribute.attribute_code == "product_type") {
            attribute.attribute_value = attribute.attribute_options[0].id;
          } else {
            attribute.attribute_value = "";
          }
        });
      }

      vm.$validator.reset();
      vm.server_errors = '';
      vm.isFileDimensionsAllowedMax = false;
      vm.isFileDimensionsAllowedMin = false;
      vm.changeFile = '';
      vm.uploadAvatar = '';
      $("input[type='file']").val('');
      $('#add_product_image_preview').css({
        'background-image': ''
      });
      $('#add_product_image_preview').removeClass('add-product-image-preview');
    },

    reset() {
      let vm = this;
      vm.isSuccess = false;
      vm.isLoader = false;
      vm.nameIsExists = false;
      vm.isLoaderCheckExists = false;
      vm.messageErrorServer = false;
      vm.resetProduct();
    },

    checkNameExists(id, value, code) {
      let vm = this;
      vm.nameIsExists = false;
      value = value ? value.trim() : "";

      let defautValueSku = value ? helpers.changeAlias(value).replace(/ /g, '_') : '';

      vm.product.attributes.forEach((attribute) => {
        if (attribute.attribute_code === 'sku') {
          attribute.attribute_value = defautValueSku
        }
      })

      if (value && value.length > 1) {
        clearTimeout(vm.typingTimer);
        vm.typingTimer = setTimeout(function () {
          vm.isLoaderCheckExists = true;
          vm.changeDataInputType(code);
          let data = {
            attribute_id: id,
            attribute_value: value
          }
          vm.$store.dispatch(PRODUCT_ADD_NEW_CHECK_EXIST, data).then(response => {
            if (response.data.meta) {
              if (response.data.meta.is_exists) {
                vm.nameIsExists = true;
              }
              vm.isLoaderCheckExists = false;
            }
          }).catch(() => {
            vm.isLoaderCheckExists = false;
          });
        }, doneTypingInterval);
      }
    },

    checkSkuExists(id, value, code) {
      let vm = this;
      vm.skuIsExists = false;
      value = value ? value.trim() : "";
      if (value && value.length > 1) {
        clearTimeout(vm.typingTimer);
        vm.typingTimer = setTimeout(function () {
          vm.isLoaderCheckExists = true;
          vm.changeDataInputType(code);
          let data = {
            attribute_id: id,
            attribute_value: value
          }
          vm.$store.dispatch(PRODUCT_ADD_NEW_CHECK_EXIST, data).then(response => {
            if (response.data.meta) {
              if (response.data.meta.is_exists) {
                vm.skuIsExists = true;
              }
              vm.isLoaderCheckExists = false;
            }
          }).catch(() => {
            vm.isLoaderCheckExists = false;
          });
        }, doneTypingInterval);
      }
    },

    setDefaultValueSku(value) {
      let defautValueSku = value ? helpers.changeAlias(value).replace(/ /g, '_') : '';

      this.product.attributes.forEach((attribute) => {
        if (attribute.attribute_code === 'sku') {
          attribute.attribute_value = defautValueSku
        }
      })

      this.$forceUpdate()
    },

    changeDataInputType(attribute_code) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute_code]) {
          vm.server_errors[attribute_code] = '';
        }
      }, doneTypingInterval);
    },

    reloadTooltip() {
      $(".form-group").tooltip({
        selector: '[data-toggle="m-tooltip"]',
        template: '<div class="m-tooltip tooltip nt-tooltip-info-warning" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
      });
    },
  },
}