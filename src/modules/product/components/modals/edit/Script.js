/* eslint-disable no-undef */
import { mapGetters, mapState } from "vuex";

import helpers from "@/utils/utils";
import AttributeWithDataType from "../../attribubte/AttributeWithDataType";
import { PRODUCT_UPDATE } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  components: {
    "attribute-with-data-type": AttributeWithDataType,
  },
  mounted() {
    let vm = this;
    $('.btn-product-edit').click(function () {
      vm.thenCustomer();
    });
  },
  watch: {
    detailProduct: {
      handler(after) {
        if (after) {
          let vm = this;
          let countChange = 0;
          vm.thenCustomer();
          if (after.attributes && after.attributes.length > 0) {
            after.attributes.forEach(function (item) {
              item.attribute_value = item.attribute_value == null || item.attribute_value == undefined ? "" : item.attribute_value;
              if (item.data_type_id == 10 || item.data_type_id == 11 || item.data_type_id == 13) {
                if (item.attribute_value_cache) {
                  if (item.attribute_value) {
                    if (item.attribute_value.join() != item.attribute_value_cache.join()) {
                      countChange++;
                    }
                  } else if (item.attribute_value.length <= 0) {
                    countChange++;
                  }

                } else {
                  if (item.attribute_value && item.attribute_value.length > 0) {
                    countChange++;
                  }
                }
              } else {
                if (item.attribute_value != item.attribute_value_cache) {
                  countChange++;
                }
              }
            });
          }
          if (countChange) {
            vm.isChange = true;
          } else {
            vm.isChange = false;
          }
        }
      },
      deep: true
    },
    errors: {
      handler(after) {
        let vm = this;
        if (after.any()) {
          vm.isChange = true;
        }
      },
      deep: true
    }
  },
  computed: {
    ...mapState({
      currentTeam: state => state.organization.currentTeam,
    }),
    ...mapGetters(['detailProduct', 'currentTeam']),
    helpers() {
      return helpers;
    },
  },
  data() {
    return {
      productId: parseInt(this.$route.params.product_id),
      server_errors: '',
      isSuccess: false,
      isLoader: false,
      messageErrorServer: '',
      isChange: false,
      isCancel: false,
      attributes: [],
      attributeCodes: ['product_name', 'sku', 'unit', 'product_type', 'product_image', 'listed_price', 'sort_description']
    }
  },
  methods: {
    thenCustomer() {
      let vm = this;

      let attributes = [];
      let sortAttributes = [];

      if (vm.detailProduct.attributes && vm.detailProduct.attributes.length > 0) {
        vm.detailProduct.attributes.forEach(function (item) {
          if (item.attribute_code == 'product_image' || item.attribute_code == 'product_code') {
            return;
          } else {
            attributes.push(item);
          }
        });
      }

      if (attributes.length > 0) {
        vm.attributeCodes.forEach(function (attribute_code) {
          attributes.forEach(function (item) {
            if (item.attribute_code == attribute_code) {
              sortAttributes.push(item);
            }
          });
        });
      }

      vm.attributes = sortAttributes;
    },

    reset() {
      let vm = this;
      vm.detailProduct.attributes = vm.detailProduct.attributes_cache;
      vm.$emit("cancel_update", true);
      vm.thenCustomer();
      vm.resetEditCustomer();
    },

    resetEditCustomer() {
      let vm = this;
      vm.isSuccess = false;
      vm.$validator.reset();
      vm.server_errors = '';
      $(".s--modal .modal-body").animate({ scrollTop: 0 }, 0);
    },

    update(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;

          let detailProduct = vm.detailProduct;
          detailProduct = vm.checkUpdatePushToServe(detailProduct);
          detailProduct.team_id = vm.currentTeamId;

          vm.$store.dispatch(PRODUCT_UPDATE, detailProduct).then(response => {
            if (response.data.status_code && response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              vm.isLoader = false;
              vm.isChange = true;
              vm.$snotify.error('Cập nhật sản phẩm thất bại.', TOAST_ERROR);
            } else {
              if (response.data.data && response.data.data.status) {
                vm.isChange = false;
                vm.isSuccess = true;
                vm.server_errors = '';

                vm.$snotify.success('Cập nhật sản phẩm thành công.', TOAST_SUCCESS);

                $(".s--modal .modal-body").animate({ scrollTop: 0 }, 300);

                vm.$emit("update_success", true);
              } else {
                if (response.data.data.sort_delete) {
                  vm.$snotify.error('Cập nhật sản phẩm thất bại.', TOAST_ERROR);
                  $("#messageTrashProduct").modal('show');
                }
              }

              vm.isLoader = false;
            }

            setTimeout(function () {
              vm.isSuccess = false;
            }, 5000);
          }).catch(error => {
            vm.isLoader = false;
            vm.isChange = true;
            vm.$snotify.error('Cập nhật sản phẩm thất bại.', TOAST_ERROR);
            // vm.messageErrorServer = 'Lỗi, vui lòng thử lại sau.';
            vm.server_errors = error.response.data.errors;
            $(".s--modal .modal-body").animate({ scrollTop: 0 }, 300);
          });
        } else {
          $(".modal-body").animate({ scrollTop: $(".m-form .m-form__group.has-danger").offset().top }, 300);
        }
      });
    },

    checkUpdatePushToServe(data) {
      let newData = {};
      newData.id = data.id;
      newData['attributes'] = [];

      if (data.attributes && data.attributes.length > 0) {
        data.attributes.forEach(function (item) {
          item.attribute_value = item.attribute_value == null || item.attribute_value == undefined ? "" : item.attribute_value;
          if (item.data_type_id == 10 || item.data_type_id == 11 || item.data_type_id == 13) {
            if (item.attribute_value_cache) {
              if (item.attribute_value) {
                if (item.attribute_value.join() != item.attribute_value_cache.join()) {
                  newData['attributes'].push(item);
                }
              } else if (item.attribute_value.length <= 0) {
                newData['attributes'].push(item);
              }

            } else {
              if (item.attribute_value && item.attribute_value.length > 0) {
                newData['attributes'].push(item);
              }
            }
          } else {
            if (item.attribute_value != item.attribute_value_cache) {
              newData['attributes'].push(item);
            }
          }

          // Convert data
          if (item.data_type_id == 4) {
            item.attribute_value = item.attribute_value ? item.attribute_value.replace(/ /g, "") : '';
          }
          if (item.data_type_id == 2 || item.data_type_id == 3) {
            item.attribute_value = item.attribute_value ? helpers.formatDateTime(item.attribute_value) : '';
          }
        });
      }

      return newData;
    },
  }
};
