export default {
  props: {
    is_deleted_multiple: {
      type: Boolean,
      default: false
    },
    product_name: {
      type: String,
      default: ''
    },
  },
}
