import { mapGetters } from "vuex";

import { PRODUCT_TRASH } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  props: {
    selected_ids: {
      type: Array,
      required: true,
      default: []
    },
    total_deleted: {
      type: Number,
      required: true,
      default: 0
    },
    is_deleted_multiple: {
      type: Boolean,
      default: false
    },
    product_name: {
      type: String,
      default: ''
    },
  },

  computed: {
    ...mapGetters(["currentTeam"]),
  },

  methods: {
    trash() {
      let vm = this;
      let params = {};

      params.team_id = vm.currentTeamId;
      params.product_id = vm.selected_ids;

      vm.$store.dispatch(PRODUCT_TRASH, params).then((response) => {
        if (response.data.data.success) {
          vm.$emit('is_change', true);
          vm.$snotify.success('Xóa tạm sản phẩm thành công.', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Xóa tạm sản phẩm thất bại.', TOAST_ERROR);
      });
    }
  },
}
