import {
  PERMISSION_ADD_PRODUCT,
  PERMISSION_SHOW_PRODUCT,
  PERMISSION_PRODUCT_RESTORED,
  PERMISSION_UPDATE_PRODUCT
} from '@/types/const'
import utils from '@/utils/utils'
import Product from './views/product/Product.vue'
import Show from './views/show/Show.vue'
import ShowInfoBasic from './components/info-basic/ShowInfoBasic'
import ShowInfoDescription from './components/info-desc/ShowInfoDescription'

export default [{
  name: "products",
  path: "/products",
  component: Product,
  meta: {
    breadcrumb: [{
      name: "Sản phẩm",
    }, {
      name: "Danh sách sản phẩm",
    }],
    actions: [{
      type: 1,
      class: "nt-button nt-button-top nt-button-warning",
      name: "Thêm sản phẩm",
      isIcon: 1,
      iconClass: "la la-plus",
      isModal: 1,
      idModal: "#addNewProduct",
      permissionActionName: PERMISSION_ADD_PRODUCT,
      serviceName: "product"
    }]
  }
}, {
  name: "productDetail",
  path: "/products/:product_id",
  component: Show,
  redirect: {
    name: 'ProductInfoBasic'
  },
  children: utils.prefixRouter('/team/:team_id/products', [{
    path: "/:product_id",
    name: "ProductInfoBasic",
    component: ShowInfoBasic,
    meta: {
      // check redirect 403
      permissionName: PERMISSION_SHOW_PRODUCT,
      serviceName: "product",
      breadcrumb: [{
        name: "Sản phẩm",
        link: "/team/:team_id/products"
      }, {
        name: "Chi tiết sản phẩm",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "productRestore",
        position: "before",
        permissionActionName: PERMISSION_PRODUCT_RESTORED,
        serviceName: "product"
      }, {
        type: 1,
        class: "nt-button nt-button-top nt-button-warning btn-product-edit",
        name: "Chỉnh sửa",
        isIcon: 1,
        iconClass: "icon icon-edit",
        isModal: 1,
        idModal: "#editProduct",
        permissionActionName: PERMISSION_UPDATE_PRODUCT,
        serviceName: "product"
      }]
    },
  }, {
    path: "/:product_id/description",
    name: "ProductInfoDescription",
    component: ShowInfoDescription,
    meta: {
      // check redirect 403
      permissionName: PERMISSION_SHOW_PRODUCT,
      serviceName: "product",
      breadcrumb: [{
        name: "Sản phẩm",
        link: "/team/:team_id/products"
      }, {
        name: "Chi tiết sản phẩm",
      }],
      actions: [{
        type: 4,
        class: "nt-button nt-button-top nt-button-primary nt-button-restored nt-button-hide",
        name: "Phục hồi",
        isIcon: 1,
        iconClass: "icon icon-loading",
        nameFunction: "productRestore",
        position: "before",
        permissionActionName: PERMISSION_PRODUCT_RESTORED,
        serviceName: "product"
      }, {
        type: 1,
        class: "nt-button nt-button-top nt-button-warning btn-product-edit",
        name: "Chỉnh sửa",
        isIcon: 1,
        iconClass: "icon icon-edit",
        isModal: 1,
        idModal: "#editProduct",
        permissionActionName: PERMISSION_UPDATE_PRODUCT,
        serviceName: "product",
        isMultipleAction: true
      }],
      tags: {
        title: "Chi tiết sản phẩm | Sản phẩm",
        content: "",
        description: ""
      }
    },
  }])
}]