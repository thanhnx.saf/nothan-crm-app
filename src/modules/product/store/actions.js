import productAPI from "@/services/product";
import FileAPI from "@/services/file";
import { ProductVM } from "../transformers/index";
import { FileVM } from "@/modules/file/transformers/index";
import {
  PRODUCT_GET_ALL,
  PRODUCT_CREATE,
  PRODUCT_ADD_NEW_CHECK_EXIST,
  PRODUCT_GET_ATTRIBUTES,
  PRODUCT_GET_USER_ATTRIBUTES,
  PRODUCT_UPDATE,
  PRODUCT_GET_ALL_BY_TEAM,
  PRODUCT_DETAIL,
  PRODUCT_GET_ALL_WITHOUT_PAGINATE,
  PRODUCT_TRASH,
  PRODUCT_RESTORE,
  PRODUCT_CHECK_HAS_DEAL_BEFORE_DELETE,
  PRODUCT_SHOW
} from "@/types/actions.type";
import {
  SET_PRODUCT_ATTRIBUTES,
  SET_PRODUCT_USER_ATTRIBUTES,
  SET_PRODUCTS,
  SET_PRODUCT_ALL_BY_TEAM,
  SET_DETAIL_PRODUCT,
  SET_DETAIL_PRODUCT_IMAGE
} from "@/types/mutations.type";

export const actions = {
  async [PRODUCT_GET_ALL]({ commit }, params) {
    let products = await productAPI.getAll(params);
    let dataProducts = products.data.data;
    if (dataProducts) {
      commit(SET_PRODUCTS, ProductVM.listVM.transform(dataProducts, products.data.meta));
    }
    return products;
  },
  async [PRODUCT_GET_ALL_WITHOUT_PAGINATE](context, params) {
    let products = await productAPI.getAllWithoutPaginate(params);
    // if(dataProducts){
    //     commit(SET_PRODUCTS, ProductVM.listVM.transform(dataProducts, products.data.meta));
    // }
    return products;
  },
  [PRODUCT_GET_ATTRIBUTES]({ commit }) {
    return new Promise((resolve, reject) => {
      productAPI.getAttributes().then(response => {
        commit(SET_PRODUCT_ATTRIBUTES, ProductVM.attributeVM.transform(response.data.data));
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  [PRODUCT_GET_USER_ATTRIBUTES]({ commit }) {
    return new Promise((resolve, reject) => {
      productAPI.getAttributeByUser().then(response => {
        commit(SET_PRODUCT_USER_ATTRIBUTES, ProductVM.userAttributeVM.transform(response.data.data));
        resolve(response);
      }).catch(error => {
        reject(error);
      });
    });
  },
  async [PRODUCT_CREATE](context, params) {
    try {
      params = ProductVM.createVM.transform(params);
      let productCreated = await productAPI.create(params);
      return productCreated;
    } catch (e) {
      return e;
    }
  },
  async [PRODUCT_ADD_NEW_CHECK_EXIST](context, params) {
    try {
      let exist = await productAPI.checkExists(params);
      return exist;
    } catch (e) {
      return e;
    }
  },
  async [PRODUCT_UPDATE](context, params) {
    try {
      params = ProductVM.updateVM.transform(params);
      let productUpdated = await productAPI.update(params.id, params);
      return productUpdated;
    } catch (e) {
      return e;
    }
  },
  async [PRODUCT_GET_ALL_BY_TEAM]({ commit }, params) {
    try {
      let productAllByTeam = await productAPI.getProductByTeams(params);
      commit(SET_PRODUCT_ALL_BY_TEAM, productAllByTeam.data.data);
      return productAllByTeam;
    } catch (error) {
      console.log(error)
    }
  },
  async [PRODUCT_DETAIL]({ commit }, id) {
    try {
      let productDetail = await productAPI.show(id);
      let dataProductDetail = productDetail.data.data;
      let attributes = [];

      let fileId = "";

      if (dataProductDetail.attributes) {
        Object.keys(dataProductDetail.attributes).forEach(function (index) {
          let attribute = dataProductDetail.attributes[index];
          if (attribute.attribute_code == "product_image") {
            fileId = attribute.attribute_value;
          }
          attributes.push(attribute);
        });
      }

      dataProductDetail.attributes = attributes;

      if (fileId) {
        let file = await FileAPI.show(fileId);
        commit(SET_DETAIL_PRODUCT_IMAGE, FileVM.ShowVM.transform(file.data.data));
      }

      commit(SET_DETAIL_PRODUCT, ProductVM.showVM.transform(dataProductDetail));

      return productDetail;
    } catch (e) {
      return e;
    }
  },

  async [PRODUCT_SHOW](context, params) {
    try {
      let productDetail = await productAPI.showProduct(params);
      return productDetail;
    } catch (e) {
      return e;
    }
  },
  async [PRODUCT_TRASH](context, params) {
    try {
      return await productAPI.multipleDelete(params);
    } catch (e) {
      return e;
    }
  },
  async [PRODUCT_RESTORE](context, params) {
    try {
      return await productAPI.multipleRestore(params);
    } catch (e) {
      return e;
    }
  },
  async [PRODUCT_CHECK_HAS_DEAL_BEFORE_DELETE](context, params) {
    try {
      return await productAPI.checkProductHasDealBeforeDelete(params);
    } catch (e) {
      return e;
    }
  }
}
