export const getters = {
  products(state) {
    return state.list;
  },
  attributes(state) {
    return state.attributes;
  },
  attributeAddNewProduct(state) {
    let attributes = state.attributes;
    let sortAttributes = [];

    let attributeCodes = ['product_name', 'sku', 'unit', 'product_type', 'product_image', 'listed_price', 'sort_description'];

    if (attributes.length > 0) {
      attributeCodes.forEach(function (attribute_code) {
        attributes.forEach(function (item) {
          if (item.attribute_code == attribute_code) {
            if (item.attribute_code == "product_type") {
              item.attribute_value = item.attribute_options[0].id;
            }
            sortAttributes.push(item);
          }
        });
      });
    }

    return sortAttributes;
  },
  productByTeams(state) {
    return state.productByTeams;
  },

  detailProduct(state) {
    return state.product;
  },

  detailProductImage(state) {
    return state.product_image;
  }
}
