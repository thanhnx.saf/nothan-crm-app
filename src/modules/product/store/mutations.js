import {
  SET_PRODUCT_ALL_BY_TEAM,
  SET_PRODUCT_ATTRIBUTES,
  SET_PRODUCT_USER_ATTRIBUTES,
  SET_PRODUCTS,
  SET_DETAIL_PRODUCT,
  SET_DETAIL_PRODUCT_IMAGE
} from "@/types/mutations.type";

export const mutations = {
  [SET_PRODUCTS](state, products) {
    state.list = products;
  },
  [SET_PRODUCT_ATTRIBUTES](state, attributes) {
    state.attributes = attributes;
    if (state.user_attributes.length > 0) {
      let result = [];
      let listAttrInUser = [];
      state.user_attributes.forEach(function (uAttr) {
        attributes.forEach(function (attr) {
          if (uAttr.attribute_code == attr.attribute_code) {
            attr.is_show = uAttr.is_show;
            attr.sort_order = uAttr.sort_order;
            attr.user_id = uAttr.user_id;
            attr.external_attribute = uAttr.external_attribute;
            attr.code_external_attribute = uAttr.code_external_attribute;
            if (attr.attribute_options && attr.attribute_options.length > 0) {
              attr.is_multiple_value = 1;
            } else {
              attr.is_multiple_value = 0;
            }
            result.push(attr);
            listAttrInUser.push(attr.id);
          }
        })
      });
      let countAttr = state.user_attributes.length;
      let userId = state.user_attributes[0].user_id;
      if (listAttrInUser.length !== attributes.length) {
        let attributeOuts = [];
        attributes.forEach(function (attr) {
          if (listAttrInUser.indexOf(attr.id) === -1) {
            attributeOuts.push(attr);
          }
        });

        attributeOuts.forEach(function (attr) {
          countAttr++;
          let temp_data = {
            attribute_code: attr.attribute_code,
            attribute_id: attr.id,
            attribute_name: attr.attribute_name,
            is_show: 0,
            sort_order: countAttr,
            user_id: userId
          };
          state.user_attributes.push(temp_data);
          attr.is_show = 0;
          attr.sort_order = countAttr;
          attr.user_id = userId;
          result.push(attr);
        });
      }
      state.attributes = result;
    }
  },
  [SET_PRODUCT_USER_ATTRIBUTES](state, attributes) {
    state.user_attributes = attributes;
  },
  [SET_PRODUCT_ALL_BY_TEAM](state, products) {
    state.productByTeams = products;
  },
  [SET_DETAIL_PRODUCT](state, product) {
    state.product = product;
  },
  [SET_DETAIL_PRODUCT_IMAGE](state, file) {
    state.product_image = file;
  }
}
