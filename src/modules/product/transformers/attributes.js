export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformAttributes(item));
    });

    return new_data;
  },

  transformAttributes(data) {
    let transform = {}

    transform = {
      id: data.id,
      attribute_name: data.attribute_name,
      attribute_code: data.attribute_code,
      team_id: data.team_id,
      data_type_id: data.data_type_id,
      is_hidden: data.is_hidden,
      is_required: data.is_required,
      is_default_attribute: data.is_default_attribute,
      default_value: data.default_value,
      is_unique: data.is_unique,
      is_multiple_value: data.is_multiple_value,
      hint: data.hint,
      note: data.note,
      attribute_options: data.attribute_options,
    };

    return transform;
  }
}
