/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import InfoTabHead from "../../components/info-tab/ShowInfoTabHead.vue";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import EditProduct from "../../components/modals/edit/Edit.vue";
import MessageTrashProduct from "../../components/modals/trash-product-message/MessageTrashProduct";
import { PRODUCT_DETAIL, PRODUCT_RESTORE } from "@/types/actions.type";
import helpers from "@/utils/utils";

export default {
  components: {
    "info-tab-head": InfoTabHead,
    EditProduct: EditProduct,
    MessageTrashProduct: MessageTrashProduct
  },

  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(['detailProduct', 'currentTeam'])
  },

  created() {
    let vm = this;
    vm.$bus.$on('productRestore', ($event) => {
      if ($event) {
        $("#restoreProduct").modal('show');
      }
    });
  },

  mounted() {
    let vm = this;
    vm.getShowProduct();
  },

  data() {
    return {
      product: {
      },
      isLoaded: false,
      productId: parseInt(this.$route.params.product_id),
      isLoading: false,
      isChangeData: false
    }
  },

  methods: {
    restore() {
      let vm = this;
      let params = {};

      params.team_id = vm.currentTeamId;
      params.product_id = [vm.productId];
      params.type = 2;

      vm.$store.dispatch(PRODUCT_RESTORE, params).then((response) => {
        if (response.data.data.success) {
          vm.$store.dispatch(PRODUCT_DETAIL, vm.productId);
          $('.nt-list-top-actions a').removeClass('nt-button-disabled');
          $('.nt-list-top-actions .nt-button-restored').addClass('nt-button-hide');
          // $('.nt-list-top-actions .nt-button-restored').css({'opacity': "0"});
          vm.$snotify.success('Phục hồi sản phẩm thành công.', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Phục hồi sản phẩm thất bại.', TOAST_ERROR);
      });
    },

    getShowProduct() {
      let vm = this;
      vm.isLoaded = true;
      vm.$store.dispatch(PRODUCT_DETAIL, vm.productId).then(response => {
        let data = response.data.data;
        if (data) {
          if (data.deleted_at) {
            $('.nt-list-top-actions a').addClass('nt-button-disabled');
            $('.nt-list-top-actions .nt-button-restored').removeClass('nt-button-disabled');
            $('.nt-list-top-actions .nt-button-restored').removeClass('nt-button-hide');
            // $('.nt-list-top-actions .nt-button-restored').css({'opacity': "1"});
          }
          vm.isLoaded = false;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    updateSuccess() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_DETAIL, vm.productId);
    },

    cancelUpdate() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_DETAIL, vm.productId);
    }
  }
}
