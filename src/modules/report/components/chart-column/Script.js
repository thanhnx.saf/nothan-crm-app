import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

export default {
  name: "column-chart",
  props: {
    chartData: {
      type: Object,
      required: true,
      default: () => ({}),
    }
  },
  mounted() {
    let vm = this;
    if (vm.chartData && vm.chartData.data_chart.length > 0 && vm.chartData.chart_exists) {
      vm.createChartColumn();
    }
  },
  methods: {
    createChartColumn() {
      let vm = this;
      let chart = am4core.create(this.$refs.chartColumn, am4charts.XYChart);

      chart.data = vm.chartData.data_chart;
      chart.colors.list = [
        am4core.color("#9CBDFF"),
        am4core.color("#9093F8"),
        am4core.color("#F2D312"),
        am4core.color("#FF9CC4"),
        am4core.color("#EF7D7D"),
        am4core.color("#7BCF44"),
        am4core.color("#F69C31"),
        am4core.color("#BF844F"),
        am4core.color("#A964BE"),
        am4core.color("#D13750"),
        am4core.color("#EBEDF2"),
      ];

      let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

      categoryAxis.dataFields.category = "attribute_value";
      categoryAxis.renderer.grid.template.location = 0;
      categoryAxis.renderer.line.strokeOpacity = 1;
      categoryAxis.renderer.minGridDistance = 30;

      // Configure axis label
      var label = categoryAxis.renderer.labels.template;
      label.truncate = true;
      label.maxWidth = 80;
      label.tooltipText = "{attribute_value}";

      // eslint-disable-next-line no-unused-vars
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      var series1 = chart.series.push(new am4charts.ColumnSeries());
      series1.columns.template.tooltipText = "{categoryX}: {valueY.value}";
      series1.dataFields.categoryX = "attribute_value";
      series1.dataFields.valueY = "quantity";
      if (chart.data.length == 1) {
        series1.columns.template.width = am4core.percent(10);
      } else if (chart.data.length > 1 && chart.data.length <= 3) {
        series1.columns.template.width = am4core.percent(20);
      } else {
        series1.columns.template.width = am4core.percent(60);
      }

      series1.columns.template.adapter.add("fill", (fill, target) => {
        return target.dataItem ? chart.colors.getIndex(target.dataItem.index) : fill;
      });
    }
  }
}
