/* eslint-disable no-undef */
import {
  mapState
} from "vuex";

$("#parentDiv").css("width", "1000px");
$("#parentDiv").css("overflow", "auto");

export default {
  props: {
    chartId: {
      type: String,
      default: ''
    },
    chartName: {
      type: String,
      default: ''
    },
    isShowDatePicker: {
      type: Boolean,
      default: true
    }
  },
  mounted() {
    let vm = this;
    this.checkTimeOrganization();
    let oneDay = 24 * 60 * 60 * 1000;
    let dateCreateOrganization = new Date(vm.currentTeam.created_at);
    let currentDate = new Date();
    let diffDays = Math.round(Math.abs((currentDate.getTime() - dateCreateOrganization.getTime()) / (oneDay)));

    vm.hardChart.start_time = {};
    if (diffDays < 14) {
      vm.hardChart.start_time[0] = dateCreateOrganization;
      vm.hardChart.start_time[1] = new Date();

    } else {
      vm.hardChart.start_time[0] = new Date(Date.parse(new Date(Date.now() - 13 * 24 * 60 * 60 * 1000)));
      vm.hardChart.start_time[1] = new Date();
    }
    let shortcutsThis = $('body .mx-shortcuts-wrapper').find('button:nth-of-type(2)');
    shortcutsThis.addClass('active');
    shortcutsThis.trigger('click');
  },
  data() {
    return {
      today: new Date(),
      typeBtn: 2,
      team_id: parseInt(this.$route.params.team_id),
      lang: {
        days: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
        months: [
          "Tháng 1",
          "Tháng 2",
          "Tháng 3",
          "Tháng 4",
          "Tháng 5",
          "Tháng 6",
          "Tháng 7",
          "Tháng 8",
          "Tháng 9",
          "Tháng 10",
          "Tháng 11",
          "Tháng 12"
        ],
        pickers: [
          "7 ngày tiếp theo",
          "30 ngày tiếp theo ",
          "7 ngày trước",
          "30 ngày trước"
        ],
        placeholder: {
          date: "Chọn ngày",
          dateRange: "Chọn khoảng thời gian"
        },
      },
      shortcuts: [{
        text: '7 ngày gần nhất',
        onClick: () => {
          this.hardChart.start_time = [
            this.sevenDayAgo,
            new Date()
          ];
          this.typeBtn = 1;
          this.chooseRangeTime();

        }
      }, {
        text: '14 ngày gần nhất',
        onClick: () => {
          this.hardChart.start_time = [
            this.fourteenDayAgo,
            new Date()
          ];
          this.typeBtn = 2;
          this.chooseRangeTime();
        }
      }, {
        text: '30 ngày gần nhất',
        onClick: () => {
          this.hardChart.start_time = [
            this.thirtyDayAgo,
            new Date()
          ];
          this.typeBtn = 3;
          this.chooseRangeTime();
        }
      }, {
        text: 'Tùy chỉnh',
        onClick: () => {
          this.typeBtn = 4;
        }
      }],
      hardChart: {
        start_time: {}
      },
      timeSelected: []
    }
  },
  watch: {
    currentTeam: {
      handler() {
        let vm = this;
        let oneDay = 24 * 60 * 60 * 1000;
        let dateCreateOrganization = new Date(vm.currentTeam.created_at);
        let currentDate = new Date();
        let diffDays = Math.round(Math.abs((currentDate.getTime() - dateCreateOrganization.getTime()) / (oneDay)));
        let shortcutsThis = $('body .mx-shortcuts-wrapper').find('button:nth-of-type(2)');
        vm.hardChart.start_time = {};
        if (diffDays < 14) {
          vm.hardChart.start_time[0] = dateCreateOrganization;
          vm.hardChart.start_time[1] = new Date();
        } else {
          vm.hardChart.start_time[0] = new Date(Date.parse(new Date(Date.now() - 13 * 24 * 60 * 60 * 1000)));
          vm.hardChart.start_time[1] = new Date();
        }
        shortcutsThis.trigger('click');
      }
    },

    typeBtn: {
      handler(after) {
        if (after === 1) {
          let shortcuts = $('body .mx-shortcuts-wrapper').find('button');
          let shortcutsThis = $('body .mx-shortcuts-wrapper').find('button:nth-of-type(1)');
          shortcuts.removeClass('active');
          shortcutsThis.addClass('active');
        }
        if (after === 2) {
          let shortcuts = $('body .mx-shortcuts-wrapper').find('button');
          let shortcutsThis = $('body .mx-shortcuts-wrapper').find('button:nth-of-type(2)');
          shortcuts.removeClass('active');
          shortcutsThis.addClass('active');
        }
        if (after === 3) {
          let shortcuts = $('body .mx-shortcuts-wrapper').find('button');
          let shortcutsThis = $('body .mx-shortcuts-wrapper').find('button:nth-of-type(3)');
          shortcuts.removeClass('active');
          shortcutsThis.addClass('active');
        }
      }
    },
    timeSelected: {
      handler(after) {
        if (after.length > 0) {
          this.typeBtn = '';
          let shortcuts = $('body .mx-shortcuts-wrapper').find('button');
          let shortcutsCustom = $('body .mx-shortcuts-wrapper').find('button:nth-of-type(4)');
          shortcuts.removeClass('active');
          shortcutsCustom.addClass('active');
        }
      }
    }

  },
  computed: {
    ...mapState({
      currentTeam: state => state.organization.currentTeam,
    }),
    helpers() {
      return helpers;
    },
    sevenDayAgo() {
      let vm = this;
      let date = '';
      let timeCreatedTeam = Date.parse(vm.currentTeam.created_at);
      let sevenDay = Date.parse(new Date(Date.now() - 6 * 24 * 60 * 60 * 1000));
      if (timeCreatedTeam < sevenDay) {
        date = sevenDay;
      } else {
        date = timeCreatedTeam;
      }
      return new Date(date);
    },
    fourteenDayAgo() {
      let vm = this;
      let date = '';
      let timeCreatedTeam = Date.parse(vm.currentTeam.created_at);
      let fourteenDay = Date.parse(new Date(Date.now() - 13 * 24 * 60 * 60 * 1000));
      if (timeCreatedTeam < fourteenDay) {
        date = fourteenDay;
      } else {
        date = timeCreatedTeam;
      }
      return new Date(date);
    },
    thirtyDayAgo() {
      let vm = this;
      let date = '';
      let timeCreatedTeam = Date.parse(vm.currentTeam.created_at);
      let thirtyDay = Date.parse(new Date(Date.now() - 29 * 24 * 60 * 60 * 1000));
      if (timeCreatedTeam < thirtyDay) {
        date = thirtyDay;
      } else {
        date = timeCreatedTeam;
      }
      return new Date(date);
    },
  },
  methods: {
    chooseRangeTime() {
      let vm = this;
      vm.$emit('ChangeDateTimeSelect', vm.hardChart);
    },
    changeDateTimeSelect(value) {
      let vm = this;
      vm.timeSelected = value;
      vm.hardChart.start_time[0] = value[0];
      vm.hardChart.start_time[1] = value[1];
      vm.$emit('ChangeDateTimeSelect', vm.hardChart);
    },
    clearDateTimeSelect() {
      let shortcuts = $('body .mx-shortcuts-wrapper').find('button');
      shortcuts.removeClass("active");
      this.timeSelected = [];
    },
    checkTimeOrganization() {
      let vm = this;
      let oneDay = 24 * 60 * 60 * 1000;

      let dateCreateOrganization = new Date(vm.currentTeam.created_at);
      let currentDate = new Date();
      let diffDays = Math.round(Math.abs((currentDate.getTime() - dateCreateOrganization.getTime()) / (oneDay)));

      if (diffDays < 14) {
        vm.hardChart.start_time[0] = Date.parse(vm.currentTeam.created_at);
        vm.hardChart.start_time[1] = new Date();
      } else {
        vm.hardChart.start_time[0] = new Date(Date.parse(new Date(Date.now() - 13 * 24 * 60 * 60 * 1000)));
        vm.hardChart.start_time[1] = new Date();
      }
    }
  },
}