import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

am4core.useTheme(am4themes_animated);

export default {
  name: "pie-chart",
  props: {
    service: {
      type: String,
      required: true,
      default: ""
    },
    chartData: {
      type: Object,
      required: true,
      default: () => ({}),
    }
  },
  mounted() {
    let vm = this;
    if (vm.chartData && vm.chartData.data_chart.length > 0 && vm.chartData.chart_exists) {
      vm.createChartPie();
    }
  },
  methods: {
    createChartPie() {
      let vm = this;
      let chart = am4core.create(this.$refs.chartPie, am4charts.PieChart);

      chart.data = vm.chartData.data_chart;
      chart.radius = am4core.percent(50);
      chart.innerRadius = am4core.percent(30);

      let pieSeries = chart.series.push(new am4charts.PieSeries());

      pieSeries.dataFields.value = "quantity";
      pieSeries.dataFields.category = "attribute_value";
      pieSeries.slices.template.fillOpacity = 1;

      pieSeries.colors.list = [
        am4core.color("#9CBDFF"),
        am4core.color("#9093F8"),
        am4core.color("#F2D312"),
        am4core.color("#FF9CC4"),
        am4core.color("#EF7D7D"),
        am4core.color("#7BCF44"),
        am4core.color("#F69C31"),
        am4core.color("#BF844F"),
        am4core.color("#A964BE"),
        am4core.color("#D13750"),
        am4core.color("#EBEDF2"),
      ];

      // Configure axis label
      var label = pieSeries.labels.template;
      if (vm.service == 'customer') {
        label.text = "{attribute_value} ({percent}) \n {quantity}";
      }
      if (vm.service == 'deal') {
        label.text = "{attribute_value} ({percent}) \n {quantity.formatNumber('#.###.##')} đ";
      }
      label.truncate = true;
      label.maxWidth = 230;
      label.tooltipText = "{attribute_value} ({percent})";
    }
  }
}
