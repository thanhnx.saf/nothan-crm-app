import helpers from '@/utils/utils';

export default {
  props: {
    chartData: {
      type: Object,
      required: true,
      default: () => ({}),
    },
    calculator_key: {
      type: String,
      required: true,
      default: ""
    }
  },
  data: function () {
    return {
      maxValue: 0
    }
  },
  mounted() {
    let vm = this;
    vm.findMaxValue();
  },
  computed: {
    helpers() {
      return helpers;
    },
  },
  methods: {
    findMaxValue() {
      let vm = this;

      if (vm.chartData && vm.chartData.data_chart.length > 0 && vm.calculator_key) {
        vm.chartData.data_chart.forEach(function (row) {
          if (vm.maxValue < row[vm.calculator_key]) {
            vm.maxValue = row[vm.calculator_key];
          }
        });
      }
    },
    valueToPercent(value) {
      return ((value / this.maxValue) * 100).toFixed(2);
    },
  }
}
