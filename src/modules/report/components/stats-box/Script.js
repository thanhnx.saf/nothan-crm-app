export default {
  props: {
    data: {
      required: true
    }
  },
  name: "TopStatsBox"
}
