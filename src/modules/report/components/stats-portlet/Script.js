import * as htmlToImage from 'html-to-image';
import { mapGetters } from "vuex";

import {
  EXPORT_CUSTOMER_FLEXIBLE_REPORT,
  EXPORT_CUSTOMER_CHARGE_PERSON_REPORT,
  EXPORT_DEAL_FLEXIBLE_REPORT,
  EXPORT_DEAL_CHARGE_PERSON_REPORT
} from '@/types/actions.type';

export default {
  props: {
    service: {
      type: String,
      required: true,
      default: ""
    },
    chart_title: {
      type: String,
      required: true,
      default: ''
    },
    attribute_id: {
      type: [String, Number],
      required: true
    },
    chart_exists: {
      type: Boolean,
      required: true,
      default: true
    }
  },
  computed: {
    ...mapGetters(["currentTeam", "currentUser"])
  },
  data: function () {
    return {
      currentView: "chart",
      chartImageExport: ''
    }
  },
  methods: {
    changeView(viewType) {
      let vm = this;
      vm.currentView = viewType;
    },
    exportImageChart() {
      let vm = this;

      let chartElementHTML = document.getElementById('report_attribute_' + vm.attribute_id);

      htmlToImage.toPng(chartElementHTML)
        .then(function (dataUrl) {
          var link = document.createElement('a');
          link.download = vm.chart_title + '.png';
          link.href = dataUrl;
          link.click();
        });
    },
    exportToExcel() {
      let vm = this;
      let apiRequest = '';

      if (vm.currentTeamId && vm.attribute_id) {
        let params = {
          team_id: vm.currentTeamId,
          attribute_id: vm.attribute_id,
        };

        if (vm.service && vm.service == "customer") {
          if (vm.attribute_id == 'charge_person') {
            apiRequest = EXPORT_CUSTOMER_CHARGE_PERSON_REPORT;
          } else {
            apiRequest = EXPORT_CUSTOMER_FLEXIBLE_REPORT;
          }
        }
        if (vm.service && vm.service == "deal") {
          if (vm.attribute_id == 'charge_person') {
            apiRequest = EXPORT_DEAL_CHARGE_PERSON_REPORT;
          } else {
            apiRequest = EXPORT_DEAL_FLEXIBLE_REPORT;
          }
        }

        vm.$store.dispatch(apiRequest, params).then((response) => {
          if (response.data && response.data.status_code != 422) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');

            link.href = url;
            link.setAttribute('download', vm.chart_title + '.xlsx');
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
          }
        }).catch(error => {
          console.log(error);
        });
      }
    }
  }
}
