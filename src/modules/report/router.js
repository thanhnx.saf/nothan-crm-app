import { PERMISSION_STATEMENT_CUSTOMER, PERMISSION_STATEMENT_SALE } from '@/types/const'
import ReportCustomer from '@/modules/customer/views/report/Report.vue'
import ReportDeal from '@/modules/deal/views/report/Report.vue'

export default [{
  name: "reportsCustomer",
  path: "/reports-customer",
  component: ReportCustomer,
  meta: {
    breadcrumb: [{
      name: "Thống kê báo cáo",
    }, {
      name: "Báo cáo khách hàng",
    }],
    permissionName: PERMISSION_STATEMENT_CUSTOMER,
    serviceName: "report"
  }
}, {
  name: "reportsDeal",
  path: "/reports-deal",
  component: ReportDeal,
  meta: {
    breadcrumb: [{
      name: "Thống kê báo cáo",
    }, {
      name: "Báo cáo giao dịch",
    }],
    permissionName: PERMISSION_STATEMENT_SALE,
    serviceName: "report"
  }
}]
