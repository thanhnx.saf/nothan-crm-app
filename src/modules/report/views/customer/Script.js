import { mapGetters } from "vuex";

import StatsPortlet from "../../components/stats-portlet/StatsPortlet";
import PieChart from "../../components/chart-pie/ChartPie";
import ColumnChart from "../../components/chart-column/ChartColumn";
import ColumnTable from "../../components/chart-table/ChartTable";

import {
  GET_CUSTOMER_FLEXIBLE_REPORT,
  GET_CUSTOMER_CHARGE_PERSON_REPORT
} from '@/types/actions.type'

export default {
  name: "reports-customer",
  props: {
    attributeIds: {
      type: Array,
      required: true,
      default: () => []
    }
  },
  watch: {
    attributeIds: {
      handler(after) {
        if (after) {
          this.loadDataChartAttributes(after);
        }
      },
      deep: true
    }
  },
  components: {
    "stats-portlet": StatsPortlet,
    "pie-chart": PieChart,
    "column-chart": ColumnChart,
    "table-chart": ColumnTable
  },
  mounted() {
    let vm = this;
    vm.loadDataChartAttributes(vm.attributeIds);
  },
  computed: {
    ...mapGetters(["currentTeam", "listUsers"])
  },
  created() {

  },
  data() {
    return {
      flexibleReportData: [],
      columns: []
    }
  },
  methods: {
    lowerCaseAttributeName(attributeName) {
      let lowerCaseString = '';

      if (attributeName) {
        lowerCaseString = attributeName.toLowerCase();
      }
      return lowerCaseString;
    },
    async loadDataChartAttributes(attributeIds) {
      let vm = this;
      let apiRequest = '';
      let attributes = {};

      vm.flexibleReportData = [];

      if (attributeIds && attributeIds.length > 0) {

        attributeIds.forEach(function (id) {
          if (!attributes[id]) {
            attributes[id] = id;
          }
        });

        let responseData = attributeIds.map(async id => {
          let params = {
            team_id: vm.currentTeamId,
            attribute_id: id,
          };

          if (id == 'charge_person') {
            apiRequest = GET_CUSTOMER_CHARGE_PERSON_REPORT;
          } else {
            apiRequest = GET_CUSTOMER_FLEXIBLE_REPORT;
          }

          const response = await vm.$store.dispatch(apiRequest, params);
          let data = response.data.data;
          data.attribute_id = id;
          data.columns_table = vm.buildFieldsTable(data.name);

          return data ? data : {};
        });

        let flexibleReportDataObj = {};
        let flexibleReportDataArr = [];
        const flexibleReportData = await Promise.all(responseData);

        if (flexibleReportData && flexibleReportData.length > 0) {
          flexibleReportData.forEach(function (reportData) {
            let chartQuantityTotal = 0;

            if (reportData.data_chart) {
              reportData.data_chart.forEach(function (chartItem) {
                chartQuantityTotal += chartItem.quantity;
              });
            }

            if (chartQuantityTotal == 0) {
              reportData.chart_exists = false;
              reportData.container_text = "Chưa có khách hàng nào trong dữ liệu";
            } else {
              reportData.chart_exists = true;
            }

            if (!flexibleReportDataObj[reportData.attribute_id]) {
              flexibleReportDataObj[reportData.attribute_id] = reportData;
            }
          })
        }

        if (attributes && Object.keys(attributes).length > 0) {
          Object.keys(attributes).forEach(function (attributeId) {
            flexibleReportDataArr.push(flexibleReportDataObj[attributeId]);
          });
        }
        vm.flexibleReportData = flexibleReportDataArr;
      }
    },
    isObjectExistInArray(obj, list) {
      if (list) {
        list.forEach(function (item) {
          if (list.hasOwnProperty(item) && list[item] === obj) {
            return true;
          }
        });
      }
      return false;
    },
    buildFieldsTable(attributeName) {
      return [
        {
          name: attributeName,
          key: "attribute_value",
        },
        {
          name: "Số lượng khách hàng",
          key: "quantity",
        }
      ];
    }
  }
}
