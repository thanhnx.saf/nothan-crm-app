import {
  mapGetters
} from "vuex"
import moment from 'moment'

import StatsPortlet from "../../components/stats-portlet/StatsPortlet";
import PieChart from "../../components/chart-pie/ChartPie";
import ColumnChart from "../../components/chart-column/ChartColumn";
import ColumnTable from "../../components/chart-table/ChartTable";

import {
  GET_DEAL_FLEXIBLE_REPORT,
  GET_DEAL_CHARGE_PERSON_REPORT
} from '@/types/actions.type'

export default {
  name: "reports-deal",
  props: {
    attributeIds: {
      type: Array,
      required: true,
      default: () => []
    },
    range: {
      type: Object,
      default: {
        from: moment().format('YYYY-MM-DD'),
        to: moment().format('YYYY-MM-DD')
      }
    },
    isContainDealCancel: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    attributeIds: {
      handler(after) {
        if (after) {
          this.loadDataChartAttributes(after);
        }
      },
      deep: true
    },
    range: {
      handler() {
        this.loadDataChartAttributes()
      },
      deep: true
    },
    isContainDealCancel: {
      handler() {
        this.loadDataChartAttributes()
      },
      deep: true
    },
  },
  components: {
    "stats-portlet": StatsPortlet,
    "pie-chart": PieChart,
    "column-chart": ColumnChart,
    "table-chart": ColumnTable
  },
  mounted() {
    let vm = this;
    vm.loadDataChartAttributes(vm.attributeIds);
  },
  computed: {
    ...mapGetters(["currentTeam"])
  },
  data() {
    return {
      flexibleReportData: [],
      columns: [],
      savedAttributeIds: []
    }
  },
  methods: {
    lowerCaseAttributeName(attributeName) {
      let lowerCaseString = '';

      if (attributeName) {
        lowerCaseString = attributeName.toLowerCase();
      }
      return lowerCaseString;
    },
    async loadDataChartAttributes(attributeIds) {
      this.savedAttributeIds = attributeIds || this.savedAttributeIds
      let vm = this
      let apiRequest = ''
      let attributes = {}
      vm.flexibleReportData = []

      if (!this.savedAttributeIds && !this.savedAttributeIds.length) {
        return
      }

      this.savedAttributeIds.forEach(function (id) {
        if (!attributes[id]) {
          attributes[id] = id;
        }
      })

      let responseData = this.savedAttributeIds.map(async id => {
        let params = {
          team_id: vm.currentTeamId,
          attribute_id: id,
          ...this.range,
          is_contain_deal_cancel: this.isContainDealCancel
        }

        if (id == 'charge_person') {
          apiRequest = GET_DEAL_CHARGE_PERSON_REPORT
        } else {
          apiRequest = GET_DEAL_FLEXIBLE_REPORT
        }

        const response = await vm.$store.dispatch(apiRequest, params)
        let data = response.data.data
        data.attribute_id = id
        data.columns_table = vm.buildFieldsTable(data.name)

        return data ? data : {}
      })

      let flexibleReportDataObj = {}
      let flexibleReportDataArr = []
      const flexibleReportData = await Promise.all(responseData)

      if (flexibleReportData && flexibleReportData.length > 0) {
        flexibleReportData.forEach(function (reportData) {
          let chartQuantityTotal = 0;

          if (reportData.data_chart.length > 0) {
            reportData.data_chart.forEach(function (chartItem) {
              chartQuantityTotal += chartItem.quantity;
            });
          }

          if (chartQuantityTotal == 0) {
            reportData.chart_exists = false;
            reportData.container_text = "Tổng các giá trị giao dịch bằng 0";
          } else {
            reportData.chart_exists = true;
          }

          if (!flexibleReportDataObj[reportData.attribute_id]) {
            flexibleReportDataObj[reportData.attribute_id] = reportData;
          }
        })
      }

      if (attributes && Object.keys(attributes).length > 0) {
        Object.keys(attributes).forEach(function (attributeId) {
          flexibleReportDataArr.push(flexibleReportDataObj[attributeId]);
        });
      }
      vm.flexibleReportData = flexibleReportDataArr
    },
    isObjectExistInArray(obj, list) {
      if (list) {
        list.forEach(function (item) {
          if (list.hasOwnProperty(item) && list[item] === obj) {
            return true;
          }
        })
      }
      return false
    },
    buildFieldsTable(attributeName) {
      return [{
        name: attributeName,
        key: "attribute_value",
      }, {
        name: "Giá trị giao dịch (đ)",
        key: "quantity",
      }]
    }
  }
}