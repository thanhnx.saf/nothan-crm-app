/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import {
  CUSTOMER_CREATE_ATTRIBUTE,
  CUSTOMER_ADD_ATTRIBUTE_CHECK_EXIST
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE,
  IS_STATUS_BAR,
  CUSTOMER_DETAIL_BOX_GENERAL,
  CUSTOMER_DETAIL_BOX_BUY,
  CUSTOMER_DETAIL_BOX_USE
} from "@/types/const";

const customMessagesAddAttributeCustom = {
  custom: {
    'attribute_name': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'data_type_id': {
      required: 'Không được để trống trường này.'
    },
    'attribute_options': {
      required: 'Không được để trống trường này.'
    },
    'customer_types': {
      required: 'Không được để trống trường này.'
    },
    'show_position': {
      required: 'Không được để trống trường này.'
    }
  }
};

const doneTypingInterval = 500;

export default {
  mounted() {
    let vm = this;
    vm.$validator.localize('en', customMessagesAddAttributeCustom);
  },
  watch: {
    attribute: {
      handler() {

      },
      deep: true
    },
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["attributeDataType", "currentTeam"]),
  },
  data() {
    return {
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
      isStatusBar: IS_STATUS_BAR,
      attribute: {
        attribute_name: '',
        attribute_code: '',
        data_type_id: '',
        is_hidden: 0,
        is_required: 0,
        is_unique: 0,
        is_multiple_value: 0,
        attribute_options: [],
        show_position: '',
        team_id: ''
        //customer_types: []
      },
      isLoader: false,
      isSuccess: false,
      server_errors: '',
      attributeOptions: [],
      listExistsAttributeOptions: [],
      customerTypes: [
        { id: 1, name: 'Cá nhân' },
        { id: 2, name: 'Tổ chức' },
      ],
      isDisabledRequired: false,
      isDisabledUnique: true,
      isLoaderCheckExists: false,
      nameIsExists: false,

      CUSTOMER_DETAIL_BOX_GENERAL: CUSTOMER_DETAIL_BOX_GENERAL,
      CUSTOMER_DETAIL_BOX_BUY: CUSTOMER_DETAIL_BOX_BUY,
      CUSTOMER_DETAIL_BOX_USE: CUSTOMER_DETAIL_BOX_USE
    }
  },
  methods: {
    getAttributeCode(attribute_name) {
      let vm = this;

      attribute_name = attribute_name ? attribute_name : "";

      let attribute_code = attribute_name ? helpers.changeAlias(attribute_name).replace(/ /g, '_') : "";

      vm.attribute.attribute_code = attribute_code;

      // Check name exists
      vm.checkNameExists(attribute_name);
    },

    create(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;

          if (vm.attributeOptions.length > 0) {
            let attributeOptions = [];
            vm.attributeOptions.forEach(function (item) {
              if (item.name) {
                attributeOptions.push(item.name);
              }
            });
            vm.attribute.attribute_options = attributeOptions;
          }

          if (vm.attribute.data_type_id == vm.isMultipleSelect || vm.attribute.data_type_id == vm.isCheckboxMultiple || vm.attribute.data_type_id == vm.isTag || vm.attribute.data_type_id == vm.isUserMultiple) {
            vm.attribute.is_multiple_value = 1;
          } else {
            vm.attribute.is_multiple_value = 0;
          }

          vm.attribute.is_required = (vm.attribute.is_required || vm.attribute.is_required == true) ? 1 : 0;
          vm.attribute.is_unique = (vm.attribute.is_unique || vm.attribute.is_unique == true) ? 1 : 0;
          vm.attribute.team_id = vm.currentTeamId;

          vm.$store.dispatch(CUSTOMER_CREATE_ATTRIBUTE, vm.attribute).then(response => {
            if (response.data.status_code && response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              vm.$snotify.error('Thêm mới trường dữ liệu thất bại.', TOAST_ERROR);
            } else {
              vm.isSuccess = true;
              vm.$emit('is_success', true);
              vm.$snotify.success('Thêm mới trường dữ liệu thành công.', TOAST_SUCCESS);
              vm.resetAddAttributeCustom();

              $(".s--modal .modal-body").animate({ scrollTop: 0 }, 300);
              // $('.s--alert').slideDown();
              setTimeout(function () {
                vm.isSuccess = false;
              }, 5000);
            }

            vm.isLoader = false;
          }).catch(error => {
            vm.isLoader = false;
            vm.$snotify.error('Thêm mới trường dữ liệu thất bại.', TOAST_ERROR);
            console.log(error);
          });
        }
      });
    },

    resetAddAttributeCustom() {
      let vm = this;
      vm.$validator.reset();
      vm.server_errors = '';
      vm.attribute = {
        attribute_name: '',
        attribute_code: '',
        data_type_id: '',
        is_hidden: 0,
        is_required: 0,
        is_unique: 0,
        is_multiple_value: 0,
        attribute_options: [],
        show_position: '',
        team_id: ''
        //customer_types: []
      };
      vm.attributeOptions = [];
      vm.listExistsAttributeOptions = [];
      vm.nameIsExists = false;
    },

    reset() {
      let vm = this;
      vm.isSuccess = false;
      vm.resetAddAttributeCustom();
    },

    changeAttributeOptions(value) {
      let vm = this;

      value = value ? value.replace(/\r?\n/g, '<br>') : "";
      let value_array = value ? (value.split('<br>')) : [];
      let new_value_array = [];

      vm.attributeOptions = [];

      if (value_array.length > 0) {
        value_array.forEach(function (item, index) {
          if (item) {
            vm.attributeOptions.push({ id: index + 1, name: item });
            new_value_array.push(item);
          }
        });

        vm.listExistsAttributeOptions = Object.values(new_value_array.reduce((data, item) => {
          if ((item).trim() != "" || (item).trim() != null) {
            let key = (item).trim().toLowerCase();
            data[key] = data[key] || [];
            data[key].push(item);
            return data;
          }
        }, {})).reduce((data, item) => item.length > 1 ? data.concat(item) : data, []);
      }
    },

    checkNameExists(value) {
      let vm = this;
      vm.nameIsExists = false;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        vm.changeDataInputType('attribute_name');
        vm.isLoaderCheckExists = true;
        if (value) {
          let data = {
            attribute_name: "attribute_name",
            attribute_value: value
          }
          vm.$store.dispatch(CUSTOMER_ADD_ATTRIBUTE_CHECK_EXIST, data).then(response => {
            if (response.data.meta) {
              if (response.data.meta.is_exists) {
                vm.nameIsExists = true;
              }
              vm.isLoaderCheckExists = false;
            }
          }).catch(error => {
            vm.isLoaderCheckExists = false;
            console.log(error);
          });
        } else {
          vm.nameIsExists = false;
          vm.isLoaderCheckExists = false;
        }
      }, doneTypingInterval);
    },

    changeDataTypeId() {
      let vm = this;
      vm.isDisabledRequired = false;
      vm.isDisabledUnique = false;
      vm.attribute.is_required = 0;
      vm.attribute.is_unique = 0;
      if (vm.attribute.data_type_id == vm.isRadio) {
        vm.isDisabledRequired = true;
        vm.attribute.is_required = 1;
      } else if (vm.attribute.data_type_id == vm.isText || vm.attribute.data_type_id == vm.isEmail || vm.attribute.data_type_id == vm.isPhone) {
        vm.isDisabledUnique = true;
      }
      vm.changeDataInputType('data_type_id');
    },

    changeDataInputType(attribute_code) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute_code]) {
          vm.server_errors[attribute_code] = '';
        }
      }, doneTypingInterval);
    }

  }
};
