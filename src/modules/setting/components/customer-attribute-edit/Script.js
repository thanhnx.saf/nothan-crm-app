/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import {
  CUSTOMER_UPDATE_ATTRIBUTE,
  CUSTOMER_GET_ATTRIBUTES
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE,
  IS_STATUS_BAR,
  CUSTOMER_DETAIL_BOX_GENERAL,
  CUSTOMER_DETAIL_BOX_BUY,
  CUSTOMER_DETAIL_BOX_USE
} from "@/types/const";

const customMessagesUpdateAttributeCustom = {
  custom: {
    'attribute_name': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'data_type_id': {
      required: 'Không được để trống trường này.'
    },
    'attribute_options': {
      required: 'Không được để trống trường này.'
    },
    'customer_types': {
      required: 'Không được để trống trường này.'
    },
    'show_position': {
      required: 'Không được để trống trường này.'
    }
  }
};
const doneTypingInterval = 500;

export default {
  props: {
    attribute: {
      type: Object,
      default: {}
    },
  },
  components: {
  },
  mounted() {
    let vm = this;
    vm.$validator.localize('en', customMessagesUpdateAttributeCustom);
    vm.checkDisabledProperty(vm.attribute);
  },
  watch: {
    attribute: {
      handler(after) {
        let vm = this;
        if (after) {
          vm.checkDisabledProperty(after);

          let countChange = 0;

          if (after.attribute_name != after.attribute_name_cache) {
            countChange++;
          }

          if (after.is_required != after.is_required_cache) {
            countChange++;
          }

          if (after.is_unique != after.is_unique_cache) {
            countChange++;
          }

          if (after.is_hidden != after.is_hidden_cache) {
            countChange++;
          }

          if (after.attribute_options != after.attribute_options_cache) {
            countChange++;
          }

          if (after.show_position != after.show_position_cache) {
            countChange++;
          }

          if (countChange) {
            vm.isChange = true;
          } else {
            vm.isChange = false;
          }
        }
      },
      deep: true
    },
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["attributeDataType", "currentTeam"]),
  },
  data() {
    return {
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
      isStatusBar: IS_STATUS_BAR,

      CUSTOMER_DETAIL_BOX_GENERAL: CUSTOMER_DETAIL_BOX_GENERAL,
      CUSTOMER_DETAIL_BOX_BUY: CUSTOMER_DETAIL_BOX_BUY,
      CUSTOMER_DETAIL_BOX_USE: CUSTOMER_DETAIL_BOX_USE,

      modelCustomer: {
        level: "cap_do_khach_hang"
      },
      isLoader: false,
      isSuccess: false,
      server_errors: '',
      convertAttributeOptions: [],
      listExistsAttributeOptions: [],
      isDisabledRequired: false,
      isDisabledUnique: false,
      isChange: false
    }
  },
  methods: {
    checkDisabledProperty(data) {
      let vm = this;
      vm.isDisabledRequired = false;
      vm.isDisabledUnique = false;
      if (data.data_type_id == vm.isRadio) {
        vm.isDisabledRequired = true;
      } else if (data.data_type_id == vm.isText || data.data_type_id == vm.isEmail || data.data_type_id == vm.isPhone) {
        vm.isDisabledUnique = true;
      }
    },

    getAttributeCode(attribute_name) {
      let vm = this;

      attribute_name = attribute_name ? attribute_name : "";

      let attribute_code = attribute_name ? helpers.changeAlias(attribute_name).replace(/ /g, '_') : "";

      vm.attribute.attribute_code = attribute_code;

      vm.changeDataInputType('attribute_name');
    },
    update(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;

          if (vm.attribute.attribute_options) {
            let attributeOptions = "";
            attributeOptions = vm.attribute.attribute_options.split('\n');

            let sortAttributeOptions = [];
            if (attributeOptions && attributeOptions.length > 0) {
              attributeOptions.forEach(function (option, index) {
                sortAttributeOptions.push({
                  option_value: option,
                  sort_order: index + 1
                });
              });
            }

            vm.attribute.attribute_options = vm.mapAttributeOptionsPushToServer(sortAttributeOptions);
          }

          if (vm.attribute.data_type_id == vm.isMultipleSelect || vm.attribute.data_type_id == vm.isCheckboxMultiple || vm.attribute.data_type_id == vm.isTag || vm.attribute.data_type_id == vm.isUserMultiple) {
            vm.attribute.is_multiple_value = 1;
          } else {
            vm.attribute.is_multiple_value = 0;
          }

          vm.attribute.is_required = (vm.attribute.is_required || vm.attribute.is_required == true) ? 1 : 0;
          vm.attribute.is_unique = (vm.attribute.is_unique || vm.attribute.is_unique == true) ? 1 : 0;
          vm.attribute.team_id = vm.currentTeamId;

          vm.$store.dispatch(CUSTOMER_UPDATE_ATTRIBUTE, vm.attribute).then(response => {
            if (response.data.status_code && response.data.status_code == 422) {
              vm.server_errors = response.data.errors;
              vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
            } else {
              vm.isSuccess = true;
              vm.$snotify.success('Cập nhật trường dữ liệu thành công.', TOAST_SUCCESS);
              vm.resetEditAttributeCustom();

              $(".s--modal .modal-body").animate({ scrollTop: 0 }, 300);


              setTimeout(function () {
                vm.isSuccess = false;
              }, 5000);
            }

            vm.resetAttributeToUpdateSuccess().then(function () {
              vm.isLoader = false;
            });
          }).catch(() => {
            vm.isLoader = false;
            vm.resetAttributeToUpdateSuccess();
            vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
          });
        }
      });
    },

    async resetAttributeToUpdateSuccess() {
      let vm = this;
      await vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then(function (response) {
        let data = response.data.data;
        if (data && data.length > 0) {
          data.forEach(function (attribute) {
            if (attribute.id == vm.attribute.id) {
              let attributeOptions = [];
              attributeOptions = attribute.attribute_options;

              attributeOptions.sort(function (before, after) {
                return before.sort_order - after.sort_order;
              });

              // Set cached
              vm.attribute.attribute_name_cache = attribute.attribute_name;
              vm.attribute.attribute_code_cache = attribute.attribute_code;
              vm.attribute.is_required_cache = attribute.is_required;
              vm.attribute.is_unique_cache = attribute.is_unique;
              vm.attribute.is_hidden_cache = attribute.is_hidden;
              vm.attribute.show_position_cache = attribute.show_position;
              vm.attribute.attribute_options_full = attribute.attribute_options;
              let attributeOptionsNew = '';
              if (attributeOptions && attributeOptions.length > 0) {
                attribute.attribute_options.forEach(function (item) {
                  attributeOptionsNew += item.option_value + '\n';
                });
              }
              vm.attribute.attribute_options_cache = attributeOptionsNew;
              vm.attribute.attribute_options = attributeOptionsNew;
            }
          });
        }
      });
    },

    mapAttributeOptionsPushToServer(data) {
      let vm = this;
      let newData = {};
      let newArrayData = [];

      if (data && data.length > 0) {
        data.forEach(function (item) {
          if (vm.attribute.attribute_options_full.length > 0) {
            vm.attribute.attribute_options_full.forEach(function (option) {
              if (item.option_value) {
                if (item.option_value.trim().toLowerCase() == option.option_value.trim().toLowerCase()) {
                  newData[item.option_value] = {
                    id: option.id,
                    option_value: item.option_value,
                    sort: item.sort_order
                  }
                } else {
                  if (!newData[item.option_value]) {
                    newData[item.option_value] = {
                      id: "",
                      option_value: item.option_value,
                      sort: item.sort_order
                    }
                  }
                }
              }
            });
          } else {
            newData[item.option_value] = {
              id: "",
              option_value: item.option_value
            }
          }
        });

        Object.keys(newData).forEach(function (index) {
          newArrayData.push(newData[index]);
        });

        if (newArrayData && newArrayData.length > 0) {
          newArrayData.sort(function (before, after) {
            return before.sort - after.sort;
          });
        }
      }

      return newArrayData;
    },

    resetEditAttributeCustom() {
      let vm = this;
      vm.$validator.reset();
      vm.server_errors = '';
      vm.listExistsAttributeOptions = [];
    },

    reset() {
      let vm = this;
      vm.isSuccess = false;
      vm.resetEditAttributeCustom();
      vm.$emit('is_cancel', true);
    },

    changeAttributeOptions(value) {
      let vm = this;
      value = value ? value.replace(/\r?\n/g, '<br>') : "";
      let value_array = value ? (value.split('<br>')) : [];
      let new_value_array = [];

      value_array.forEach(function (item) {
        if (item) {
          new_value_array.push(item);
        }
      });

      vm.convertAttributeOptions = [];

      if (value_array.length > 0) {
        // value_array.forEach(function (item, index) {
        //     if(item){
        //         vm.convertAttributeOptions.push({id: index + 1, name: item});
        //     }
        // });

        vm.listExistsAttributeOptions = Object.values(new_value_array.reduce((data, item) => {
          if ((item).trim() != "" || (item).trim() != null) {
            let key = (item).trim().toLowerCase();
            data[key] = data[key] || [];
            data[key].push(item);
            return data;
          }
        }, {})).reduce((data, item) => item.length > 1 ? data.concat(item) : data, []);
      }
    },

    changeDataInputType(attribute_code) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute_code]) {
          vm.server_errors[attribute_code] = '';
        }
      }, doneTypingInterval);
    },
  }
};
