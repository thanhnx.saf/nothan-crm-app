/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import AddAttributeCustom from '../customer-attribute-add/AddAttributeCustom.vue';
import EditAttributeCustom from '../customer-attribute-edit/EditAttributeCustom.vue';
import {
  CUSTOMER_GET_ATTRIBUTES,
  CUSTOMER_DELETE_ATTRIBUTE,
  CUSTOMER_GET_ATTRIBUTE_DATA_TYPE,
  CUSTOMER_UPDATE_ATTRIBUTE
} from "@/types/actions.type";
import {
  IS_TEXT,
  IS_EMAIL,
  IS_PHONE,
  IS_RADIO,
  // CUSTOMER
  CUSTOMER_DETAIL_BOX_GENERAL,
  CUSTOMER_DETAIL_BOX_BUY,
  CUSTOMER_DETAIL_BOX_USE
} from "@/types/const";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  components: {
    AddAttributeCustom,
    EditAttributeCustom,
  },
  mounted() {
    let vm = this;
    vm.getAttributeDataType();
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["attributeCustom", "currentTeam"]),
  },
  data() {
    return {
      isText: IS_TEXT,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isRadio: IS_RADIO,
      isLoader: false,
      indexChangeRequiredCustomAttribute: null,
      indexChangeUniqueCustomAttribute: null,
      indexChangeShowHideCustomAttribute: null,
      indexDeleteCustomAttribute: null,
      attributeSelectItem: null,
      isDisabledRequired: false,
      isDisabledUnique: false,

      CUSTOMER_DETAIL_BOX_GENERAL: CUSTOMER_DETAIL_BOX_GENERAL,
      CUSTOMER_DETAIL_BOX_BUY: CUSTOMER_DETAIL_BOX_BUY,
      CUSTOMER_DETAIL_BOX_USE: CUSTOMER_DETAIL_BOX_USE
    }
  },
  methods: {
    getAttributeDataType() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTE_DATA_TYPE);
    },
    getAttributesNotLoader() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES);
    },
    showModalChangeRequired(index) {
      let vm = this;
      vm.indexChangeRequiredCustomAttribute = index;
      setTimeout(function () { $('.s--modal-change-required-custom-attribute').modal('show'); }, 100);

    },
    showModalChangeUnique(index) {
      let vm = this;
      vm.indexChangeUniqueCustomAttribute = index;
      setTimeout(function () { $('.s--modal-change-unique-custom-attribute').modal('show'); }, 100);
    },
    showModalChangeHideShow(index) {
      let vm = this;
      vm.indexChangeShowHideCustomAttribute = index;
      setTimeout(function () { $('.s--modal-change-show-hide-custom-attribute').modal('show'); }, 100);
    },
    showModalDelete(index) {
      let vm = this;
      vm.indexDeleteCustomAttribute = index;
      setTimeout(function () { $('.s--modal-delete-custom-attribute').modal('show'); }, 100);
    },
    showModalEdit(index) {
      let vm = this;
      let attribute = vm.attributeCustom[index];
      vm.attributeSelectItem = vm.transformAttributeToEdit(attribute);
      setTimeout(function () { $('#edit_attribute_custom').modal('show'); }, 100);
    },
    requiredAttributeItem(attribute) {
      let vm = this;

      attribute.is_required = attribute && attribute.is_required ? 0 : 1;

      vm.$store.dispatch(CUSTOMER_UPDATE_ATTRIBUTE, attribute).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
        } else {
          vm.$snotify.success('Cập nhật trường dữ liệu thành công.', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
      });
    },
    uniqueAttributeItem(attribute) {
      let vm = this;

      attribute.is_unique = attribute && attribute.is_unique ? 0 : 1;

      vm.$store.dispatch(CUSTOMER_UPDATE_ATTRIBUTE, attribute).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
        } else {
          vm.$snotify.success('Cập nhật trường dữ liệu thành công.', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
      });
    },
    showHideAttributeItem(attribute) {
      let vm = this;

      attribute.is_hidden = attribute && attribute.is_hidden ? 0 : 1;

      vm.$store.dispatch(CUSTOMER_UPDATE_ATTRIBUTE, attribute).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
        } else {
          vm.$snotify.success('Cập nhật trường dữ liệu thành công.', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Cập nhật trường dữ liệu thất bại.', TOAST_ERROR);
      });
    },
    deleteCustomerAttribute(attribute) {
      let vm = this;
      let params = {};
      params.id = attribute.id;
      params.team_id = vm.currentTeamId;
      vm.$store.dispatch(CUSTOMER_DELETE_ATTRIBUTE, params).then((response) => {
        if (response.data.data.success) {
          vm.getAttributesNotLoader();
          vm.indexChangeRequiredCustomAttribute = null;
          vm.indexChangeUniqueCustomAttribute = null;
          vm.indexChangeShowHideCustomAttribute = null;
          vm.indexDeleteCustomAttribute = null;
          vm.$snotify.success('Xóa vĩnh viễn trường tùy chỉnh thành công.', TOAST_SUCCESS);
        } else {
          vm.$snotify.error('Xóa vĩnh viễn trường tùy chỉnh thất bại.', TOAST_ERROR);
        }
      });
    },
    isSuccess(isSuccess) {
      let vm = this;
      if (isSuccess) {
        vm.getAttributesNotLoader();
      }
    },
    isCancel(isCancel) {
      let vm = this;
      if (isCancel) {
        vm.getAttributesNotLoader();
      }
    },
    transformAttributeToEdit(attribute) {
      let attributeSelectItem = {};
      attributeSelectItem.id = attribute.id;
      attributeSelectItem.data_type_id = attribute.data_type_id;
      attributeSelectItem.attribute_name = attribute.attribute_name;
      attributeSelectItem.attribute_code = attribute.attribute_code;
      attributeSelectItem.is_required = attribute.is_required;
      attributeSelectItem.is_unique = attribute.is_unique;
      attributeSelectItem.is_hidden = attribute.is_hidden;
      attributeSelectItem.show_position = attribute.show_position;
      // Set cached
      attributeSelectItem.attribute_name_cache = attribute.attribute_name;
      attributeSelectItem.attribute_code_cache = attribute.attribute_code;
      attributeSelectItem.is_required_cache = attribute.is_required;
      attributeSelectItem.is_unique_cache = attribute.is_unique;
      attributeSelectItem.is_hidden_cache = attribute.is_hidden;
      attributeSelectItem.show_position_cache = attribute.show_position;

      let sortAttributeOptions = [];
      sortAttributeOptions = attribute.attribute_options;
      sortAttributeOptions.sort(function (before, after) {
        return before.sort_order - after.sort_order;
      });

      let attributeOptions = '';
      if (sortAttributeOptions && sortAttributeOptions.length > 0) {
        attribute.attribute_options.forEach(function (item) {
          attributeOptions += item.option_value + '\n';
        });
      }
      attributeSelectItem.attribute_options_cache = attributeOptions;

      attributeSelectItem.attribute_options = attributeOptions;
      attributeSelectItem.attribute_options_full = attribute.attribute_options;

      return attributeSelectItem;
    }
  }
};
