/* eslint-disable no-undef */
import {
  CUSTOMER_GET_ATTRIBUTE_OPTION,
  CUSTOMER_SAVE_ATTRIBUTE_OPTION
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import MultipleLevelComponent from "../multiple-levels/MultipleLevelComponent.vue";

const messageSettings = {
  custom: {
    "nt-settings-add-attribute-option-title": {
      required: "Không được để trống trường này.",
      max: "Giới hạn 250 ký tự"
    },
    "nt-settings-add-attribute-option-description": {
      max: "Giới hạn 1025 ký tự"
    },
    "nt-settings-edit-attribute-option-title": {
      required: "Không được để trống trường này.",
      max: "Giới hạn 250 ký tự"
    },
    "nt-settings-edit-attribute-option-description": {
      max: "Giới hạn 1025 ký tự"
    }
  }
};

export default {
  props: {
    attribute_id: {
      default: '',
      required: true,
      type: [Number, String]
    },
    slot_option: {
      default: '',
      required: true,
      type: String
    },
    component: {
      default: '',
      required: true,
      type: String
    }
  },
  components: {
    "multiple-level-component": MultipleLevelComponent
  },
  mounted() {
    let vm = this;
    vm.getAttributeOptions(vm.attribute_id);
    vm.$validator.localize("en", messageSettings);
  },
  data() {
    return {
      isLoader: false,
      isLoadData: false,
      convert_data_list: [],
      new_data_list: [],
      data_list: [],
      server_errors: "",
      addData: {
        title: "",
        description: ""
      },
      selectedItem: {},
      deleteSelectedItem: {},
      data_list_cache: []
    }
  },
  computed: {
    isChange() {
      let vm = this;
      let dataCheckList = [];
      if (vm.data_list && vm.data_list.length > 0) {
        vm.data_list.forEach(function (item) {
          let tempData = {
            id: item.id,
            attribute_id: item.attribute_id,
            option_value: item.option_value,
            sort_order: item.sort_order,
            parent_id: item.parent_id,
            is_default: item.is_default,
            is_drag: item.is_drag,
            note: item.note,
            count: item.count,
            option_value_cache: item.option_value
          };
          dataCheckList.push(tempData);
        })
      }

      if (vm.compareArray(dataCheckList, vm.data_list_cache) == false) {
        return true;
      } else {
        return false;
      }
    }
  },
  watch: {},
  methods: {
    toggleCollapse() {
      let vm = this;
      if (!$("#m-portlet__body-" + vm.attribute_id).hasClass("nt-show-collapse")) {
        $("#m-portlet__body-" + vm.attribute_id).slideDown(400);
        $("#m-portlet__body-" + vm.attribute_id).addClass("nt-show-collapse");
        $("#nt-settings-collapse-arrow-" + vm.attribute_id).removeClass("icon-down-arrow-1");
        $("#nt-settings-collapse-arrow-" + vm.attribute_id).addClass("icon-up-arrow-1");
      } else {
        $("#m-portlet__body-" + vm.attribute_id).slideUp(400);
        $("#m-portlet__body-" + vm.attribute_id).removeClass("nt-show-collapse");
        $("#nt-settings-collapse-arrow-" + vm.attribute_id).removeClass("icon-up-arrow-1");
        $("#nt-settings-collapse-arrow-" + vm.attribute_id).addClass("icon-down-arrow-1");
      }
    },
    getAttributeOptions(attribute_id) {
      let vm = this;
      if (Number.isInteger(attribute_id)) {
        vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTE_OPTION, attribute_id).then(response => {
          let data = response.data.data;
          if (data) {
            let new_data_sort = data.sort(function (before, after) {
              return before.sort_order - after.sort_order;
            });
            data.forEach(function (item) {
              item.option_value_cache = item.option_value;
            });
            new_data_sort.forEach(function (item, index) {
              item.sort_order = index + 1;
            })
            vm.data_list = new_data_sort;
            vm.data_list_cache = JSON.parse(JSON.stringify(vm.data_list));
            vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
            vm.mapActionDataToMultiLevel();
          }
        }).catch(error => {
          console.log(error);
        });
      }
    },

    mapActionDataToMultiLevel() {
      let vm = this;
      let tmpDataObject = {};
      let newData = [];

      if (vm.convert_data_list && vm.convert_data_list.length > 0) {
        // Tạo 1 object để dễ dàng truy xuất theo id mà không cần find in array
        vm.convert_data_list.forEach(function (data) {
          tmpDataObject[data.id] = data;
        });

        // add level
        for (let i = 0; i < vm.convert_data_list.length; i++) {

          vm.convert_data_list[i].level = 0;
          vm.convert_data_list[i].childrens = [];

          if (vm.convert_data_list[i].parent_id > 0 && tmpDataObject[vm.convert_data_list[i].parent_id]) {
            vm.convert_data_list[i].level = tmpDataObject[vm.convert_data_list[i].parent_id].level + 1;
          }
        }

        //Sắp xếp lại mảng theo thứ tự từ con nhỏ nhất -> cha
        vm.convert_data_list.sort(function (before, after) {
          return after.level - before.level;
        });

        //Lặp mảng, trả con về với cha tương ứng và xoá con
        vm.convert_data_list.forEach(function (data) {
          if (data.parent_id > 0) {
            if (tmpDataObject[data.parent_id]) {
              tmpDataObject[data.parent_id].childrens.push(data);
            }

            delete tmpDataObject[data.id];
          }
        });

        //Convert từ oject về mảng
        vm.convert_data_list.forEach(function (data) {
          if (tmpDataObject[data.id]) {
            newData.push(tmpDataObject[data.id]);
          }
        });
      }

      //Gán lại biến global
      vm.new_data_list = newData;
      vm.isLoadData = true;
    },

    checkDisabledAction(data) {
      if (data) {
        if (data.option_value && data.option_value.trim().toLowerCase() == data.option_value_cache.trim().toLowerCase()) {
          return true;
        }
      } else {
        return false;
      }
      return false;
    },

    sortAttributeOptions(data) {
      let vm = this;
      let attributes = [];
      data.forEach(function (item) {
        attributes.push(item);
      })
      vm.data_list = attributes;
      vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
      vm.mapActionDataToMultiLevel();
    },

    saveAddNew() {
      let vm = this;
      vm.$validator.validateAll("addDataAttributeOption").then(() => {
        if (!vm.errors.any()) {
          let id = Math.floor((Math.random() * 10000000));
          let newAttributeOption = {
            attribute_id: vm.attribute_id,
            count: 0,
            id: "add_new_" + id,
            option_value: vm.addData.title,
            option_value_cache: vm.addData.title,
            parent_id: 0,
            sort_order: vm.new_data_list.length + 1,
            is_default: 0,
            is_drag: 1,
            note: vm.addData.description
          };
          vm.data_list.push(newAttributeOption);
          vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
          vm.mapActionDataToMultiLevel();
          vm.addData = {
            title: "",
            description: ""
          };
          vm.$validator.reset();
          $("#addNewItem-" + vm.attribute_id).modal("hide");
        }
      });
    },

    saveEditItem() {
      let vm = this;
      vm.$validator.validateAll("editDataAttributeOption").then(() => {
        if (!vm.errors.any()) {
          vm.data_list.forEach(function (item) {
            if (item.id === vm.selectedItem.id) {
              item.option_value = vm.selectedItem.option_value;
              item.note = vm.selectedItem.note;
            }
          })
          vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
          vm.mapActionDataToMultiLevel();
          vm.$validator.reset();
          $("#editItem-" + vm.attribute_id).modal("hide");
        }
      });
    },

    saveDeleteItem() {
      let vm = this;
      vm.data_list.forEach(function (item, index) {
        if (item.id === vm.deleteSelectedItem.id) {
          vm.data_list.splice(index, 1);
        }
        vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
        vm.mapActionDataToMultiLevel();
        $("#deleteItem-" + vm.attribute_id).modal("hide");
      })
    },

    deleteLevel(item) {
      let vm = this;
      if (item) {
        vm.deleteSelectedItem = item;
        $("#deleteItem-" + vm.attribute_id).modal("show");
      }
    },

    showEditPopup(item) {
      let vm = this;
      if (item) {
        vm.selectedItem = item;
        $("#editItem-" + vm.attribute_id).modal("show");
      }
    },

    closeAddNew() {
      let vm = this;
      vm.addData = {
        title: "",
        description: ""
      };
      vm.$validator.reset();
      vm.server_errors = "";
    },

    completeSave() {
      let vm = this;
      if (!vm.errors.any()) {
        let params = {
          attribute_id: vm.attribute_id,
          attribute_options: vm.new_data_list
        };
        vm.isLoader = true;
        vm.$store.dispatch(CUSTOMER_SAVE_ATTRIBUTE_OPTION, params).then(response => {
          let data = response.data;
          if (data && data.errors && data.status_code == 422) {
            vm.$snotify.error("Cập nhật " + vm.component.toLowerCase() + " khách hàng thất bại.", TOAST_ERROR);
          } else if (data.data && data.data.status) {
            vm.getAttributeOptions(vm.attribute_id);
            vm.$snotify.success("Cập nhật " + vm.component.toLowerCase() + " khách hàng thành công.", TOAST_SUCCESS);
          }
          vm.isLoader = false;
        }).catch(() => {
          vm.$snotify.error("Cập nhật " + vm.component.toLowerCase() + " khách hàng thất bại.", TOAST_ERROR);
        });
      }
    },

    closeEditItem() {
      let vm = this;
      vm.selectedItem.option_value = vm.selectedItem.option_value_cache;
      vm.$validator.reset();
      vm.server_errors = "";
    },

    resetData() {
      let vm = this;
      let data = [];
      if (vm.data_list_cache && vm.data_list_cache.length > 0) {
        vm.data_list_cache.forEach(function (item) {
          let tempData = {
            id: item.id,
            attribute_id: item.attribute_id,
            option_value: item.option_value,
            sort_order: item.sort_order,
            parent_id: item.parent_id,
            is_default: item.is_default,
            is_drag: item.is_drag,
            note: item.note,
            count: item.count,
            option_value_cache: item.option_value
          };
          data.push(tempData);
        })
      }
      vm.data_list = data;
      vm.convert_data_list = JSON.parse(JSON.stringify(vm.data_list));
      vm.mapActionDataToMultiLevel();
    },

    checkUniqueLevel(value, optionId = 0) {
      let vm = this
      let exist = vm.new_data_list.find(function (item) {
        if (optionId != 0) {
          return item.id != optionId && item.option_value.toLowerCase() == value.toLowerCase();
        } else {
          return item.option_value.toLowerCase() == value.toLowerCase();
        }
      })
      if (exist) {
        vm.server_errors = vm.component + " khách hàng bị trùng.";
      } else {
        vm.server_errors = "";
      }
    },

    compareArray(arr1, arr2) {
      if (!arr1 || !arr2) return false;
      let count = 0;
      arr1.forEach(function (e1, id1) {
        arr2.forEach(function (e2, id2) {
          if (typeof arr1 == 'object' && typeof arr2 == 'object') {
            if (id1 === id2 && JSON.stringify(e1) === JSON.stringify(e2)) {
              count++;
            } else {
              return;
            }
          } else {
            return false;
          }
        })
      })
      if (arr1.length > arr2.length) {
        if (arr1.length === count) {
          return true;
        } else {
          return false;
        }
      } else {
        if (arr2.length === count) {
          return true;
        } else {
          return false;
        }
      }
    },

    isObjectExistInArray(list, object) {
      var isExist = false;

      if (list && list.length > 0) {
        list.forEach(() => {
          let index = list.findIndex(item => item.id === object.id);

          if (index == 0) {
            isExist = true;
            return;
          }
        });
      }
      return isExist;
    }
  }
}
