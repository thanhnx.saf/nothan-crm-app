/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import {
  DEAL_UPDATE_ATTRIBUTE,
  DEAL_GET_ATTRIBUTES
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";
import {
  IS_DATE,
  IS_DATE_TIME,
  IS_TEXT,
  IS_TEXT_AREA,
  IS_EMAIL,
  IS_PHONE,
  IS_NUMBER,
  IS_PRICE,
  IS_SELECT,
  IS_SELECT_FIND,
  IS_RADIO,
  IS_MULTIPLE_SELECT,
  IS_CHECKBOX_MULTIPLE,
  IS_TAG,
  IS_USER,
  IS_USER_MULTIPLE,
  IS_STATUS_BAR,
} from "@/types/const";

const customMessagesUpdateAttributeCustom = {
  custom: {
    'attribute_name': {
      required: 'Không được để trống trường này.',
      max: "Giới hạn 250 ký tự."
    },
    'data_type_id': {
      required: 'Không được để trống trường này.'
    },
    'attribute_options': {
      required: 'Không được để trống trường này.'
    },
    'customer_types': {
      required: 'Không được để trống trường này.'
    },
    'show_position': {
      required: 'Không được để trống trường này.'
    }
  }
};
const doneTypingInterval = 500;

export default {
  props: {
    attribute: {
      type: Object,
      default: {}
    },
    cache_name_attributes: {
      type: [Array, Object]
    }
  },
  mounted() {
    let vm = this;
    vm.$validator.localize('en', customMessagesUpdateAttributeCustom);
    vm.checkDisabledProperty(vm.attribute);
  },
  watch: {
    attribute: {
      handler(after) {
        let vm = this;
        if (after) {
          vm.checkDisabledProperty(after);

          let countChange = 0;

          if (after.attribute_name != after.attribute_name_cache) {
            countChange++;
          }

          if (after.is_required != after.is_required_cache) {
            countChange++;
          }

          if (after.is_unique != after.is_unique_cache) {
            countChange++;
          }

          if (after.is_hidden != after.is_hidden_cache) {
            countChange++;
          }

          if (after.attribute_options != after.attribute_options_cache) {
            countChange++;
          }

          if (after.show_position != after.show_position_cache) {
            countChange++;
          }

          if (countChange) {
            vm.isChange = true;
          } else {
            vm.isChange = false;
          }
        }
      },
      deep: true
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["getAllDealDataType", "currentTeam"]),
  },
  data() {
    return {
      isDate: IS_DATE,
      isDateTime: IS_DATE_TIME,
      isText: IS_TEXT,
      isTextArea: IS_TEXT_AREA,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isNumber: IS_NUMBER,
      isPrice: IS_PRICE,
      isSelect: IS_SELECT,
      isSelectFind: IS_SELECT_FIND,
      isRadio: IS_RADIO,
      isMultipleSelect: IS_MULTIPLE_SELECT,
      isCheckboxMultiple: IS_CHECKBOX_MULTIPLE,
      isTag: IS_TAG,
      isUser: IS_USER,
      isUserMultiple: IS_USER_MULTIPLE,
      isStatusBar: IS_STATUS_BAR,

      isLoader: false,
      isSuccess: false,
      server_errors: '',
      convertAttributeOptions: [],
      listExistsAttributeOptions: [],
      isDisabledRequired: false,
      isDisabledUnique: false,
      isChange: false,
      nameIsExists: false
    }
  },
  methods: {
    checkDisabledProperty(data) {
      let vm = this;
      vm.isDisabledRequired = false;
      vm.isDisabledUnique = false;
      if (data.data_type_id == vm.isRadio) {
        vm.isDisabledRequired = true;
      } else if (data.data_type_id == vm.isText || data.data_type_id == vm.isEmail || data.data_type_id == vm.isPhone) {
        vm.isDisabledUnique = true;
      }
    },

    getAttributeCode(attribute_name) {
      let vm = this;
      attribute_name = attribute_name ? attribute_name : "";
      let attribute_code = attribute_name ? helpers.changeAlias(attribute_name).replace(/ /g, '_') : "";
      vm.attribute.attribute_code = attribute_code;
      vm.changeDataInputType('attribute_name');
    },

    changeAttributeOptions(value) {
      let vm = this;
      value = value ? value.replace(/\r?\n/g, '<br>') : "";
      let value_array = value ? (value.split('<br>')) : [];
      let new_value_array = [];
      vm.attributeOptions = [];
      if (value_array.length > 0) {
        value_array.forEach(function (item, index) {
          if (item) {
            vm.attributeOptions.push({ id: index + 1, name: item });
            new_value_array.push(item);
          }
        });

        vm.listExistsAttributeOptions = Object.values(new_value_array.reduce((data, item) => {
          if ((item).trim() != "" || (item).trim() != null) {
            let key = (item).trim().toLowerCase();
            data[key] = data[key] || [];
            data[key].push(item);
            return data;
          }
        }, {})).reduce((data, item) => item.length > 1 ? data.concat(item) : data, []);
      }
    },

    update(scope) {
      let vm = this;
      vm.$validator.validateAll(scope).then(() => {
        if (!vm.errors.any()) {
          vm.isLoader = true;

          if (vm.attribute.attribute_options && vm.attribute.attribute_options.length > 0) {
            let attributeOptions = vm.attribute.attribute_options.split('\n');
            let attributeOptionsClear = attributeOptions.filter(function (option) {
              return option !== "" && option !== null && typeof option !== "undefined";
            })
            let sortAttributeOptions = [];
            if (attributeOptionsClear && attributeOptionsClear.length > 0) {
              attributeOptionsClear.forEach(function (option, index) {
                sortAttributeOptions.push({
                  option_value: option,
                  sort_order: index + 1
                });
              });
            }
            vm.attribute.attribute_options = vm.mapAttributeOptionsPushToServer(sortAttributeOptions);
          }

          if (vm.attribute.data_type_id == vm.isMultipleSelect || vm.attribute.data_type_id == vm.isCheckboxMultiple || vm.attribute.data_type_id == vm.isTag || vm.attribute.data_type_id == vm.isUserMultiple) {
            vm.attribute.is_multiple_value = 1;
          } else {
            vm.attribute.is_multiple_value = 0;
          }

          vm.attribute.is_required = (vm.attribute.is_required || vm.attribute.is_required == true) ? 1 : 0;
          vm.attribute.is_unique = (vm.attribute.is_unique || vm.attribute.is_unique == true) ? 1 : 0;
          vm.attribute.team_id = vm.currentTeamId;

          vm.$store.dispatch(DEAL_UPDATE_ATTRIBUTE, vm.attribute).then(response => {
            if (response.data.status_code && response.data.status_code == 422) {
              if (response.data.message === "attribute code exist" || response.data.message === "attribute name exist") {
                vm.nameIsExists = true;
              } else {
                vm.server_errors = "";
              }
            } else {
              vm.isSuccess = true;
              vm.$snotify.success('Cập nhật trường dữ liệu thành công', TOAST_SUCCESS);
              vm.resetEditAttributeCustom();

              vm.$emit('is_success', true);

              $(".s--modal .modal-body").animate({ scrollTop: 0 }, 300);

              vm.nameIsExists = false;
              setTimeout(function () {
                vm.isSuccess = false;
              }, 5000);
            }

            vm.resetAttributeToUpdateSuccess().then(function () {
              vm.isLoader = false;
            });
          }).catch(() => {
            vm.isLoader = false;
            vm.resetAttributeToUpdateSuccess();
            vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
          });
        }
      });
    },

    async resetAttributeToUpdateSuccess() {
      let vm = this;
      await vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(function (response) {
        let data = response.data.data;
        if (data && data.length > 0) {
          data.forEach(function (attribute) {
            if (attribute.id == vm.attribute.id) {
              let attributeOptions = [];
              attributeOptions = attribute.attribute_options;

              attributeOptions.sort(function (before, after) {
                return before.sort_order - after.sort_order;
              });

              // Set cached
              vm.attribute.attribute_name_cache = attribute.attribute_name;
              vm.attribute.attribute_code_cache = attribute.attribute_code;
              vm.attribute.is_required_cache = attribute.is_required;
              vm.attribute.is_unique_cache = attribute.is_unique;
              vm.attribute.is_hidden_cache = attribute.is_hidden;
              vm.attribute.data_type_id = attribute.data_type_id;
              vm.attribute.show_position_cache = attribute.show_position;
              vm.attribute.attribute_options_full = attribute.attribute_options;
              let attributeOptionsNew = '';
              if (attributeOptions && attributeOptions.length > 0) {
                attribute.attribute_options.forEach(function (item) {
                  attributeOptionsNew += item.option_value + '\n';
                });
              }
              vm.attribute.attribute_options_cache = attributeOptionsNew;
              vm.attribute.attribute_options = attributeOptionsNew;
            }
          });
        }
      });
    },

    mapAttributeOptionsPushToServer(data) {
      let vm = this;
      let newData = [];
      if (vm.attribute.attribute_options_full.length > 0) {
        if (data && data.length > 0) {
          if (data.length === vm.attribute.attribute_options_full.length) {
            newData = vm.mapAttributeOptionsEditAndSwitch(data);
          } else {
            if (vm.attribute.attribute_options_full.length > data.length) {
              newData = vm.mapAttributeOptionsDeleteAndSwitch(data);
            } else {
              newData = vm.mapAttributeOptionsAddNewAndSwitch(data);
            }
          }
        }
      } else {
        if (data && data.length > 0) {
          data.forEach(function (item, index) {
            newData.push({
              id: "",
              option_value: item.option_value,
              sort_order: index + 1
            });
          });
        }
      }
      if (newData && newData.length > 0) {
        newData.sort(function (before, after) {
          return before.sort_order - after.sort_order;
        });
      }
      return newData;
    },

    mapAttributeOptionsEditAndSwitch(data) {
      let vm = this;
      let newData = [];
      let changePosition = {};
      data.forEach(function (item, index_item) {
        if ($.inArray(item.option_value.toLowerCase(), vm.cache_name_attributes) > -1) {
          vm.attribute.attribute_options_full.forEach(function (option, index_option) {
            if (index_option === index_item && option.option_value.toLowerCase() === item.option_value.toLowerCase()) {
              newData.push({
                id: option.id,
                option_value: item.option_value,
                sort_order: index_item + 1
              });
            } else if (index_option !== index_item && option.option_value.toLowerCase() === item.option_value.toLowerCase()) {
              changePosition[vm.attribute.attribute_options_full[index_item].id] = vm.attribute.attribute_options_full[index_option].id;
              changePosition[vm.attribute.attribute_options_full[index_option].id] = vm.attribute.attribute_options_full[index_item].id;
              newData.push({
                id: option.id,
                option_value: option.option_value,
                sort_order: index_item + 1
              });
            } else {
              return;
            }
          });
        } else {
          return;
        }
      });
      data.forEach(function (item, index_item) {
        if ($.inArray(item.option_value.toLowerCase(), vm.cache_name_attributes) <= -1) {
          vm.attribute.attribute_options_full.forEach(function (option, index_option) {
            if (index_option === index_item) {
              let idChange = 0;
              if (changePosition[option.id]) {
                idChange = changePosition[option.id];
              } else {
                idChange = option.id;
              }
              newData.push({
                id: idChange,
                option_value: item.option_value,
                sort_order: index_item + 1
              });
            } else {
              return;
            }
          });
        }
      });
      return newData;
    },

    mapAttributeOptionsAddNewAndSwitch(data) {
      let vm = this;
      let newData = [];
      let changePosition = {};
      let indexAddNew = [];
      data.forEach(function (item, index_item) {
        if ($.inArray(item.option_value.toLowerCase(), vm.cache_name_attributes) > -1) {
          vm.attribute.attribute_options_full.forEach(function (option, index_option) {
            if (index_option === index_item && option.option_value.toLowerCase() === item.option_value.toLowerCase()) {
              newData.push({
                id: option.id,
                option_value: item.option_value,
                sort_order: index_item + 1
              });
              indexAddNew.push(index_item);
            } else if (index_option !== index_item && option.option_value.toLowerCase() === item.option_value.toLowerCase()) {
              if (vm.attribute.attribute_options_full[index_item] && vm.attribute.attribute_options_full[index_option]) {
                changePosition[vm.attribute.attribute_options_full[index_item].id] = vm.attribute.attribute_options_full[index_option].id;
                changePosition[vm.attribute.attribute_options_full[index_option].id] = vm.attribute.attribute_options_full[index_item].id;
              } else if (!vm.attribute.attribute_options_full[index_item] && vm.attribute.attribute_options_full[index_option]) {
                changePosition["add_new"] = vm.attribute.attribute_options_full[index_option].id;
                changePosition[vm.attribute.attribute_options_full[index_option].id] = "add_new";
              } else if (vm.attribute.attribute_options_full[index_item] && !vm.attribute.attribute_options_full[index_option]) {
                changePosition["add_new"] = vm.attribute.attribute_options_full[index_item].id;
                changePosition[vm.attribute.attribute_options_full[index_item].id] = "add_new";
              }
              indexAddNew.push(index_item);
              newData.push({
                id: option.id,
                option_value: option.option_value,
                sort_order: index_item + 1
              });
            } else {
              return;
            }
          });
        }
      });
      data.forEach(function (item, index_item) {
        if ($.inArray(item.option_value.toLowerCase(), vm.cache_name_attributes) <= -1) {
          if ($.inArray(index_item, indexAddNew) <= -1) {
            newData.push({
              id: "add_new",
              option_value: item.option_value,
              sort_order: index_item + 1
            });
          } else {
            vm.attribute.attribute_options_full.forEach(function (option, index_option) {
              if (index_option === index_item && $.inArray(index_item, indexAddNew) > -1) {
                let idChange = 0;
                if (changePosition[option.id]) {
                  idChange = changePosition[option.id];
                } else {
                  idChange = option.id;
                }
                newData.push({
                  id: idChange,
                  option_value: item.option_value,
                  sort_order: index_item + 1
                });
              } else {
                return;
              }
            });
          }
        }
      });
      newData.forEach(function (item) {
        if (item.id === "add_new") {
          item.id = "";
        }
      })
      return newData;
    },

    mapAttributeOptionsDeleteAndSwitch(data) {
      let vm = this;
      let newData = [];
      let issetId = [];
      let changePosition = {};
      data.forEach(function (item, index_item) {
        vm.attribute.attribute_options_full.forEach(function (option, index_option) {
          if ($.inArray(item.option_value.toLowerCase(), vm.cache_name_attributes) > -1) {
            if (index_option === index_item && option.option_value.toLowerCase() === item.option_value.toLowerCase()) {
              newData.push({
                id: option.id,
                option_value: item.option_value,
                sort_order: index_item + 1
              });
              issetId.push(option.id);
            } else if (index_option !== index_item && option.option_value.toLowerCase() === item.option_value.toLowerCase()) {
              newData.push({
                id: option.id,
                option_value: option.option_value,
                sort_order: index_item + 1
              });
              changePosition[vm.attribute.attribute_options_full[index_item].id] = vm.attribute.attribute_options_full[index_option].id;
              changePosition[vm.attribute.attribute_options_full[index_option].id] = vm.attribute.attribute_options_full[index_item].id;
              issetId.push(option.id);
            } else {
              return;
            }
          }
        });
      });
      data.forEach(function (item, index_item) {
        vm.attribute.attribute_options_full.forEach(function (option, index_option) {
          if ($.inArray(item.option_value.toLowerCase(), vm.cache_name_attributes) <= -1) {
            if (index_option === index_item) {
              let idChange = 0;
              if (changePosition[option.id]) {
                idChange = changePosition[option.id];
              } else {
                idChange = option.id;
              }
              newData.push({
                id: idChange,
                option_value: item.option_value,
                sort_order: index_item + 1
              });
              issetId.push(idChange);
            } else {
              return;
            }
          }
        });
      });
      vm.attribute.attribute_options_full.forEach(function (option) {
        if ($.inArray(option.id, issetId) <= -1) {
          option.is_delete = true;
          newData.push(option);
        }
      })
      return newData;
    },

    changeDataInputType(attribute_code) {
      let vm = this;
      clearTimeout(vm.typingTimer);
      vm.typingTimer = setTimeout(function () {
        if (vm.server_errors && vm.server_errors[attribute_code]) {
          vm.server_errors[attribute_code] = '';
        }
      }, doneTypingInterval);
    },

    resetEditAttributeCustom() {
      let vm = this;
      vm.$validator.reset();
      vm.server_errors = '';
      vm.listExistsAttributeOptions = [];
    },

    reset() {
      let vm = this;
      vm.isSuccess = false;
      vm.nameIsExists = false;
      vm.resetEditAttributeCustom();
      vm.$emit('is_cancel', true);
    },
  }
}