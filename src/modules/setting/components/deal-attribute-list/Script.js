/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import {
  DEAL_GET_ATTRIBUTE_DATA_TYPE,
  DEAL_GET_ATTRIBUTES,
  DEAL_DELETE_ATTRIBUTE,
  DEAL_UPDATE_ATTRIBUTE,
  DEAL_SORT_ATTRIBUTES
} from "@/types/actions.type";
import addNewAttributeDeal from "../deal-attribute-add/AddAttributeDeal.vue";
import editAttributeDeal from "../deal-attribute-edit/EditAttributeDeal.vue";
import {
  IS_TEXT,
  IS_EMAIL,
  IS_PHONE,
  IS_RADIO,
} from "@/types/const";
import {
  TOAST_SUCCESS,
  TOAST_ERROR
} from "@/types/config";

export default {
  components: {
    "add-new-attribute-deal": addNewAttributeDeal,
    "edit-attribute-deal": editAttributeDeal
  },
  mounted() {
    let vm = this;
    vm.getAttributeDataType();
    vm.attributes = JSON.parse(JSON.stringify(vm.attributeDealCustom));

    if (vm.attributes && vm.attributes.length > 0) {
      vm.sortOrderBase = vm.attributes[0].sort_order;
    }
  },
  data() {
    return {
      isText: IS_TEXT,
      isEmail: IS_EMAIL,
      isPhone: IS_PHONE,
      isRadio: IS_RADIO,
      attributes: [],
      selectedItem: null,
      sortOrderBase: 0,
      cacheAttributeOptionsName: [],
      isEdit: false
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    ...mapGetters(["attributeDealCustom", "currentTeam", "getAllDealDataType"]),
  },
  methods: {
    getAttributes() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTES).then(() => {
        vm.attributes = JSON.parse(JSON.stringify(vm.attributeDealCustom));
        if (vm.isEdit && vm.selectedItem) {
          vm.attributes.forEach(function (attribute) {
            if (attribute.id === vm.selectedItem.id) {
              vm.selectedItem = vm.transformAttributeToEdit(attribute);
            }
          })
        }
      }).catch(error => {
        return error;
      })
    },

    getAttributeDataType() {
      let vm = this;
      vm.$store.dispatch(DEAL_GET_ATTRIBUTE_DATA_TYPE);
    },

    getDataType(typeId) {
      let vm = this;
      let type = "";
      if (vm.getAllDealDataType && vm.getAllDealDataType.length > 0) {
        let typeObject = vm.getAllDealDataType.find(function (type) {
          return type.id === typeId
        })
        type = typeObject.data_type_name ? typeObject.data_type_name : "";
      }
      return type;
    },

    switchCheckbox(item, type) {
      let vm = this;
      vm.selectedItem = item;
      if (type === "required") {
        if (item.data_type_id !== vm.isRadio) {
          $("#modalRequireAttribute").modal("show");
        }
      } else if (type === "unique") {
        if (item.data_type_id === vm.isText || item.data_type_id === vm.isEmail || item.data_type_id === vm.isPhone) {
          $("#modalUniqueAttribute").modal("show");
        }
      } else {
        $("#modalHiddenAttribute").modal("show");
      }
    },

    showEditPopup(item) {
      let vm = this;
      vm.selectedItem = vm.transformAttributeToEdit(item);
      vm.isEdit = true;
      setTimeout(function () {
        $("#editAttributeDeal").modal("show");
      }, 10)
    },

    deleteItem(item) {
      let vm = this;
      vm.selectedItem = item;
      $("#deleteDealAttribute").modal("show");
    },

    sortNestedData() {
      let vm = this;
      if (vm.attributes && vm.attributes.length > 0) {
        let currentBaseSortOrder = vm.sortOrderBase;
        let params = [];
        vm.attributes.forEach(function (item) {
          item.sort_order = currentBaseSortOrder;
          currentBaseSortOrder++;
          let temp = {
            attribute_id: item.id,
            sort: item.sort_order
          };
          params.push(temp);
        });
        setTimeout(function () {
          vm.$store.dispatch(DEAL_SORT_ATTRIBUTES, {
            data: params
          }).then(response => {
            if (response.data.status_code && response.data.status_code == 422) {
              vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
            } else {
              vm.$snotify.success('Cập nhật trường dữ liệu thành công', TOAST_SUCCESS);
            }
          }).catch(() => {
            vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
          })
        }, 500)
      }
    },

    isSuccessAction() {
      let vm = this;
      vm.getAttributes();
    },

    requiredAttributeItem() {
      let vm = this;
      vm.selectedItem.is_required = vm.selectedItem && vm.selectedItem.is_required ? 0 : 1;
      vm.$store.dispatch(DEAL_UPDATE_ATTRIBUTE, vm.selectedItem).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
        } else {
          vm.$snotify.success('Cập nhật trường dữ liệu thành công', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
      });
    },

    uniqueAttributeItem() {
      let vm = this;
      vm.selectedItem.is_unique = vm.selectedItem && vm.selectedItem.is_unique ? 0 : 1;
      vm.$store.dispatch(DEAL_UPDATE_ATTRIBUTE, vm.selectedItem).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
        } else {
          vm.$snotify.success('Cập nhật trường dữ liệu thành công', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
      });
    },

    showHideAttributeItem() {
      let vm = this;
      vm.selectedItem.is_hidden = vm.selectedItem && vm.selectedItem.is_hidden ? 0 : 1;
      vm.$store.dispatch(DEAL_UPDATE_ATTRIBUTE, vm.selectedItem).then(response => {
        if (response.data.status_code && response.data.status_code == 422) {
          vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
        } else {
          vm.$snotify.success('Cập nhật trường dữ liệu thành công', TOAST_SUCCESS);
        }
      }).catch(() => {
        vm.$snotify.error('Cập nhật trường dữ liệu thất bại', TOAST_ERROR);
      });
    },

    transformAttributeToEdit(attribute) {
      let vm = this;
      let attributeSelectItem = {};
      attributeSelectItem.id = attribute.id;
      attributeSelectItem.data_type_id = attribute.data_type_id;
      attributeSelectItem.attribute_name = attribute.attribute_name;
      attributeSelectItem.attribute_code = attribute.attribute_code;
      attributeSelectItem.is_required = attribute.is_required;
      attributeSelectItem.is_unique = attribute.is_unique;
      attributeSelectItem.is_hidden = attribute.is_hidden;
      attributeSelectItem.show_position = attribute.show_position;
      // Set cached
      attributeSelectItem.attribute_name_cache = attribute.attribute_name;
      attributeSelectItem.attribute_code_cache = attribute.attribute_code;
      attributeSelectItem.is_required_cache = attribute.is_required;
      attributeSelectItem.is_unique_cache = attribute.is_unique;
      attributeSelectItem.is_hidden_cache = attribute.is_hidden;
      attributeSelectItem.data_type_id = attribute.data_type_id;
      attributeSelectItem.show_position_cache = attribute.show_position;

      let sortAttributeOptions = [];
      sortAttributeOptions = attribute.attribute_options;
      sortAttributeOptions.sort(function (before, after) {
        return before.sort_order - after.sort_order;
      });

      let attributeOptions = '';
      if (sortAttributeOptions && sortAttributeOptions.length > 0) {
        attribute.attribute_options.forEach(function (item) {
          attributeOptions += item.option_value + '\n';
        });
      }
      attributeSelectItem.attribute_options_cache = attributeOptions;

      attributeSelectItem.attribute_options = attributeOptions;
      attributeSelectItem.attribute_options_full = attribute.attribute_options;

      if (attributeSelectItem.attribute_options_full && attributeSelectItem.attribute_options_full) {
        vm.cacheAttributeOptionsName = [];
        attributeSelectItem.attribute_options_full.forEach(function (option) {
          vm.cacheAttributeOptionsName.push(option.option_value.toLowerCase());
        })
      }
      return attributeSelectItem;
    },

    deleteDealAttribute() {
      let vm = this;
      vm.$store.dispatch(DEAL_DELETE_ATTRIBUTE, vm.selectedItem).then(response => {
        if (response.data.data.success) {
          vm.getAttributes();
          vm.$snotify.success('Xóa vĩnh viễn trường tùy chỉnh thành công.', TOAST_SUCCESS);
        } else {
          vm.$snotify.error('Xóa vĩnh viễn trường tùy chỉnh không thành công. Vui lòng thử lại.', TOAST_ERROR);
        }
      }).catch(error => {
        return error;
      })
    },

    isCancel() {
      let vm = this;
      vm.getAttributes();
      vm.isEdit = false;
    }
  }
}