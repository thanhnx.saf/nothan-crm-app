import Vue from "vue";
import VueNestable from "vue-nestable";

Vue.use(VueNestable);

export default {
  props: {
    attribute_options: {
      default: [],
      type: Array,
      required: true
    }
  },
  mounted() {
    let vm = this;
    vm.attribute_options.forEach(function (attribute) {
      vm.attributeOptions.push(attribute);
    })
  },
  watch: {
    attribute_options: {
      handler() {
        let vm = this;
        vm.attributeOptions = [];
        vm.attribute_options.forEach(function (attribute) {
          if (attribute.is_drag == 0 && attribute.is_default == 1) {
            vm.attributeDefaultNoDrag = attribute;
          }
          vm.attributeOptions.push(attribute);
        })
      },
      deep: true
    }
  },
  data() {
    return {
      attributeOptions: [],
      attributeDefaultNoDrag: {}
    }
  },
  methods: {
    showEditPopup(item) {
      let vm = this;
      if (item) {
        vm.$emit("edit_attribute_option", item);
      }
    },

    deleteItem(item) {
      let vm = this;
      if (item) {
        vm.$emit("delete_attribute_option", item);
      }
    },

    sortNestedData() {
      let vm = this;
      if (vm.attributeOptions && vm.attributeOptions.length > 0) {
        vm.attributeOptions.forEach(function (item, index) {
          item.sort_order = index + 1;
        });
      }
      vm.$emit("sort_attribute_option", vm.attributeOptions);
    },
  }
}
