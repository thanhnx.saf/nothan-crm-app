/* eslint-disable no-undef */
import { mapGetters } from "vuex";

import helpers from "@/utils/utils";
import SidebarLeft from "../sidebar/SidebarSettings.vue";
import {
  GET_LIST_PERMISSION,
  GET_LIST_STRUCTURE,
  GET_LIST_PERMISSION_BY_STRUCTURE,
  SET_PERMISSION_FOR_STRUCTURE
} from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR, ID_BASE_URL } from "@/types/config";

export default {
  components: {
    "sidebar-left": SidebarLeft
  },
  mounted() {
    let vm = this;
    vm.getListPermission();
    vm.getListStructures();
  },
  data() {
    return {
      ID_BASE_URL: ID_BASE_URL,
      isPermissionSuccess: false,
      isStructureSuccess: false,
      isIssetStructure: false,
      isNotIssetStructure: false,
      isPermissionByStructureSuccess: false,
      listRules: [
        {
          id: 1,
          name: "Khách hàng của tổ chức"
        },
        {
          id: 2,
          name: "Khách hàng phòng ban phụ trách"
        },
        {
          id: 3,
          name: "Khách hàng được phụ trách"
        }
      ],
      isOnLeft: false,
      isOnRight: false,
      listStructuresIssetPermission: [],
      listStructuresIssetPermissionCache: null,
      isLoadingSavePermission: false,
      errorPermission: {},
      isFirstLoad: false,
      isFistRight: false,

      listPermissionParent: []
    }
  },
  watch: {
    isOnLeft: {
      handler() {
        let vm = this;
        $("#nt-table-lock-left").scroll(function () {
          if (vm.isOnLeft) {
            if ($("#table-lock-scroll").children(".nt-table-list").height() > 600) {
              if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                if ($("#table-lock-scroll").get(0).scrollWidth > $("#table-lock-scroll").width()) {
                  $("#table-lock-scroll").css("height", "620px");
                  $("#table-lock-scroll-content").css("margin-bottom", "13px");
                } else {
                  vm.isFistRight = true;
                  $("#table-lock-scroll").css("height", "600px");
                  $("#table-lock-scroll-content").css("margin-bottom", "0px");
                }
              } else {
                vm.isFistRight = true;
                $("#table-lock-scroll").css("height", "600px");
                $("#table-lock-scroll-content").css("margin-bottom", "0px");
              }
            }
            let scrollTop = $(this).scrollTop();
            $("#table-lock-scroll").scrollTop(scrollTop);
          }
        })
      },
      deep: true
    },
    isOnRight: {
      handler() {
        let vm = this;
        $("#table-lock-scroll").scroll(function () {
          if (vm.isOnRight) {
            if ($(this).children(".nt-table-list").height() > 600) {
              if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight - 20) {
                if ($(this).get(0).scrollWidth > $(this).width()) {
                  $(this).css("height", "620px");
                  $(this).css("margin-bottom", "13px");
                } else {
                  //Firefox
                  if (!vm.isFistRight) {
                    $(this).css("height", "613px");
                  } else {
                    $(this).css("height", "600px");
                  }
                  $(this).css("margin-bottom", "0px");
                  $(this).bind('DOMMouseScroll', function (e) {
                    if (e.originalEvent.detail > 0) {
                      if ($("#nt-table-lock-left").scrollTop() + $("#nt-table-lock-left").innerHeight() >= $("#nt-table-lock-left")[0].scrollHeight) {
                        $(this).scrollTop($("#nt-table-lock-left").scrollTop());
                      }
                    }
                  });
                  //IE, Opera, Safari
                  $(this).bind('mousewheel', function (e) {
                    if (e.originalEvent.wheelDelta < 0) {
                      if ($("#nt-table-lock-left").scrollTop() + $("#nt-table-lock-left").innerHeight() >= $("#nt-table-lock-left")[0].scrollHeight) {
                        $(this).scrollTop($("#nt-table-lock-left").scrollTop());
                      }
                    }
                  });
                }
              } else {
                if (!vm.isFistRight) {
                  $(this).css("height", "613px");
                } else {
                  $(this).css("height", "600px");
                }
                $(this).css("margin-bottom", "0px");
              }
            }
            let scrollTop = $(this).scrollTop();
            $("#nt-table-lock-left").scrollTop(scrollTop);
          }
        })
      },
      deep: true
    }
  },
  computed: {
    helpers() {
      return helpers;
    },
    isLoadedSuccess() {
      let vm = this;
      if (vm.isPermissionSuccess && vm.isStructureSuccess && vm.isPermissionByStructureSuccess && (vm.isIssetStructure && vm.listStructures && vm.listStructures.length > 0 && vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length > 0 || (vm.isNotIssetStructure || vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length <= 0))) {
        return true;
      } else {
        return false;
      }
    },
    countPermission() {
      let vm = this;
      let count = 0;
      if (vm.listPermission && vm.listPermission.length > 0) {
        vm.listPermission.forEach(function (permission) {
          if (permission.parent !== 0) {
            count++;
          }
        })
      }
      return count;
    },
    isChange() {
      let vm = this;
      let cacheArray = JSON.parse(vm.listStructuresIssetPermissionCache);
      let currentArray = [];
      vm.listStructuresIssetPermission.forEach(function (structure) {
        let temp = structure;
        temp.permissions = vm.sortArrayPermission(temp.permissions);
        currentArray.push(temp);
      })
      if (vm.helpers.compareArray(currentArray, cacheArray)) {
        return false;
      } else {
        return true;
      }
    },
    ...mapGetters(["listPermission", "listStructures", "listPermissionByStructure"]),
  },
  methods: {
    getListPermission() {
      let vm = this;
      vm.$store.dispatch(GET_LIST_PERMISSION).then(response => {
        let data = response.data.data;
        if (data) {
          vm.isPermissionSuccess = true;
          vm.listPermissionParent = data;
        }
      }).catch(error => {
        console.log(error);
      });
    },

    getListStructures() {
      let vm = this;
      vm.$store.dispatch(GET_LIST_STRUCTURE).then(response => {
        let data = response.data.data;
        if (data) {
          if (data.length > 0) {
            vm.isIssetStructure = true;
          } else {
            vm.isNotIssetStructure = true;
          }
          vm.isStructureSuccess = true;
        }
        vm.getListPermissionByStructure();
      }).catch(error => {
        console.log(error);
      });
    },

    getListPermissionByStructure() {
      let vm = this;
      vm.$store.dispatch(GET_LIST_PERMISSION_BY_STRUCTURE).then(() => {
        vm.setStructuresIssetPermission();
      }).catch(error => {
        console.log(error);
      });
    },

    sortArrayPermission(array) {
      array.sort(function (a, b) {
        return a - b;
      })
      return array;
    },

    setStructuresIssetPermission() {
      let vm = this;
      let result = [];

      if (vm.listStructures && vm.listStructures.length > 0) {
        vm.listStructures.forEach(function (structure) {
          let temp = structure;
          temp.permissions = [];
          if (vm.listPermissionByStructure && vm.listPermissionByStructure.length > 0) {
            vm.listPermissionByStructure.forEach(function (permission) {
              if (structure.id === permission.id) {
                temp.permissions = vm.sortArrayPermission(permission.permissions);
              }
            })
          }

          // add checked parent category
          temp.is_checked_parent = {};
          temp.count_permission_category = {};
          temp.count_structure_permission_category = {};
          vm.listPermissionParent.forEach(function (permissionParent) {
            let countStructureCurrentPermission = 0;
            if (!temp.is_checked_parent[permissionParent.id]) {
              let permission_ids = [];

              if (permissionParent.permissions && permissionParent.permissions.length > 0) {
                permissionParent.permissions.forEach(function (permission) {
                  permission_ids.push(permission.id);
                })
              }

              if (structure && structure.permissions.length > 0) {
                structure.permissions.forEach(function (permissionStructureId) {
                  permission_ids.forEach(function (permissionId) {
                    if (permissionStructureId === permissionId) {
                      countStructureCurrentPermission++;
                    }
                  })
                })
              }

              temp.is_checked_parent[permissionParent.id] = vm.helpers.compareTwoArrayIds(structure.permissions, permission_ids);
              temp.count_permission_category[permissionParent.id] = permission_ids.length;

              temp.count_structure_permission_category[permissionParent.id] = countStructureCurrentPermission;

            }
          });

          result.push(temp);

        })
        vm.listStructuresIssetPermission = result;
        vm.listStructuresIssetPermissionCache = JSON.stringify(result);
      }


      vm.isPermissionByStructureSuccess = true;
      if (!vm.isFirstLoad) {
        $(document).ready(function ($) {
          let baseWidth = $("#table-lock-scroll").width();
          let contentWidth = $("#table-lock-scroll-content").width();
          if (parseInt(contentWidth) < parseInt(baseWidth) && $("#table-lock-scroll-content").hasClass("nt-table-list-all")) {
            $("#table-lock-scroll-content").removeClass("nt-table-list-all");
            $("#table-lock-scroll-content").addClass("w-100");
          } else if (parseInt(contentWidth) >= (parseInt(baseWidth) - 10) && !$("#table-lock-scroll-content").hasClass("nt-table-list-all") && $("#table-lock-scroll-content").hasClass("w-100")) {
            $("#table-lock-scroll-content").removeClass("w-100");
            $("#table-lock-scroll-content").addClass("nt-table-list-all");
          }
        })
        vm.isFirstLoad = true;
        setTimeout(function () {
          $("#table-lock-scroll").scroll(function () {
            if ($(this).scrollLeft() > 0) {
              $(".nt-table-lock-line").each(function () {
                $(this).css("border-right", "solid 1px #CCD2E2");
              })
            } else {
              $(".nt-table-lock-line").each(function () {
                $(this).css("border-right", "none");
              })
            }
            if ($(this).scrollTop() > 0 && !vm.isOnLeft) {
              vm.isOnRight = true;
            }
          })
          $("#nt-table-lock-left").scroll(function () {
            if ($(this).scrollTop() > 0 && !vm.isOnRight) {
              vm.isOnLeft = true;
            }
          })
          $('#nt-table-lock-left').on('mouseenter', function () {
            vm.isOnLeft = true;
          })
          $('#nt-table-lock-left').on('mouseleave', function () {
            vm.isOnLeft = false;
          })
          $('#table-lock-scroll').on('mouseenter', function () {
            vm.isOnRight = true;
          })
          $('#table-lock-scroll').on('mouseleave', function () {
            vm.isOnRight = false;
          })
        }, 5)
      }

      setTimeout(function () {
        vm.isLoadingSavePermission = false;
      }, 100)

    },

    checkPermission(structure, permission) {
      let vm = this;
      let categoryId = permission.category_id;
      let permissionStructureIds = [];
      let permissionIds = [];
      vm.listPermissionParent.forEach(function (permissionParent) {
        if (categoryId == permissionParent.id) {
          let permission_ids = [];
          if (permissionParent.permissions && permissionParent.permissions.length > 0) {
            permissionParent.permissions.forEach(function (permission) {
              permission_ids.push(permission.id);
            })
          }
          permissionIds = permission_ids;
        }
      });

      if (structure && structure.permissions.length > 0) {
        structure.permissions.forEach(function (permissionStructureId) {
          permissionIds.forEach(function (permissionId) {
            if (permissionStructureId === permissionId) {
              permissionStructureIds.push(permissionStructureId);
            }
          })
        })
      }

      if (vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length > 0) {
        vm.listStructuresIssetPermission.forEach(function (structurePermission) {
          if (structure.id === structurePermission.id) {
            structure.count_structure_permission_category[categoryId] = permissionStructureIds.length;
            structure.is_checked_parent[categoryId] = false;
            if (permissionStructureIds.length === permissionIds.length) {
              structure.is_checked_parent[categoryId] = true;
            }
          }
        });
        vm.listStructuresIssetPermission = JSON.parse(JSON.stringify(vm.listStructuresIssetPermission));
      }
    },

    checkAllPermission(structure) {
      let vm = this;
      let permissions = [];

      if (vm.listPermissionParent && vm.listPermissionParent.length > 0) {
        vm.listPermissionParent.forEach(function (permissionParent) {
          structure.is_checked_parent[permissionParent.id] = true;
        });
      }

      if (vm.listPermission && vm.listPermission.length > 0) {
        vm.listPermission.forEach(function (permission) {
          if (permission.parent_id !== 0 && permission.permission_id) {
            permissions.push(permission.permission_id);
          }
        });
        if (permissions && permissions.length > 0) {
          permissions.sort();
        }
      }

      if (vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length > 0 && permissions.length > 0) {
        vm.listStructuresIssetPermission.forEach(function (structurePermission) {
          if (structure.id === structurePermission.id) {
            structurePermission.permissions = permissions;
          }
        });
        vm.listStructuresIssetPermission = JSON.parse(JSON.stringify(vm.listStructuresIssetPermission));
      }
    },

    checkPermissionParentCategory(structure, permission) {
      let vm = this;

      let isChecked = structure.is_checked_parent[permission.current_id];

      let permissionIds = [];
      vm.listPermissionParent.forEach(function (permissionParent) {
        if (permission.current_id == permissionParent.id) {
          let permission_ids = [];
          if (permissionParent.permissions && permissionParent.permissions.length > 0) {
            permissionParent.permissions.forEach(function (permission) {
              permission_ids.push(permission.id);
            })
          }
          permissionIds = permission_ids;
        }
      });

      // convert permission structure object
      if (structure.permissions) {
        structure.obj_permissions = {};
        structure.permissions.forEach(function (permissionId) {
          if (!structure.obj_permissions[permissionId]) {
            structure.obj_permissions[permissionId] = permissionId;
          }
        });
      }

      if (isChecked) {
        if (permissionIds && permissionIds.length > 0) {
          permissionIds.forEach(function (permissionId) {
            if (!structure.obj_permissions[permissionId]) {
              structure.obj_permissions[permissionId] = permissionId;
            }
          })
        }

        let permissionStructureIds = [];
        if (structure && structure.permissions.length > 0) {
          structure.permissions.forEach(function (permissionStructureId) {
            Object.keys(structure.obj_permissions).forEach(function (permissionId) {
              if (permissionStructureId === permissionId) {
                permissionStructureIds.push(permissionStructureId);
              }
            })
          })
        }
        structure.count_structure_permission_category[permission.current_id] = permissionStructureIds.length;
      } else {
        if (structure.permissions && structure.permissions.length > 0 && permissionIds && permissionIds.length > 0) {
          Object.keys(structure.obj_permissions).forEach(function (structurePermissionKey) {
            permissionIds.forEach(function (permissionId) {
              if (structurePermissionKey == permissionId && structure.obj_permissions[structurePermissionKey]) {
                delete structure.obj_permissions[structurePermissionKey];
              }
            });
          })
        }
        structure.count_structure_permission_category[permission.current_id] = 0;
      }

      let newPermissions = [];
      Object.keys(structure.obj_permissions).forEach(function (structurePermissionKey) {
        newPermissions.push(parseInt(structurePermissionKey));
      });

      if (vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length > 0) {
        vm.listStructuresIssetPermission.forEach(function (structurePermission) {
          if (structure.id === structurePermission.id) {
            structurePermission.permissions = newPermissions;
          }
        });
        vm.listStructuresIssetPermission = JSON.parse(JSON.stringify(vm.listStructuresIssetPermission));
      }
    },

    minusAllPermission(structure) {
      let vm = this;
      structure.is_checked_parent = {};
      structure.count_structure_permission_category = {};

      if (vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length > 0) {
        vm.listStructuresIssetPermission.forEach(function (structurePermission) {
          if (structure.id === structurePermission.id) {
            structurePermission.permissions = [];
          }
        })
        vm.listStructuresIssetPermission = JSON.parse(JSON.stringify(vm.listStructuresIssetPermission));
      }
    },

    saveSetPermission() {
      let vm = this;
      vm.isLoadingSavePermission = true;
      let params = [];
      let countPermission = 0;
      vm.listStructuresIssetPermission.forEach(function (structure) {
        if (structure.permissions && structure.permissions.length > 0) {
          countPermission++;
        }
        let temp = {
          department_id: structure.id,
          permission: structure.permissions.join()
        };
        params.push(temp);
      });
      let requestParams = null;
      if (countPermission === 0) {
        requestParams = "";
      } else {
        requestParams = JSON.stringify(params);
      }
      vm.$store.dispatch(SET_PERMISSION_FOR_STRUCTURE, { permissions: requestParams }).then(response => {
        if (response.status == 422) {
          if (response.data.errors.message === "Phòng ban không tồn tại.") {
            $("#errorSetPermissionForNotExist").modal("show");
          } else {
            $("#errorSetPermissionChildrenBiggerParent").modal("show");
          }
          //vm.$snotify.error('Cập nhật quyền thất bại', TOAST_ERROR);
        } else if (response.data.data && response.data.data.success) {
          vm.$snotify.success('Cập nhật quyền thành công.', TOAST_SUCCESS);
          vm.getListPermissionByStructure();
        } else {
          vm.$snotify.error('Cập nhật quyền thất bại.', TOAST_ERROR);
        }
      }).catch(error => {
        console.log(error);
      });
    },

    cancelPermission() {
      let vm = this;
      vm.listStructuresIssetPermission = JSON.parse(vm.listStructuresIssetPermissionCache);
      vm.isLoadingSavePermission = false;
    },

    reloadPage() {
      return window.location.reload();
    },

    switchCollapseArrow(event, type, keyId) {
      let vm = this;
      let current = event.currentTarget;
      let arrow = $(current).children(".nt-button-collapse").children(".icon");
      if (type == 'left') {
        if ($(arrow).hasClass("icon-down-arrow-1")) {
          if (vm.listPermission && vm.listPermission.length > 0) {
            vm.listPermission.forEach(function (permission) {
              if (permission.parent === keyId) {
                permission.hidden = false;
              }
            })
          }
          setTimeout(function () {
            let heightTable = $("#table-lock-scroll").children(".nt-table-list").height();
            if (heightTable < 600) {
              $("#table-lock-scroll").css("height", (heightTable + 22) + "px");
            } else {
              $("#table-lock-scroll").css("height", "600px");
            }
          }, 10);
          $(arrow).removeClass("icon-down-arrow-1");
          $(arrow).addClass("icon-up-arrow-1");
        } else {
          if (vm.listPermission && vm.listPermission.length > 0) {
            vm.listPermission.forEach(function (permission) {
              if (permission.parent === keyId) {
                permission.hidden = true;
              }
            })
          }
          setTimeout(function () {
            let heightTable = $("#table-lock-scroll").children(".nt-table-list").height();
            if (heightTable < 600) {
              $("#table-lock-scroll").css("height", (heightTable + 22) + "px");
            } else {
              $("#table-lock-scroll").css("height", "600px");
            }
          }, 10)
          $(arrow).removeClass("icon-up-arrow-1");
          $(arrow).addClass("icon-down-arrow-1");
        }
      } else {
        if ($(arrow).hasClass("icon-left-arrow")) {
          if (vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length > 0) {
            vm.listStructuresIssetPermission.forEach(function (structure) {
              if (structure.parent_id === keyId) {
                structure.hidden = true;
                vm.recursiveStructure(structure, true);
              }
            })
          }
          $(arrow).removeClass("icon-left-arrow");
          $(arrow).addClass("icon-chevron");
        } else {
          if (vm.listStructuresIssetPermission && vm.listStructuresIssetPermission.length > 0) {
            vm.listStructuresIssetPermission.forEach(function (structure) {
              if (structure.parent_id === keyId) {
                structure.hidden = false;
                vm.recursiveStructure(structure, false);
              }
            })
          }
          $(arrow).removeClass("icon-chevron");
          $(arrow).addClass("icon-left-arrow");
        }
      }
    },

    async recursiveStructure(data, update) {
      if (!data.has_children) {
        return;
      }
      let vm = this;
      await vm.listStructuresIssetPermission.forEach(function (structure) {
        if (structure.parent_id === data.id) {
          structure.hidden = update;
          vm.recursiveStructure(structure, update);
        }
      });
      return true;
    }
  }
}
