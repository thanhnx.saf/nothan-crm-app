import { mapGetters } from "vuex";

import { PRODUCT_RESTORE } from "@/types/actions.type";
import { TOAST_SUCCESS, TOAST_ERROR } from "@/types/config";

export default {
  props: {
    selected_ids: {
      type: Array,
      required: true,
      default: []
    },
    total_restored: {
      type: [Number, String],
      required: true,
      default: 0
    },
    is_restored_multiple: {
      type: Boolean,
      default: false
    },
    product_name: {
      type: String,
      default: ''
    },
    current_params: {
      type: Object,
      default: {}
    }
  },

  computed: {
    ...mapGetters(["currentTeam"]),
  },

  methods: {
    restore() {
      let vm = this;
      let params = {};

      params.team_id = vm.currentTeamId;
      params.product_id = vm.selected_ids;
      params.type = vm.selected_ids && vm.selected_ids.length > 0 ? 2 : 1;
      params.search = vm.current_params.search;

      vm.$store.dispatch(PRODUCT_RESTORE, params).then((response) => {
        if (response.data.data.success) {
          vm.$snotify.success('Phục hồi sản phẩm thành công.', TOAST_SUCCESS);
          vm.$emit('is_change', true);
        }
      }).catch(() => {
        vm.$snotify.error('Phục hồi sản phẩm thất bại.', TOAST_ERROR);
      });
    }
  },
}
