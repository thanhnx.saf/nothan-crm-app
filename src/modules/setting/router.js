import utils from '@/utils/utils'
import SettingsInformationCustomer from './views/customer/SettingsInformationCustomer.vue'
import SettingsInformationDeal from './views/deal/SettingsInformationDeal.vue'
import TablePermission from './components/permission/TablePermission.vue'
import ListTrashCustomer from './views/trash-customer/ListTrashCustomer.vue'
import ListTrashProducts from './views/trash-product/ListTrashProducts.vue'
import Settings from './views/setting/Settings.vue'

export default [{
  name: "settingsDetails",
  path: "/settings/information/customer",
  component: Settings,
  children: utils.prefixRouter('/team/:team_id/settings', [{
    path: "/information/customer",
    name: "SettingsInfoCustomer",
    component: SettingsInformationCustomer,
    meta: {
      permissionName: "all",
      serviceName: "setting",
      breadcrumb: [{
        name: "Cài đặt",
      }, {
        name: "Quản lí trường thông tin",
      }, {
        name: "Khách hàng",
      }]
    }
  }, {
    path: "/information/deal",
    name: "SettingsInfoDeal",
    component: SettingsInformationDeal,
    meta: {
      breadcrumb: [{
        name: "Cài đặt",
      }, {
        name: "Quản lí trường thông tin",
      }, {
        name: "Giao dịch",
      }]
    }
  }, {
    path: "/permission",
    name: "SettingsPermission",
    component: TablePermission,
    meta: {
      permissionName: "all",
      serviceName: "setting",
      breadcrumb: [{
        name: "Cài đặt",
      }, {
        name: "Phân quyền người dùng",
      }]
    }
  }, {
    path: "/trash/customer",
    name: "SettingsTrashCustomer",
    component: ListTrashCustomer,
    meta: {
      breadcrumb: [{
        name: "Cài đặt",
      }, {
        name: "Thùng rác",
      }, {
        name: "Khách hàng",
      }]
    }
  }, {
    path: "/trash/product",
    name: "SettingsTrashProduct",
    component: ListTrashProducts,
    meta: {
      breadcrumb: [{
        name: "Cài đặt",
      }, {
        name: "Thùng rác",
      }, {
        name: "Sản phẩm",
      }]
    }
  }])
}]