import {
  CUSTOMER_GET_ATTRIBUTES
} from "@/types/actions.type";
import SettingMultipleCustomer from "../../components/customer-setting/SettingMultipleCustomer.vue";
import ListAttributeCustom from "../../components/customer-attribute-list/ListAttributeCustom.vue";

export default {
  mounted() {
    let vm = this;
    vm.getAttributes();
  },
  data() {
    return {
      attributeLevelId: 0,
      attributeGroupId: 0,
      loadDataSuccess: false
    }
  },
  components: {
    "settings-multiple-customer": SettingMultipleCustomer,
    "list-attribute-advanced": ListAttributeCustom
  },
  methods: {
    getAttributes() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then(response => {
        let data = response.data.data;
        if (data && data.length > 0) {
          data.forEach(function (item) {
            if (item.attribute_code == "level") {
              vm.attributeLevelId = item.id;
            }
            // if (item.attribute_code == "level") {
            //     vm.attributeLevelId = item.id;
            // }
            vm.loadDataSuccess = true;
          });
        }
      }).catch(error => {
        console.log(error);
      });
    }
  }
}
