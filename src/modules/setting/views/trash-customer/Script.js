/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import Pagination from "@/components/pagination/Pagination.vue";
import UserObjectLoading from "@/components/loading/UserObject";
import RestoreCustomer from "../../components/trash-restore-customer/RestoreCustomer.vue";
import {
  PERMISSION_UPDATE_PERSON_IN_CHARGE,
  PERMISSION_DELETE_PERSON_IN_CHARGE,
  PERMISSION_EXPORT_CUSTOMER,
  PERMISSION_SHOW_CUSTOMER
} from "@/types/const";
import {
  CUSTOMER_GET_ALL,
  CUSTOMER_GET_ATTRIBUTES,
  CUSTOMER_GET_USER_ATTRIBUTES
} from "@/types/actions.type"

const SEARCH_TIMEOUT_DEFAULT = 10000;

export default {
  components: {
    pagination: Pagination,
    "user-object-loading": UserObjectLoading,
    RestoreCustomer: RestoreCustomer,
  },
  mounted() {
    let vm = this;
    vm.getAll();
    vm.getAttributeByUser();
  },
  created() {

  },
  watch: {},
  computed: {
    helpers() {
      return helpers;
    },
    isLoader() {
      let vm = this;
      if (vm.dataReady === true && vm.attributeReady === true) {
        return true;
      } else {
        return false;
      }
    },
    ...mapGetters(["listCustomers", "attributeCustomerFull", "currentUser", "currentTeam", "listUsers", "userCheckAddOn", "userCurrentPermissions"]),
  },
  data() {
    return {
      PERMISSION_UPDATE_PERSON_IN_CHARGE: PERMISSION_UPDATE_PERSON_IN_CHARGE,
      PERMISSION_DELETE_PERSON_IN_CHARGE: PERMISSION_DELETE_PERSON_IN_CHARGE,
      PERMISSION_EXPORT_CUSTOMER: PERMISSION_EXPORT_CUSTOMER,
      PERMISSION_SHOW_CUSTOMER: PERMISSION_SHOW_CUSTOMER,
      dataReady: false,
      attributeReady: false,
      customerChecked: [],
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {
        deleted_at: 1
      },
      beforeSearchCache: null,
      customerSelected: null,
      sortAttributeValue: 'DESC',
      currentSortAttribute: null,
      isSearch: false,
      loadSearch: false,
      isLoaderListCustomer: false,
      currentAddCustomerType: null,
      loadDataCustomer: false,
      showFilter: false,
      isAllData: true,
      customerCheckedPerPage: [],
      customerCheckAllPerPage: [],
      oldCurrentPage: 0,
      listAssignerInTeam: [],
      cacheForFilterAttributes: "",
      searchText: "",

      selectedIds: [],
      totalRestored: null,
      isRestoredMultiple: false,
      customerName: ''
    }
  },
  methods: {
    // getUrlParams() {
    //     let vm = this;
    //     let oldParams = router.history.current.query;
    //     if (!vm.loadDataCustomer && oldParams && Object.keys(oldParams).length > 0) {
    //         if (oldParams["page"]) {
    //             vm.pagination.current_page = parseInt(oldParams["page"]);
    //             vm.currentParams.page = parseInt(oldParams["page"]);
    //         }filterData
    //         if (oldParams["search"]) {
    //             vm.currentParams.search = oldParams["search"];
    //             vm.searchText = oldParams["search"];
    //         }
    //     }
    // },
    getAll(type = "all") {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      if (vm.currentParams.per_page) {
        delete vm.currentParams.per_page;
      }
      vm.isLoaderListCustomer = true;
      vm.currentParams.deleted_at = 1;

      vm.$store.dispatch(CUSTOMER_GET_ALL, vm.currentParams).then(response => {
        if (type != "pagination") {
          vm.customerChecked = [];
        }
        if (typeof vm.customerCheckAllPerPage[response.data.meta.current_page] == "undefined") {
          vm.customerCheckAllPerPage[response.data.meta.current_page] = false;
        }
        if (typeof vm.customerCheckedPerPage[response.data.meta.current_page] == "undefined") {
          vm.customerCheckedPerPage[response.data.meta.current_page] = [];
        }
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.oldCurrentPage = response.data.meta.current_page;
        let items = 0;
        if (response.data.meta.current_page < response.data.meta.last_page) {
          items = response.data.meta.per_page;
        } else {
          items = response.data.meta.total - (response.data.meta.per_page * (response.data.meta.last_page - 1));
        }
        vm.pagination.items_in_page = parseInt(items);

        vm.beforeSearchCache = JSON.stringify(response.data.data);
        vm.dataReady = true;
        if (vm.loadSearch) {
          vm.isSearch = true;
        } else {
          vm.isSearch = false;
        }
        vm.loadDataCustomer = false;
        vm.isLoaderListCustomer = false;

      }).catch(error => {
        console.log(error);
      });
    },

    getAttributes() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_ATTRIBUTES).then(() => {
        vm.attributeReady = true;
      }).catch(error => {
        console.log(error);
      });
    },

    getAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(CUSTOMER_GET_USER_ATTRIBUTES).then(() => {
        vm.getAttributes();
      }).catch(error => {
        console.log(error);
      });
    },

    checkedCustomer(customer) {
      let vm = this;
      if (!vm.checkExistValueInChecked(customer)) {
        vm.customerCheckedPerPage[vm.oldCurrentPage].push(customer);
      } else {
        let newChecked = [];
        vm.customerCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
          if (item.id !== customer.id) {
            newChecked.push(item);
          }
        });
        vm.customerCheckedPerPage[vm.oldCurrentPage] = newChecked;
      }

      let itemCheckedInPage = vm.customerCheckedPerPage[vm.oldCurrentPage];
      if (itemCheckedInPage && vm.pagination.items_in_page == itemCheckedInPage.length) {
        vm.customerCheckAllPerPage[vm.oldCurrentPage] = true;
      } else {
        vm.customerCheckAllPerPage[vm.oldCurrentPage] = false;
      }
    },

    checkExistValueInChecked(customer) {
      let vm = this;
      if (vm.customerCheckedPerPage[vm.oldCurrentPage] && vm.customerCheckedPerPage[vm.oldCurrentPage].length > 0) {
        let exist = vm.customerCheckedPerPage[vm.oldCurrentPage].find(function (item) {
          return item.id == customer.id;
        })
        if (exist && Object.keys(exist).length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },

    checkedAllToggle() {
      let vm = this;
      if (!vm.customerCheckAllPerPage[vm.oldCurrentPage]) {
        let itemIsChecked = [];
        if (vm.customerCheckedPerPage[vm.oldCurrentPage] && vm.customerCheckedPerPage[vm.oldCurrentPage].length > 0) {
          vm.customerCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
            itemIsChecked.push(item.id);
          })
        }
        let newChecked = [];
        vm.listCustomers.forEach(function (customer) {
          if ($.inArray(customer.id, itemIsChecked) == -1) {
            vm.customerChecked.push(customer);
            newChecked.push(customer);
          }
        });
        vm.customerCheckedPerPage[vm.oldCurrentPage] = newChecked;
      } else {
        let newChecked = [];
        vm.customerChecked.forEach(function (customer) {
          if (customer.in_page != vm.pagination.current_page) {
            newChecked.push(customer);
          }
        });
        vm.customerCheckedPerPage[vm.oldCurrentPage] = newChecked;
        vm.customerChecked = newChecked;
      }
    },

    getPaginationCustomers() {
      let vm = this;
      vm.loadDataCustomer = true;
      vm.getAll("pagination");
      $('#table-responsive-data').animate({
        scrollLeft: 0
      }, 200);
    },

    customerSearch(event) {
      let vm = this;
      let keyCode = event.keyCode;
      let searchText = event.target.value ? event.target.value : '';
      searchText = searchText ? searchText.trim() : '';

      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }

      if (keyCode === 13) {
        vm.customerSearchSetQuery(searchText);
        vm.doneTyping();
      } else {
        vm.clearTimeoutSearch = setTimeout(() => {
          vm.customerSearchSetQuery(searchText);
          vm.doneTyping();
        }, SEARCH_TIMEOUT_DEFAULT);
      }
    },

    customerSearchForClick() {
      let vm = this;
      let searchText = vm.$refs.customerSearch.value ? vm.$refs.customerSearch.value : '';
      searchText = searchText ? searchText.trim() : '';
      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }
      vm.customerSearchSetQuery(searchText);
      vm.doneTyping();
    },

    customerSearchSetQuery(searchText) {
      let vm = this;
      vm.currentParams.page = 1;
      vm.currentParams.search = searchText;
      vm.searchText = searchText;
      vm.customerChecked = [];
      vm.customerCheckAllPerPage = [];
      vm.customerCheckedPerPage = [];
      vm.$refs.customerSearch.blur();
    },

    customerTyping() {
      let vm = this;
      clearTimeout(vm.typingTimer);
    },

    doneTyping() {
      let vm = this;
      if (vm.searchText && vm.searchText.length) {
        vm.pagination.current_page = 1;
        vm.currentParams.search = vm.searchText;
        vm.loadDataCustomer = true;
        vm.getAll();
        vm.loadSearch = true;
      } else {
        if (vm.currentParams.search) {
          vm.pagination.current_page = 1;
          delete vm.currentParams.search;
        }
        if (!vm.searchText || (vm.searchText && vm.searchText.length == 0)) {
          vm.loadDataCustomer = true;
          vm.getAll();
          vm.loadSearch = false;
        }
      }
    },

    getQuantityCustomerAction() {
      let vm = this;
      let result = 0;
      if (vm.customerChecked.length > 0) {
        if (vm.customerChecked.length > vm.pagination.total_item) {
          result = vm.pagination.total_item;
        } else {
          result = vm.customerChecked.length;
        }
      } else {
        result = vm.pagination.total_item;
      }
      return result;
    },

    showModalRestore(customer) {
      let vm = this;
      vm.selectedItem = customer;

      setTimeout(function () {
        $("#restoreCustomer").modal('show');
      }, 100);
    },

    isChange(isChange) {
      if (isChange) {
        let vm = this;
        vm.loadDataCustomer = true;
        vm.customerChecked = [];
        vm.customerCheckAllPerPage = [];
        vm.customerCheckedPerPage = [];
        vm.totalRestored = null;
        vm.pagination.current_page = 1;
        $('.nt-table-search-box').val('');
        vm.searchText = "";
        delete vm.currentParams.search;
        vm.loadSearch = false;
        vm.getAll();
      }
    },

    checkBeforeRestore(customers, is_multiple = false) {
      let vm = this;
      let ids = [];
      let params = {};

      params.team_id = vm.currentTeamId;

      if (is_multiple) {
        if (customers && customers.length > 0) {
          customers.forEach(function (item) {
            ids.push(item.id);
          });
        }
        vm.totalRestored = customers && customers.length > 0 ? ids.length : 'tất cả';
      } else {
        ids.push(customers.id);
        vm.customerName = '';
        if (vm.attributeCustomerFull && vm.attributeCustomerFull.length > 0) {
          vm.attributeCustomerFull.forEach(function (attribute) {
            if (customers && customers.attributes.length > 0) {
              customers.attributes.forEach(function (attributeItem) {
                if (attribute.id == attributeItem.attribute_id && attribute.attribute_code == "full_name") {
                  vm.customerName = attributeItem.value;
                }
              })
            }

          });
        }
        vm.totalRestored = ids.length;
      }

      vm.isRestoredMultiple = is_multiple;
      vm.selectedIds = ids;

      setTimeout(function () {
        $("#restoreCustomer").modal('show');
      }, 100);
    }
  }
}