/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import helpers from "@/utils/utils";
import Pagination from "@/components/pagination/Pagination.vue";
import ImageLoading from "@/components/loading/ImageLoading.vue";
import RestoreProduct from "../../components/trash-restore-product/RestoreProduct.vue";
import {
  PERMISSION_SHOW_PRODUCT
} from "@/types/const";
import {
  PRODUCT_GET_ALL,
  PRODUCT_GET_ATTRIBUTES,
  PRODUCT_GET_USER_ATTRIBUTES
} from "@/types/actions.type";
import RestoreCustomer from "../../components/trash-restore-customer/RestoreCustomer.vue";

const SEARCH_TIMEOUT_DEFAULT = 10000;

export default {
  components: {
    RestoreCustomer,
    pagination: Pagination,
    ImageLoading: ImageLoading,
    RestoreProduct: RestoreProduct
  },
  mounted() {
    this.loadAsync();
  },
  created() {},
  watch: {},
  computed: {
    helpers() {
      return helpers;
    },
    isLoader() {
      let vm = this;
      if (vm.dataReady === true && vm.attributeReady === true) {
        return true;
      } else {
        return false;
      }
    },
    ...mapGetters(["products", "attributes", "currentTeam", "userCheckAddOn", "userCurrentPermissions"]),
  },
  data() {
    return {
      PERMISSION_SHOW_PRODUCT: PERMISSION_SHOW_PRODUCT,
      isSearch: false,
      isLoaderProducts: false,
      loadDataProducts: false,
      productChecked: [],
      allProductChecked: [],
      productCheckedPerPage: [],
      productCheckAllPerPage: [],
      oldCurrentPage: 0,
      pagination: {
        current_page: 0,
        total_pages: 0,
        total_item: 0,
        per_page: 0,
        items_in_page: 0
      },
      currentParams: {
        deleted_at: 1
      },
      beforeSearchCache: null,
      dataReady: false,
      attributeReady: false,

      selectedIds: [],
      totalRestored: 0,
      isRestoredMultiple: false,
      productName: '',
      searchText: ''
    }
  },
  methods: {
    
    async loadAsync() {
      await this.getAttributes();
      await this.getAll();
    },

    getAll(type = "all") {
      let vm = this;
      let currentPage = vm.pagination.current_page > 0 ? vm.pagination.current_page : 1;
      vm.currentParams.page = currentPage;
      if (vm.currentParams.per_page) {
        delete vm.currentParams.per_page;
      }
      vm.isLoaderProducts = true;
      vm.$store.dispatch(PRODUCT_GET_ALL, vm.currentParams).then(response => {
        if (type != "pagination") {
          vm.productChecked = [];
        }
        if (typeof vm.productCheckAllPerPage[response.data.meta.current_page] == "undefined") {
          vm.productCheckAllPerPage[response.data.meta.current_page] = false;
        }
        if (typeof vm.productCheckedPerPage[response.data.meta.current_page] == "undefined") {
          vm.productCheckedPerPage[response.data.meta.current_page] = [];
        }
        vm.pagination.current_page = response.data.meta.current_page;
        vm.pagination.total_pages = response.data.meta.last_page;
        vm.pagination.total_item = response.data.meta.total;
        vm.pagination.per_page = response.data.meta.per_page;
        vm.oldCurrentPage = response.data.meta.current_page;
        let items = 0;
        if (response.data.meta.current_page < response.data.meta.last_page) {
          items = response.data.meta.per_page;
        } else {
          items = response.data.meta.total - (response.data.meta.per_page * (response.data.meta.last_page - 1));
        }
        vm.pagination.items_in_page = parseInt(items);
        vm.beforeSearchCache = JSON.stringify(response.data.data);
        vm.dataReady = true;
        if (vm.loadSearch) {
          vm.isSearch = true;
        } else {
          vm.isSearch = false;
        }
        vm.loadDataProducts = false;
        vm.isLoaderProducts = false;
      }).catch(error => {
        console.log(error);
      });
    },

    getAttributes() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_GET_ATTRIBUTES).then(() => {
        vm.attributeReady = true;
      }).catch(error => {
        console.log(error);
      });
    },
    getAttributeByUser() {
      let vm = this;
      vm.$store.dispatch(PRODUCT_GET_USER_ATTRIBUTES).then(() => {
        vm.getAttributes();
      }).catch(error => {
        console.log(error);
      });
    },
    checkedProduct(product) {
      let vm = this;
      if (!vm.checkExistValueInChecked(product)) {
        vm.productCheckedPerPage[vm.oldCurrentPage].push(product);
      } else {
        let newChecked = [];
        vm.productCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
          if (item.id !== product.id) {
            newChecked.push(item);
          }
        });
        vm.productCheckedPerPage[vm.oldCurrentPage] = newChecked;
      }
      let itemCheckedInPage = vm.productCheckedPerPage[vm.oldCurrentPage];
      if (itemCheckedInPage && vm.pagination.items_in_page == itemCheckedInPage.length) {
        vm.productCheckAllPerPage[vm.oldCurrentPage] = true;
      } else {
        vm.productCheckAllPerPage[vm.oldCurrentPage] = false;
      }
    },
    checkedAllToggle() {
      let vm = this;

      if (!vm.productCheckAllPerPage[vm.oldCurrentPage]) {
        let itemIsChecked = [];

        if (vm.productCheckedPerPage[vm.oldCurrentPage] && vm.productCheckedPerPage[vm.oldCurrentPage].length > 0) {
          vm.productCheckedPerPage[vm.oldCurrentPage].forEach(function (item) {
            itemIsChecked.push(item.id);
          })
        }
        let newChecked = [];
        vm.products.forEach(function (product) {
          if ($.inArray(product.id, itemIsChecked) == -1) {
            vm.productChecked.push(product);
            newChecked.push(product);
          }
        });
        vm.productCheckedPerPage[vm.oldCurrentPage] = newChecked;
      } else {
        let newChecked = [];
        vm.productChecked.forEach(function (product) {
          if (product.in_page != vm.pagination.current_page) {
            newChecked.push(product);
          }
        });
        vm.productCheckedPerPage[vm.oldCurrentPage] = newChecked;
        vm.productChecked = newChecked;
      }
    },
    checkExistValueInChecked(product) {
      let vm = this;
      if (vm.productCheckedPerPage[vm.oldCurrentPage] && vm.productCheckedPerPage[vm.oldCurrentPage].length > 0) {
        let exist = vm.productCheckedPerPage[vm.oldCurrentPage].find(function (item) {
          return item.id == product.id;
        })
        if (exist && Object.keys(exist).length > 0) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    },
    getPaginationProducts() {
      let vm = this;
      vm.loadDataProducts = true;
      vm.getAll("pagination");
      $('#table-responsive-data').animate({
        scrollLeft: 0
      }, 200);
    },
    getQuantityItemChecked() {
      let vm = this;
      let result = 0;
      if (vm.productChecked.length > 0) {
        if (vm.productChecked.length > vm.pagination.total_item) {
          result = vm.pagination.total_item;
        } else {
          result = vm.productChecked.length;
        }
      } else {
        result = vm.pagination.total_item;
      }
      return result;
    },
    productSearch(event) {
      let vm = this;
      let keyCode = event.keyCode;
      let searchText = event.target.value ? event.target.value : '';
      searchText = searchText ? searchText.trim() : '';

      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }

      if (keyCode === 13) {
        vm.productSearchSetQuery(searchText);
        vm.doneTyping();
      } else {
        vm.clearTimeoutSearch = setTimeout(() => {
          vm.productSearchSetQuery(searchText);
          vm.doneTyping();
        }, SEARCH_TIMEOUT_DEFAULT);
      }
    },
    productSearchForClick() {
      let vm = this;
      let searchText = vm.$refs.productSearch.value ? vm.$refs.productSearch.value : '';
      searchText = searchText ? searchText.trim() : '';
      if (vm.clearTimeoutSearch) {
        clearTimeout(vm.clearTimeoutSearch);
      }
      vm.productSearchSetQuery(searchText);
      vm.doneTyping();
    },
    productSearchSetQuery(searchText) {
      let vm = this;
      vm.currentParams.page = 1;
      vm.currentParams.search = searchText;
      vm.searchText = searchText;
      vm.productChecked = [];
      vm.productCheckAllPerPage = [];
      vm.productCheckedPerPage = [];
      vm.$refs.productSearch.blur();
    },
    productTyping() {
      let vm = this;
      clearTimeout(vm.typingTimer);
    },
    doneTyping() {
      let vm = this;
      if (vm.searchText && vm.searchText.length) {
        vm.pagination.current_page = 1;
        vm.currentParams.search = vm.searchText;
        vm.loadDataProducts = true;
        vm.getAll();
        vm.loadSearch = true;
      } else {
        if (vm.currentParams.search) {
          vm.pagination.current_page = 1;
          delete vm.currentParams.search;
        }
        if (!vm.searchText || (vm.searchText && vm.searchText.length == 0)) {
          vm.loadDataProducts = true;
          vm.getAll();
          vm.loadSearch = false;
        }
      }
    },

    isChange(isChange) {
      if (isChange) {
        let vm = this;
        vm.loadDataProducts = true;
        vm.productChecked = [];
        vm.productCheckAllPerPage = [];
        vm.productCheckedPerPage = [];
        vm.totalRestored = null;
        vm.pagination.current_page = 1;
        $('.nt-table-search-box').val('');
        vm.searchText = "";
        delete vm.currentParams.search;
        vm.loadSearch = false;
        vm.getAll();
      }
    },

    checkBeforeRestore(products, is_multiple = false) {
      let vm = this;
      let ids = [];
      let params = {};

      params.team_id = vm.currentTeamId;

      if (is_multiple) {
        if (products && products.length > 0) {
          products.forEach(function (item) {
            ids.push(item.id);
          });
        }
        vm.totalRestored = products && products.length > 0 ? ids.length : 'tất cả';
      } else {
        ids.push(products.id);
        vm.productName = '';
        if (vm.attributes && vm.attributes.length > 0) {
          vm.attributes.forEach(function (attribute) {
            if (products && products.attributes.length > 0) {
              products.attributes.forEach(function (attributeItem) {
                if (attribute.id == attributeItem.attribute_id && attribute.attribute_code == "product_name") {
                  vm.productName = attributeItem.value;
                }
              })
            }

          });
        }
        vm.totalRestored = ids.length;
      }

      vm.isRestoredMultiple = is_multiple;
      vm.selectedIds = ids;

      setTimeout(function () {
        $("#restoreProduct").modal('show');
      }, 100);
    }
  }
}