import userAPI from "@/services/user";
import {
  userVM
} from "../transformers/index";
import {
  SET_USER_CURRENT,
  SET_USERS_BY_TEAM,
  SET_USER_CHECK_ADD_ON_EXPIRE,
  SET_LIST_PERMISSION,
  SET_LIST_STRUCTURE,
  SET_LIST_PERMISSION_BY_STRUCTURE,
  SET_PERMISSION_BY_CURRENT_USER,
  SET_APP_INFO
} from "@/types/mutations.type";
import {
  USER_CURRENT,
  USER_BY_ID,
  USER_GET_LIST_BY_TEAM,
  USER_CHECK_ADD_ON_EXPIRE,
  GET_LIST_PERMISSION,
  GET_LIST_STRUCTURE,
  GET_PERMISSION_BY_CURRENT_USER,
  GET_LIST_PERMISSION_BY_STRUCTURE,
  SET_PERMISSION_FOR_STRUCTURE,
  GET_APP_INFO,
  SAVE_DEVICE_TOKEN_NOTI,
  DELETE_DEVICE_TOKEN_NOTI
} from "@/types/actions.type";

export const actions = {
  async [USER_CURRENT]({
    commit
  }) {
    try {
      const userCurrent = await userAPI.getCurrentUser();
      if (userCurrent.data.data) {
        commit(SET_USER_CURRENT, userVM.showVM.transform(userCurrent.data.data));
      }
      return userCurrent;
    } catch (error) {
      return error;
    }
  },
  async [USER_BY_ID](context, id) {
    try {
      const userData = await userAPI.show(id);
      return userData;
    } catch (error) {
      return error;
    }
  },
  async [USER_GET_LIST_BY_TEAM]({
    commit
  }) {
    try {
      let users = await userAPI.getUserByTeam();
      if (users.data.data) {
        commit(SET_USERS_BY_TEAM, userVM.listVM.transform(users.data.data));
      }
      return users;
    } catch (e) {
      return e;
    }
  },

  async [GET_LIST_PERMISSION]({
    commit
  }) {
    try {
      let responseData = await userAPI.getListPermission();
      if (responseData.data.data) {
        commit(SET_LIST_PERMISSION, userVM.permissionVM.transform(responseData.data.data));
      }
      return responseData;
    } catch (e) {
      return e;
    }
  },
  async [GET_LIST_STRUCTURE]({
    commit
  }) {
    try {
      let responseData = await userAPI.getListStructure();
      if (responseData.data.data) {
        let listStructureWithRecursive = await userVM.structureVM.transform(responseData.data.data);
        commit(SET_LIST_STRUCTURE, listStructureWithRecursive);
      }
      return responseData;
    } catch (e) {
      return e;
    }
  },
  async [GET_PERMISSION_BY_CURRENT_USER]({
    commit
  }, params) {
    try {
      let responseData = await userAPI.getPermissionByCurrentUser(params);
      if (responseData.data.data) {
        responseData.data.data.is_department = responseData.data.is_department;
        commit(SET_PERMISSION_BY_CURRENT_USER, userVM.listPermissionByCurrentUserVM.transform(responseData.data.data));
      }
      return responseData;
    } catch (e) {
      return e;
    }
  },
  async [GET_LIST_PERMISSION_BY_STRUCTURE]({
    commit
  }) {
    try {
      let responseData = await userAPI.getListPermissionByStructure();
      if (responseData.data.data) {
        commit(SET_LIST_PERMISSION_BY_STRUCTURE, responseData.data.data);
      }
      return responseData;
    } catch (e) {
      return e;
    }
  },
  async [SET_PERMISSION_FOR_STRUCTURE](context, params) {
    try {
      let responseData = await userAPI.setPermissionForStructure(params);
      return responseData;
    } catch (e) {
      return e;
    }
  },
  async [USER_CHECK_ADD_ON_EXPIRE]({
    commit
  }, team_id) {
    try {
      let response = await userAPI.checkAddOnExpire(team_id);
      let apps = await userAPI.getAppInfo();
      let appData = apps.data.data;
      let userCheckAddOnExpire = response.data.data;

      let current_user = userCheckAddOnExpire.current_user || {};
      appData.forEach(app => {
        let isEnabled = current_user[app.app_slug] ? current_user[app.app_slug].is_permission : false;
        // , 'hrm', 'cem'
        const enabledApps = ['crm', 'cem', 'hrm'];
        app.is_enabled = enabledApps.indexOf(app.app_slug) !== -1 ? true : isEnabled;
      });

      commit(SET_APP_INFO, appData);
      commit(SET_USER_CHECK_ADD_ON_EXPIRE, userCheckAddOnExpire);
      return response;
    } catch (e) {
      return e;
    }
  },
  async [GET_APP_INFO]({
    commit
  }) {
    try {
      const response = await userAPI.getAppInfo();
      if (response.data.data) {
        commit(SET_APP_INFO, response.data.data);
      }
      return response;
    } catch (error) {
      return error;
    }
  },
  async [SAVE_DEVICE_TOKEN_NOTI](context, params) {
    try {
      const response = await userAPI.saveDeviceTokenNoti(params);
      return response;
    } catch (error) {
      return error;
    }
  },
  async [DELETE_DEVICE_TOKEN_NOTI](context, params) {
    try {
      const response = await userAPI.deleteDeviceTokenNoti(params);
      return response;
    } catch (error) {
      return error;
    }
  },
}