import { SET_USER_CURRENT, SET_USERS_BY_TEAM, SET_USER_CHECK_ADD_ON_EXPIRE, SET_LIST_PERMISSION, SET_LIST_STRUCTURE, SET_PERMISSION_BY_CURRENT_USER, SET_LIST_PERMISSION_BY_STRUCTURE, SET_APP_INFO } from "@/types/mutations.type";

export const mutations = {
  [SET_USER_CURRENT](state, user) {
    state.currentUser = user;
  },
  [SET_USERS_BY_TEAM](state, users) {
    state.list = users;
  },
  [SET_USER_CHECK_ADD_ON_EXPIRE](state, data) {
    state.userCheckAddOn = data;
  },
  [SET_LIST_PERMISSION](state, data) {
    state.listPermission = data;
  },
  [SET_LIST_STRUCTURE](state, data) {
    state.listStructures = data;
  },
  [SET_PERMISSION_BY_CURRENT_USER](state, userCurrentPermissions) {
    state.userCurrentPermissions = userCurrentPermissions;
  },
  [SET_LIST_PERMISSION_BY_STRUCTURE](state, data) {
    state.listPermissionByStructure = data;
  },
  [SET_APP_INFO](state, data) {
    state.appInfo = data;
  },
};
