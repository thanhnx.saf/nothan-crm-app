import listVM from './list';
import showVM from './show';
import teamVM from './team';
import permissionVM from './listPermission';
import structureVM from './listStructure';
import permissionByStructureVM from './listPermissionByStructure';
import listPermissionByCurrentUserVM from './listPermissionByCurrentUser';

export const userVM = {
  showVM,
  listVM,
  teamVM,
  permissionVM,
  structureVM,
  permissionByStructureVM,
  listPermissionByCurrentUserVM
}
