export default {
  transform(data) {
    let vm = this;
    let new_data = [];

    data.forEach(function (item) {
      new_data.push(vm.transformItem(item));
    });

    return new_data;
  },

  transformItem(data) {
    let transform = {};

    transform = {
      id: data.id,
      full_name: data.full_name,
      email: data.email,
      phone_number: data.phone_number,
      role_id: data.role_id,
      avatar_url: data.avatar_url,
      birthday: data.birthday,
      address: data.address,
      created_at: data.created_at,
      updated_at: data.updated_at
    };

    return transform;
  }
}
