/* eslint-disable no-undef */
export default {
  transform(data) {
    let vm = this;
    let new_data = [];
    let tempId = 1;
    data.forEach(function (item) {
      let tempData = {
        id: tempId,
        current_id: item.id,
        name: item.name,
        category_id: item.category_id ? item.category_id : "",
        code: item.code ? item.code : "",
        show_children: true,
        hidden: false,
        parent: 0,
        created_at: item.created_at ? item.created_at : "",
        updated_at: item.updated_at ? item.updated_at : "",
      };
      new_data.push(tempData);
      let childrenData = vm.transformChildren(item.permissions, tempId);
      if (typeof childrenData != "undefined") {
        $.merge(new_data, childrenData.permissions);
        tempId = childrenData.currentId;
      }
    });

    return new_data;
  },

  transformChildren(data, parent) {
    let permissions = [];
    let countId = parent;
    data.forEach(function (item) {
      countId++;
      let tempData = {
        id: countId,
        permission_id: item.id,
        name: item.name,
        category_id: item.category_id ? item.category_id : "",
        code: item.code ? item.code : "",
        show_children: false,
        hidden: false,
        parent: parent,
        created_at: item.created_at ? item.created_at : "",
        updated_at: item.updated_at ? item.updated_at : "",
      };
      permissions.push(tempData);
    })
    let result = {
      permissions: permissions,
      currentId: countId
    };
    return result;
  }
}
