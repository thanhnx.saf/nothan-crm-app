export default {
  transform(data) {
    let vm = this;
    let new_data = {};
    let is_department = false;

    is_department = data.is_department;

    if (Object.keys(data).length > 0) {
      Object.keys(data).forEach(function (key) {
        new_data[key] = vm.transformItem(data[key]);
      });
    }

    new_data.is_department = is_department;

    return new_data;
  },

  transformItem(data) {
    let transform = {};

    if (data && data.length > 0) {
      data.forEach(function (item) {
        if (!transform[item]) {
          transform[item] = item;
        }
      });
    }

    return transform;
  }
}
