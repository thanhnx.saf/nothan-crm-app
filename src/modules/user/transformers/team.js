export default {
  transform(data) {
    let transform = {};
    transform = {
      id: data.id,
      name: data.name,
      founding_date: data.founding_date,
      user_id: data.user_id,
      business_field_id: data.business_field_id,
      scale: data.scale,
      logo_url: data.logo_url,
    };
    return transform;
  }
}
