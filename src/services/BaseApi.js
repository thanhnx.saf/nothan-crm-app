import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import qs from 'qs'

import './Interceptors'

export default {
  request(config) {
    return axios.request(config)
  },

  get(url, config) {
    return axios.get(url, config)
  },

  getSerialize(url, config) {
    const paramsSerializer = (params) => {
      return qs.stringify(params, { encode: true });
    }
    return axios.get(url, { ...config, paramsSerializer })
  },

  getByBlob(url, config) {
    return axios.get(url, { ...config, responseType: 'blob' })
  },

  delete(url, config) {
    return axios.delete(url, config)
  },

  post(url, data, config) {
    return axios.post(url, data, config)
  },

  put(url, data, config) {
    return axios.put(url, data, config)
  },

  setup() {
    Vue.use(VueAxios, axios);
  }
  
}
