import Vue from 'vue'
import VueCookies from 'vue-cookies'
import { ROOT_SUBDOMAIN } from '@/types/config'

Vue.use(VueCookies)

const ID_TOKEN_KEY = 'token'

const cookies = window.$cookies

export const getToken = () => {
  if (cookies.get(ID_TOKEN_KEY) != null) {
    if (Object.keys(cookies.get(ID_TOKEN_KEY)).length > 0) {
      return cookies.get(ID_TOKEN_KEY).access_token
    }
  }

  return null
}

export const saveToken = (token) => {
  let domain = null

  if (ROOT_SUBDOMAIN !== 'localhost') {
    domain = ROOT_SUBDOMAIN
  }
  cookies.set(ID_TOKEN_KEY, token, null, null, domain)
}

export const destroyToken = () => {
  let domain = null

  if (ROOT_SUBDOMAIN !== 'localhost') {
    domain = ROOT_SUBDOMAIN
  }
  cookies.remove(ID_TOKEN_KEY, null, domain)
}

export default { getToken, saveToken, destroyToken }
