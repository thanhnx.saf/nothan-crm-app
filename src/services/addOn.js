import * as config from "@/types/config";
import BaseApi from "./BaseApi";

const apiBaseURL = config.UMS_BASE_URL;

const addOnApi = {
  checkAddOnExpire(teamId) {
    return BaseApi.get(apiBaseURL + "addons/check-expire?team_id=" + teamId);
  },
};

export default addOnApi;
