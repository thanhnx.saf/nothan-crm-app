import * as config from "@/types/config";
import BaseApi from './BaseApi'

const apiBaseURL = config.UMS_BASE_URL;

const apiUrl = apiBaseURL + "users";

const AuthServiceAPI = {
  register(params) {
    return BaseApi.post(apiUrl, params);
  },
  login(params) {
    return BaseApi.post(apiBaseURL + "oauth2/token", params);
  },
};
export default AuthServiceAPI;
