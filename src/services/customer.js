/* eslint-disable no-undef */
import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.CMS_BASE_URL;
const apiUrl = baseUrl + "customers";
const apiAttribute = baseUrl + "attributes";
const apiFilter = baseUrl + "filters";
const apiCustomerType = baseUrl + "customer-types";

const mailBaseUrl = config.MAIL_BASE_URL;

const apiContact = baseUrl + "contacts";
const apiReport = baseUrl + "reports-customer";

export default {
  getCustomerAttribute(params) {
    return BaseApi.getSerialize(apiAttribute + '/attribute-import', {
      params
    })
  },

  parseParams(params) {
    let parseParams = {};
    let currentPage = typeof params !== "undefined" && params.hasOwnProperty("page") > 0 ? params.page : false;
    let perPage = typeof params !== "undefined" && params.hasOwnProperty("per_page") > 0 ? params.per_page : false;
    let search = typeof params !== "undefined" && params.hasOwnProperty("search") > 0 ? params.search : false;
    let sort = typeof params !== "undefined" && params.hasOwnProperty("sort") > 0 ? params.sort : false;
    let filter = typeof params !== "undefined" && params.hasOwnProperty("filter") > 0 ? params.filter : false;
    let deletedAt = typeof params !== "undefined" && params.hasOwnProperty("deleted_at") > 0 ? params.deleted_at : false;

    if (deletedAt !== false) {
      parseParams.deleted_at = deletedAt;
    }

    if (currentPage !== false) {
      parseParams.page = currentPage;
    }
    if (search !== false) {
      parseParams.search = search;
    }
    if (sort !== false) {
      if (sort.attribute_id == "type_customer") {
        sort.type_sort = "customer_types";
      } else if ($.isNumeric(sort.attribute_id)) {
        sort.type_sort = "attributes";
      }
      parseParams.sort = sort;
    }
    if (filter !== false) {
      let filterWithType = {
        assigners: [],
        attributes: []
      };
      filter.forEach(function (item) {
        if (item.attribute_id == "charge_person") {
          filterWithType.assigners.push(item);
        }
        if (typeof item.attribute_id !== "undefined" && item.attribute_id !== "charge_person") {
          filterWithType.attributes.push(item);
        } else {
          return;
        }
      })
      parseParams.filter = filterWithType;
    }
    if (perPage !== false) {
      parseParams.per_page = perPage;
    }
    return parseParams;
  },

  parseParamsUrl(params) {
    let parseParams = {};
    let currentPage = typeof params !== "undefined" && params.hasOwnProperty("page") > 0 ? params.page : false;
    let perPage = typeof params !== "undefined" && params.hasOwnProperty("per_page") > 0 ? params.per_page : false;
    let search = typeof params !== "undefined" && params.hasOwnProperty("search") > 0 ? params.search : false;
    let sort = typeof params !== "undefined" && params.hasOwnProperty("sort") > 0 ? params.sort : false;
    let filter = typeof params !== "undefined" && params.hasOwnProperty("filter") > 0 ? params.filter : false;
    if (currentPage !== false) {
      parseParams.page = currentPage;
    }
    if (search !== false) {
      parseParams.search = search;
    }
    if (sort !== false) {
      if (sort.attribute_id == "type_customer") {
        sort.type_sort = "customer_types";
      } else if ($.isNumeric(sort.attribute_id)) {
        sort.type_sort = "attributes";
      } else if (sort.attribute_id == "charge_person") {
        sort.type_sort = "assigners";
      }
      parseParams.sort = sort;
    }
    if (filter !== false) {
      filter.forEach(function (item) {
        let value = item.attribute_value;
        if (typeof value == "object") {
          let tempValue = "";
          value.forEach(function (item) {
            tempValue += item + ",";
          })
          let temp = tempValue.trim();
          value = temp.substring(0, temp.length - 1);
        }
        parseParams[item.attribute_code] = value;
      })
    }
    if (perPage !== false) {
      parseParams.per_page = perPage;
    }
    return parseParams;
  },

  getAll(params) {
    // let parseParamsUrl = this.parseParamsUrl(params);
    // let paramSerialize = qs.stringify(parseParamsUrl, {encode: false});
    // let currentPath = router.history.current.path;
    // window.history.replaceState(null, null, currentPath + "?" + paramSerialize );
    let parseParams = this.parseParams(params);
    return BaseApi.getSerialize(apiUrl, {
      params: parseParams
    });
  },

  getCountCustomerByFilter(params) {
    let parseParams = this.parseParams(params);
    return BaseApi.getSerialize(baseUrl + 'customer-filter', {
      params: parseParams
    });
  },

  dowloadDefault(teamId) {
    return BaseApi.getByBlob(apiUrl + "/download-default?team_id=" + teamId);
  },
  dowloadFileAfterChangeCache(params) {
    return BaseApi.post(apiUrl + "/download-file-cache", params);
  },
  dowloadFileAfterChange(key) {
    return BaseApi.getByBlob(apiUrl + "/download-file?key=" + key);
  },
  importCheckFileExcel(params) {
    return BaseApi.post(apiUrl + '/check-file-excel', params);
  },
  show(id) {
    return BaseApi.get(`${apiUrl}/${id}`);
  },
  showCustomer(params) {
    return BaseApi.getSerialize(apiUrl + "/" + params.customer_id, {
      params
    });
  },
  create(params) {
    return BaseApi.post(apiUrl, params);
  },
  update(id, params) {
    return BaseApi.put(apiUrl + "/" + id, params);
  },
  assign(params) {
    return BaseApi.put(apiUrl + "/assign", params);
  },
  removeAssign(params) {
    return BaseApi.post(apiUrl + "/remove-assigner", params);
  },
  getAllAssignerInTeam() {
    return BaseApi.get(apiUrl + "/user-is-being-assigned");
  },
  delete(id) {
    return BaseApi.delete(apiUrl + "/" + id);
  },
  multipleDelete(params) {
    return BaseApi.post(apiUrl + '/sort-delete', params);
  },
  forceDelete(id) {
    return BaseApi.delete(apiUrl + "/force-delete/" + id);
  },
  forceMultipleDelete(ids) {
    return BaseApi.delete(apiUrl + "/force-delete/multi-delete", {
      params: ids
    });
  },
  checkCustomerHasDealBeforeDelete(params) {
    return BaseApi.post(apiUrl + '/check-customer-has-deal', params);
  },
  restore(id) {
    return BaseApi.put(apiUrl + "/restore/", id);
  },
  multipleRestore(params) {
    return BaseApi.post(apiUrl + "/restore-delete", params);
  },
  getAttributes() {
    return BaseApi.get(apiAttribute);
  },
  getAttributeByUser() {
    return BaseApi.get(apiAttribute + "/visible");
  },
  updateAttributebyUser(params) {
    return BaseApi.put(apiAttribute + "/visible", params);
  },
  deleteAttributeById(params) {
    return BaseApi.delete(apiAttribute + '/' + params.id, {
      params
    });
  },
  getFilters(params) {
    return BaseApi.get(apiFilter, {
      params
    });
  },

  getAttributesByFilter(filterId) {
    return BaseApi.get(apiFilter + "/" + filterId + "/attributes");
  },

  createFilter(params) {
    return BaseApi.post(apiFilter, params);
  },

  updateFilter(params) {
    let filterId = params.filter_id;
    delete params.filter_id;
    return BaseApi.put(apiFilter + "/" + filterId, params);
  },

  deleteFilter(filterId, params) {
    return BaseApi.delete(apiFilter + "/" + filterId, {
      params
    });
  },

  checkExists(params) {
    return BaseApi.post(apiUrl + "/check-attribute-exist", params);
  },

  assignPersonCharge(params) {
    return BaseApi.put(apiUrl + "/assign", params);
  },

  getAllAttributeDataType() {
    return BaseApi.get(baseUrl + "attribute-data-type");
  },

  createAttribute(params) {
    return BaseApi.post(baseUrl + "attributes", params);
  },

  checkExistsCreateAttribute(params) {
    return BaseApi.post(baseUrl + "attributes/check-attribute-value", params);
  },

  updateAttribute(id, params) {
    return BaseApi.put(baseUrl + "attributes/" + id, params);
  },

  getCustomerPrefix() {
    return BaseApi.get(apiUrl + "/prefix");
  },

  updateCustomerPrefix(params) {
    return BaseApi.put(apiUrl + "/prefix", params);
  },

  exportToExcel() {
    return BaseApi.get(apiUrl + "/export-to-excel");
  },

  getAttributeOptions(attribute_id) {
    return BaseApi.get(baseUrl + "attribute-options?attribute_id=" + attribute_id);
  },

  createAttributeOptions(params) {
    return BaseApi.post(baseUrl + "attribute/options", params);
  },

  updateAttributeOptions(option_id, params) {
    return BaseApi.put(baseUrl + "attribute-options/" + option_id, params);
  },

  deleteAttributeOptions(option_id) {
    return BaseApi.delete(baseUrl + "attribute-options/" + option_id);
  },

  sortAttributeOptions(params) {
    return BaseApi.put(baseUrl + "attribute-options/sort", params);
  },
  importCheckAttributesUnique(params) {
    return BaseApi.post(baseUrl + "attributes/check-attributes-unique", params);
  },
  import(params) {
    return BaseApi.post(apiUrl + "/import", params);
  },
  getCustomerType(id) {
    return BaseApi.get(apiCustomerType, id);
  },
  sendMailAssignersCustomer(params) {
    return BaseApi.post(mailBaseUrl + "mail-service?type=6", params);
  },
  sendMailAddMultipleAssigners(params) {
    return BaseApi.post(mailBaseUrl + "mail-service?type=8", params);
  },
  sendMailDeleteMultipleAssigners(params) {
    return BaseApi.post(mailBaseUrl + "mail-service?type=9", params);
  },

  /*** API Customer Contacts */
  getContacts(params) {
    return BaseApi.getSerialize(apiContact, {
      params
    });
  },

  getAllCustomerContacts(params) {
    return BaseApi.getSerialize(apiUrl + '/list-by-user', {
      params
    });
  },

  createContact(params) {
    return BaseApi.post(apiContact, params);
  },

  updateContact(params) {
    return BaseApi.put(apiContact + "/" + params.id, params);
  },

  deleteContact(params) {
    return BaseApi.delete(apiContact + "/" + params.contact_id, {
      params
    });
  },

  /*** API Customer Reports */

  getInfoTopHardChart(team_id) {
    return BaseApi.get(apiReport + "/hard", team_id);
  },

  getCustomerFlexibleReport(params) {
    return BaseApi.getSerialize(apiReport + "/hard-chart-attributes", {
      params
    });
  },

  getCustomerChargePersonReport(params) {
    return BaseApi.getSerialize(apiReport + "/hard-chart-attribute-charge_person", {
      params
    });
  },

  exportCustomerFlexibleReport(params) {
    return BaseApi.getByBlob(apiReport + "/export-hard-chart-attributes", {
      params
    });
  },

  exportCustomerChargePersonReport(params) {
    return BaseApi.getByBlob(apiReport + "/export-hard-chart-attribute-charge_person", {
      params
    });
  },

  saveConfigCustomerAttributeReportForUser(params) {
    return BaseApi.post(apiReport + "/save-attributes-config", params);
  },

  getConfigCustomerAttributeReportForUser(params) {
    return BaseApi.getSerialize(apiReport + "/user-configs", {
      params
    });
  },

  getInfoHardChart(params) {
    return BaseApi.getSerialize(apiReport + "/hard-chart", {
      params
    });
  },


  getConfigCustomerAttributeTable(params) {
    return BaseApi.getSerialize(apiUrl + "/user-config-visible", {
      params
    });
  },

  changeCustomerAttributeTable(params) {
    return BaseApi.post(apiUrl + "/update-config-visible", params);
  },


  /*** API LEVEL CUSTOMER */
  updateLevelCustomer(params) {
    return BaseApi.post(apiUrl + "/update-level", params);
  },

  emailTemplateEmailFrame(params) {
    return BaseApi.get(baseUrl + 'mail-templates/email-layout-by-id', {
      params
    })
  },

  checkBeforeSendEmailCustomers(params) {
    return BaseApi.post(baseUrl + 'validate-send-customer-emails', params)
  },

  sendEmailCustomers(params) {
    return BaseApi.post(baseUrl + 'send-customer-emails', params)
  },

  checkBeforeUpdateAttributeCustomers(params) {
    return BaseApi.post(apiUrl + '/get-customers-and-attributes-can-update-multiple', params)
  },

  updateAttributeCustomers(params) {
    return BaseApi.post(apiUrl + '/send-update-multiple', params)
  },

  checkValidateBeforeUpdateAttributeCustomers(params) {
    return BaseApi.post(apiUrl + '/validate-update-multiple', params)
  },

  getTotalCustomersPhoneNumber(params) {
    return BaseApi.post(baseUrl + 'sms/get-total-customers-phone-number', params)
  },

  validateSmsSendCustomers(params) {
    return BaseApi.post(baseUrl + 'sms/validate-sms', params)
  },

  sendSmsCustomers(params) {
    return BaseApi.post(baseUrl + 'sms/send-sms-to-queue', params)
  },

  getListFileConvertTable(id) {
    return BaseApi.getSerialize(baseUrl + 'file-import', {
      id
    })
  },

  importListFileConvert(params) {
    return BaseApi.post(baseUrl + 'ask/import', params)
  },

  downloadFileImport(params) {
    return BaseApi.getByBlob( baseUrl + 'ask/import/download/' + params)
  },

  checkFileConvert(params) {
    return BaseApi.get(baseUrl + 'ask/check-file/' + params)
  },

  uploadFileFix(params) {
    return BaseApi.post(baseUrl + 'ask/upload-file', params)
  },

  getConfigListCustomer() {
     return BaseApi.getSerialize(apiUrl + '/user-config-setting')
  }
};
