import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.UMS_BASE_URL

export default {
  getActiveTeamAddons() {
    return BaseApi.getSerialize(`${baseUrl}dashboard/active-team-addons`)
  },

  getNumberOfUsers() {
    return BaseApi.getSerialize(`${baseUrl}dashboard/number-of-users`)
  },

  getNumberOfCustomers() {
    return BaseApi.getSerialize(`${baseUrl}dashboard/number-of-customers`)
  },

  getNumberOfSmss(params) {
    return BaseApi.getSerialize(`${baseUrl}dashboard/number-of-smss`, {
      params
    })
  },

  getNumberOfEmails(params) {
    return BaseApi.getSerialize(`${baseUrl}dashboard/number-of-emails`, {
      params
    })
  },
}
