/* eslint-disable no-undef */
import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.DMS_BASE_URL;
const apiUrl = baseUrl + "deals";
const apiFilter = baseUrl + "filters";
const apiAttribute = baseUrl + "attributes";

const apiReport = baseUrl + "reports-deal";


export default {
  getUserConfig(user_id, action_type = 'pipeline_sort') {
    return BaseApi.get(apiReport + '/user-configs', {
      params: {
        user_id,
        action_type
      }
    })
  },

  updateUserConfig(body) {
    return BaseApi.post(apiReport + '/save-attributes-config', body)
  },

  getDealStatistics(params) {
    return BaseApi.getSerialize(apiUrl + '/calculate-transaction-amount', {
      params
    })
  },

  getResultDeal(params) {
    return BaseApi.post(apiUrl + '/download-file', params, {
      responseType: 'blob'
    })
  },

  importDeal(params) {
    return BaseApi.post(apiUrl + '/import', params)
  },

  validateImportDeal(params) {
    return BaseApi.post(apiUrl + '/validate-excel', params)
  },

  getDefaultAttributes(params) {
    return BaseApi.getSerialize(apiAttribute + '/attribute-import-default', {
      params
    })
  },

  exportExcelDefaultAttribute(params) {
    return BaseApi.post(apiAttribute + '/export-attribute-import-default', params, {
      responseType: 'blob'
    })
  },

  exportExcelAttribute(params) {
    return BaseApi.post(apiAttribute + '/export-attribute-import', params, {
      responseType: 'blob'
    })
  },

  getDealAttribute(params) {
    return BaseApi.getSerialize(apiAttribute + '/attribute-import', {
      params
    })
  },

  importCheckFileExcel(params) {
    return BaseApi.post(apiUrl + '/check-file-excel', params);
  },

  parseParams(params) {
    let parseParams = {};
    parseParams.sort_by = params.sort_by
    parseParams.sort_direction = params.sort_direction
    let currentPage = typeof params !== "undefined" && params.hasOwnProperty("page") > 0 ? params.page : false;
    let perPage = typeof params !== "undefined" && params.hasOwnProperty("per_page") > 0 ? params.per_page : false;
    let search = typeof params !== "undefined" && params.hasOwnProperty("search") > 0 ? params.search : false;
    let sort = typeof params !== "undefined" && params.hasOwnProperty("sort") > 0 ? params.sort : false;
    let filter = typeof params !== "undefined" && params.hasOwnProperty("filter") > 0 ? params.filter : false;
    let customerId = typeof params !== "undefined" && params.hasOwnProperty("customer_id") > 0 ? params.customer_id : false;
    if (currentPage !== false) {
      parseParams.page = currentPage;
    }
    if (customerId !== false) {
      parseParams.customer_id = customerId;
    }
    if (search !== false) {
      parseParams.search = search;
    }
    if (sort !== false) {
      if ($.isNumeric(sort.attribute_id)) {
        sort.type_sort = "attributes";
      }
      parseParams.sort = sort;
    }
    if (filter !== false) {
      let filterWithType = {
        assigners: [],
        attributes: []
      };
      filter.forEach(function (item) {
        if (item.attribute_id == "charge_person") {
          filterWithType.assigners.push(item);
        }
        if (typeof item.attribute_id !== "undefined" && item.attribute_id !== "charge_person") {
          filterWithType.attributes.push(item);
        } else {
          return;
        }
      })
      parseParams.filter = filterWithType;
    }
    if (perPage !== false) {
      parseParams.per_page = perPage;
    }
    return parseParams;
  },

  getAll(params) {
    let parseParams = this.parseParams(params);
    return BaseApi.getSerialize(apiUrl, {
      params: parseParams
    });
  },

  getAllDealByCustomer(params) {
    return BaseApi.getSerialize(apiUrl + "/deal-by-customer", {
      params
    });
  },

  getAllProductByDealId(params) {
    return BaseApi.getSerialize(apiUrl + "/list-product-by-deal", {
      params
    });
  },

  show(id) {
    return BaseApi.get(`${apiUrl}/${id}`);
  },

  showDeal(params) {
    return BaseApi.getSerialize(apiUrl + "/" + params.deal_id, {
      params
    });
  },

  getAttributes() {
    return BaseApi.get(apiAttribute);
  },

  getAttributeByUser() {
    return BaseApi.get(apiAttribute + "/visible");
  },

  create(params) {
    return BaseApi.post(apiUrl, params);
  },

  update(id, params) {
    return BaseApi.put(apiUrl + '/' + id, params);
  },

  delete(id) {
    return BaseApi.delete(apiUrl + '/' + id);
  },

  multiDelete(ids) {
    return BaseApi.delete(apiUrl + '/multi', {
      params: ids
    });
  },

  allDelete() {
    return BaseApi.delete(apiUrl + '/all');
  },

  forceDelete(id) {
    return BaseApi.delete(apiUrl + '/force-delete/' + id);
  },

  forceMultiDelete(ids) {
    return BaseApi.delete(apiUrl + '/force-delete/multi', {
      params: ids
    });
  },

  forceAllDelete() {
    return BaseApi.delete(apiUrl + '/force-delete/all');
  },

  restore(id) {
    return BaseApi.post(apiUrl + '/restore/' + id);
  },

  multiRestore(params) {
    return BaseApi.post(apiUrl + '/restore/multi', params);
  },

  allRestore() {
    return BaseApi.post(apiUrl + '/restore/all');
  },

  printToExcel(params) {
    return BaseApi.getByBlob(apiUrl + '/export', {
      params
    });
  },

  totalPriceOfAllDeal() {
    return BaseApi.get(apiUrl + '/total-deal-money');
  },

  getAllPaymentHistory(params) {
    // let urlRequest = apiUrl + '/payment-history'+'?team_id='+params['team_id']+'&deal_id='+params['deal_id'];
    return BaseApi.getSerialize(apiUrl + '/payment-history', {
      params
    });
  },

  createPayment(params) {
    return BaseApi.post(apiUrl + '/payment-history', params);
  },

  dealUpdateDeliveryStatus(params) {
    return BaseApi.put(baseUrl + 'deal-product/update-status-delivery', params);
  },

  checkPaymentOfDeal(body) {
    return BaseApi.post(baseUrl + 'deal-product/check-payment-of-deal', body);
  },

  getDealIdsByFilter(params) {
    let parseParams = this.parseParams(params);
    return BaseApi.getSerialize(apiUrl + '/get-deal-ids-by-filter', {
      params: parseParams
    })
  },

  getListCustomerByProductUsed(params) {
    return BaseApi.getSerialize(apiUrl + "/list-customer-by-product-used", {
      params
    });
  },

  getDealFilters(params) {
    return BaseApi.get(apiFilter, {
      params
    });
  },

  getDealFilterAttributes(id) {
    return BaseApi.get(apiFilter + "/" + id + "/attributes");
  },

  dealFilterCreate(params) {
    return BaseApi.post(apiFilter, params);
  },

  dealFilterUpdate(id, params) {
    return BaseApi.put(apiFilter + "/" + id, params);
  },

  dealFilterDelete(id, params) {
    return BaseApi.delete(apiFilter + "/" + id, {
      params
    });
  },
  /*** API Customer Reports */

  getInfoTopHardChart(params) {
    return BaseApi.get(apiReport + "/hard", { params });
  },

  getInfoHardChart(params) {
    return BaseApi.getSerialize(apiReport + "/hard-chart", {
      params
    });
  },

  getDealFlexibleReport(params) {
    return BaseApi.getSerialize(apiReport + "/hard-chart-attributes", {
      params
    });
  },

  getDealChargePersonReport(params) {
    return BaseApi.getSerialize(apiReport + "/hard-chart-attribute-charge_person", {
      params
    });
  },

  exportDealFlexibleReport(params) {
    return BaseApi.getByBlob(apiReport + "/export-hard-chart-attributes", {
      params
    });
  },

  exportDealChargePersonReport(params) {
    return BaseApi.getByBlob(apiReport + "/export-hard-chart-attribute-charge_person", {
      params
    });
  },

  saveConfigDealAttributeReportForUser(params) {
    return BaseApi.post(apiReport + "/save-attributes-config", params);
  },

  getConfigDealAttributeReportForUser(params) {
    return BaseApi.getSerialize(apiReport + "/user-configs", {
      params
    });
  },

  exportListDeal(params) {
    return BaseApi.getByBlob(apiUrl + "/export/list", {
      params
    });
  },

  checkPaymentStatusSuccessOfDeal(params) {
    return BaseApi.post(baseUrl + "deal-product/check-success-of-deal", params);
  },

  getAllAttributeDataType() {
    return BaseApi.get(baseUrl + "attribute/get-data-type");
  },

  createAttribute(params) {
    return BaseApi.post(baseUrl + "attributes", params);
  },

  checkExistsCreateAttribute(params) {
    return BaseApi.post(baseUrl + "attributes/check-attribute-value", params);
  },

  updateAttribute(id, params) {
    return BaseApi.put(baseUrl + "attributes/" + id, params);
  },

  deleteAttributeById(params) {
    return BaseApi.delete(apiAttribute + '/' + params.id, {
      params
    });
  },
  sortAttributes(params) {
    return BaseApi.post(apiAttribute + '/sort', params);
  },
  checkProductHasDealBeforeDelete(params) {
    return BaseApi.post(baseUrl + "deal-product/check-product-use-team", params);
  }
}
