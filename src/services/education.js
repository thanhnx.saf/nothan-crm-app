/* eslint-disable no-undef */
import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.EMS_BASE_URL;
const baseUrlCMS = config.CMS_BASE_URL;
const apiClassUrl = baseUrl + 'classes';
const apiFilter = baseUrl + 'filters';

export default {
  parseParams(params) {
    let parseParams = {};
    let currentPage = typeof params !== "undefined" && params.hasOwnProperty("page") > 0 ? params.page : false;
    let perPage = typeof params !== "undefined" && params.hasOwnProperty("per_page") > 0 ? params.per_page : false;
    let search = typeof params !== "undefined" && params.hasOwnProperty("search") > 0 ? params.search : false;
    let sort = typeof params !== "undefined" && params.hasOwnProperty("sort") > 0 ? params.sort : false;
    let filter = typeof params !== "undefined" && params.hasOwnProperty("filter") > 0 ? params.filter : false;
    let deletedAt = typeof params !== "undefined" && params.hasOwnProperty("deleted_at") > 0 ? params.deleted_at : false;

    if (deletedAt !== false) {
      parseParams.deleted_at = deletedAt;
    }

    if (currentPage !== false) {
      parseParams.page = currentPage;
    }
    if (search !== false) {
      parseParams.search = search;
    }
    if (sort !== false) {
      if (sort.attribute_id == "type_customer") {
        sort.type_sort = "customer_types";
      } else if ($.isNumeric(sort.attribute_id)) {
        sort.type_sort = "attributes";
      }
      parseParams.sort = sort;
    }
    if (filter !== false) {
      let filterWithType = {
        assigners: [],
        attributes: []
      };
      filter.forEach(function (item) {
        if (item.attribute_id == "charge_person") {
          filterWithType.assigners.push(item);
        } if (typeof item.attribute_id !== "undefined" && item.attribute_id !== "charge_person") {
          filterWithType.attributes.push(item);
        } else {
          return;
        }
      })
      parseParams.filter = filterWithType;
    }
    if (perPage !== false) {
      parseParams.per_page = perPage;
    }
    return parseParams;
  },

  getAll(params) {
    let parseParams = this.parseParams(params);
    return BaseApi.getSerialize(baseUrl + "classes", { params: parseParams });
  },
  create(params) {
    return BaseApi.post(baseUrl + "classes", params);
  },
  checkExistsCreate(params) {
    return BaseApi.post(baseUrl + "classes/check-name", params);
  },
  show(id) {
    return BaseApi.get(`${apiClassUrl}/${id}`);
  },
  update(id, params) {
    return BaseApi.put(baseUrl + "classes/" + id, params);
  },
  delete(params) {
    return BaseApi.delete(baseUrl + "classes/" + params.class_id, { params });
  },
  updateStatusClass(params) {
    return BaseApi.post(baseUrl + "classes/change-status-close/" + params.id, params);
  },
  getAllListLessonClass(params) {
    return BaseApi.getSerialize(baseUrl + "classes/list-lesson-class", { params });
  },
  updateLessonClass(params) {
    return BaseApi.post(baseUrl + "lessons/update-lesson", params);
  },
  checkInLesson(params) {
    return BaseApi.post(baseUrl + "students/muster", params);
  },
  checkHistoryCheckInLesson(params) {
    return BaseApi.post(baseUrl + "students/check-history-checkin", params);
  },
  joinStudentsToClass(params) {
    return BaseApi.post(baseUrl + "classes/save-member-in-class", params);
  },
  getAllStudentInTheLesson(params) {
    return BaseApi.getSerialize(baseUrl + "students", { params });
  },
  getAllStudentOfClass(params) {
    return BaseApi.getSerialize(baseUrl + "classes/get-member-in-class", { params });
  },
  checkHomeworkLesson(params) {
    return BaseApi.post(baseUrl + "students/home-work", params);
  },
  checkHistoryCheckHomeworkLesson(params) {
    return BaseApi.post(baseUrl + "students/check-home-work", params);
  },
  saveNoteHomeworkLesson(params) {
    return BaseApi.post(baseUrl + "students/note-home-work", params);
  },
  teacherCommentLesson(params) {
    return BaseApi.post(baseUrl + "students/note", params);
  },
  getMemberInClassOfDeal(params) {
    return BaseApi.getSerialize(apiClassUrl + "/get-member-in-class-of-deal", { params });
  },
  removeStudentClass(params) {
    return BaseApi.post(apiClassUrl + "/remove-student-class", params);
  },
  reserveStudentClass(params) {
    return BaseApi.post(apiClassUrl + "/reserve-student-class", params);
  },
  changeClass(params) {
    return BaseApi.post(apiClassUrl + "/change-class-student", params);
  },
  getClassByProductId(params) {
    return BaseApi.getSerialize(apiClassUrl + "/get-by-product-id", { params });
  },
  getLearningOfStudent(params) {
    return BaseApi.getSerialize(apiClassUrl + "/get-learning-of-member", { params });
  },
  getListClassOfCustomerDetail(params) {
    return BaseApi.getSerialize(baseUrl + 'students/list-class', { params });
  },
  exportListMemberClass(params) {
    return BaseApi.getByBlob(baseUrl + "exports/list-member-class", { params });
  },
  exportLearningSituation(params) {
    return BaseApi.getByBlob(baseUrl + "exports/history-member-class", { params });
  },
  exportListMemberWaitingClass(params) {
    return BaseApi.getByBlob(baseUrl + "exports/list-member-queue-class", { params });
  },

  getAllStudentEducation(params) {
    let parseParams = this.parseParams(params);
    return BaseApi.getSerialize(baseUrl + "user-education", { params: parseParams });
  },
  exportListStudentEducation(params) {
    return BaseApi.getByBlob(baseUrl + "user-education/download", { params });
  },
  getFilters(params) {
    return BaseApi.get(apiFilter, { params });
  },

  getAttributesByFilter(filterId) {
    return BaseApi.get(apiFilter + "/" + filterId + "/attributes");
  },

  createFilter(params) {
    return BaseApi.post(apiFilter, params);
  },

  updateFilter(params) {
    let filterId = params.filter_id;
    delete params.filter_id;
    return BaseApi.put(apiFilter + "/" + filterId, params);
  },

  deleteFilter(filterId, params) {
    return BaseApi.delete(apiFilter + "/" + filterId, { params });
  },

  createAccountEducation(params) {
    return BaseApi.post(baseUrlCMS + "customers-account/create", params);
  },

  blockAccountEducation(params) {
    let selected_id = params.selected_id;
    return BaseApi.post(baseUrl + "user-education/disable-account" + '/' + selected_id, { params });
  },

  activeAccountEducation(params) {
    let selected_id = params.selected_id;
    return BaseApi.post(baseUrl + "user-education/active-account-disable" + '/' + selected_id, { params });
  },

  getAccountEduOnlineStatus(params) {
    return BaseApi.getSerialize(baseUrl + "user-education/account", { params });
  }
}
