import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.NMS_BASE_URL;
const apiUrl = baseUrl + "notes";

export default {
  create(params) {
    return BaseApi.post(apiUrl, params);
  },
  getAllByObjectId(params) {
    let parseParams = { 'object_id': params.object_id, 'type': params.type };
    return BaseApi.getSerialize(apiUrl + "/by-object", { params: parseParams });
  },
}
