import router from '@/router'

import BaseApi from './BaseApi'
import * as config from "@/types/config";

const team_id = router && router.history.current.params['team_id']
const baseNotiUrl = config.NOTI_BASE_URL

export default {
  markAsSeen() {
    return BaseApi.post(baseNotiUrl + 'update-seen-status', {
      team_id
    })
  },

  markAsRead(notification_id) {
    return BaseApi.post(baseNotiUrl + 'update-read-status', {
      team_id,
      notification_id
    })
  },

  getList(user_id, page) {
    return BaseApi.get(baseNotiUrl + 'get-list-notification-by-team', {
      params: {
        team_id,
        user_id,
        page
      }
    })
  }
}