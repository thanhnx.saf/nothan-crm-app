/* eslint-disable no-undef */
import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.PMS_BASE_URL;
const apiUrl = baseUrl + "products";
const apiAttribute = baseUrl + "attributes";

export default {
  getProductAttribute(params) {
    return BaseApi.getSerialize(apiAttribute + '/attribute-import', { params })
  },

  parseParams(params) {
    let parseParams = {};
    let currentPage = typeof params !== "undefined" && params.hasOwnProperty("page") > 0 ? params.page : false;
    let search = typeof params !== "undefined" && params.hasOwnProperty("search") > 0 ? params.search : false;
    let sort = typeof params !== "undefined" && params.hasOwnProperty("sort") > 0 ? params.sort : false;
    let filter = typeof params !== "undefined" && params.hasOwnProperty("filter") > 0 ? params.filter : false;
    let products = typeof params !== "undefined" && params.hasOwnProperty("products") > 0 ? params.products : false;

    let deletedAt = typeof params !== "undefined" && params.hasOwnProperty("deleted_at") > 0 ? params.deleted_at : false;

    if (deletedAt !== false) {
      parseParams.deleted_at = deletedAt;
    }

    if (currentPage !== false) {
      parseParams.page = currentPage;
    }
    if (search !== false) {
      parseParams.search = search;
    }
    if (sort !== false) {
      if (sort.attribute_id == "type_customer") {
        sort.type_sort = "customer_types";
      } else if ($.isNumeric(sort.attribute_id)) {
        sort.type_sort = "attributes";
      }
      parseParams.sort = sort;
    }
    if (filter !== false) {
      let filterWithType = {
        customer_types: [],
        assigners: [],
        attributes: []
      };
      filter.forEach(function (item) {
        if (item.attribute_id == "type_customer") {
          filterWithType.customer_types.push(item);
        } else if (item.attribute_id == "charge_person") {
          filterWithType.assigners.push(item);
        } else if ($.isNumeric(item.attribute_id)) {
          filterWithType.attributes.push(item);
        } else {
          return;
        }
      })
      parseParams.filter = filterWithType;
    }
    if (products !== false) {
      parseParams.products = products;
    }
    return parseParams;
  },
  getAll(params) {
    let parseParams = this.parseParams(params);
    return BaseApi.getSerialize(apiUrl, { params: parseParams });
  },
  getAllWithoutPaginate(params) {
    return BaseApi.getSerialize(apiUrl + "/products-by-team", { params });
  },
  getAttributes() {
    return BaseApi.get(apiAttribute);
  },
  getAttributeByUser() {
    return BaseApi.get(apiAttribute + "/visible");
  },
  create(params) {
    return BaseApi.post(apiUrl, params);

  },
  checkExists(params) {
    return BaseApi.post(apiUrl + "/check-attribute-exist", params);
  },
  update(id, params) {
    return BaseApi.put(apiUrl + '/' + id, params);
  },
  getProductByTeams(params) {
    return BaseApi.getSerialize(apiUrl + "/products-by-team", { params });
  },
  show(id) {
    return BaseApi.get(`${apiUrl}/${id}`);
  },
  delete(id) {
    return BaseApi.delete(apiUrl + '/' + id);
  },
  multipleDelete(params) {
    return BaseApi.post(apiUrl + '/sort-delete', params);
  },
  allDelete() {
    return BaseApi.delete(apiUrl + '/all');
  },
  forceDelete(id) {
    return BaseApi.delete(apiUrl + '/force-delete/' + id);
  },
  forceMultipleDelete(ids) {
    return BaseApi.delete(apiUrl + '/force-delete/multi', { params: ids });
  },
  checkProductHasDealBeforeDelete(params) {
    return BaseApi.post(apiUrl + '/force-delete/multi', params);
  },
  forceAllDelete() {
    return BaseApi.delete(apiUrl + '/force-delete/all');
  },
  restore(id) {
    return BaseApi.put(apiUrl + '/restore/' + id);
  },
  multipleRestore(params) {
    return BaseApi.post(apiUrl + '/restore-delete', params);
  },
  showProduct(params) {
    return BaseApi.querySerialize(apiUrl + "/" + params.product_id, { params });
  },
  /*show(id) {
      return BaseApi.get(`${apiUrl}/${id}`);
  },
  import(file) {
      return BaseApi.post(apiUrl + '/import', file);
  },
  update(id, params) {
      return BaseApi.put(apiUrl + '/' + id, params);
  },
  multipleUpdate(params) {
      return BaseApi.put(apiUrl + '/multi', params);
  },
  delete(id) {
      return BaseApi.delete(apiUrl + '/' + id);
  },
  multipleDelete(ids) {
      return BaseApi.delete(apiUrl + '/multi', { params:ids });
  },
  allDelete() {
      return BaseApi.delete(apiUrl + '/all');
  },
  forceDelete(id) {
      return BaseApi.delete(apiUrl + '/force-delete/' + id);
  },
  forceMultipleDelete(ids) {
      return BaseApi.delete(apiUrl + '/force-delete/multi', { params: ids });
  },
  forceAllDelete() {
      return BaseApi.delete(apiUrl + '/force-delete/all');
  },
  restore(id) {
      return BaseApi.put(apiUrl + '/restore/' + id);
  },
  multipleRestore(params) {
      return BaseApi.put(apiUrl + '/restore/multi', params);
  },
  allRestore() {
      return BaseApi.get(apiUrl + '/restore/all');
  },
  getAttributes(userId) {
      return BaseApi.get(apiUrl + '/attributes/' + userId);
  }*/
}
