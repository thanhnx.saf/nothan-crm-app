import router from '@/router'

import BaseApi from './BaseApi'
import * as config from "@/types/config";

const team_id = router && router.history.current.params['team_id']
const baseTaskUrl = config.TASK_BASE_URL

export default {
  getCategories() {
    return BaseApi.get(`${baseTaskUrl}categories`, {
      params: {
        team_id,
      }
    })
  },

  queryAllTasks(params) {
    return BaseApi.get(`${baseTaskUrl}tasks/query-all`, { params })
  },

  createTask(body) {
    return BaseApi.post(`${baseTaskUrl}tasks`, { ...body, team_id })
  },

  getTasks(category_id) {
    return BaseApi.get(`${baseTaskUrl}tasks`, {
      params: {
        category_id
      }
    })
  },

  updateCategoryId(id, category_id) {
    return BaseApi.put(`${baseTaskUrl}tasks/update-category-id`, null, {
      params: {
        id,
        category_id
      }
    })
  },

  updateTask(body, id) {
    return BaseApi.put(`${baseTaskUrl}tasks/${id}`, body)
  },

  deleteTask(id) {
    return BaseApi.delete(`${baseTaskUrl}tasks/${id}`)
  },

  saveUserConfig(body) {
    return BaseApi.post(`${baseTaskUrl}settings`, {
      ...body,
      team_id
    })
  },

  getUserConfig() {
    return BaseApi.get(`${baseTaskUrl}settings`, {
      params: {
        team_id,
      }
    })
  },

  updateUserConfig(body, id) {
    return BaseApi.put(`${baseTaskUrl}settings/${id}`, {
      ...body,
      team_id
    })
  },
}