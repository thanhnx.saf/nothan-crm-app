import * as config from "@/types/config";
import BaseApi from './BaseApi'

const baseUrl = config.UMS_BASE_URL;
const apiUrl = baseUrl + "users";
const teamUrl = baseUrl + "teams";
const permissionUrl = baseUrl + "permission";
const structureUrl = baseUrl + "departments";

export default {
  getAll() {
    return BaseApi.get(apiUrl);
  },
  show(id) {
    return BaseApi.get(`${apiUrl}/${id}`);
  },
  update(id, params) {
    return BaseApi.put(apiUrl + "/" + id, params);
  },
  delete(id) {
    return BaseApi.delete(apiUrl + "/" + id);
  },
  getCurrentUser() {
    return BaseApi.get(apiUrl + "/token");
  },
  getUserByTeam() {
    return BaseApi.get(apiUrl + "/team");
  },
  getCurrentTeam(teamId) {
    return BaseApi.get(teamUrl + "/" + teamId);
  },
  checkAddOnExpire(teamId) {
    return BaseApi.get(baseUrl + "addons/check-expire?team_id=" + teamId);
  },
  getListPermission() {
    return BaseApi.get(permissionUrl + "/permission-by-team");
  },
  getListStructure() {
    return BaseApi.get(structureUrl);
  },
  getPermissionByCurrentUser(params) {
    return BaseApi.post(baseUrl + "permission/check", params);
  },
  getListPermissionByStructure() {
    return BaseApi.get(permissionUrl + "/list");
  },
  setPermissionForStructure(params) {
    return BaseApi.post(permissionUrl + "/set-permission", params);
  },
  getAppInfo() {
    return BaseApi.get(baseUrl + "app-info/all-information");
  },

  saveDeviceTokenNoti(params) {
    return BaseApi.post(apiUrl + "/device-token", params);
  },

  deleteDeviceTokenNoti(params) {
    return BaseApi.post(apiUrl + "/device-token/delete", params);
  }
}