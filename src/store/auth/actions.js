import { LOGOUT } from "@/types/actions.type"
import { PURGE_AUTH } from "@/types/mutations.type"

export const actions = {
  [LOGOUT]({ commit }) {
    commit(PURGE_AUTH);
  },
}
