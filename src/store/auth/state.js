import JwtService from "@/services/Jwt";

export const state = {
  errors: {},
  token: {},
  isAuthenticated: !!JwtService.getToken()
};
