export const CMS_BASE_URL = process.env.VUE_APP_CMS_BASE_URL;
export const FMS_BASE_URL = process.env.VUE_APP_FMS_BASE_URL;
export const UMS_BASE_URL = process.env.VUE_APP_UMS_BASE_URL;
export const ID_BASE_URL = process.env.VUE_APP_ID_BASE_URL;
export const EM_BASE_URL = process.env.VUE_APP_EDU_BASE_URL;
export const ROOT_SUBDOMAIN = process.env.VUE_APP_SUB_DOMAIN;
export const MAIL_BASE_URL = process.env.VUE_APP_MAIL_BASE_URL;
export const NMS_BASE_URL = process.env.VUE_APP_NMS_BASE_URL;
export const PMS_BASE_URL = process.env.VUE_APP_PMS_BASE_URL;
export const DMS_BASE_URL = process.env.VUE_APP_DMS_BASE_URL;
export const EMS_BASE_URL = process.env.VUE_APP_EMS_BASE_URL;
export const NOTI_BASE_URL = process.env.VUE_APP_NOTI_BASE_URL;
export const TASK_BASE_URL = process.env.VUE_APP_TASK_BASE_URL;

export const FIREBASE_CONFIG = {
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY_CONFIG,
  authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN_CONFIG,
  databaseURL: process.env.VUE_APP_FIREBASE_DATABASE_URL_KEY_CONFIG,
  projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID_CONFIG,
  storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET_CONFIG,
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID_CONFIG,
  appId: process.env.VUE_APP_FIREBASE_APP_ID_CONFIG,
  // measurementId: process.env.VUE_APP_FIREBASE_MEASUREMENT_ID_CONFIG
}
export const FIREBASE_PUBLIC_VAPID_KEY_CONFIG = process.env.VUE_APP_FIREBASE_PUBLIC_VAPID_KEY_CONFIG;

export const TOAST_SUCCESS = "Thành công";
export const TOAST_ERROR = "Lỗi";
export const TOAST_WARNING = "Cảnh báo";
export const TOAST_INFO = "Thông tin";