export const IS_DATE = 2;
export const IS_DATE_TIME = 3;
export const IS_TEXT = 1;
export const IS_TEXT_AREA = 12;
export const IS_EMAIL = 5;
export const IS_PHONE = 4;
export const IS_NUMBER = 6;
export const IS_PRICE = 7;
export const IS_SELECT = 8;
export const IS_SELECT_FIND = 9;
export const IS_RADIO = 14;
export const IS_MULTIPLE_SELECT = 10;
export const IS_CHECKBOX_MULTIPLE = 13;
export const IS_TAG = 11;
export const IS_USER = 15;
export const IS_USER_MULTIPLE = 16;
export const IS_STATUS_BAR = 17;

export const MAX_INPUT = 250;
export const MAX_AREA = 1025;
export const MIN_PHONE = 10;
export const MAX_PHONE = 12;
export const MAX_EMAIL = 75;

// CUSTOMER
export const CUSTOMER_DETAIL_BOX_GENERAL = 1;
export const CUSTOMER_DETAIL_BOX_BUY = 2;
export const CUSTOMER_DETAIL_BOX_USE = 3;

// PERMISSIONS
export const PERMISSION_CREATE_CUSTOMER = "create_customer";
export const PERMISSION_SHOW_CUSTOMER = "show_customer";
export const PERMISSION_UPDATE_CUSTOMER_BASE = "update_customer_base";
export const PERMISSION_UPDATE_CUSTOMER_GENERAL = "update_customer_general";
export const PERMISSION_UPDATE_CUSTOMER_BUYER = "update_buyer";
export const PERMISSION_UPDATE_CUSTOMER_END_USER = "update_end_user";
export const PERMISSION_UPDATE_PERSON_IN_CHARGE = "update_person_in_charge";
export const PERMISSION_DELETE_PERSON_IN_CHARGE = "delete_person_in_charge";
export const PERMISSION_ADD_NOTE_CUSTOMER = "add_note_customer";
export const PERMISSION_IMPORT_CUSTOMER = "import_customer";
export const PERMISSION_EXPORT_CUSTOMER = "export_customer";
export const PERMISSION_UPDATE_LEVEL_MULTIPLE = "update_level_multiple";
export const PERMISSION_MOVE_CUSTOMER_TO_TRASH = "sort_delete_customer";
export const PERMISSION_CUSTOMER_RESTORED = "customer_restore";

export const PERMISSION_ADD_CONTACT = "add_contact";
export const PERMISSION_UPDATE_CONTACT = "update_contact";
export const PERMISSION_DELETE_CONTACT = "delete_contact";

export const PERMISSION_ADD_PRODUCT = "add_product";
export const PERMISSION_SHOW_PRODUCT = "show_product";
export const PERMISSION_UPDATE_PRODUCT = "update_product";
export const PERMISSION_MOVE_PRODUCT_TO_TRASH = "sort_delete_product";
export const PERMISSION_PRODUCT_RESTORED = "restore_product";

export const PERMISSION_ADD_DEAL = "add_deal";
export const PERMISSION_SHOW_DEAL = "show_deal";
export const PERMISSION_UPDATE_DEAL = "update_deal";
export const PERMISSION_UPDATE_PRODUCT_DEAL = "update_product_deal";
export const PERMISSION_UPDATE_DEAL_EXTRA = "update_deal_extra";
export const PERMISSION_ADD_NOTE_DEAL = "add_note_deal";
export const PERMISSION_IMPORT_DEAL = "import_deal";
export const PERMISSION_EXPORT_DEAL = "export_deal";

export const PERMISSION_EXPORT_DEAL_BASIC = "export_deal_basic";
export const PERMISSION_EXPORT_DEAL_PRODUCT = "export_deal_product";
export const PERMISSION_CREATE_PAYMENT = "create_payment"

export const PERMISSION_ADD_CLASS = "add_class";
export const PERMISSION_SHOW_CLASS = "show_class";
export const PERMISSION_UPDATE_CLASS_BASIC = "update_class_basic";
export const PERMISSION_UPDATE_SCHEDULE = "update_schedule";
export const PERMISSION_UPDATE_STUDENT_CLASS = "update_student_class";
export const PERMISSION_UPDATE_LESSON = "update_lesson";
export const PERMISSION_DELETE_CLASS = "delete_class";

export const PERMISSION_EXPORT_MEMBER_CLASS = "export_member_class";
export const PERMISSION_EXPORT_MEMBER_HISTORY = "export_member_history";
export const PERMISSION_EXPORT_MEMBER_QUEUE_CLASS = "export_member_queue_class";

export const PERMISSION_STATEMENT_CUSTOMER = "statement_customer";
export const PERMISSION_STATEMENT_SALE = "statement_sale";

export const PERMISSION_USER = "permisson_user";
export const PERMISSION_MANAGE_USER = "manage_user";

export const IS_PUBLISH_FILE = 1;
export const IS_PRIVATE_FILE = 0;

