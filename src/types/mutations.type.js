export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_ERROR = "setError";
export const SET_USER_BY_TEAM = "setUserByTeam";

// MUTATION CONS USER
export const SET_USER_CURRENT = "setUserCurrent";
export const SET_USERS_BY_TEAM = "setUsersByTeam";
export const SET_USER_CHECK_ADD_ON_EXPIRE = "setUserCheckAddOnExpire";
export const SET_LIST_PERMISSION = "setListPermission";
export const SET_LIST_STRUCTURE = "setListStructure";
export const SET_PERMISSION_BY_CURRENT_USER = "setPermissionByCurrentUser";
export const SET_LIST_PERMISSION_BY_STRUCTURE = "setListPermissionByStructure";
export const SET_APP_INFO = "setAppInfo";

// MUTATION CONS ORGANIZATION
export const SET_ORGANIZATION_CURRENT = "setOrganizationCurrent";
export const SET_ORGANIZATIONS = "setOrganizations";

// MUATAION CONST CUSTOMER
export const SET_CUSTOMERS = "setCustomers";
export const SET_CUSTOMERS_BY_IDS = "setCustomersByIds";
export const SET_CUSTOMER_ATTRIBUTES = "setCustomerAttributes";
export const SET_CUSTOMER_USER_ATTRIBUTES = "setCustomerUserAttributes";
export const SET_CUSTOMER_IMPORT_CHECK_ATTRIBUTE_UNIQUE = "setCustomerImportCheckAttributesUnique";

export const SET_DETAIL_CUSTOMER = "setDetailCustomer";
export const SET_CONTACT_MAIN_AND_LEVEL = "setContactMainAndLevels";
export const SET_DETAIL_CUSTOMER_AVATAR = "setDetailCustomerAvatar";
export const SET_CUSTOMER_TYPE = "setCustomerType";
export const SET_CUSTOMER_UPDATED = "setCustomerUpdated";
export const SET_CUSTOMER_FILTERS = "setCustomerFilters";
export const SET_CUSTOMER_FILTERS_CREATE = "setCustomerFiltersCreate";
export const SET_CUSTOMER_FILTERS_UPDATE = "setCustomerFiltersUpdate";
export const SET_CUSTOMER_FILTERS_DELETE = "setCustomerFiltersDelete";
export const SET_CUSTOMER_FILTER_ATTRIBUTES = "setCustomerFilterAttributes";
export const SET_CUSTOMER_ATTRIBUTE_DATA_TYPE = "setCustomerAttributeDataType";

export const SET_CUSTOMER_ATTRIIBUTE_OPTIONS = "setCustomerAttributeOptions";
export const SET_CUSTOMER_CONTACTS = "setCustomerContacts";
export const SET_CUSTOMER_CONTACTS_OF_TEAM = "setCustomerContactOfTeam";
export const SET_CUSTOMER_CONFIG_ATTRIBUTE_TABLE = "setCustomerConfigAttributeTable";
export const SET_CUSTOMER_CONFIG = "setCustomerConfigs";
export const SET_CUSTOMERS_DETAILS_BASIC_INFO = 'setCustomersDetailsBasicInfo';
export const SET_CUSTOMERS_DETAILS_BUYER_INFO = 'setCustomersDetailsBuyerInfo';
export const SET_CUSTOMERS_DETAILS_USER_INFO = 'setCustomersDetailsUserInfo';

//NOTE
export const SET_NOTE_BY_OBJECT_ID = "setNoteByObjectId";

// MUTATION CONST PRODUCT
export const SET_PRODUCTS = "setProducts";
export const SET_PRODUCT_ATTRIBUTES = "setProductAttributes";
export const SET_PRODUCT_USER_ATTRIBUTES = "setProductUserAttributes";
export const SET_PRODUCT_ALL_BY_TEAM = "setProductAllByTeam";
export const SET_DETAIL_PRODUCT = "setProductDetail";
export const SET_DETAIL_PRODUCT_IMAGE = "setProductDetailImage";

//CONST DEAL
export const SET_DEALS = "setDealsCustomer";
export const SET_CUSTOMER_DETAIL_DEAL = "setCustomerDetailDeal";
export const SET_DEAL_ATTRIBUTES = "setDealAttributes";
export const SET_DEAL_USER_ATTRIBUTES = "setDealUserAttributes";
export const SET_DEAL_DETAIL = "setDealDetail";
export const SET_STATUS_ADD_NEW = "setStatusAddNew";
export const SET_ALL_PAYMENT_HISTORY = "setAllPaymentHistory";
export const SET_DEAL_GET_ALL_PRODUCT = "setDealGetAllProduct";
export const SET_DEAL_FILTERS = "setDealFilter";
export const SET_DEAL_FILTER_ATTRIBUTES = "setDealFilterAttributes";
export const SET_DEAL_FILTERS_CREATE = "setDealFiltersCreate";
export const SET_DEAL_FILTERS_UPDATE = "setDealFiltersUpdate";
export const SET_DEAL_FILTERS_DELETE = "setDealFiltersDelete";
export const SET_DEAL_ATTRIBUTE_DATA_TYPE = "setDealAttributeDataType";


/*CONST EDUCATION*/
export const SET_EDUCATION = "setEducation";
export const SET_CLASS_DETAIL = "setClassDetail";
export const SET_CLASS_LIST_LESSON_GET_ALL = "setClasslistLessonGetAll";
export const SET_CLASS_LIST_STUDENT_GET_ALL = "setClassListStudentGetAll";
export const SET_CLASS_LIST_BY_PRODUCT_ID = "setClassListByProductId";
export const SET_LEARNING_OF_STUDENTS = "setLearningOfStudents";
export const SET_LIST_CLASS_OF_CUSTOMER_DETAIL = "setListClassOfCustomerDetail";
export const SET_ALL_STUDENT_EDUCATION = "setAllStudentEducation";

export const SET_STUDENT_FILTERS = "setStudentFilters";
export const SET_STUDENT_FILTERS_CREATE = "setStudentFiltersCreate";
export const SET_STUDENT_FILTERS_UPDATE = "setStudentFiltersUpdate";
export const SET_STUDENT_FILTERS_DELETE = "setStudentFiltersDelete";
export const SET_STUDENT_FILTER_ATTRIBUTES = "setStudentFilterAttributes";
export const SET_STUDENT_ATTRIBUTES = "setStudentAttributes";
export const SET_ACCOUNT_EDU_ONLINE_STATUS = "setAccountEduOnlineStatus";

/*REPORTS*/
export const SET_CUSTOMER_TOP_HARD_REPORT = "setCustomerTopHardReport";

//CUSTOMER CARE
export const SET_EMAIL_TEMPLATE_LIST = "setEmailTemplateList";
export const SET_EMAIL_TEMPLATE_DETAILS = "setEmailTemplateDetails";

export const SET_EMAIL_SCHEDULER_LIST = "setEmailSchedulerList";
export const SET_EMAIL_SCHEDULER_DETAILS = "setEmailSchedulerDetails";
