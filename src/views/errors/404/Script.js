import { ID_BASE_URL } from "@/types/config"

export default {
  data() {
    return {
      idpChooseOrganization: ID_BASE_URL + '/choose-organization',
    }
  },
}
