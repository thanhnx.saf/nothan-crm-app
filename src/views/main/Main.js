/* eslint-disable no-undef */
import {
  mapGetters
} from "vuex";

import {
  ID_BASE_URL
} from "@/types/config";
import {
  USER_CHECK_ADD_ON_EXPIRE,
  GET_PERMISSION_BY_CURRENT_USER,
  GET_APP_INFO,
  ORGANIZATION_CURRENT,
  ORGANIZATION_GET_ALL,
  USER_CURRENT,
  USER_GET_LIST_BY_TEAM
} from "@/types/actions.type";
import Actions from "@/components/actions/Actions";
import Breadcrumb from "@/components/breadcrum/Breadcrum";
import MainFooter from '@/components/mainFooter/MainFooter.vue'
import MainHeader from '@/components/mainHeader/MainHeader.vue'
import PopupExpire from "@/components/popup/expire/PopupExpire";
import PopupIsCommingExpire from "@/components/popup/expire-coming/PopupIsCommingExpire";
import PopupPoliciesAddCustomer from "@/components/popup/customer-policies/PopupPolicesAddCustomer";
import PopupUserCurrentNotPermission from "@/components/popup/403/PopupUserCurrentNotPermission";
import PopupUserCurrentNotPermissionRedirect403 from "@/components/popup/403-redirect/PopupUserCurrentNotPermissionRedirect403";
import SupportButton from '@/components/support-button/SupportButton'
import PageForbidden from '../errors/403/403.vue'

const IS_NOT_BASE_LAYOUT = ["PageNotFound", "PageForbidden", "Maintenance"];

export default {
  name: "Main",
  components: {
    "the-header": MainHeader,
    "the-breadcrumb": Breadcrumb,
    "the-actions": Actions,
    PopupExpire,
    PopupIsCommingExpire,
    'popup-policy-add-customer': PopupPoliciesAddCustomer,
    'the-footer': MainFooter,
    PopupUserCurrentNotPermission: PopupUserCurrentNotPermission,
    PopupUserCurrentNotPermissionRedirect403: PopupUserCurrentNotPermissionRedirect403,
    SupportButton,
    PageForbidden
  },
  computed: {
    ...mapGetters(["isAuthenticated", "currentUser", "userCheckAddOn", "userCurrentPermissions", "currentTeam"]),
    breadcrumbs() {
      let vm = this;
      return vm.$route.meta.breadcrumb;
    }
  },
  mounted() {
    this.checkIsCurrentTeam();
    this.checkIsAddon(this.$router.currentRoute);
    this.checkUserCurrentPermission(this.$router.currentRoute);
    this.getPermissionByCurrentUser();
    this.getAppInfo();
    this.getAllOrganization();
    this.getCurrentUser();
    this.getUsersByTeam();
  },
  created() {
    $('#PageForbidden').hide()
    $('#app').show()
  },
  data() {
    return {
      isNotBaseLayout: IS_NOT_BASE_LAYOUT.includes(this.$router.currentRoute.name),
      addOnExpire: {},
      addOnExpireAfter15Day: {}
    }
  },
  watch: {
    '$route'(to) {
      let vm = this;
      vm.isNotBaseLayout = IS_NOT_BASE_LAYOUT.includes(to.name);
      // vm.checkIsCurrentTeam();
      // vm.checkIsAddon(to);
      // vm.checkUserCurrentPermission(to);
      vm.setPageTitle(to);
      $('[data-toggle="m-tooltip"]').tooltip('hide');
    },
    currentUser: {
      handler(after) {
        let vm = this;
        if (after.team_ids && after.team_ids.length > 0) {
          let isExistsTeamId = false;
          after.team_ids.forEach(function (team_id) {
            if (team_id == vm.currentTeamId) {
              isExistsTeamId = true;
            }
          });
          if (!isExistsTeamId) {
            //  return window.location.href = "/403";
            $('#PageForbidden').show()
            $('#app').hide()
            return
          }
        } else {
          //   return window.location.href = "/403";
          $('#PageForbidden').show()
          $('#app').hide()
          return
        }
      },
      deep: true
    }
  },
  methods: {
    setPageTitle(to) {
      if (!to.meta['breadcrumb']) {
        return
      }
      const breadcrumb = to.meta['breadcrumb'].map(t => t.name)
      let title = 'Nỏ Thần Web App'
      if (this.currentTeam.name && breadcrumb.length) {
        title = breadcrumb[breadcrumb.length - 1]
        title += ` | ${this.currentTeam.name} > `
        breadcrumb.pop()
        title += breadcrumb.join(' > ')
        title += ' | Nỏ Thần Web App'
      }
      document.title = title
    },
    getAllOrganization() {
      this.$store.dispatch(ORGANIZATION_GET_ALL);
    },
    getCurrentUser() {
      this.$store.dispatch(USER_CURRENT)
    },

    getUsersByTeam() {
      this.$store.dispatch(USER_GET_LIST_BY_TEAM);
    },

    async checkIsCurrentTeam() {
      let vm = this;
      let team_id = vm.currentTeamId;

      if (team_id > 0) {
        await vm.$store.dispatch(ORGANIZATION_CURRENT, team_id);

        await vm.$store.dispatch(USER_CHECK_ADD_ON_EXPIRE, team_id).then(response => {
          if (response.data.data.isExpire.length > 0 && response.data.data.isExpireAfter15Day.length === 0) {
            $('#popupExpire').modal('show');
            vm.addOnExpire = response.data.data.isExpire;
          }

          if (response.data.data.isExpireAfter15Day.length > 0) {
            // Show poupup
            $('#popupIsCommingExpire').modal('show');
            vm.addOnExpireAfter15Day = response.data.data.isExpireAfter15Day;
          }

          vm.checkIsAddon(vm.$route);
        }).catch(error => {
          console.log(error);
        });

        vm.setPageTitle(vm.$route);
      } else {
        return window.location.href = ID_BASE_URL + '/choose-organization';
      }

      if (IS_NOT_BASE_LAYOUT.includes(vm.$router.currentRoute.name)) {
        return;
      }
    },

    checkIsAddon(route) {
      let vm = this;

      let roles = route.meta && route.meta.roles && Object.keys(route.meta.roles).length > 0 ? route.meta.roles : {};
      let currentUser = vm.userCheckAddOn && vm.userCheckAddOn.current_user ? vm.userCheckAddOn.current_user : "";

      if (roles && Object.keys(roles).length > 0) {
        Object.keys(roles).forEach(function (role) {
          if (currentUser[role] && currentUser[role].is_permission) {
            // Xử lý các action liên quan đến addon
          } else {
            // return window.location.href = "/404";
          }
        });
      }
    },

    async getPermissionByCurrentUser() {
      let vm = this;
      let team_id = vm.currentTeamId;

      if (team_id > 0) {
        let params = {
          team_id: team_id
        }
        await vm.$store.dispatch(GET_PERMISSION_BY_CURRENT_USER, params);
        vm.checkUserCurrentPermission(vm.$route);
      }
    },

    checkUserCurrentPermission(route) {
      let vm = this;
      let addon = route.meta && route.meta.serviceName ? route.meta.serviceName : "";
      let action = route.meta && route.meta.permissionName ? route.meta.permissionName : "";
      let multiple = route.meta && route.meta.isPermissionMultiple ? route.meta.isPermissionMultiple : false;
      if (vm.userCurrentPermissions && vm.userCurrentPermissions.is_department) {
        if (multiple && Array.isArray(action)) {
          let count = 0;
          action.forEach(function (item) {
            if (vm.userCurrentPermissions[addon] && vm.userCurrentPermissions[addon][item]) {
              count++;
            }
          });
          if (count <= 0) {
            // return window.location.href = "/403";
            $('#PageForbidden').show()
            $('#app').hide()
            return
          }
        } else {
          if (action != "all" && (vm.userCurrentPermissions[addon] && !vm.userCurrentPermissions[addon][action])) {
            // return window.location.href = "/403";
            $('#PageForbidden').show()
            $('#app').hide()
            return
          }
        }
      }
    },
    getAppInfo() {
      let vm = this;
      vm.$store.dispatch(GET_APP_INFO);
    }

  }
};